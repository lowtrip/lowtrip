<?php
return [
    'language' => 'ru-RU',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'fileCache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
];
