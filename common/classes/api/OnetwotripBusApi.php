<?php

namespace common\classes\api;

Class OnetwotripBusApi
{
    public function __construct()
    {
        $this->api_url = 'https://www.onetwotrip.com/_bus';
        $this->referrer_part = '';
        $this->useReferrerParams = true;
    }

    protected function sendRequest($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    /**
     * Включить добавление реф параметра
     */
    public function includeReferrerParams($flag = true)
    {
        $this->useReferrerParams = $flag;
    }

    # Поиск метастанций по части названия или гео координатам.
    # Параметры запроса для поиска по названию:
    # $text - обязательный параметр   / текст запроса (минимум один символ).
    # Пример запроса с поиском по названию:
    # www.onetwotrip.com/_bus/geo/suggest?query=Казань
    public function geoSuggest($text = '', $limit = 100)
    {
        $method = 'geo/suggest';
        $params = array(
            'query' => $text,
            'limit' => $limit,
        );
        $request = $this->api_url.'/'.$method.'?'.http_build_query($params);
        return $this->sendRequest($request);
    }

    # Получение расписания автобусов по метастанциям на дату.
    # startGeoId - код метастанции отправления
    # endGeoId - код метастанции прибытия
    # date - дата отправления в формате YYYY-MM-DD
    # adults - количество взрослых
    # children - количество детей
    # https://www.onetwotrip.com/_bus/run/search?startGeoId=16&endGeoId=342&date=2018-10-28&adults=2&children=1
    public function runSearch ($from, $to, $date, $adults = 1, $children = 0)
    {
        $method = 'run/search';
        $params = array(
            'startGeoId' => $from,
            'endGeoId' => $to,
            'date' => $date,
            'adults' => $adults,
            'children' => $children,
        );
        $request = $this->api_url.'/'.$method.'?'.http_build_query($params);

        return $this->sendRequest($request);
    }

    # Добавляет запрос в ответ
    protected function addRequestToResp($request, $response)
    {
        $buffer = json_decode($response);
        $buffer->request = $request;
        return json_encode($buffer);
    }
}