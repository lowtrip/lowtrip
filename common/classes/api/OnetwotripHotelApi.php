<?php

namespace common\classes\api;

class OnetwotripHotelAPI
{

    public function __construct()
    {
        $this->api_url = 'http://partner.onetwotrip.com/wl_hapi/canon/api';
        $this->referer_id = 'marker=lowtrip&scp=60,affiliate,4368-14624-0-2';
    }

	public function suggestRequest($city) 
	{
        $method = 'suggestRequest';

		$params = array(
			'query' => $city,
			'limit' => 10,
		);

        $url = $this->api_url.'/'.$method.'?'.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function searchRequest($city_id, $date_start, $date_end, $adults)
	{
        $method = 'searchRequest';

		$params = array(
			'object_id' => $city_id,
			'object_type' => 'geo',
			'date_start' => $date_start,
			'date_end' => $date_end,
			'adults' => $adults,
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',
		);

		$url = $this->api_url.'/'.$method.'?'.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function searchPolling($request_id) 
	{
        $method = 'searchPolling';

		$params = array(
			'request_id' => $request_id,
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',
		);

		$url = $this->api_url.'/'.$method.'?'.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function hotelRequest($hotel_id, $adults)
	{
        $method = 'hotelRequest';

		$params = array(
			'id' => $hotel_id,
            'adults' => $adults
		);

		$url = $this->api_url.'/'.$method.'?'.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function getTypes() 
	{
        $method = 'getTypes';

		$params = array(
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',

		);

		$url = $this->api_url.'/'.$method.'?'.http_build_query($params);
        
		return file_get_contents($url);
	}

}