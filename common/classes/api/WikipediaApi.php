<?php

namespace common\classes\api;

class WikipediaApi
{
	public function findByGeo ($lat, $lng)
	{
		$params = array(
			'format' => 'json',
			'action' => 'query',
			'list' => 'geosearch',
			'gsradius' => 10000,
			'gscoord' => $lat.'|'.$lng,
			'gslimit' => 2,
		);

		$url = 'https://ru.wikipedia.org/w/api.php?';
        $url = $url.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function findByTitle ($title)
	{
		$params = array(
			'titles' => $title,
		);

		$url = 'https://ru.wikipedia.org/w/api.php?format=xml&action=query&prop=extracts&exintro=true&explaintext=true&';
		$url = $url.http_build_query($params);

		return file_get_contents($url);
	}

	public function findImageByPageID ($page_id)
	{
		$params = array(
			'pageids' => $page_id,
			'pithumbsize' => 480,
		);

		$url = 'https://ru.wikipedia.org/w/api.php?format=xml&action=query&prop=pageimages&piprop=thumbnail&';
		$url = $url.http_build_query($params);

		return file_get_contents($url);
	}
}