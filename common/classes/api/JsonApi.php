<?php

namespace common\classes\api;

class JsonApi
{
    protected function sendResponse($request, $response)
    {
        $data = json_decode($response);
        $data->request = $request;
        return json_encode($data);
    }
}