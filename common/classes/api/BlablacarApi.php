<?php

namespace common\classes\api;

use Yii;

Class BlablacarApi
{
    public $options;
    public $utm;

    private $api_url;
    private $api_key;

    public function __construct($options)
    {
        $this->api_url = 'https://public-api.blablacar.com/api/v2/trips';
        $this->api_key = '86f6bbd6fd1a4f13bba5e2e7313355d9';
        $this->partner_url = 'https://c75.travelpayouts.com/click?shmarker=53486&promo_id=1671&source_type=customlink&type=click&custom_url=';
        $this->utm = 'utm_source=WID-FREE&utm_medium=other&utm_content=widget&utm_campaign=RU_WID-FREE_ALL_LOWTRIP&comuto_cmkt=RU_WID-FREE_ALL_LOWTRIP';

        $this->options = array(
            //'key' => $this->api_key,
            'fn' => '',
            'tn' => '',
            'locale' => 'ru_RU',
            '_format' => 'json',
            'cur' => 'RUB',
            //'fc' => '',
            //'tc' => '',
            'db' => '',
            'de' => '',
            'hb' => '',
            'he' => '24',
            'page' => '1',
            'seats' => '',
            //'photo' => '',
            //'fields' => '',
            'sort' => 'trip_price',
            'order' => 'asc',
            'limit' => '1',
            //'radius' => '',
            'radius_from' => '10',
            'radius_to' => '10'
        );

        $this->setOptions($options);
    }

    public function setOptions($options){
        foreach ($options as $key => $option)
        {
            $this->options[$key] = $option;
        }
    }

    public function buildRequest()
    {
        $query = $this->api_url;
        $query = $query.'?key='.$this->api_key;
        $query = $query.'&'.http_build_query($this->options);
        return $query;
    }

    public function sendRequest()
    {
        $request = $this->buildRequest();
        $response = file_get_contents($request);

        $response = json_decode($response);

        $link = 'https://www.blablacar.ru/search?fn='.$this->options['fn'].'&tn='.$this->options['tn'];
        $link = $link.'&db='.$this->options['db'].'&de='.$this->options['de'];
        $link = $link.'&locale=ru_RU&cur=RUB&sort=trip_price&order=asc';
        $link = $link.'&hb='.$this->options['hb'].'&he='.$this->options['he'].'&seats='.$this->options['seats'];
        $link = $link.'&radius_from='.$this->options['radius_from'].'&radius_to='.$this->options['radius_to'];

        $link = urlencode($link);

        $link = $this->partner_url.$link;
        $response->link = $link;

        $response->zapros = $request;

        return json_encode($response);
    }
}