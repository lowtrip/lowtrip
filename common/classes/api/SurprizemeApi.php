<?php

namespace common\classes\api;

Class SurprizemeApi
{
    public function __construct()
    {
        $this->api_url = 'https://app.surprizeme.ru/api';
        $this->referer_id = '12';
    }

    #lang - Код языка товара(контент, описание)
    #"ru", "en", "ch", "he"
    #city - Слаг города
    #"saint-p", "msk", "kazan", "krasnodar", "orel", "nizhny-novgorod", "voronezh", "ryazan", "haifa", "berlin", "kaluga", "novosibirsk", "kaliningrad"

    public function getProducts($city_slug, $lang = 'ru')
    {
        $path = '/products?';

        $params = array(
            'lang' => $lang,
            'city' => $city_slug,
        );

        $url = $this->api_url.$path.http_build_query($params);
        $json = file_get_contents($url);

        $json = json_decode($json);
        foreach ($json->data as $obj)
        {
            $obj_slug = $obj->slug;
            $obj->url = 'https://surprizeme.ru/'.$lang.'/store/'.$obj_slug.'/?coupon='.$this->referer_id;
        }
        $json->request_url = $url;
        $json = json_encode($json);

        return $json;
    }
}