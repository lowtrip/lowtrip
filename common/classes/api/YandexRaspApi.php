<?php

namespace common\classes\api;

use Yii;
use yii\db\Exception;

use common\classes\api\JsonApi;

Class YandexRaspApi extends JsonApi
{
    public $system;
    public $transfers;
    public $limit;
    public $offset;

    public function __construct()
    {
        $this->api_url = 'https://api.rasp.yandex.net/v3.0';
        $this->api_key = '4c99e33e-b071-48b0-8c8e-92e0dd199f3a';
        $this->format = 'json';
        $this->lang = 'ru_RU';
        $this->system = 'yandex';
        $this->limit = 100;
        $this->offset = 0;
        $this->transfers = false;
    }

    // "yandex", "esr"
    public function setSystem($system) {
        $this->system = $system;
    }


    public function setTransfers($transfers = true) {
        $this->transfers = $transfers;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
         $this->limit = $limit;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function stationsList()
    {
        $method = 'stations_list';
        $params = array(
            'apikey' => $this->api_key,
            'lang' => $this->lang,
            'format' => $this->format,
        );
        $request = $this->api_url.'/'.$method.'/?'.http_build_query($params);

        return file_get_contents($request);
    }

    // Расписание рейсов между станциями
    // property string $date 'YYYY-MM-DD'
    public function searchTrains($from, $to, $date = "", $transport_types = 'train')
    {
        $method = 'search';
        $params = array(
            'apikey' => $this->api_key,
            'format' => $this->format,
            'from' => $from,
            'to' => $to,
            'lang' => $this->lang,
            'date' => $date,
            'transport_types' => $transport_types,
            'system' => $this->system,
            'offset' => $this->offset,
            'limit' => $this->limit,
            'transfers' => $this->transfers ? 'true' : 'false'
        );

        if (!$this->transfers) {
            $params['show_systems'] = 'esr';
            $params['add_days_mask'] = 'true';
            $params['result_timezone'] = 'Europe/Moscow';
        }

        $request = $this->api_url.'/'.$method.'/?'.http_build_query($params);

        $response = @file_get_contents($request);

        return $this->sendResponse($request, $response);
    }

    public function trainTimetable($uid, $date = "")
    {
        $method = 'thread';
        $params = array(
            'apikey' => $this->api_key,
            'uid' => $uid,
            'date' => $date,
            'lang' => $this->lang,
            'format' => $this->format,
            'show_systems' => 'all',
        );
        $request = $this->api_url.'/'.$method.'/?'.http_build_query($params);

        return @file_get_contents($request);
    }

    /*
     * Расписание рейсов автовокзала
     */
    public function busSchedule($station_code){
        $method = 'schedule';
        $params = [
            'apikey' => $this->api_key,
            'station' => $station_code,
            'transport_types' => 'bus',
        ];
        $request = $this->api_url.'/'.$method.'/?'.http_build_query($params);
        $curl = curl_init($request);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        return json_decode(curl_exec($curl));
    }
}