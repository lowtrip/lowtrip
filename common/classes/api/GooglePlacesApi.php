<?php

namespace common\classes\api;

class GooglePlacesApi
{
	private $api_url;
	private $key;
	
	public 	$format;

	public function __construct()
	{
        $this->api_url = 'https://maps.googleapis.com/maps/api/place';
        $this->key = 'AIzaSyDL9Y3TSB_MR_HoaJAPaNECUDC5Qc2zNvY';
        $this->format = 'json';
	}

	public function nearbySearch($lat, $lng, $type)
	{
        $method = 'nearbysearch';

		$params = array(
			'location' => $lat.','.$lng,
			'radius' => 5000,
			'rankby' => 'prominence',
			'type' => $type,
			'language' => 'ru',
			'key' => $this->key
		);

        $url = "$this->api_url/$method/$this->format?";
        $url = $url.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function radarSearch($lat, $lng, $type)
	{
        $method = 'radarsearch';

		$params = array(
			'location' => $lat.','.$lng,
			'radius' => 5000,
			'type' => $type,
			'language' => 'ru',
			'key' => $this->key
		);

		$url = "$this->api_url/$method/$this->format?";
        $url = $url.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function getPhoto($photoreference, $width)
	{
        $method = 'photo';

		$params = array(
			'photoreference' => $photoreference,
			'maxwidth' => $width,
			'key' => $this->key
		);

        $url = "$this->api_url/$method?";
        $url = $url.http_build_query($params);
        
		return file_get_contents($url);
	}

	public function placeData()
	{
        $method = 'details';

		$params = array(
			'location' => $place_id,
			'language' => 'ru',
			'key' => $this->key
		);

		$url = "$this->api_url/$method/$this->format?";
        $url = $url.http_build_query($params);
        
		return file_get_contents($url);
	}
}