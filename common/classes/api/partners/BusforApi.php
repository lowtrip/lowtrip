<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 31.08.2018
 * Time: 15:37
 */
namespace common\classes\api\partners;
use common\controllers\ApiController;

class BusforApi
{
    public $api_url;
    public $api_key;
    public $response;
    public function __construct()
    {
        $this->api_url = "https://gillbus.com/online2/";
        $this->api_key = "ccB+f15084Jm27YKlIwYLySPDt4c/KCAicKRUwMZmYCCM7bHBSATXQ=";
    }

    /**
     * @param $url адрес запроса
     * @param bool $isJson отправляем параметры в json или нет
     * @param $paramsString подготовленная строка с параметрами
     */
    private function setParams($url, $isJson = false, $paramsString){
        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        if(!is_null($paramsString)) {
            if ($isJson == true) {
                curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($paramsString)));
            } else {
                curl_setopt($this->curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Content-Length: ' . strlen($paramsString)));
            }
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $paramsString);
        }
    }
    public function login(){
        $url = $this->api_url . 'login/online/' . $this->api_key;
        $this->curl = curl_init($url);
        $this->setParams($url, false, "timeZone=Europe/Moscow");

        $response = new \SimpleXMLElement(curl_exec($this->curl));
        curl_close($this->curl);
        return $response;
    }

    public function getCountries(){
        $this->login();
        $url = $this->api_url . 'getCountries';
        $this->curl = curl_init($url);
        $this->setParams($url, false, null);

        $response = new \SimpleXMLElement(curl_exec($this->curl));
        curl_close($this->curl);
        return $response;
    }

    public function getCities($country_id){
        $this->login();
        $url = $this->api_url . 'getCities';
        $this->curl = curl_init($url);
        $params = "countryId={$country_id}";
        $this->setParams($url, false, $params);

        $response = new \SimpleXMLElement(curl_exec($this->curl));
        curl_close($this->curl);
        return $response;
    }


    public function searchTripsList($from, $to, $date, $people = 1){
        $this->login();
        $url = $this->api_url . 'searchTripsList';
        $this->curl = curl_init($url);
        $params = json_encode([['from' => $from , 'to' => $to]]);
        $this->setParams($url, true, $params);

        $response = new \SimpleXMLElement(curl_exec($this->curl));
        curl_close($this->curl);
        return $response;
    }

    public function searchTrips($from, $to, $date, $people = 1){
        $this->login();
        $url = $this->api_url . 'searchTrips';
        $this->curl = curl_init($url);
        $params = "startCityId={$from}&endCityId={$to}&startDateSearch={$date}&ticketCount={$people}";
        $this->setParams($url, false, $params);

        $response = new \SimpleXMLElement(curl_exec($this->curl));
        curl_close($this->curl);
        return $response;
    }


}