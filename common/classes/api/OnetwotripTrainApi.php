<?php

namespace common\classes\api;

Class OnetwotripTrainApi
{
    public function __construct()
    {
        $this->api_url = 'https://www.onetwotrip.com/_api/rzd';
        $this->referrer_part = 'referrer=affiliate&referrer_mrk=4368-14624-0-2';
        //$this->useReferrerParams = true;
    }

    protected function sendRequest($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    /**
     * Включить добавление реф параметра
     */
    public function includeReferrerParams($flag = true)
    {
        $this->useReferrerParams = $flag;
    }

    # Поиск метастанций по части названия или гео координатам.
    # Параметры запроса для поиска по названию:
    # searchText - обязательный параметр   / текст запроса (минимум один символ).
    # Пример запроса с поиском по названию:
    # www.onetwotrip.com/_api/rzd/suggestStations?searchText=Казань
    public function suggestStations($text)
    {
        $method = 'suggestStations';
        $params = array(
          'searchText' => $text,
        );
        $request = $this->api_url.'/'.$method.'?'.http_build_query($params);
        $response = file_get_contents($request);
        return $response;
    }

    # Получение расписания поездов по метастанциям на дату.
    # date - дата отправления в формате DDMMYYYY
    # from - код метастанции отправления
    # to - код метастанции прибытия
    # source - клиент с которого идет запрос (web)
    # https://www.onetwotrip.com/_api/rzd/metaTimetable/?from=22823&to=22871&date=19082016&source=web
    public function metaTimetable ($from, $to, $date, $addRequestToResponse = false)
    {
        $method = 'metaTimetable';
        $params = array(
            'from' => $from,
            'to' => $to,
            'date' => $date,
            'source' => 'web'
        );
        $request = $this->api_url.'/'.$method.'?'.http_build_query($params);

        // Нужно ли добавить реф параметр
        if ($this->useReferrerParams) {
            $request = $request.'&'.$this->referrer_part;
        }

        //$response = file_get_contents($request);
        $response = $this->sendRequest($request);

        if ($addRequestToResponse) {
            $response = $this->addRequestToResp($request, $response);
        }

        return $response;
    }

    # Добавляет запрос в ответ
    protected function addRequestToResp($request, $response)
    {
        $buffer = json_decode($response);
        $buffer->request = $request;
        return json_encode($buffer);
    }
}