<?php

namespace common\classes\api;

class IziTravelApi
{
	private $key;

	public function __construct()
	{
		$this->key = '1fbf9284-b590-4edf-baf1-1e5c752278d0';
		$this->api_url = 'https://api.izi.travel';
	}

	public function sendRequest($url)
	{
		$curl = curl_init();
		
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"accept: application/json",
				"X-IZI-API-KEY: $this->key"
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);

		return $response;
	}

	public function findCityUUID($city_name)
	{
		$params = array(
			'languages' => 'ru',
			'type' => 'city',
			'query' => $city_name,
        );
        
		$url = 'https://api.izi.travel/mtg/objects/search?';
        $url = $url.http_build_query($params);
        
		return $this->sendRequest($url);
	}

	public function get_children_of_MTGObject($uuid)
	{
		$params = array(
			'languages' => 'ru',
			// 'type' => 'city',
			// 'query' => $city_name,
        );
        
		$url = 'https://api.izi.travel/mtgobjects/'.$uuid.'/children?';
        $url = $url.http_build_query($params);
        
		return $this->sendRequest($url);
	}

	public function get_citys_children($uuid)
	{
		$params = array(
			'languages' => 'ru',
			'type' => 'tour',
			'limit' => '1000',
        );
        
		$url = 'https://api.izi.travel/cities/'.$uuid.'/children?';
        $url = $url.http_build_query($params);
        
		return $this->sendRequest($url);
	}
}