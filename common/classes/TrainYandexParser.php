<?php

namespace common\classes;

use Yii;

use common\models\CityStations;
use common\models\TrainNumbers;
use common\models\TrainNumbersExtData;
use common\models\TrainStations;
use common\models\TrainUid;
use common\models\TrainCarriers;
use common\models\TrainCalendars;
use common\models\TrainSchedules;
use common\models\TrainTimetable;

use common\classes\api\YandexRaspApi;

class TrainYandexParser {

    public $debug;
    public $updateInterval;

    public function __construct()
    {
        // Интервал обновления
        $this->updateInterval = 3600 * 24 * 7;
        $this->debug = false;
    }

    public function setDebug($debug = true)
    {
        $this->debug = $debug;
    }

    /**
     * Парсинг расписаний поездов. В качестве данных для запроса берутся отправление и прибытие каждого поезда.
     */
    public function updateAllTrainsRasp()
    {
        $trainExtData = TrainNumbersExtData::find()->asArray()->all();

        if (!empty($trainExtData)) {
            foreach ($trainExtData as $k => $row)
            {
                if ($this->debug) echo "$k. Подготовка поиска...\n";

                $fromCode = $this->findCityOrStationYandexCode(intval($row['departure_id']));
                $toCode   = $this->findCityOrStationYandexCode(intval($row['arrival_id']));

                if (!empty($fromCode) && !empty($toCode)) {
                    $this->updateRasp($fromCode, $toCode);
                }
            }
        }
    }

    /**
     * По id станции найти код города в котором она находится, или вернуть код станции
     * @param integer $station_id
     */
    protected function findCityOrStationYandexCode($station_id)
    {
        $cityYandexCode = null;
        $station = TrainStations::findById($station_id)->asArray()->one();

        if (!empty($station['city_id'])) {
            $cityYandexCode = CityStations::findYandexCode($station['city_id']);
        }

        return !empty($cityYandexCode) ? $cityYandexCode : $station['yandex'];
    }

    // Обновить расписания для маршрутов из fromCode в toCode
    // @param string  $fromCode
    // @param string  $toCode
    // @param integer $offset
    public function updateRasp($fromCode, $toCode, $offset = 0)
    {
        if ($this->debug) echo "Парсинг поездов от $fromCode до $toCode;\n";

        $api = new YandexRaspApi();
        $api->setOffset($offset);

        $json = $api->searchTrains($fromCode, $toCode);

        $content = json_decode($json);

        $total = $content->pagination->total;

        if ($this->debug) echo "Найдено $total результатов на все дни;\n";

        $limit = $api->getLimit();
        $offset = $api->getOffset();

        if (!empty($content->segments)) {
            $this->analyzeTrainNumbers($content->segments);
        }

        if ($total > ($limit + $offset)) {
            if ($this->debug) echo "Осталось спарсить ".$total-$limit-$offset." результатов\n";

            $this->updateRasp($fromCode, $toCode, $offset + $limit);
        }
        else {
            // Все расписания обновлены, можно парсить маршрутные листы
            if ($this->debug) echo "Парсинг маршрутных листов\n";

            $this->parseTimetables();
        }
    }

    // Анализ ответа от яндекс расписаний
    // @param array $trains
    protected function analyzeTrainNumbers($trains)
    {
        foreach ($trains as $train)
        {
            $trainNumber_model = TrainNumbers::findTrain($train->thread->number);

            // Если нет поезда с таким номером, создаем
            if (empty($trainNumber_model)) {
                if ($this->debug) echo "Поезда с номером ".$train->thread->number." еще не существует, создаем;\n";

                $trainNumber_model = $this->createNewTrain($train);
            }

            $trainUid_model = TrainUid::findByYandexUid($train->thread->uid);

            // Если нет уникального расписания поезда, создаем
            if (empty($trainUid_model)) {
                if ($this->debug) echo "Расписания с номером ".$train->thread->uid." еще не существует, создаем;\n";

                $trainUid_model = $this->createTrainUid($trainNumber_model->id, $train->thread->uid);

                if (!empty($trainUid_model)) {
                    $this->createCalendarWithRelations($trainUid_model->uid, $train->schedule);
                }
            }

            // Если расписание Неделю не обновлялось, нужно обновить
            if (!empty($trainUid_model) && $this->needUpdate($trainUid_model->updated_at)) {
                if ($this->debug) echo "Обновление расписания $trainUid_model->uid;\n";
                
                $this->updateCalendarWithRelations($trainUid_model->uid, $train->schedule);

                // Обновление атрибута updated_at
                $trainUid_model->save();
            }
        }
    }

    // Создать новый поезд
    // @param array $train
    protected function createNewTrain($train)
    {
        $model = new TrainNumbers();
        $model->train_number = $train->thread->number;
        $model->type = $train->thread->transport_type;
        $model->title = $train->thread->title;
        $model->carrier_code = $this->checkCarrierExisted($train->thread->carrier);

        return $model->save() ? $model : null;
    }

    // Создать новое уникальное расписание поезда
    // @param integer $train_id
    // @param string $yandex_uid
    protected function createTrainUid($train_id, $yandex_uid)
    {
        if ($this->debug) echo "Создание для поезда id:$train_id, yandex_uid:$yandex_uid модели TrainUid;\n";

        $model = new TrainUid();
        $model->train_id = $train_id;
        $model->yandex_uid = $yandex_uid;

        return $model->save() ? $model : null;
    }

    // Создать календари с отношениями к расписанию
    // @param integer $uid
    // @param array $schedules
    protected function createCalendarWithRelations($uid, $shedules)
    {
        if ($this->debug) echo "Создание календарей для расписания $uid;\n";

        if (!empty($shedules)) {
            $calendar_ids = [];

            foreach ($shedules as $shedule)
            {
                $calendar = $this->createCalendar($shedule);
                if (!empty($calendar)) {
                    $calendar_ids[] = $calendar->id;
                }
            }

            if (!empty($calendar_ids)) {
                foreach ($calendar_ids as $calendar_id)
                {
                    if ($this->debug) echo "Для календаря $calendar_id создаем связь с расписанием $uid;\n";
                    
                    $this->createUidCalendarRelation($uid, $calendar_id);
                }
            }
        }
    }

    // Обновить календари с отношениями к расписанию
    // Если календарь на месяц уже существует, обновляем дни; Иначе, создаем новый календарь.
    // @param integer $uid
    // @param array $schedules
    protected function updateCalendarWithRelations($uid, $shedules)
    {
        if (!empty($shedules)) {
            // Получаем существующие календари
            $uid_calendars = TrainUid::findCalendarsQuery($uid);

            foreach ($shedules as $shedule)
            {
                $calendar_id = $this->sheduleHasCalendar($shedule, $uid_calendars);

                // Если календарь (year, month) уже существует, обновим его дни
                if ($calendar_id) {
                    $this->updateCalendarDays($calendar_id, $shedule);
                }
                // Но если календаря нет, создаем его вместе со связью
                else {
                    $calendar = $this->createCalendar($shedule);
                    if (!empty($calendar)) {
                        $this->createUidCalendarRelation($uid, $calendar->id);
                    }
                }
            }
        }
    }

    // Если календарь на год и месяц уже существует, возвращаем id этого календаря
    // @param object $shedule
    // @param array $calendars
    protected function sheduleHasCalendar($shedule, $calendars)
    {
        $exists = false;

        foreach ($calendars as $calendar)
        {
            if ($calendar['year'] == $shedule->year && 
                $calendar['month'] == $shedule->month) {
                $exist = true;
                $calendar_id = $calendar['id'];
                break;
            }
        }

        return $exists ? $calendar_id : false;
    }

    // Создать календарь
    // @param object $schedule
    protected function createCalendar($shedule)
    {
        if ($this->debug) echo "Создание календаря на $shedule->year $shedule->month\n";

        $model = new TrainCalendars();
        $model->year = $shedule->year;
        $model->month = (int) $shedule->month;
        $model->days = json_encode($shedule->days);

        return $model->save() ? $model : null;
    }

    // Обновить дни календаря
    // @param integer $calendar_id
    // @param object $schedule
    protected function updateCalendarDays($calendar_id, $shedule)
    {
        $model = TrainCalendars::findOne($calendar_id);

        if (!empty($model) && !empty($shedule)) {
            $model->days = json_encode($shedule->days);
            $model->save();
        }
    }

    // Создать связь shedule между календарем и расписанием
    // @param integer $uid
    // @param integer $calendar_id
    protected function createUidCalendarRelation($uid, $calendar_id)
    {
        $model = new TrainSchedules();
        $model->train_uid = $uid;
        $model->calendar_id = $calendar_id;

        return $model->save() ? $model : null;
    }

    // Найти модель или создать новую модель о перевозчике
    // @param object $carrier
    protected function checkCarrierExisted($carrier)
    {
        $model = TrainCarriers::find()
            ->where(['code' => $carrier->code])
            ->one();

        if (empty($model)) {
            $model = new TrainCarriers();
            $model->code = $carrier->code;
            $model->contacts = $carrier->contacts;
            $model->url = $carrier->url;
            $model->title = $carrier->title;
            $model->phone = $carrier->phone;
            $model->address = $carrier->address;
            $model->logo = $carrier->logo;
            $model->email = $carrier->email;

            $model->save();
        }

        return $model->code;
    }

    // Требуется обновление
    // @param string $date_string
    protected function needUpdate($date_string)
    {
        $diff = time() - strtotime($date_string);
        return $diff > $this->updateInterval;
    }


    // Парсинг маршрутных листов для расписаний без метки has_timetable
    public function parseTimetables()
    {
        $rows = TrainUid::findWithoutTimetable();

        foreach ($rows as $row)
        {
            $this->parseTimetableForUid($row);
            sleep(1);
        }
    }

    // Парсинг маршрутного листа для расписания 
    // @param object $trainUidModel
    public function parseTimetableForUid($trainUidModel)
    {
        $api = new YandexRaspApi();
        $content = $api->trainTimetable($trainUidModel->yandex_uid);

        if ($content !== FALSE) {

            $content = json_decode($content);

            $status = false;

            if (!empty($content->stops)) {
                $status = $this->addTimetables($trainUidModel->uid, $content->stops);

                // Если маршрутный лист сохранен, отмечаем это в расписании
                if ($status == true) {
                    $trainUidModel->has_timetable = 1;
                    $trainUidModel->save();
                }
            }
        }
    }

    // Добавить записи в маршрутный лист
    // @param integer $uid
    // @param array $stops
    protected function addTimetables($uid, $stops)
    {
        $status = false;

        foreach($stops as $stop)
        {
            $status = $this->createTimetable($uid, $stop);
            if (!$status) {
                // Произошла ошибка при сохранении записи, нужно удалить весь маршрутный лист
                TrainTimetable::deleteAll(['train_uid' => $uid]);
                break;
            }
        }

        return $status;
    }

    /**
     * Добавить одну запись в маршрутный лист
     * 
     * @param integer $uid
     * @param object $stop
     */
    protected function createTimetable($uid, $stop)
    {
        $station = TrainStations::findStationByYandexCode($stop->station->code);

        if (!empty($station)) {
            $model = new TrainTimetable();
            $model->train_uid = $uid;
            $model->station_id = $station->id;
            $model->departure = $stop->departure;
            $model->arrival = $stop->arrival;
            $model->stop_time = $stop->stop_time;
            $model->duration = $stop->duration;

            return $model->save();
        } 

        return false;
    }

}