<?php

namespace common\classes;

use Yii;
use DateTime;

use common\helpers\DateHelper;

use common\models\TrainNumbers;
use common\models\TrainUid;
use common\models\TrainTimetable;
use common\models\TrainSchedules;
use common\models\TrainCarriers;

class TrainHandler {

    public $trainData;
    private $actual_timetable;

    public function __construct($trainData)
    {
        $this->trainData = $trainData;
    }

    public function findTimetable($dateFormattedYmd)
    {
        $res = $this->findUidOnDate($dateFormattedYmd);

        if (!empty($res)) {
            $uid = $this->trainData['uids'][$res['uid']]['uid'];
            $calendar = $this->trainData['uids'][$res['uid']]['calendars'][$res['calendar']];

            $timetable_records = TrainTimetable::findByUid($uid);

            //$closestDate = $calendar['year']."-".$calendar['month']."-".$res['day'];

            $closestDate = (new DateTime())->format('Y-m-d');

            $timetable_records = $this->changeDates($timetable_records, $closestDate);

            $timetable_records = $this->formatTimes($timetable_records);

            $this->actual_timetable = $timetable_records;
        }

        return $this->actual_timetable;
    }

    public function getTrainDistance()
    {
        $distance = 0;
        $pos = count($this->actual_timetable) - 1;
        for($i = 1; $i <= $pos; $i++)
        {
            $distance += Yii::$app->distance->getDistance(
                $this->actual_timetable[$i]['station']['lat'],
                $this->actual_timetable[$i]['station']['lng'],
                $this->actual_timetable[$i-1]['station']['lat'],
                $this->actual_timetable[$i-1]['station']['lng']
            );
        }
        return $distance;
    }

    public function getTrainDuration()
    {
        $pos = count($this->actual_timetable) - 1;
        return $this->actual_timetable[$pos]['duration'];
    }

    public static function formatTimes($timetables)
    {
        $count = count($timetables);

        foreach ($timetables as $key => $timetable)
        {
            if ($key !== 0 && $key !== $count - 1) {
                $timetables[$key]['stop_time'] = DateHelper::secondsToTime($timetable['stop_time']);
                $timetables[$key]['duration']  = DateHelper::secondsToTime($timetable['duration']);
            }
            else if ($key === $count - 1) {
                $timetables[$key]['stop_time'] = null;
                $timetables[$key]['duration']  = DateHelper::secondsToTime($timetable['duration']);
            }
            else {
                $timetables[$key]['stop_time'] = null;
                $timetables[$key]['duration']  = null;
            }
        }

        return $timetables;
    }

    public static function changeDates($timetableRecords, $date_string)
    {
        $start_date = new DateTime(substr($timetableRecords[0]['departure'], 0, 10));

        $current_date = !empty($date_string) ? (new DateTime($date_string)) : (new DateTime());

        $interval = date_diff($start_date, $current_date);

        foreach ($timetableRecords as $key => $timetable)
        {
            $timetableRecords[$key]['arrival']   = self::changeDate($timetable['arrival'], $interval);
            $timetableRecords[$key]['departure'] = self::changeDate($timetable['departure'], $interval);
        }

        return $timetableRecords;
    }

    public static function changeDate($date_string, $interval)
    {
        if (empty($date_string)) return null;

        $date = new DateTime($date_string);

        $date->add($interval);

        $ymd = $date->format('Y-m-d');
        $hi  = $date->format('H:i');

        return [
            'date' => $ymd,
            'time' => $hi
        ];
    }
    

    // Начал переписывать это говно
    public function findUidOnDate($dateString)
    {
        $date = !empty($dateString) ? DateTime::createFromFormat('Y-m-d', $dateString) : (new DateTime());

        $trainData = $this->trainData;

        $results = [];

        // Отбираем массив лучших расписаний: uid => calendar
        foreach ($trainData['uids'] as $key => $train_uid)
        {
            $pos = $this->findClosestCalendar($train_uid['calendars'], $date);

            if (!empty($pos)) {
                $results[] = array(
                    'uid' => $key,
                    'calendar' => $pos['calendar'],
                    'day' => $pos['day']
                );
            }
        }

        // Отбираем самое лучшее расписание
        if (!empty($results)) {

            $calendars = [];

            foreach ($results as $key => $pos)
            {
                $calendars[] = $trainData['uids'][$pos['uid']]['calendars'][$pos['calendar']];
            }

            $best = $this->findClosestCalendar($calendars, $date);

            $k = $best['calendar'];

            return $results[$k];
        }

        return null;
    }

    // Найти ближайший календарь к дате $date
    public function findClosestCalendar($calendars, DateTime $date)
    {
        $optimal = null;
        $findedMonth = null;
        $findedDay = null;

        $date = date_timestamp_get($date);

        $month = (int) date('m', $date);
        $day   = (int) date('j', $date);

        foreach ($calendars as $key => $calendar)
        {
            if ($calendar['month'] >= $month) {

                $searchDay = (int)$calendar['month'] === $month ? $day : 1;
                $closestDay = $this->findClosestDay($calendar['days'], $searchDay);

                if (
                    (empty($findedMonth)) ||
                    ($calendar['month'] < $findedMonth) ||
                    ($calendar['month'] == $findedMonth && $closestDay < $findedDay)
                ) {
                    $findedMonth = $calendar['month'];
                    $findedDay = $closestDay;
                    $optimal = $key;
                }
            // бытсро вговнячил хуиты
            } else {
                $searchDay = (int)$calendar['month'] === $month ? $day : 1;
                $closestDay = $this->findClosestDay($calendar['days'], $searchDay);

                if (
                    (empty($findedMonth)) ||
                    ($calendar['month'] > $findedMonth) ||
                    ($calendar['month'] == $findedMonth && $closestDay < $findedDay)
                ) {
                    $findedMonth = $calendar['month'];
                    $findedDay = $closestDay;
                    $optimal = $key;
                }
            }
        }

        return array(
            'calendar' => $optimal,
            'day' => $findedDay
        );
    }

    // Найти ближайший день в массиве дней (json) к переданному дню
    public static function findClosestDay($days, $day = 1)
    {
        $days_array = json_decode($days);
        $count = count($days_array);

        for ($i = $day - 1; $i <= $count; $i++)
        {
            if ($days_array[$i] == 1) {
                return $i + 1;
            }
        }

        return null;
    }

}