<?php

namespace common\classes;

use Yii;

use common\helpers\DateHelper;
use common\classes\api\OnetwotripTrainApi;

use common\models\TrainPopularRoutes;
use common\models\CityStations;

class TrainOnetwotripParser
{

    const SLEEP_TIME = 1;

    public function __construct()
    {
        $this->api = new OnetwotripTrainApi();
        $this->debug = false;
    }

    public function setDebug($debug = false) {
        $this->debug = $debug;
    }

    public function useReferrerParams($flag = true) {
        $this->api->includeReferrerParams();
    }

    // парсинг цен на 10 топовых направлений на 14 дней вперед (по умолчанию)
    public function parseTopRoutesPrices($top = 10, $days = 14, $topOffset = 0, $daysOffset = 0, $reverse = false)
    {
        if ($this->debug) echo "Starting 'parseTopRoutesPrices'; top = $top, days = $days, topOffset = $topOffset\n";

        $routes = TrainPopularRoutes::getPopularRoutes($top, $topOffset);
        $parsed = array();

        foreach ($routes as $k => $route)
        {
            $departure_id = $route['departure']['id'];
            $arrival_id   = $route['arrival']['id'];

            if (!isset($parsed[$departure_id]) || !in_array($arrival_id, $parsed[$departure_id])) {
                if ($this->debug) echo "$k. Route is not parsed. departure_id = $departure_id, arrival_id = $arrival_id\n";

                $this->parseRoutePrices($departure_id, $arrival_id, $days, $daysOffset);

                array_push($parsed[$departure_id], $arrival_id);
            } else {
                if ($this->debug) echo "$k. Route is already has parsed; departure_id = $departure_id, arrival_id = $arrival_id\n";
            }

            if ($reverse) {
                if (!isset($parsed[$arrival_id]) || !in_array($departure_id, $parsed[$arrival_id])) {
                    if ($this->debug) echo "$k. Route is not parsed. departure_id = $arrival_id, arrival_id = $departure_id\n";

                    $this->parseRoutePrices($arrival_id, $departure_id, $days, $daysOffset);

                    array_push($parsed[$arrival_id], $departure_id);
                } else {
                    if ($this->debug) echo "$k. Route is already has parsed; departure_id = $arrival_id, arrival_id = $departure_id\n";
                }
            }
        }
    }

    // парсинг цен по направлению
    public function parseRoutePrices($departure_id, $arrival_id, $days = 14, $daysOffset = 0)
    {
        if ($this->debug) echo "Starting 'parseRoutePrices'; departure_id = $departure_id, arrival_id = $arrival_id, days = $days\n";

        $departure_code = CityStations::findOnetwotripCode($departure_id);
        $departure_code = $departure_code['stations_id'];
        $arrival_code   = CityStations::findOnetwotripCode($arrival_id);
        $arrival_code   = $arrival_code['stations_id'];

        if (!empty($departure_code) && !empty($arrival_code)) {
            $this->parse($departure_id, $departure_code, $arrival_id, $arrival_code, $days, $daysOffset);
        }
    }

    // Парсинг цен: обращение к апи, анализ результатов
    protected function parse($departure_id, $departure_code, $arrival_id, $arrival_code, $days, $daysOffset = 0)
    {
        if ($this->debug) echo "Starting 'parse';\n";

        for ($i = 0; $i < $days; $i++) {
            $dateRequest = DateHelper::currentDateWithOffset($i + $daysOffset);

            $date1 = $dateRequest->format('dmY');
            $date2 = $dateRequest->format('Y-m-d');

            $result = $this->api->metaTimetable($departure_code, $arrival_code, $date1, false);

            Yii::$app->statisticsTrain->analyzeData($result, $departure_id, $arrival_id, $date2);
            sleep(self::SLEEP_TIME);
        }
    }
    
}