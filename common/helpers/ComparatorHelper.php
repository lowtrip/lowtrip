<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 25.09.2018
 * Time: 17:39
 */

namespace common\helpers;


class ComparatorHelper
{
    public static function compare($str1, $str2){
    $count = max(strlen($str1), strlen($str2));
    $eq = 0;
    for($i = 0; $i < $count; $i++){
        if(strcasecmp(mb_substr($str1, $i, 1), mb_substr($str2, $i, 1)) == 0){
            $eq++;
        } else {
            $min = (float)(min(strlen($str1), strlen($str2)) / 2);
            if($min == 0){
                $min = 1;
            }
            return $eq / $min;
        }
    }
    return 1;
}
}