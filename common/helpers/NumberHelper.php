<?php

namespace common\helpers;

class NumberHelper
{
    public static function inRange($value, $min, $max)
    {
        return is_numeric($value) && $min <= $value && $max >= $value;
    }
}

?>