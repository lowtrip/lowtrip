<?php

namespace common\helpers;

class DateHelper {

    public static function validateDate($date, $format = 'Y-m-d')
    {
        $d = \DateTime::createFromFormat($format, $date);
        
        return $d && $d->format($format);
    }

    public static function dateIsActual($date, $format = 'Y-m-d') 
    {
        $now = (new \DateTime())->format($format);

        return static::validateDate($date, $format) && $date >= $now;
    }

    public static function reverseDateFormats($date, $formatFrom = 'Y-m-d', $formatTo = 'dmY')
    {
        return \DateTime::createFromFormat($formatFrom, $date)->format($formatTo);
    }

    public static function currentDateWithOffset($offset = 0)
    {
        $date = new \DateTime();

        $period = 'P'.$offset.'D';
        $interval = new \DateInterval($period);

        return $date->add($interval);
    }

    public static function secondsToTime($seconds)
    {
        $days    = intval(floor($seconds / 86400));
        $hours   = intval(floor(($seconds - $days * 86400) / 3600));
        $minutes = intval(floor(($seconds - $hours * 3600) / 60));

        if ($days !== 0) {
            return $days." д ".$hours." ч";
        }
        if ($hours !== 0) {
            return $hours." ч ".$minutes." мин";
        }
        else {
            return $minutes." мин";
        }
    }
}

?>