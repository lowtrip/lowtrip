<?php

namespace common\helpers\response;

use common\helpers\DateHelper;

class BusHelper {

    public static function onetwotrip($res, $date, $adults = 1, $children = 0) {
        $startDateTime = new \DateTime($res->startDateTime);
        $endDateTime = new \DateTime($res->endDateTime);

        $schema = (object)[];

        $schema->PARTNER_LOGO = 'onetwotrip.svg';

        $schema->START_DATETIME = $startDateTime->format('Y-m-d H:i:s');
        $schema->START_TIME = $startDateTime->format('H:i');
        $schema->START_DATE = $startDateTime->format('d.m.Y');
        $schema->STATION_BEGIN_NAME = $res->startStation->name;
        $schema->STATION_BEGIN_ADDRESS = $res->startStation->address;

        $schema->END_DATETIME = $endDateTime->format('Y-m-d H:i:s');
        $schema->END_TIME = $endDateTime->format('H:i');
        $schema->END_DATE = $endDateTime->format('d.m.Y');
        $schema->STATION_END_NAME = $res->endStation->name;
        $schema->STATION_END_ADDRESS = $res->endStation->address;

        $schema->CARRIER_NAME = $res->carrier->title;
        $schema->TIME_IN_ROAD = DateHelper::secondsToTime(
            $endDateTime->getTimestamp() - $startDateTime->getTimestamp()
        );

        $schema->COST = round($res->price->amount);

        $schema->URL = static::onetwotripUrl($res, $date, $adults, $children);

        return $schema;
    }

    public static function unitiki($res, $date, $hash) {
        $startDateTime = new \DateTime($res->datetime_start);
        $endDateTime = new \DateTime($res->datetime_end);

        $schema = (object)[];

        $schema->PARTNER_LOGO = 'unitiki.jpg';

        $schema->START_DATETIME = $startDateTime->format('Y-m-d H:i:s');
        $schema->START_TIME = $startDateTime->format('H:i');
        $schema->START_DATE = $startDateTime->format('d.m.Y');
        $schema->STATION_BEGIN_NAME = $res->station_start->station_title;
        $schema->STATION_BEGIN_ADDRESS = $res->station_start->station_address;

        $schema->END_DATETIME = $endDateTime->format('Y-m-d H:i:s');
        $schema->END_TIME = $endDateTime->format('H:i');
        $schema->END_DATE = $endDateTime->format('d.m.Y');
        $schema->STATION_END_NAME = $res->station_end->station_title;
        $schema->STATION_END_ADDRESS = $res->station_end->station_address;

        $schema->CARRIER_NAME = $res->carrier_title;
        $schema->TIME_IN_ROAD = DateHelper::secondsToTime(
            $endDateTime->getTimestamp() - $startDateTime->getTimestamp()
        );

        $schema->COST = round($res->price_unitiki);

        $schema->URL = static::unitikiUrl($res, $hash);

        return $schema;
    }

    public static function busfor($res, $date, $adults = 1, $children = 0) {
        $startDate = $res->START_DATE.' '.$res->START_TIME;
        $startDateTime = \DateTime::createFromFormat('d.m.Y H:i', $startDate);

        $endDate = $res->END_DATE.' '.$res->END_TIME;
        $endDateTime = \DateTime::createFromFormat('d.m.Y H:i', $endDate);

        $schema = (object)[];

        $schema->PARTNER_LOGO = 'busfor-logo.png';

        $schema->START_DATETIME = $startDateTime->format("Y-m-d H:i:s");
        $schema->START_TIME = $startDateTime->format('H:i');
        $schema->START_DATE = $startDateTime->format('d.m.Y');
        $schema->STATION_BEGIN_NAME = $res->STATION_BEGIN_NAME;
        $schema->STATION_BEGIN_ADDRESS = $res->STATION_BEGIN_ADDRESS;

        $schema->END_DATETIME = $endDateTime->format("Y-m-d H:i:s");
        $schema->END_TIME = $endDateTime->format('H:i');
        $schema->END_DATE = $endDateTime->format('d.m.Y');
        $schema->STATION_END_NAME = $res->STATION_END_NAME;
        $schema->STATION_END_ADDRESS = $res->STATION_END_ADDRESS;

        $schema->CARRIER_NAME = $res->CARRIER_NAME;
        $schema->TIME_IN_ROAD = $res->TIME_IN_ROAD;

        $schema->COST = round($res->TARIFF->COST);

        $schema->URL = static::busforUrl();

        return $schema;
    }

    public static function onetwotripUrl($res, $date, $adults = 1, $children = 0) {
        $base = "https://www.onetwotrip.com/ru/bus";
        $route = $res->startStation->geo->trnslt.'_'.$res->endStation->geo->trnslt;
        $runId = $res->runId;
        $params = array(
            'run_id' => $runId,
            'start_geo_id' => $res->startStation->geo->geopointId,
            'end_geo_id' => $res->endStation->geo->geopointId,
            'run_name' => $res->runName,
            'adults' => $adults,
            'children' => $children,
            'date' => $date,
            's' => true,
            'scp' => '60,affiliate,4368-14624-0-2'
        );
        return $base.'/'.$route.'/book?'.http_build_query($params);
    }

    public static function unitikiUrl($res, $hash) {
        $base = "https://unitiki.com/order/passfill";
        $rideId = $res->ride_segment_id;

        $params = array(
            'search_hash' => $hash,
        );
        return $base.'/'.$rideId.'?'.http_build_query($params);
    }

    public static function busforUrl() {
        return 'https://c48.travelpayouts.com/click?shmarker=53486&promo_id=1234&source_type=customlink&type=click&custom_url=https%3A%2F%2Fbusfor.ru%2F';
    }
}