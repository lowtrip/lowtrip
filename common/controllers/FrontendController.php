<?php

namespace common\controllers;

use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{
    public function init()
    {
        parent::init();
    }

    protected function checkParamsNotEmpty() : bool
    {
        $params = func_get_args();

        foreach ($params as $param)
        {
            if (empty($param)) {
                return false;
            }
        }

        return true;
    }

    protected function getRequestParams() : array
    {
        $request = Yii::$app->request;

        if ($request->isGet) {
            return $request->get();
        }
        if ($request->isPost) {
            return $request->post();
        }

        return array();
    }
}