<?php

namespace common\components;

use Faker\Provider\DateTime;
use yii\base\Component;

class DateFormatter extends Component
{
    const Month = ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'];

    public static function toDateString($date)
    {
        $timestamp = strtotime($date);
        $day = date('d', $timestamp);
        $month = self::Month[date('m', $timestamp) - 1];
        $year = date('Y', $timestamp);
        return "$day $month $year";
    }
}