<?php

namespace common\components;

use yii\base\Component;

use  yii\db\Query;

use common\models\TrainRoutes;
use common\models\TrainRoutesStatistics;
use common\models\TrainRoutesStatisticsPrices;

use common\models\TrainPlaceTypes;

use common\models\TrainPopularCities;
use common\models\TrainPopularRoutes;

use common\models\TrainNumbers;
use common\models\TrainNames;

class StatisticsTrain extends Component {

    public function analyze($data, $first_id, $second_id, $date) {
        
        // Обновление цен
        if ($this->checkData($data)) {

            $this->analyzeTrainNames($data);

            $this->increaseCityCounter($first_id);
            $this->increaseCityCounter($second_id);
            $this->increaseRouteCounter($first_id, $second_id);

            $this->routeStatistics($data, $first_id, $second_id, $date);
        } else {
            $this->resetPriceStatistic($first_id, $second_id, $date);
        }
    }

    public function analyzeData($data, $first_id, $second_id, $date) {
        if ($this->checkData($data)) {
            $this->analyzeTrainNames($data);

            $this->routeStatistics($data, $first_id, $second_id, $date, true);
        }
    }

    // Проверка JSON данных о расписании поездов, пришедших с OneTwoTrip
    // Если массив `result` не пустой, и поле `success` == true
    public function checkData($data)
    {
        $check = json_decode($data);

        return !empty($check->result) && $check->success == true;
    }

    // Счетчик популярности города

    // Увеличиваем счетчик популярности города на единицу
    public function increaseCityCounter($city_id)
    {
        if (!$this->updateCityCounter($city_id)) {
            $this->initCityCounter($city_id);
        }
    }

    // Непосредственно увеличиваем счетчик популярности города на единицу
    public function updateCityCounter($city_id)
    {
        $result = TrainPopularCities::findByCityId($city_id);

        if (!empty($result)) {
            $result->counter += 1;
            $result->save();
            return true;
        }

        return false;
    }

    // Создаем счетчик популярности города
    public function initCityCounter($city_id)
    {
        $result = new TrainPopularCities();
        $result->city_id = $city_id;
        $result->counter = 1;
        $result->save();
    }

    // Счетчик популярности маршрута

    // Увеличиваем счетчик популярности жд маршрута на единицу
    public function increaseRouteCounter($first_id, $second_id)
    {
        $route = TrainRoutes::getRoute($first_id, $second_id);
        if (!empty($route)) {
            $result = $this->updateRouteCounter($route);
            if ($result === false) {
                $this->initRouteCounter($route);
            }
        }
    }

    // Непосредственно увеличиваем счетчик популярности жд маршрута на единицу
    public function updateRouteCounter($route)
    {
        $result = TrainPopularRoutes::findByRouteId($route->route_id);

        if (!empty($result)) {
            $result->counter += 1;
            $result->save();
            return true;
        }

        return false;
    }

    // Создаем счетчик популярности жд маршрута
    public function initRouteCounter($route)
    {
        $result = new TrainPopularRoutes();
        $result->route_id = $route->route_id;
        $result->first_id = $route->first_city_id;
        $result->second_id = $route->second_city_id;
        $result->counter = 1;
        $result->save();
    }

    public function routeStatistics($data, $first_id, $second_id, $date, $debug = false)
    {
        $route = TrainRoutes::getRoute($first_id, $second_id);

        $this->checkRouteIsDirect($route->route_id);

        if (empty($route)) return false;

        $statistic = TrainRoutesStatistics::findRouteStatisticByDate($route->route_id, $date);

        if (empty($statistic)) {
            $this->createRouteStatistic($route->route_id, $date);
            $statistic = TrainRoutesStatistics::findRouteStatisticByDate($route->route_id, $date);

            $this->priceStatistic($statistic, $data);
        }

        if ($this->checkLastUpdate($statistic)) {
            $this->priceStatistic($statistic, $data);
            $statistic->save();
        }

    }

    public function createRouteStatistic($route_id, $date)
    {
        $new = new TrainRoutesStatistics();
        $new->route_id = $route_id;
        $new->date = $date;
        $new->save();
    }

    public function checkLastUpdate($statistic)
    {
        $now = (new \DateTime())->getTimestamp();
        $updated_at = $statistic->updated_at;

        return abs($now - $updated_at) >= 600;
    }

    public function parsePrices($data)
    {
        $data_obj = json_decode($data);
        $results = $data_obj->result;

        $prices = array();

        foreach ($results as $result) {
            $places = $result->places;

            foreach($places as $place) {

                $placetype = $place->type;
                $cost = intval($place->cost);

                if ($cost < $prices[$placetype] || !array_key_exists($placetype, $prices)) {
                    $prices[$placetype] = $cost;
                }
            }
        }

        return $prices;
    }

    public function priceStatistic($statistic, $data)
    {
        $statistic_id = $statistic->statistic_id;
        $prices = $this->parsePrices($data);

        $avaliableId = [];

        foreach ($prices as $type => $price) {
            $row = TrainRoutesStatisticsPrices::findByStatIdPlaceType($statistic_id, $type);

            if (!empty($row)) {
                $row->min_price = $price;
                $row->avaliable = 1;
            } else {
                $row = new TrainRoutesStatisticsPrices();
                $row->statistic_id = $statistic_id;
                $row->place_type_id = $type;
                $row->min_price = $price;
                $row->avaliable = 1;
            }

            if ($row->save()) {
                $avaliableId[] = intval($row->id);
            }
        }

        $this->changeAvaliability($statistic_id, $avaliableId);
    }

    /**
     * Сделать цены недоступными(для статистики на дату)
     */
    public function resetPriceStatistic($firstId, $secondId, $date)
    {
        $route = TrainRoutes::getRoute($firstId, $secondId);
        $statistic = TrainRoutesStatistics::findRouteStatisticByDate($route->route_id, $date);

        if (!empty($statistic)) {
            $prices = TrainRoutesStatisticsPrices::findStatsNotInId($statistic->statistic_id);
            foreach ($prices as $price)
            {
                $price->avaliable = 0;
                $price->save();
            }
        }
    }

    public function analyzeTrainNames($data)
    {
        $data = json_decode($data);
        $trains = $data->result;
        if (!empty($trains)) {
            foreach ($trains as $train)
            {
                if (isset($train->trainNumber) && isset($train->name)) {
                    $this->checkOrUpdateTrainName($train->trainNumber, $train->name);
                }
            }
        }
    }

    public function checkOrUpdateTrainName($trainNumber, $trainName)
    {
        $train = TrainNumbers::findTrain($trainNumber);

        if (!empty($train) && empty($train->name_id)) {

            $trainNamesModel = TrainNames::findByName($trainName);

            if (!empty($trainNamesModel)) {
                $train->name_id = $trainNamesModel->id;
                try {
                    $train->save();
                } catch(\Exception $e) {
                    echo $e->getMessage();
                    echo "Cant save TrainNumbers model";
                }
                return;
            } else {
                $newTrainNamesModel = new TrainNames();
                $newTrainNamesModel->name = $trainName;

                try {
                    $newTrainNamesModel->save();
                } catch(\Exception $e){
                    echo $e->getMessage();
                    echo "Cant save TrainNames model";
                }

                $this->checkOrUpdateTrainName($trainNumber, $trainName);
            }
        }
    }

    // @property object $route
    public function checkRouteIsDirect($routeId)
    {
        $route = TrainRoutes::find()->where(['route_id' => $routeId])->one();
        
        if (!empty($route) && intval($route->direct) !== 1) {
            $route->direct = 1;
            $route->save();
        }
    }

    public function changeAvaliability($statistic_id, $avaliableId)
    {
        $notAvaliable = TrainRoutesStatisticsPrices::findStatsNotInId($statistic_id, $avaliableId);

        if (!empty($notAvaliable)) {
            foreach ($notAvaliable as $row) {
                $row->avaliable = 0;
                $row->save();
            }
        }
    }
}