<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_routes_statistics_prices".
 *
 * @property integer $id
 * @property integer $statistic_id
 * @property integer $place_type_id
 * @property integer $min_price
 *
 * @property TrainPlaceTypes $placeType
 * @property TrainRoutesStatistics $statistic
 */
class TrainRoutesStatisticsPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes_statistics_prices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statistic_id', 'place_type_id', 'min_price'], 'required'],
            [['statistic_id', 'place_type_id', 'min_price', 'avaliable'], 'integer'],
            [['place_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainPlaceTypes::className(), 'targetAttribute' => ['place_type_id' => 'id']],
            [['statistic_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainRoutesStatistics::className(), 'targetAttribute' => ['statistic_id' => 'statistic_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'statistic_id' => 'Statistic ID',
            'place_type_id' => 'Place Type ID',
            'min_price' => 'Min Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(TrainPlaceTypes::className(), ['id' => 'place_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatistic()
    {
        return $this->hasOne(TrainRoutesStatistics::className(), ['statistic_id' => 'statistic_id']);
    }

    public static function findByStatIdPlaceType($statistic_id, $place_type_id)
    {
        return static::find()
            ->where(['statistic_id' => $statistic_id])
            ->andWhere(['place_type_id' => $place_type_id])
            ->one();
    }

    /*
     * Находит все статистики цен по $statistic_id, которые не входят в массив id $not_in_array
    */
    public static function findStatsNotInId($statistic_id, $not_in_array = [])
    {
        return static::find()
            ->where(['statistic_id' => $statistic_id])
            ->andWhere(['not in', 'id', $not_in_array])
            ->all();
    }
}
