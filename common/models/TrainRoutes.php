<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_routes".
 *
 * @property integer $route_id
 * @property integer $first_city_id
 * @property integer $second_city_id
 * @property integer $direct
 */
class TrainRoutes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_city_id', 'second_city_id'], 'required'],
            [['first_city_id', 'second_city_id', 'direct'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'route_id' => 'Route ID',
            'first_city_id' => 'First City ID',
            'second_city_id' => 'Second City ID',
        ];
    }

    public function getDeparture()
    {
        return $this->hasOne(City::className(), ['id' => 'first_city_id']);
    }

    public function getArrival()
    {
        return $this->hasOne(City::className(), ['id' => 'second_city_id']);
    }

    public static function getRandomRoutes($limit = 0)
    {
        return static::find()
            ->orderBy('RAND()')
            ->limit($limit)
            ->with('departure')
            ->with('arrival')
            ->all();
    }

    public static function getRoute($from, $to)
    {
        return static::find()
            ->where(['first_city_id' => $from])
            ->andWhere(['second_city_id' => $to])
            ->one();
    }

    public static function getRoutesWithoutDirectInfo($limit = 0)
    {
        return static::find()
            ->where('direct is NULL')
            ->limit($limit)
            ->all();
    }
}
