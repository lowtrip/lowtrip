<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "city_stations".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $stations_id
 * @property integer $has_stations
 * @property string $yandex
 */
class CityStations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_stations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['city_id', 'stations_id', 'has_stations'], 'required'],
            [['city_id', 'stations_id', 'has_stations'], 'integer'],
            [['yandex'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'stations_id' => 'Stations ID',
            'yandex' => 'Yandex',
            'has_stations' => 'Has Stations',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function findOnetwotripCode($city_id)
    {
        return static::find()
            ->select('city_id, has_stations, stations_id, yandex')
            ->where(['city_id' => $city_id])
            ->asArray()
            ->one();
    }

    public static function findYandexCode($city_id)
    {
        $row = static::find()
            ->select('yandex')
            ->where(['city_id' => $city_id])
            ->asArray()
            ->one();

        return !empty($row) ? $row['yandex'] : null;
    }

    public static function findByYandexCode($code)
    {
        return static::find()
            ->where(['yandex' => $code])
            ->asArray()
            ->one();
    }

    public static function findAllByYandexCode($codes)
    {
        return static::find()
            ->where(['in', 'yandex', $codes])
            ->asArray()
            ->all();
    }

    public static function findCityIdByOnetwotrip($stations_id)
    {
        $row = static::find()
            ->where(['stations_id' => $stations_id])
            ->asArray()
            ->one();

        return !empty($row) ? $row['city_id'] : null;
    }


}
