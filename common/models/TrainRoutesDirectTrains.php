<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_routes_direct_trains".
 *
 * @property integer $id
 * @property integer $route_id
 * @property integer $train_id
 */
class TrainRoutesDirectTrains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes_direct_trains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'train_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Route ID',
            'train_id' => 'Train ID',
        ];
    }

    public function getRoute()
    {
        return $this->hasOne(TrainRoutes::className(), ['route_id' => 'route_id']);
    }

    public function getTrain()
    {
        return $this->hasOne(TrainNumbers::className(), ['id' => 'train_id']);
    }

}
