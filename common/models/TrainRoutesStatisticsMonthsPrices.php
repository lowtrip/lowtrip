<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_routes_statistics_months_prices".
 *
 * @property integer $statistic_id
 * @property integer $place_type_id
 * @property integer $min_price
 */
class TrainRoutesStatisticsMonthsPrices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes_statistics_months_prices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['statistic_id', 'place_type_id', 'min_price'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statistic_id' => 'Statistic ID',
            'place_type_id' => 'Place Type ID',
            'min_price' => 'Min Price',
        ];
    }

    public function getStat()
    {
        return $this->hasOne(TrainRoutesStatisticsMonths::className(), ['id' => 'statistic_id']);
    }

    public function getType()
    {
        return $this->hasOne(TrainPlaceTypes::className(), ['id' => 'place_type_id']);
    }

    public static function findMinPricesByMonts($statistic_id)
    {
        return (new Query())
            ->select('statistic_id, MIN(min_price)')
            ->from(static::tableName())
            ->groupby('statistic_id');
    }
}
