<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_carriers".
 *
 * @property integer $code
 * @property string $contacts
 * @property string $url
 * @property string $title
 * @property string $phone
 * @property string $address
 * @property string $logo
 * @property string $email
 */
class TrainCarriers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_carriers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['code'], 'integer'],
            [['contacts'], 'string', 'max' => 512],
            [['url'], 'string', 'max' => 128],
            [['title', 'email'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 20],
            [['address', 'logo'], 'string', 'max' => 256],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'contacts' => 'Contacts',
            'url' => 'Url',
            'title' => 'Title',
            'phone' => 'Phone',
            'address' => 'Address',
            'logo' => 'Logo',
            'email' => 'Email',
        ];
    }

    public static function findByCode($code)
    {
        return (new Query())
            ->select('*')
            ->from(static::tableName())
            ->where(['code' => $code])
            ->one();
    }
}
