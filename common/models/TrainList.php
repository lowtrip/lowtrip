<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_list".
 *
 * @property integer $id
 * @property string $code
 * @property string $from_city
 * @property string $to_city
 * @property integer $from_id
 * @property integer $to_id
 * @property integer $added
 */
class TrainList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            [['added'], 'integer'],
            [['from_code', 'to_code'], 'string', 'max' => 10],
            [['code'], 'string', 'max' => 4],
            [['from_city', 'to_city'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'from_city' => 'From City',
            'to_city' => 'To City',
            'from_code' => 'From ID',
            'to_code' => 'To ID',
            'added' => 'Added',
        ];
    }
}
