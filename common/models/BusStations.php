<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 10.09.2018
 * Time: 19:48
 */

namespace common\models;


use yii\db\ActiveRecord;
use yii\db\Query;

class BusStations extends ActiveRecord
{
    public static function tableName()
    {
        return 'bus_stations';
    }

    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['type', 'esr', 'yandex'], 'string', 'max' => 20],
            [['name', 'name_slug', 'region', 'region_slug', 'country', 'country_slug'], 'string', 'max' => 40],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'type' => 'Type',
            'name' => 'Name',
            'name_slug' => 'Name Slug',
            'region' => 'Region',
            'region_slug' => 'Region Slug',
            'country' => 'Country',
            'country_slug' => 'Country Slug',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    public function getCity(){
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
    public static function findStationsIdInCity($city_id)
    {
        return  (new Query())
            ->select('id')
            ->from(static::tableName())
            ->where(['city_id' => $city_id])
            ->andWhere(['OR', ['type' => 'bus_station'], ['type' => 'station']]);
    }

    public static function findStationByYandexCode($yandex_code)
    {
        return static::find()
            ->where(['yandex' => $yandex_code])
            ->one();
    }

    public static function findStationYandexCode($station_id)
    {
        $row =  (new Query())
            ->select('yandex')
            ->from(static::tableName())
            ->where(['id' => $station_id])
            ->one();

        return !empty($row) ? $row['yandex'] : null;
    }

    public static function findById($id)
    {
        return static::find()
            ->where(['id' => $id]);
    }

    public function behaviors()
    {
        return [
            'name_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'name_slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'region_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'region_slug',
                'attribute' => 'region',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'country_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'country_slug',
                'attribute' => 'country',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

        ];
    }
}