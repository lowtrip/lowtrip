<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_train_type_route".
 *
 * @property integer $id
 * @property integer $first_id
 * @property integer $second_id
 * @property string $title
 * @property string $description
 * @property string $text1
 * @property string $text2
 * @property string $text3
 * @property string $text4
 */
class PageTrainTypeRoute extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_train_type_route';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['first_id', 'second_id', 'title', 'description', 'text1', 'text2', 'text3', 'text4'], 'required'],
            [['first_id', 'second_id'], 'integer'],
            [['title', 'description', 'text1', 'text2', 'text3', 'text4'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_id' => 'First ID',
            'second_id' => 'Second ID',
            'title' => 'Title',
            'description' => 'Description',
            'text1' => 'Text1',
            'text2' => 'Text2',
            'text3' => 'Text3',
            'text4' => 'Text4',
        ];
    }

    public static function findPage($first_id, $second_id)
    {
        return static::find()
            ->where(['first_id' => $first_id])
            ->andWhere(['second_id' => $second_id])
            ->one();
    }
}
