<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_names".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 */
class TrainNames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_names';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    // Найти запись по имени поезда
    public static function findByName($name)
    {
        return static::find()
            ->where(['name' => $name])
            ->one();
    }

    // Найти запись по slug поезда
    public static function findBySlug($slug)
    {
        return static::find()
            ->where(['slug' => $slug])
            ->one();
    }

    // Найти запись по slug поезда
    public static function findById($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->one();
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }
}
