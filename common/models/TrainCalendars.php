<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_calendars".
 *
 * @property integer $id
 * @property string $year
 * @property integer $month
 * @property string $days
 */
class TrainCalendars extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_calendars';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'month', 'days'], 'required'],
            [['year'], 'safe'],
            [['month'], 'integer'],
            [['days'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Year',
            'month' => 'Month',
            'days' => 'Days',
        ];
    }

    
}
