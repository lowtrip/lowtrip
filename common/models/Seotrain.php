<?php

namespace common\models;

use Yii;

class Seotrain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seotrain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_id', 'second_id'], 'integer'],
            [['text', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_id' => 'First ID',
            'second_id' => 'Second ID',
            'text' => 'Text',
        ];
    }
}
