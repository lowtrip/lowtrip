<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_place_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 *
 * @property TrainRoutesStatisticsPrices[] $trainRoutesStatisticsPrices
 */
class TrainPlaceTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_place_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 30],
            [['id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainRoutesStatisticsPrices()
    {
        return $this->hasMany(TrainRoutesStatisticsPrices::className(), ['place_type_id' => 'id']);
    }

    public static function findPlaceType($type)
    {
        return static::find()
            ->where(['slug' => $type])
            ->one();
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }
}
