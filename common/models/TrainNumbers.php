<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_numbers".
 *
 * @property integer $id
 * @property string $train_number
 * @property integer $departure_id
 * @property integer $arrival_id
 * @property string $type
 * @property string $name
 * @property integer $carrier_code
 * @property string $title
 */
class TrainNumbers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_numbers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['carrier_code', 'name_id'], 'integer'],
            [['train_number'], 'string', 'max' => 4],
            [['slug'], 'string', 'max' => 10],
            [['type'], 'string', 'max' => 20],
            [['title'], 'string', 'max' => 60],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'train_number' => 'Train Number',
            'type' => 'Type',
            'name_id' => 'Name ID',
            'carrier_code' => 'Carrier Code',
            'title' => 'Title',
        ];
    }

    public function getCarrier()
    {
        return $this->hasOne(TrainCarriers::className(), ['code' => 'carrier_code']);
    }

    public function getUids()
    {
        return $this->hasMany(TrainUid::className(), ['train_id' => 'id']);
    }

    public function getName()
    {
        return $this->hasOne(TrainNames::classname(), ['id' => 'name_id']);
    }

    public function getExtData()
    {
        return $this->hasOne(TrainNumbersExtData::classname(), ['train_id' => 'id']);
    }

    // Поиск поезда по номеру
    // Active Record
    public static function findTrain($train_number)
    {
        return static::find()
            ->where(['train_number' => $train_number])
            ->one();
    }

    /** Получить данные о поезде, в том числе расписания
     *  Active Record
     *  @property string $train_number
     *  @return array
     */ 
    public static function findTrainData($train_number)
    {
        return static::find()
            ->where(['train_number' => $train_number])
            ->with(['carrier', 'uids.calendars'])
            ->asArray()
            ->one();
    }

    /** Получить данные о поездах, у которых нет дополнительной информации
     *  Active Record
     *  @return array
     */
    public static function findTrainsWithoutExtData()
    {
        $staticTable = static::tableName();
        $joinTable = TrainNumbersExtData::tableName();

        return static::find()
            ->leftJoin($joinTable, "$staticTable.id = $joinTable.train_id")
            ->where(['OR', "$joinTable.departure_id is NULL", "$joinTable.arrival_id is NULL"])
            ->all();
    }

    // Найти uid уникальные расписания по маршруту first_id -> second_id
    // property 
    public static function findUniqueUids($first_id, $second_id)
    {
        $firstStations  = TrainStations::findStationsIdInCity($first_id);
        $secondStations = TrainStations::findStationsIdInCity($second_id);

        $aStations = TrainTimetable::findRowsWithStationsIn($firstStations);
        $bStations = TrainTimetable::findRowsWithStationsIn($secondStations);

        return (new Query())
            ->select('a.train_uid')
            ->from(['a' => $aStations])
            ->innerJoin(['b' => $bStations], 'a.train_uid = b.train_uid')
            ->where(['>', 'a.duration', 'b.duration'])
            ->groupBy('a.train_uid');
    }

    public static function findTrainsOnRoute($first_id, $second_id)
    {
        $uniqueTrainUids = static::findUniqueUids($first_id, $second_id);

        return (new Query())
            ->select('a.id, a.train_number, a.slug, a.title')
            ->from('train_numbers AS a')
            ->innerJoin('train_uid as b', 'a.id = b.train_id')
            ->innerJoin(['c' => $uniqueTrainUids], 'b.uid = c.train_uid')
            ->groupBy('a.id')
            ->orderBy('a.train_number')
            ->all();
    }

    public function behaviors()
    {
        return [
            'slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'slug',
                'attribute' => 'train_number',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],
        ];
    }

}
