<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 24.09.2018
 * Time: 22:08
 */

namespace common\models;


use yii\db\ActiveRecord;

class PageBusCityToCity extends ActiveRecord
{
    public static function tableName()
    {
        return "page_bus_city_to_city";
    }

    public function rules()
    {
        return [
            [['from_id'], 'integer'],
            [['main_text', 'advantages', 'tips'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_id' => 'From City ID',
            'main_text' => 'Random words for main text',
            'advantages' => 'Random words for text about advantages',
            'tips' => 'Random words for tips'
        ];
    }
}