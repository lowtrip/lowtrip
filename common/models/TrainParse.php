<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_parse".
 *
 * @property integer $id
 * @property string $from_city
 * @property string $to_city
 * @property string $from_code
 * @property string $to_code
 * @property integer $parsed
 */
class TrainParse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_parse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_city', 'to_city', 'from_code', 'to_code'], 'required'],
            [['parsed'], 'integer'],
            [['from_city', 'to_city'], 'string', 'max' => 40],
            [['from_code', 'to_code'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_city' => 'From City',
            'to_city' => 'To City',
            'from_code' => 'From Code',
            'to_code' => 'To Code',
            'parsed' => 'Parsed',
        ];
    }
}
