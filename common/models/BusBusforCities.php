<?php

namespace common\models;

use yii\db\ActiveRecord;

class BusBusforCities extends ActiveRecord
{
    public static function tableName()
    {
        return 'bus_busfor_cities';
    }

    public function rules()
    {
        return [
            [['lowtrip_city_id', 'onetwotrip', 'unitiki'], 'integer'],
            [['busfor_city_id'], 'string', 'max' => 255],
        ];
    }
}