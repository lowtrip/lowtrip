<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "page_train_type_city".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $title
 * @property string $description
 * @property string $seotext
 */
class PageTrainTypeCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page_train_type_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['city_id', 'title', 'description', 'seotext'], 'required'],
            [['city_id'], 'integer'],
            [['title', 'description', 'seotext'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'title' => 'Title',
            'description' => 'Description',
            'seotext' => 'Seotext',
        ];
    }

    public static function findPage($city_id)
    {
        return static::find()
            ->where(['city_id' => $city_id])
            ->one();
    }
}
