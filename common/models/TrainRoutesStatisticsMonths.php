<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_routes_statistics_months".
 *
 * @property integer $id
 * @property integer $route_id
 * @property string $year
 * @property integer $month
 */
class TrainRoutesStatisticsMonths extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes_statistics_months';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'month'], 'integer'],
            [['year'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Route ID',
            'year' => 'Year',
            'month' => 'Month',
        ];
    }

    public function getPrices()
    {
        return $this->hasMany(TrainRoutesStatisticsMonthsPrices::className(), ['statistic_id' => 'id']);
    }

    // Найти минимальные цены по маршруту, по месяцам, все записи
    public static function getMinPricesByMonthsAll($route_id)
    {
        $static = static::tableName();
        $prices = TrainRoutesStatisticsMonthsPrices::tableName();
        $types = TrainPlaceTypes::tableName();

        return (new Query())
            ->select('*')
            ->from("$static AS a")
            ->innerJoin("$prices AS b", 'a.id = b.statistic_id')
            ->innerJoin("$types AS c", 'c.id = b.place_type_id')
            ->where(['a.route_id' => $route_id])
            ->orderBy(['a.month' => SORT_DESC])
            ->all();
    }

    public static function getMinPricesByMonths($route_id)
    {
        $static = static::tableName();
        $prices = TrainRoutesStatisticsMonthsPrices::tableName();

        return (new Query())
            ->select('a.year, a.month, MIN(b.min_price) as price')
            ->from("$static AS a")
            ->innerJoin("$prices AS b", 'a.id = b.statistic_id')
            ->where(['a.route_id' => $route_id])
            ->groupBy('a.month, b.statistic_id')
            ->orderBy(['a.year' => SORT_DESC, 'a.month' => SORT_DESC])
            ->all();
    }

    public static function getMinPricesLastMonth($route_id)
    {
        $row = static::find()
            ->where(['route_id' => $route_id])
            ->with('prices.type')
            ->orderBy(['month' => SORT_DESC])
            ->limit(1)
            ->asArray()
            ->all();

        return !empty($row) ? $row[0] : null;
    }


    public static function getMinTypePrice($route_id, $place_type_id)
    {
        $static = static::tableName();
        $join = TrainRoutesStatisticsMonthsPrices::tableName();

        return (new Query())
            ->select('*')
            ->from("$static AS a")
            ->innerJoin("$join AS b", 'a.id = b.statistic_id')
            ->where(['AND', ['a.route_id' => $route_id], ['b.place_type_id' => $place_type_id]])
            ->orderBy(["min_price" => SORT_ASC])
            ->limit(1)
            ->all();
    }

    public static function getMinPrice($route_id)
    {
        $static = static::tableName();
        $join = TrainRoutesStatisticsMonthsPrices::tableName();

        $query = (new Query())
            ->select('*')
            ->from("$static AS a")
            ->innerJoin("$join AS b", 'a.id = b.statistic_id')
            ->where(['a.route_id' => $route_id])
            ->orderBy(["year" => SORT_DESC, "month" => SORT_DESC, "min_price" => SORT_ASC])
            ->limit(1)
            ->all();

        return $query;
    }

    public static function getMaxPrice($route_id)
    {
        $static = static::tableName();
        $join = TrainRoutesStatisticsMonthsPrices::tableName();

        return (new Query())
            ->select('*')
            ->from("$static AS a")
            ->innerJoin("$join AS b", 'a.id = b.statistic_id')
            ->where(['a.route_id' => $route_id])
            ->orderBy(["year" => SORT_DESC, "month" => SORT_DESC, "min_price" => SORT_DESC])
            ->limit(1)
            ->all();
    }
}
