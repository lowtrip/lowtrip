<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Travel;

/**
 * TravelSearch represents the model behind the search form about `common\models\Travel`.
 */
class TravelSearch extends Travel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['travel_id', 'user_id', 'car_price', 'house_price', 'food_price', 'total_price'], 'integer'],
            [['first_city', 'second_city', 'car_link', 'house_link', 'house_description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Travel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'travel_id' => $this->travel_id,
            'user_id' => $this->user_id,
            'car_price' => $this->car_price,
            'house_price' => $this->house_price,
            'food_price' => $this->food_price,
            'total_price' => $this->total_price,
        ]);

        $query->andFilterWhere(['like', 'first_city', $this->first_city])
            ->andFilterWhere(['like', 'second_city', $this->second_city])
            ->andFilterWhere(['like', 'car_link', $this->car_link])
            ->andFilterWhere(['like', 'house_link', $this->house_link])
            ->andFilterWhere(['like', 'house_description', $this->house_description]);

        return $dataProvider;
    }
}
