<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "seo".
 *
 * @property integer $id
 * @property integer $first_id
 * @property integer $second_id
 * @property string $text
 */
class Seocar extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seocar';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['first_id', 'second_id'], 'required'],
            [['first_id', 'second_id'], 'integer'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_id' => 'First ID',
            'second_id' => 'Second ID',
            'text' => 'Text',
        ];
    }
}
