<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_popular_cities".
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $counter
 */
class TrainPopularCities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_popular_cities';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'counter'], 'required'],
            [['city_id', 'counter'], 'integer'],
            [['city_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'counter' => 'Counter',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    public static function findByCityId($city_id)
    {
        return static::find()
            ->where(['city_id' => $city_id])
            ->one();
    }

    public static function getPopularCities($limit = 5)
    {
        return static::find()
            ->orderBy(['counter' => SORT_DESC])
            ->limit($limit)
            ->with('city')
            ->all();
    }
}
