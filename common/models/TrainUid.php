<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_uid".
 *
 * @property integer $uid
 * @property integer $train_id
 * @property integer $yandex_uid
 * @property integer $shedule_id
 * @property string $updated_at
 */
class TrainUid extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_uid';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['uid', 'train_id', 'has_timetable'], 'integer'],
            [['yandex_uid'], 'string', 'max' => 20],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uid' => 'Uid',
            'train_id' => 'Train ID',
            'yandex_uid' => 'Yandex Uid',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUidSchedules()
    {
        return $this->hasMany(TrainSchedules::className(), ['train_uid' => 'uid']);
    }

    public function getCalendars()
    {
        return $this->hasMany(TrainCalendars::className(), ['id' => 'calendar_id'])
            ->via('uidSchedules');
    }

    public static function findCalendarsIdQuery($uid)
    {
        $data = [];

        $ids = (new Query())
            ->select('calendar_id')
            ->from(TrainSchedules::tableName())
            ->where(['train_uid' => $uid])
            ->all();

        foreach ($ids as $id)
        {
            $data[] = $id['calendar_id'];
        }

        return $data;
    }

    public static function findCalendarsQuery($uid)
    {
        $calendar_ids = static::findCalendarsIdQuery($uid);

        return (new Query())
            ->select('*')
            ->from(TrainCalendars::tableName())
            ->where(['in', 'id', $calendar_ids])
            ->all();
    }

    // Найти все уникальные расписания для поезда
    // Query
    public static function findTrainUidsQuery($train_id)
    {
        return (new Query())
            ->select('*')
            ->from(static::tableName())
            ->where(['train_id' => $train_id])
            ->all();
    }

    // Найти все уникальные расписания для поезда
    // Active Record
    public static function findTrainUids($train_id)
    {
        return static::find()
            ->where(['train_id' => $train_id])
            ->all();
    }

    public static function findByYandexUid($yandex_uid)
    {
        return static::find()
            ->where(['yandex_uid' => $yandex_uid])
            ->one();
    }

    public static function findWithoutTimetable()
    {
        return static::find()
            ->where(['has_timetable' => 0])
            ->all();
    }
}
