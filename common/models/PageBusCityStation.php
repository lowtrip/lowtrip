<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 23.09.2018
 * Time: 15:35
 */

namespace common\models;


use yii\db\ActiveRecord;

class PageBusCityStation extends ActiveRecord
{
    public static function tableName()
    {
        return "page_bus_city_station";
    }

    public function rules()
    {
        return [
            [['title', 'description', 'random_words'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'random_words' => 'Random Words',
            'title' => 'Title',
            'description' => 'Description'
        ];
    }
}