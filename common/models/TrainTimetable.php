<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_timetables".
 *
 * @property integer $id
 * @property integer $train_uid
 * @property integer $station_id
 * @property string $departure
 * @property string $arrival
 * @property integer $stop_time
 * @property integer $duration
 */
class TrainTimetable extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_timetables';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['train_uid'], 'required'],
            [['train_uid', 'station_id', 'stop_time', 'duration'], 'integer'],
            [['departure', 'arrival'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'train_uid' => 'Train Uid',
            'station_id' => 'Station ID',
            'departure' => 'Departure',
            'arrival' => 'Arrival',
            'stop_time' => 'Stop Time',
            'duration' => 'Duration',
        ];
    }

    public function getStation()
    {
        return $this->hasOne(TrainStations::className(), ['id' => 'station_id']);
    }

    public static function findByUid($uid)
    {
        return static::find()
            ->where(['train_uid' => $uid])
            ->with('station.city')
            ->orderBy('duration')
            ->asArray()
            ->all();
    }

    public static function findRowsWithStationsIn($stations_array)
    {
        return (new Query())
            ->select('train_uid, duration')
            ->from(static::tableName())
            ->where(['station_id' => $stations_array]);
    }
}
