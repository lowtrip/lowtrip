<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_stations".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $type
 * @property string $name
 * @property string $name_slug
 * @property string $region
 * @property string $region_slug
 * @property string $country
 * @property string $country_slug
 * @property double $lat
 * @property double $lng
 */
class TrainStations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_stations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['type', 'esr', 'yandex', 'onetwotrip'], 'string', 'max' => 20],
            [['name', 'name_slug', 'region', 'region_slug', 'country', 'country_slug'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'type' => 'Type',
            'name' => 'Name',
            'name_slug' => 'Name Slug',
            'region' => 'Region',
            'region_slug' => 'Region Slug',
            'country' => 'Country',
            'country_slug' => 'Country Slug',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }

    public function getCity()
    {
        return $this->hasOne(City::classname(), ['id' => 'city_id']);
    }

    public static function findStationsIdInCity($city_id)
    {
        return  (new Query())
            ->select('id')
            ->from(static::tableName())
            ->where(['city_id' => $city_id])
            ->andWhere(['OR', ['type' => 'train_station'], ['type' => 'station']]);
    }

    public static function findStationByYandexCode($yandex_code)
    {
        return static::find()
            ->where(['yandex' => $yandex_code])
            ->one();
    }

    public static function findStationYandexCode($station_id)
    {
        $row =  (new Query())
            ->select('yandex')
            ->from(static::tableName())
            ->where(['id' => $station_id])
            ->one();

        return !empty($row) ? $row['yandex'] : null;
    }

    public static function findById($id)
    {
        return static::find()
            ->where(['id' => $id]);
    }

    public function behaviors()
    {
        return [
            'name_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'name_slug',
                'attribute' => 'name',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'region_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'region_slug',
                'attribute' => 'region',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'country_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'country_slug',
                'attribute' => 'country',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

        ];
    }
}
