<?php

namespace common\models;

use Yii;

use yii\db\ActiveRecord;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $city_name
 * @property string $text
 * @property string $created_at
 * @property boolean $approved
 */
class Reviews extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'city_name', 'text'], 'required'],
            [['city_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['approved'], 'boolean'],
            [['city_name'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'city_name' => 'City Name',
            'text' => 'Text',
            'created_at' => 'Created At',
            'approved' => 'Approved',
        ];
    }

    public static function find()
    {
        return new ReviewQuery(get_called_class());
    }
}

class ReviewQuery extends ActiveQuery
{
    public function approved($state)
    {
        return $this->andWhere(['approved' => $state]);
    }
}
