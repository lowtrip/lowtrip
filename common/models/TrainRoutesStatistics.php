<?php

namespace common\models;

use Yii;
use yii\db\Query;
use yii\db\ActiveRecord;

use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "train_routes_statistics".
 *
 * @property integer $statistic_id
 * @property integer $route_id
 * @property string $date
 *
 * @property TrainRoutes $route
 * @property TrainRoutesStatisticsPrices[] $trainRoutesStatisticsPrices
 */
class TrainRoutesStatistics extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_routes_statistics';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'date'], 'required'],
            [['route_id'], 'integer'],
            [['date'], 'safe'],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainRoutes::className(), 'targetAttribute' => ['route_id' => 'route_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'statistic_id' => 'Statistic ID',
            'route_id' => 'Route ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(TrainRoutes::className(), ['route_id' => 'route_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPrices()
    {
        return $this->hasMany(TrainRoutesStatisticsPrices::className(), ['statistic_id' => 'statistic_id'])
            ->with('type');
    }

    // Получить route_id всех маршрутов, которые есть в статистике
    // @return Array[]
    public static function findUniqueRoutesIds()
    {
        $routes_ids =  static::find()
            ->select('route_id')
            ->groupBy('route_id')
            ->asArray()
            ->all();

        $ids = [];

        if (!empty($routes_ids)) {
            foreach ($routes_ids as $route_id)
            {
                $ids[] = $route_id['route_id'];
            }
        }

        return $ids;
    }

    public static function findRouteStatisticByDate($route_id, $date)
    {
        return static::find()
            ->where(['route_id' => $route_id])
            ->andWhere(['date' => $date])
            ->one();
    }

    public static function findStatisticsByRouteId($route_id, $limit = 14)
    {
        return static::find()
            ->where(['route_id' => $route_id])
            ->andWhere('date >= CURDATE()')
            ->with('prices')
            ->orderBy(['date' => SORT_ASC])
            ->limit($limit)
            ->asArray()
            ->all();
    }

    public static function findStatisticsByRouteMinPrices($route_id, $limit = 14) {
        $results = [];
        $statistics = static::findStatisticsByRouteId($route_id, $limit);

        foreach ($statistics as $statistic):
            $min = INF;
            foreach ($statistic['prices'] as $price):
                if ($price['avaliable']) {
                    $min = $price['min_price'] < $min ? $price['min_price'] : $min;
                } else {
                    continue;
                }
            endforeach;

            $min = $min !== INF ? $min : 0;

            array_push($results, array(
                'date' => $statistic['date'],
                'price' => $min
            ));
        endforeach;

        return $results;
    }

    public static function findStatisticsMinPricesWithType($route_id, $type_id)
    {
        $results = [];
        $static = static::tableName();
        $prices = TrainRoutesStatisticsPrices::tableName();

        $statistics =  (new Query())
            ->select('a.date, b.avaliable, b.min_price')
            ->from("$static AS a")
            ->innerJoin("$prices AS b", 'a.statistic_id = b.statistic_id')
            ->where(['a.route_id' => $route_id])
            ->andWhere(['b.place_type_id' => $type_id])
            ->andWhere('a.date >= CURDATE()')
            ->orderBy(['a.date' => SORT_ASC])
            ->limit(16)
            ->all();

        foreach ($statistics as $statistic):
            $min = $statistic['avaliable'] ? $statistic['min_price'] : 0;
            array_push($results, array(
                'date' => $statistic['date'],
                'price' => $min
            ));
        endforeach;

        return $results;
    }

    public static function findAllStatisticsByRoute($route_id)
    {
        return static::find()
            ->where(['route_id' => $route_id])
            ->with('prices')
            ->orderBy(['date' => SORT_ASC])
            ->asArray()
            ->all();
    }

    public static function findAllStatisticsInterval($route_id, $min_date, $max_date)
    {
        return static::find()
            ->where(['route_id' => $route_id])
            ->andWhere(['>=', 'date', $min_date])
            ->andWhere(['<=', 'date', $max_date])
            ->with('prices')
            ->orderBy(['date' => SORT_ASC])
            ->asArray()
            ->all();
    }

}
