<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_number_ext_data".
 *
 * @property integer $train_id
 * @property integer $departure_id
 * @property integer $arrival_id
 * @property integer $duration_min
 * @property integer $duration_max
 * @property string $place_types
 */
class TrainNumbersExtData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_numbers_ext_data';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['train_id', 'departure_id', 'arrival_id', 'duration_min', 'duration_max'], 'integer'],
            [['place_types'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'train_id' => 'Train ID',
            'departure_id' => 'Departure ID',
            'arrival_id' => 'Arrival ID',
            'duration_min' => 'Duration Min',
            'duration_max' => 'Duration Max',
            'place_types' => 'Place Types',
        ];
    }

}
