<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "train_popular_routes".
 *
 * @property integer $id
 * @property integer $route_id
 * @property integer $first_id
 * @property integer $second_id
 * @property integer $counter
 *
 * @property TrainRoutes $route
 */
class TrainPopularRoutes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_popular_routes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['route_id', 'first_id', 'second_id', 'counter'], 'required'],
            [['route_id', 'first_id', 'second_id', 'counter'], 'integer'],
            [['route_id'], 'unique'],
            [['route_id'], 'exist', 'skipOnError' => true, 'targetClass' => TrainRoutes::className(), 'targetAttribute' => ['route_id' => 'route_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'route_id' => 'Route ID',
            'first_id' => 'First ID',
            'second_id' => 'Second ID',
            'counter' => 'Counter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(TrainRoutes::className(), ['route_id' => 'route_id']);
    }

    public function getDeparture()
    {
        return $this->hasOne(City::className(), ['id' => 'first_id']);
    }

    public function getArrival()
    {
        return $this->hasOne(City::className(), ['id' => 'second_id']);
    }

    public static function findByRouteId($route_id)
    {
        return static::find()
            ->where(['route_id' => $route_id])
            ->one();
    }

    public static function getPopularRoutes($limit = 5, $offset = 0)
    {
        return static::find()
            ->orderBy(['counter' => SORT_DESC])
            ->where(['>', 'counter', 1])
            ->with('arrival')
            ->with('departure')
            ->limit($limit)
            ->offset($offset)
            ->asArray()
            ->all();
    }

    public static function getPopularRoutesWithPrices($limit = 5, $offset = 0)
    {
        $rows = static::getPopularRoutes($limit, $offset);

        $results = [];

        foreach ($rows as $row) {
            $result = $row;
            $minPrice = TrainRoutesStatisticsMonths::getMinPrice($result['route_id']);
            $result['minPrice'] = $minPrice[0]['min_price'];
            $results[] = $result;
        }

        return $results;
    }



    public static function getRouteRating($route_id)
    {
        $route_count = (new Query())
            ->select('counter')
            ->from(static::tableName())
            ->where(['route_id' => $route_id])
            ->one();

        $count = !empty($route_count['counter']) ? $route_count['counter'] : 0;

        $rating =  (new Query())
            ->select('COUNT(*) as rating')
            ->from(static::tableName())
            ->where(['>=', 'counter', $count])
            ->one();

        return $rating['rating'];
    }

    // вот хз че выйдет
    public static function getPopularRoutesFromCity($city_id, $limit = 10)
    {
        return (new Query())
            ->select('route_id, first_id, second_id')
            ->from(static::tableName())
            ->where(['first_id' => $city_id])
            ->orderBy(['counter' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    public static function getPopularRoutesToCity($city_id, $limit = 10)
    {
        return (new Query())
            ->select('route_id, first_id, second_id')
            ->from(static::tableName())
            ->where(['second_id' => $city_id])
            ->orderBy(['counter' => SORT_DESC])
            ->limit($limit)
            ->all();
    }
}
