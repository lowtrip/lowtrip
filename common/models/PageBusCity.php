<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 09.09.2018
 * Time: 14:02
 */

namespace common\models;


use yii\db\ActiveRecord;

class PageBusCity extends ActiveRecord
{
    public static function tableName()
    {
        return "page_bus_city";
    }

    public function rules()
    {
        return [
            [['city_id'], 'integer'],
            [['title', 'description'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'title' => 'Title',
            'description' => 'Description'
        ];
    }

    public static function findPage($city_id)
    {
        return static::find()
            ->where(['city_id' => $city_id])
            ->one();
    }
}