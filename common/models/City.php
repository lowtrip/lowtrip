<?php

namespace common\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "city".
 *
 * @property integer $id
 * @property string $city
 * @property string $altname
 * @property string $city_slug
 * @property string $region
 * @property string $region_slug
 * @property integer $population
 * @property double $lat
 * @property double $lng
 * @property string $country
 * @property string $country_slug
 * @property integer $city_id
 * @property integer $GMT
 */
class City extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_slug', 'region_slug', 'country_slug'], 'string'],
            [['population', 'city_id', 'GMT'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['city', 'alt_name'], 'string', 'max' => 40],
            [['region'], 'string', 'max' => 60],
            [['country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'City',
            'alt_name' => 'Alt Name',
            'city_slug' => 'City Slug',
            'region' => 'Region',
            'region_slug' => 'Region Slug',
            'population' => 'Population',
            'lat' => 'Lat',
            'lng' => 'Lng',
            'country' => 'Country',
            'country_slug' => 'Country Slug',
            'city_id' => 'City ID',
            'GMT' => 'Gmt',
        ];
    }

    public function getInfo()
    {
        return $this->hasOne(CitiesInfo::className(), ['city_id' => 'id']);
    }

    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['city_id' => 'id'])->approved(1);
    }

    public function getStations()
    {
        return $this->hasOne(CityStations::className(), ['city_id' => 'id']);
    }

    public function getTrainRoutesFrom()
    {
        return $this->hasMany(TrainRoutes::className(), ['first_city_id' => 'id']);
    }

    public function getTrainRoutesTo()
    {
        return $this->hasMany(TrainRoutes::className(), ['second_city_id' => 'id']);
    }

    public function getBusStations(){
        return $this->hasMany(BusStations::className(), ['city_id' => 'id']);
    }

    public static function findCityById($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->one();
    }

    public static function findCityBySlug($slug)
    {
        return static::find()
            ->where(['city_slug' => $slug])
            ->one();
    }

    public function findPopularRoutesFromPart($limit = 10)
    {
        return $this->hasMany(TrainPopularRoutes::className(), ['first_id' => 'id'])
            ->with('arrival')
            ->orderBy(['counter' => SORT_DESC])
            ->limit($limit);
    }

    // Популярные жд маршруты из этого города
    public function findPopularRoutesFrom($limit = 10)
    {
        return $this->findPopularRoutesFromPart($limit)->all();
    }

    // Популярные жд маршруты из этого города
    public function findPopularRoutesFromWithMinPrice($limit = 10)
    {
        $rows = $this->findPopularRoutesFromPart($limit)
            ->asArray()
            ->all();

        $results = [];

        foreach ($rows as $row) {
            $result = $row;
            $minPrice = TrainRoutesStatisticsMonths::getMinPrice($result['route_id']);
            $result['minPrice'] = $minPrice[0]['min_price'];
            $results[] = $result;
        }

        return $results;
    }

    // С расстоянием
    public function findPopularRoutesWithDistanceFrom($limit = 10)
    {
        $rows = $this->findPopularRoutesFromPart($limit)
            ->asArray()
            ->all();

        $results = [];

        foreach ($rows as $row) {
            $result = $row;
            $distance = Yii::$app->distance->getDistance($this->lat, $this->lng, $row['arrival']['lat'], $row['arrival']['lng']);
            $result['distance'] = $distance;
            $results[] = $result;
        }

        return $results;
    }

    public function findPopularRoutesToPart($limit = 10)
    {
        return $this->hasMany(TrainPopularRoutes::className(), ['second_id' => 'id'])
            ->with('departure')
            ->orderBy(['counter' => SORT_DESC])
            ->limit($limit);
    }

    // Популярные жд маршруты в этот город
    public function findPopularRoutesTo($limit = 10)
    {
        return $this->findPopularRoutesToPart($limit)->all();
    }

    public function findPopularRoutesToWithMinPrice($limit = 10)
    {
        $rows = $this->findPopularRoutesToPart($limit)
            ->asArray()
            ->all();

        $results = [];

        foreach ($rows as $row) {
            $result = $row;
            $minPrice = TrainRoutesStatisticsMonths::getMinPrice($result['route_id']);
            $result['minPrice'] = $minPrice[0]['min_price'];
            $results[] = $result;
        }

        return $results;
    }

    public function nearbyCityWithStations($second_city_id = '')
    {
        $cities = (new Query())
            ->select('a.id, city, city_slug, region, country, lat, lng')
            ->from(['a' => 'city'])
            ->innerJoin(['b' => 'city_stations'], 'a.id = b.city_id')
            ->where(['b.has_stations' => 1])
            ->andWhere(['between', 'a.lat', $this->lat - 4, $this->lat + 4])
            ->andWhere(['between', 'a.lng', $this->lng - 6, $this->lng + 6])
            ->andWhere(['not in', 'a.id',  $this->id])
            ->andWhere(['not in', 'a.id',  $second_city_id])
            ->all();

        $results = [];

        foreach ($cities as $city) {
            $result = $city;
            $distance = Yii::$app->distance->getDistance($this->lat, $this->lng, $city['lat'], $city['lng']);
            $result['distance'] = $distance;
            $results[] = $result;
        }

        usort($results, function($a, $b) {
            if ($a['distance'] == $b['distance']) {
                return 0;
            }
            return ($a['distance'] < $b['distance']) ? -1 : 1;
        });

        return array_slice($results, 0, 5);
    }

    // Крупнейшие города от заданного, отсортированные по населению
    public function topCities()
    {
        return static::find()
            ->where(['between', 'lat', $this->lat - 4, $this->lat + 4])
            ->andWhere(['between', 'lng', $this->lng - 6, $this->lng + 6])
            ->andWhere(['country_slug' => 'rossiya'])
            ->andWhere(['not', ['city_id' => null]])
            ->andWhere(['not in', 'id',  $this->id])
            ->orderBy(['population' => SORT_DESC])
            ->limit(5)
            ->all();
    }

    // Крупнейшие города в стране
    public function topCitiesCountry($country_slug, $limit = 0)
    {
        return static::find()
            ->andWhere(['country_slug' => $country_slug])
            ->andWhere(['not', ['city_id' => null]])
            ->andWhere(['not in', 'id',  $this->id])
            ->orderBy(['population' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    public static function topCitiesRussia($limit = 5)
    {
        return static::find()
            ->andWhere(['country_slug' => 'rossiya'])
            ->orderBy(['population' => SORT_DESC])
            ->limit($limit)
            ->all();
    }

    // Ближайшие города от заданного
    public function closeCities($limit = 0)
    {
        return static::find()
            ->where(['between', 'lat', $this->lat - 4, $this->lat + 4])
            ->andWhere(['between', 'lng', $this->lng - 6, $this->lng + 6])
            ->andWhere(['not', ['city_id' => null]])
            ->andWhere(['not in', 'id',  $this->id])
            ->orderBy(['city' => SORT_ASC])
            ->limit($limit)
            ->all();
    }

    public static function findCityWithStationsBySlug($slug)
    {
        return static::find()
            ->where(['city_slug' => $slug])
            ->with('stations')
            ->one();
    }

    // Поиск города по названию
    // Используется для определения города после ответа от yandex
    // Стоит конечно улучшить
    // property string $city_name
    // property float $lat
    // property float $lng
    // property float $radius
    public static function findCityByNameInRadius($city_name, $lat, $lng, $radius = 0)
    {
        return static::find()
            ->where(['city' => $city_name])
            ->andWhere(['between', 'lat', $lat - $radius, $lat + $radius])
            ->one();
    }

    // Поиск городов по фразе ("Мос", "Каза")
    // property string $term
    // property integer $except_id
    public static function findCitiesByTerm($term, $except_id = 0)
    {
        return static::find()
            ->where(['like', 'city', $term.'%', false])
            ->andWhere(['not in', 'id',  $except_id])
            ->orderBy(['population' => SORT_DESC])
            ->limit(5)
            ->asArray()
            ->all();
    }


    public function behaviors()
    {
        return [
            'city_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'city_slug',
                'attribute' => 'city',
                // optional params
                'ensureUnique' => true,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'region_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'region_slug',
                'attribute' => 'region',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

            'country_slug' => [
                'class' => 'Zelenin\yii\behaviors\Slug',
                'slugAttribute' => 'country_slug',
                'attribute' => 'country',
                // optional params
                'ensureUnique' => false,
                'replacement' => '-',
                'lowercase' => true,
                'immutable' => false,
                // If intl extension is enabled, see http://userguide.icu-project.org/transforms/general.
                'transliterateOptions' => 'Russian-Latin/BGN; Any-Latin; Latin-ASCII; NFD; [:Nonspacing Mark:] Remove; NFC;'
            ],

        ];
    }

}
