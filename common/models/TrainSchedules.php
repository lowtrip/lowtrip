<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "train_shedules".
 *
 * @property integer $train_uid
 * @property integer $calendar_id
 */
class TrainSchedules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'train_shedules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['train_uid', 'calendar_id'], 'required'],
            [['train_uid', 'calendar_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'train_uid' => 'Train Uid',
            'calendar_id' => 'Calendar ID',
        ];
    }
}
