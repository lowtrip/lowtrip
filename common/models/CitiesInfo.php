<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cities_info".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $city_name
 * @property string $page_id
 * @property string $text
 * @property string $img_url
 */
class CitiesInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cities_info';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['city_id', 'city_name', 'page_id', 'text'], 'required'],
            [['city_id'], 'integer'],
            [['text'], 'string'],
            [['city_name'], 'string', 'max' => 40],
            [['page_id'], 'string', 'max' => 20],
            [['img_url'], 'string', 'max' => 512],
            [['city_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city_id' => 'City ID',
            'city_name' => 'City Name',
            'page_id' => 'Page ID',
            'text' => 'Text',
            'img_url' => 'Img Url',
        ];
    }
}
