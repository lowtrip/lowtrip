<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "travel".
 *
 * @property integer $user_id
 * @property string $first_city
 * @property string $second_city
 * @property integer $car_price
 * @property string $car_link
 * @property integer $house_price
 * @property string $house_link
 * @property string $house_description
 * @property integer $food_price
 * @property integer $total_price
 *
 * @property User $user
 */
class Travel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'car_price', 'house_price', 'food_price', 'total_price'], 'integer'],
            [['first_city', 'second_city',  'house_description'], 'string', 'max' => 255],
            [['car_link', 'house_link',], 'string', 'max' => 512],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'first_city' => 'Отправление',
            'second_city' => 'Прибытие',
            'car_price' => 'Стоимость транспорта',
            'car_link' => 'Ссылка на транспорт',
            'house_price' => 'Стоимость жилья',
            'house_link' => 'Ссылка на жильё',
            'house_description' => 'House Description',
            'food_price' => 'Стоимость питания',
            'total_price' => 'Итого',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
}
