<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TravelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="travel-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'travel_id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'first_city') ?>

    <?= $form->field($model, 'second_city') ?>

    <?= $form->field($model, 'car_price') ?>

    <?php // echo $form->field($model, 'car_link') ?>

    <?php // echo $form->field($model, 'house_price') ?>

    <?php // echo $form->field($model, 'house_link') ?>

    <?php // echo $form->field($model, 'house_description') ?>

    <?php // echo $form->field($model, 'food_price') ?>

    <?php // echo $form->field($model, 'total_price') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
