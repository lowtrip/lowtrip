<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Travel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="travel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'first_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'second_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'car_price')->textInput() ?>

    <?= $form->field($model, 'car_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'house_price')->textInput() ?>

    <?= $form->field($model, 'house_link')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'house_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'food_price')->textInput() ?>

    <?= $form->field($model, 'total_price')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
