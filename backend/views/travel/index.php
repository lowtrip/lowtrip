<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TravelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Путешествия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="travel-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Новое путешествие', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'travel_id',
            'user_id',
            'first_city',
            'second_city',
            // 'car_price',
            // 'car_link',
            // 'house_price',
            // 'house_link',
            // 'house_description',
            // 'food_price',
            'total_price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
