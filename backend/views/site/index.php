<?php

/* @var $this yii\web\View */

$this->title = 'Статистика';
?>

<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3><?= $cities ?></h3>

                    <p>Города</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-globe"></i>
                </div>
                <a href="/admin/city" class="small-box-footer">Перейти <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3><?= $users ?></h3>

                    <p>Регистрации пользователей</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="/admin/user" class="small-box-footer">Перейти <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>53</sup></h3>

                    <p>Собрано email</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-mail"></i>
                </div>
                <a href="#" class="small-box-footer">Перейти <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $travels ?></h3>

                    <p>Путешествий</p>
                </div>
                <div class="icon">
                    <i class="ion ion-android-compass"></i>
                </div>
                <a href="/admin/travel" class="small-box-footer">Перейти <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <!-- /.row -->

</section>
