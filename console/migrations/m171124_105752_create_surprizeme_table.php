<?php

use yii\db\Migration;

/**
 * Handles the creation of table `surprizeme`.
 */
class m171124_105752_create_surprizeme_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('surprizeme', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(11)->notNull(),
            'city_slug' => $this->text(),
        ]);

        $this->createIndex(
            'idx-surprizeme-city_id',
            'surprizeme',
            'city_id'
        );

//        $this->addForeignKey(
//            'fk-surprizeme-city_id',
//            'surprizeme',
//            'city_id',
//            'city',
//            'id',
//            'CASCADE'
//        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
//        $this->dropForeignKey(
//            'fk-surprizeme-city_id',
//            'surprizeme'
//        );

        $this->dropIndex(
            'idx-surprizeme-city_id',
            'surprizeme'
        );

        $this->dropTable('surprizeme');
    }
}
