<?php

use yii\db\Migration;

/**
 * Handles the creation of table `travel`.
 */
class m170612_193600_create_travel_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('travel', [
            'travel_id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_city' => $this->string(),
            'second_city' => $this->string(),
            'car_price' => $this->integer(),
            'car_link' => $this->string(),
            'house_price' => $this->integer(),
            'house_link' => $this->string(),
            'house_description' => $this->string(),
            'food_price' => $this->integer(),
            'total_price' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-travel-user_id',
            'travel',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-travel-user_id','travel');
        $this->dropTable('travel');
    }
}
