<?php

use yii\db\Migration;

/**
 * Handles the creation of table `profile`.
 */
class m170609_130119_create_profile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('profile', [
            'user_id' => $this->primaryKey(),
            'avatar' => $this->string(),
            'first_name' => $this->string(),
            'second_name' => $this->string(),
            'middle_name' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-profile-user_id',
            'profile',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-profile-user_id','profile');
        $this->dropTable('profile');
    }
}
