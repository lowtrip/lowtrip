<?php

namespace console\controllers;

use common\models\TrainPlaceTypes;
use common\models\TrainRoutesStatistics;

use common\models\TrainRoutesStatisticsMonths;
use common\models\TrainRoutesStatisticsMonthsPrices;
use yii\console\Controller;
use yii\helpers\Console;

class TrainsStatisticController extends Controller
{
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    // Записать статистику по направлениям поездов, сгруппированную по месяцам
    // Для каждого типа места - считаем минимальную среднюю стоимость
    public function actionCreateStatisticByMonths()
    {
        $routes_ids = TrainRoutesStatistics::findUniqueRoutesIds();

        foreach ($routes_ids as $route_id)
        {
            $route_statistics = TrainRoutesStatistics::findAllStatisticsByRoute($route_id);

            $this->analyzeRouteStatistics($route_statistics, $route_id);
        }
    }

    // Записать статистику по направлениям поездов, сгруппированную по месяцам
    // Для каждого типа места - считаем минимальную среднюю стоимость
    public function actionCreateStatisticLastMonth()
    {
        $routes_ids = TrainRoutesStatistics::findUniqueRoutesIds();

        $min_date = date('Y-m-d', strtotime('first day of previous month'));
        $max_date = date('Y-m-d', strtotime('last day of previous month'));

        foreach ($routes_ids as $route_id)
        {
            $route_statistics = TrainRoutesStatistics::findAllStatisticsInterval($route_id, $min_date, $max_date);

            $this->analyzeRouteStatistics($route_statistics, $route_id);
        }
    }

    // Анализ статистических записей маршрута
    private function analyzeRouteStatistics($statistics_array, $route_id)
    {
        $statistics_total = array();

        foreach($statistics_array as $item)
        {
            $timestamp = strtotime($item['date']);
            $month = date('m', $timestamp);
            $year = date('Y', $timestamp);

            $cur_month = mktime(0, 0, 0, date('m'), 1, date('Y'));
            $stat_month = mktime(0,0,0, $month, 1, $year);

            if ($stat_month < $cur_month) {

                if (empty($statistics_total[$year])) {
                    $statistics_total[$year] = array();
                }

                if (empty($statistics_total[$year][$month])) {
                    $statistics_total[$year][$month] = array();
                }

                foreach($item['prices'] as $price)
                {

                    $place_type = $price['place_type_id'];

                    if (empty($statistics_total[$year][$month][$place_type])) {
                        $statistics_total[$year][$month][$place_type] = array();
                    }

                    array_push($statistics_total[$year][$month][$place_type], $price['min_price']);
                }
            }
        }

        foreach($statistics_total as $year_key => $year_stats)
        {
            foreach($year_stats as $month_key => $month_stats)
            {
                $statistic_id = $this->createMonthStatistic($route_id, $year_key, $month_key);

                if (!empty($statistic_id)) {
                    foreach ($month_stats as $place_type => $place_type_stats)
                    {
                        if (!empty($place_type_stats)) {

                            $min = $place_type_stats[0];

                            foreach ($place_type_stats as $value)
                            {
                                if ($value < $min) {
                                    $min = $value;
                                }
                            }

                            $this->createMonthPriceStatistic($statistic_id, $place_type, $min);
                        }
                    }
                }
            }
        }
    }

    private function createMonthStatistic($route_id, $year, $month)
    {
        $model = new TrainRoutesStatisticsMonths();
        $model->route_id = $route_id;
        $model->year = $year;
        $model->month = $month;

        if ($model->save()) {
            return $model->id;
        }
        return null;
    }

    private function createMonthPriceStatistic($statistic_id, $place_type_id, $min_price)
    {
        $model = new TrainRoutesStatisticsMonthsPrices();
        $model->statistic_id = $statistic_id;
        $model->place_type_id = $place_type_id;
        $model->min_price = $min_price;
        $model->save();
    }

    public function actionPlaceType()
    {
        $rows = TrainPlaceTypes::find()
            ->all();

        foreach ($rows as $row)
        {
            $row->save();
        }
    }

}