<?php

namespace console\controllers;

use common\models\TrainNumbers;
use yii\console\Controller;

use Yii;
use yii\helpers\FileHelper;

use common\models\City;
use common\models\TrainPlaceTypes;

class SitemapController extends Controller
{

    public function actionGenerateSitemap()
    {
        $this->actionModuleTrip();
        $this->actionModuleCar();
        $this->actionModuleTrain();
        $this->actionModuleBus();
        $this->actionSitemap();
    }

    // Генерация всех! ссылок для модуля train
    public function actionModuleTrain($module = 'train', $self_directory = true)
    {
        $base_path = Yii::getAlias( '@frontend' ).'/web/';
        if ($self_directory) {
            $path_to_dir = $base_path.$module;
            if (!is_dir($path_to_dir)) {
                FileHelper::createDirectory($path_to_dir);
            }
        } else {
            $path_to_dir = $base_path;
        }

        $sql = "SELECT a.id, a.city_slug FROM city AS a INNER JOIN city_stations AS b ON a.id = b.city_id WHERE b.has_stations = 1";
        $cities = City::findbysql($sql)->asArray()->all();
        $placeTypes = TrainPlaceTypes::find()->asArray()->all();
        $trainNumbers = TrainNumbers::find()->asArray()->all();
        $all = [];

        // Главная
        $all[] = 'https://lowtrip.ru/'.$module;

        foreach ($cities as $first) {
            // https://lowtrip.ru/train/city
            $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"];
            $all[] = $link;

            foreach ($cities as $second) {
                if ($first['id'] !== $second['id']) {
                    // https://lowtrip.ru/train/city/city
                    $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"].'/'.$second["city_slug"];
                    $all[] = $link;

                    foreach ($placeTypes as $type) {
                        // https://lowtrip.ru/train/city/city/place_type
                        $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"].'/'.$second["city_slug"].'/'.$type["slug"];
                        $all[] = $link;
                    }
                }
            }
        }

        foreach ($trainNumbers as $number) {
            $link = 'https://lowtrip.ru/'.$module.'/raspisanie/'.$number["train_number"];
            $all[] = $link;
        }

        $this->createAllSitemaps($all, $path_to_dir, $module, 0);
    }

    // Генерация всех! ссылок для модуля trip
    public function actionModuleTrip($module = 'trip', $self_directory = true)
    {
        $base_path = Yii::getAlias( '@frontend' ).'/web/';
        if ($self_directory) {
            $path_to_dir = $base_path.$module;
            if (!is_dir($path_to_dir)) {
                FileHelper::createDirectory($path_to_dir);
            }
        } else {
            $path_to_dir = $base_path;
        }

        $sql = "SELECT id, city_slug FROM city";
        $cities = City::findbysql($sql)->asArray()->all();
        $all = [];

        // https://lowtrip.ru/trip
        $all[] = 'https://lowtrip.ru/'.$module;

        foreach ($cities as $first) {
            // https://lowtrip.ru/trip/city
            $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"];
            $all[] = $link;

            foreach ($cities as $second) {
                if ($first['id'] !== $second['id']) {
                    // https://lowtrip.ru/trip/city/city
                    $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"].'/'.$second["city_slug"];
                    $all[] = $link;
                }
            }
        }

        $this->createAllSitemaps($all, $path_to_dir, $module, 0);
    }


    // Генерация всех! ссылок для модуля car
    public function actionModuleCar($module = 'car', $self_directory = true)
    {
        $base_path = Yii::getAlias( '@frontend' ).'/web/';
        if ($self_directory) {
            $path_to_dir = $base_path.$module;
            if (!is_dir($path_to_dir)) {
                FileHelper::createDirectory($path_to_dir);
            }
        } else {
            $path_to_dir = $base_path;
        }

        $sql    = "SELECT id, city_slug FROM city";
        $cities = City::findbysql($sql)->asArray()->all();
        $all    = [];

        // https://lowtrip.ru/car
        // $all[] = 'https://lowtrip.ru/'.$module;

        foreach ($cities as $first) {
            // https://lowtrip.ru/car/city
            // $link = 'https://lowtrip.ru/'.$module.'/'.$first['city_slug'];
            // $all[] = $link;

            foreach ($cities as $second) {
                if ($first['id'] !== $second['id']) {
                    // https://lowtrip.ru/car/city/city
                    $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"].'/'.$second["city_slug"];
                    $all[] = $link;
                }
            }
        }

        $this->createAllSitemaps($all, $path_to_dir, $module, 0);
    }

    // Генерация всех! ссылок для модуля bus
    public function actionModuleBus($module = 'bus', $self_directory = true)
    {
        $base_path = Yii::getAlias( '@frontend' ).'/web/';
        if ($self_directory) {
            $path_to_dir = $base_path.$module;
            if (!is_dir($path_to_dir)) {
                FileHelper::createDirectory($path_to_dir);
            }
        } else {
            $path_to_dir = $base_path;
        }

        $sqlCities = "SELECT id, city_slug FROM city";
        $cities = City::findbysql($sqlCities)->asArray()->all();
        $all = [];

        // Главная
        $all[] = 'https://lowtrip.ru/'.$module;

        foreach ($cities as $first) {
            // https://lowtrip.ru/bus/city
            $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"];
            $all[] = $link;

            foreach ($cities as $second) {
                if ($first['id'] !== $second['id']) {
                    // https://lowtrip.ru/bus/city/city
                    $link = 'https://lowtrip.ru/'.$module.'/'.$first["city_slug"].'/'.$second["city_slug"];
                    $all[] = $link;
                }
            }
        }

        $sqlStations = "SELECT a.city_slug, b.name_slug FROM city AS a INNER JOIN bus_stations AS b ON a.id = b.city_id WHERE b.type = 'bus_station'";
        $cityStations = City::findbysql($sqlStations)->asArray()->all();

        foreach ($cityStations as $row) {
            $link = 'https://lowtrip.ru/'.$module.'/'.$row['city_slug'].'/stations/'.$row['name_slug'];
            $all[] = $link;
        }

        $this->createAllSitemaps($all, $path_to_dir, $module, 0);
    }

    // Разделяет по частям все переданные ссылки для создания сайтмапов
    protected function createAllSitemaps($all, $path_to_dir, $module, $n = 1)
    {
        $k = 0;

        $list = [];

        $count = count($all);

        for ($i = 0; $i < $count; $i++) {
            if ($k < 50000 && $k < $count) {
                $list[] = $all[$i];
                $k++;
                continue;
            }
            $this->createSitemap($list, $n, $path_to_dir, $module);
            $list = [];
            $k = 0;
            $n++;
        }

        if (!empty($list)) {
            $this->createSitemap($list, $n, $path_to_dir, $module);
        }

    }

    // Создает сайтмап по указанному пути
    protected function createSitemap($list, $n, $path_to_dir, $module)
    {
        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;

        $urlset = $xml->createElement('urlset');
        $xmlns = $xml->createAttribute('xmlns');
        $xmlns->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
        $urlset->appendChild($xmlns);

        $date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');

        foreach ($list as $link)
        {
            $url = $xml->createElement('url');
            $loc = $xml->createElement('loc');
            $lastmod = $xml->createElement('lastmod');
            $changefreq = $xml->createElement('changefreq');

            $url_value = $xml->createTextNode($link);
            $lastmod_value = $xml->createTextNode($date);
            $changefreq_value = $xml->createTextNode('monthly');

            $loc->appendChild($url_value);
            $lastmod->appendChild($lastmod_value);
            $changefreq->appendChild($changefreq_value);

            $url->appendChild($loc);
            $url->appendChild($lastmod);
            $url->appendChild($changefreq);

            $urlset->appendChild($url);
        }

        $xml->appendChild($urlset);

        $filename = 'sitemap_'.$module.'_'.$n.'.xml';
        $xml->save($path_to_dir.'/'.$filename);
    }

    // Собирает все сгенирированные сайтмапы в корне и создает общий сайтмап сайтмапов
    public function actionSitemap()
    {
        $modules = ['trip', 'car', 'train', 'bus'];
        $base_path = Yii::getAlias( '@frontend' ).'/web/';
        $links = [];

        foreach ($modules as $module)
        {
            $path_to_dir = $base_path.$module;
            $files = array_diff(scandir($path_to_dir), array('..', '.'));
            foreach ($files as $file)
            {
                $link = 'https://lowtrip.ru/'.$file;
                $links[] = $link;

                $filepath_old = $path_to_dir.'/'.$file;
                $filepath_new = $base_path.$file;
                copy($filepath_old, $filepath_new);
                unlink($filepath_old);
            }
            rmdir($path_to_dir);
        }

        $xml = new \DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;

        $sitemapindex = $xml->createElement('sitemapindex');
        $xmlns = $xml->createAttribute('xmlns');
        $xmlns->value = 'http://www.sitemaps.org/schemas/sitemap/0.9';
        $sitemapindex->appendChild($xmlns);

        foreach ($links as $link)
        {
            $sitemap = $xml->createElement('sitemap');
            $loc = $xml->createElement('loc');

            $link_value = $xml->createTextNode($link);
            $loc->appendChild($link_value);
            $sitemap->appendChild($loc);
            $sitemapindex->appendChild($sitemap);
        }

        $xml->appendChild($sitemapindex);
        $filename = 'sitemap.xml';
        $xml->save($base_path.$filename);
    }
}