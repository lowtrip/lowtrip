<?php

//namespace app\commands;
namespace console\controllers;

use common\models\CityStations;
use common\models\TrainNumbers;
use common\models\TrainRoutesDirectTrains;
use yii\console\Controller;

use common\models\City;
use common\models\TrainRoutes;

class TrainRoutesController extends Controller
{

    protected function getCities()
    {
        return City::find()
            ->innerJoinWith('stations b')
            ->where(['b.has_stations' => 1])
            ->all();
    }

    protected function getCitiesUnchecked()
    {
        return CityStations::find()
            ->where(['has_stations' => 0])
            ->andWhere('stations_id <> 0')
            ->all();
    }

    public function actionBuildRoutes()
    {
        $cities = $this->getCities();

        foreach($cities as $c1 => $first)
        {
            foreach($cities as $c2 => $second)
            {
                if ($first->id !== $second->id)
                {
                    $row = new TrainRoutes();
                    $row->first_city_id = $first->id;
                    $row->second_city_id = $second->id;
                    $row->save();
                }
            }
        }
    }

    public function actionNewCityRoutes($city_id)
    {
        $cities = $this->getCities();

        $new_city = City::find()->where(['id' => $city_id])->one();

        foreach ($cities as $city)
        {
            if ($city->id !== $new_city->id)
            {
                $row = new TrainRoutes();
                $row->first_city_id = $city->id;
                $row->second_city_id = $new_city->id;
                $row->save();

                $row = new TrainRoutes();
                $row->first_city_id = $new_city->id;
                $row->second_city_id = $city->id;
                $row->save();
            }
        }
    }

    public function actionFindDirectTrains($saveTrains = true)
    {
        $count = TrainRoutes::find()
            ->where('direct is NULL')
            ->count();

        $offset = 10000;

        $routes = TrainRoutes::getRoutesWithoutDirectInfo(10000);

        while ($count > 0) {
            if ($offset > 0) {
                foreach ($routes as $route)
                {
                    $direct_trains = TrainNumbers::findTrainsOnRoute($route->first_city_id, $route->second_city_id);

                    if (!empty($direct_trains)) {
                        if ($saveTrains) {
                            foreach ($direct_trains as $train) {
                                $model = new TrainRoutesDirectTrains();
                                $model->route_id = $route->route_id;
                                $model->train_id = $train['id'];
                                $model->save();
                            }
                        }

                        $route->direct = 1;
                        $route->save();
                    } else {
                        $route->direct = 0;
                        $route->save();
                    }

                    $offset--;
                    $count--;
                }
            }
            else {
                $offset = 10000;
                $routes = TrainRoutes::getRoutesWithoutDirectInfo(10000);
            }
        }
    }

}