<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

use common\models\City;
use common\models\BusBusforCities;

use common\classes\api\OnetwotripBusApi;
use common\classes\api\UnitikiBusApi;

class BusParserController extends Controller
{
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    public function actionFindStationsOnetwotrip($automatic = false)
    {
        $cities = $this->getCitiesWithoutRecords();

        echo count($cities);

        $api = new OnetwotripBusApi();

        foreach ($cities as $k => $city)
        {
            $result = $api->geoSuggest($city->city);
            $result = json_decode($result);

            $cityUpdated = false;

            if (!empty($result->data)) {
                $variants = $result->data;

                foreach ($variants as $variant) {
                    if (
                        $variant->name == $city->city &&
                        $variant->region == $city->region &&
                        $variant->country == $city->country
                    ) {
                        $cityUpdated = $this->updateRow($city->id, $variant->geopointId);
                        break;
                    }
                }

                if (!$cityUpdated && !$automatic) {
                    foreach ($variants as $variant) {
                        if (
                            $variant->name == $city->city &&
                            $variant->country == $city->country
                        ) {
                            echo "$city->city $city->region $city->country\n";
                            echo var_dump($variant) . "\n";
                            if (Console::confirm("Add to database this station id?\n")) {
                                $this->updateRow($city->id, $variant->geopointId);
                                break;
                            }
                        }
                    }
                }
            }
        }

    }

    public function actionFindStationsUnitiki($automatic = false)
    {
        $cities = $this->getCitiesWithoutRecords('unitiki');

        echo count($cities) . "\n";

        $api = new UnitikiBusApi();

        foreach ($cities as $k => $city)
        {
            $result = $api->city_list_from(array(
                'query' => $city->city
            ));

            $cityUpdated = false;

            echo "$city->city $city->region $city->country\n";
            echo print_r($result);

            if (!empty($result)) {
                foreach ($result as $variant) {
                    if (
                        $variant->city_title == $city->city &&
                        $variant->region_title == $city->region &&
                        $variant->country_title == $city->country
                    ) {
                        $cityUpdated = $this->updateRow($city->id, $variant->city_id, 'unitiki');
                        break;
                    }
                }

                if (!$cityUpdated && !$automatic) {
                    echo "$city->city $city->region $city->country\n";

                    foreach ($result as $variant) {
                            echo print_r($variant) . "\n";
                            if (Console::confirm("Add to database this station id?\n")) {
                                $this->updateRow($city->id, $variant->city_id, 'unitiki');
                                break;
                            }
                    }
                }
            }
        }
    }

    protected function getCitiesWithoutRecords($column = 'onetwotrip')
    {
        return City::find()
            ->leftJoin('bus_busfor_cities', 'bus_busfor_cities.lowtrip_city_id = city.id')
            ->where('bus_busfor_cities.'.$column.' IS NULL')
            ->all();
    }

    protected function updateRow($city_id, $partner_id, $column = 'onetwotrip')
    {
        $row = BusBusforCities::find()
            ->where(['lowtrip_city_id' => $city_id])
            ->one();

        if (empty($row)) {
            $row = new BusBusforCities();
            $row->lowtrip_city_id = $city_id;
        }

        $row->$column = $partner_id;

        return $row->save();
    }
}