<?php

namespace console\controllers;

use Yii;
use yii\db\Exception;
use yii\helpers\Console;
use yii\console\Controller;
use yii\db\Query;

use common\classes\TrainYandexParser;
use common\classes\TrainOnetwotripParser;

class CronController extends Controller
{
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    // Парсинг цен с Onetwotrip на $top популярные направления на $days
    public function actionParseTopRoutesPrices($top = 10, $days = 14, $topOffset = 0, $daysOffset = 0)
    {
        $parser = new TrainOnetwotripParser;

        $parser->setDebug(true);
        $parser->useReferrerParams(false);

        $parser->parseTopRoutesPrices($top, $days, $topOffset, $daysOffset);
    }

    public function actionParseYandexRasp()
    {
        $parser = new TrainYandexParser();
        $parser->setDebug(true);
        $parser->updateAllTrainsRasp();
    }

}