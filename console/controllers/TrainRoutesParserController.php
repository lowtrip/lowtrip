<?php

//namespace app\commands;
namespace console\controllers;

use Yii;
use yii\helpers\FileHelper;
use yii\console\Controller;

class TrainRoutesParserController extends Controller
{

    public function actionParse()
    {
        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = 'trains.html';

        $dom = new \DOMDocument('1.0','UTF-8');

        //$url = 'https://transport.marshruty.ru/Transports/TrainList.aspx?view=TrainsInRussia';
        //$html = file_get_contents($url);
        $html = file_get_contents($path.$filename);

        $dom->loadHTML($html);

        $xpath = new \DomXPath($dom);

        //$res = $xpath->query(".//div[@id='main_global']/div[@id='main_global1']/div/div[1]/div[0]/table/tbody/tr/td/ul/li/a");
        $res = $xpath->query(".//table/tbody/tr/td/ul/li/a");
        $collection = [];

        foreach ($res as $k => $link)
        {
            $collection[] = $link->nodeValue;
            echo $collection[$k]."\n";
        }

        $file = fopen($path.'trains.csv', 'w+');
        foreach ($collection as $row)
        {
            $row = mb_convert_encoding($row, "CP-1251", "UTF-8");
            $row = $this->parseString($row);
            fputcsv($file, $row, ';');
        }
        fclose($file);
    }

    protected function parseString($string)
    {
        $number = substr($string, 0, 4);
        $string = trim(substr($string, 4));

        $stations = explode(" - ", $string);
        return [$number, $stations[0], $stations[1]];
    }

}