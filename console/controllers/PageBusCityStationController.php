<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 23.09.2018
 * Time: 15:43
 */

namespace console\controllers;


use common\models\BusStations;
use common\models\City;
use common\models\PageBusCityStation;
use yii\console\Controller;
use yii\helpers\Console;

class PageBusCityStationController extends Controller
{
    protected function getStationsWithoutPages()
    {
        return BusStations::find()
            ->Where('city_id IS NOT NULL')
            ->andWhere('type = \'bus_station\'')
            ->leftJoin('page_bus_city_station', 'page_bus_city_station.id = bus_stations.id')
            ->andwhere('page_bus_city_station.station_id IS NULL')
            ->all();
    }

    public function actionCreateNewRecords()
    {
        $stations = $this->getStationsWithoutPages();
        echo 'found ' . count($stations) . " objects\n";
            foreach ($stations as $station) {
                $row = new PageBusCityStation();
                $row->station_id = $station->id;
                $row->title = '';
                $row->description = '';
                $row->random_words = '';
                $row->save();
                echo $station->id . "\n";
            }
    }

    public function actionSeoText()
    {
        $v = array(
            array('Здесь', 'На сайте',),
            array('происходит', 'осуществляется',),
            array('пользователей', 'юзеров',),
            array('рейсов', 'движения',),
            array('быть', 'являться',),
            array('обратитесь', 'напишите', 'сообщите',),
            array('помогает', 'позволяет',),
            array('находить', 'подбирать', 'искать',),
            array('недорогие', 'дешевые', 'выгодные'),
            array('дешевое', 'недорогое', 'классное'),
            array('экономное', 'комплексное',)
        );

        $all = PageBusCityStation::find()->where(['random_words' => ''])->all();
        $count = count($all);

        while ($count > 0) {

            foreach ($v[0] as $k0 => $v0) {
                foreach ($v[1] as $k1 => $v1) {
                    foreach ($v[2] as $k2 => $v2) {
                        foreach ($v[3] as $k3 => $v3) {
                            foreach ($v[4] as $k4 => $v4) {
                                foreach ($v[5] as $k5 => $v5) {
                                    foreach ($v[6] as $k6 => $v6) {
                                        foreach ($v[7] as $k7 => $v7) {
                                            foreach($v[8] as $k8 => $v8){
                                                foreach($v[9] as $k9 => $v9) {
                                                    foreach($v[10] as $k10 => $v10) {
                                                        $page = $all[$count - 1];
                                                        $word = [$v[0][$k0], $v[1][$k1], $v[2][$k2], $v[3][$k3], $v[4][$k4], $v[5][$k5], $v[6][$k6], $v[7][$k7], $v[8][$k8], $v[9][$k9], $v[10][$k10]];
                                                        $text = implode(";", $word);
                                                        $page->random_words = $text;
                                                        $page->update();
                                                        $count = $count - 1;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



    }

    public function actionDescription() {
        $pages = PageBusCityStation::find()->where(['description' => ''])->all();
        $count = count($pages);
        echo 'found ' . $count . ' objects';
        foreach($pages as $page){
            $station  = BusStations::findOne(['id' => $page->station_id]);
            $city = City::findOne(['id' => $station->city_id]);
            $page->description = " Расписание на автобус в {$city->city}({$city->region}), можно смотреть онлайн. Купить".
             "билет можно онлайн на {$station->name}, справочная информация. Актуальное расписание на рейсы, график на 2018 год.";
            $page->update();
            echo $count-- . "\n";
        }

    }

    public function actionTitle()
    {
        $pages = PageBusCityStation::find()->where(['title' => ''])->all();
        $count = count($pages);
        echo 'found ' . $count . ' objects';
        foreach($pages as $page){
            $station = BusStations::findOne(['id' => $page->station_id]);
            $city = City::findOne(['id' => $station->city_id]);
            $page->title = " Расписание автобусов: {$city->city}, {$station->name} - График на 2018";
            $page->update();
            echo $count-- . "\n";
        }
    }
}