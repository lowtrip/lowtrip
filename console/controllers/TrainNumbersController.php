<?php

namespace console\controllers;

use common\models\TrainUid;
use Yii;
use yii\db\Exception;
use yii\helpers\Console;
use yii\console\Controller;
use yii\db\Query;

use common\models\TrainNumbers;
use common\models\TrainNumbersExtData;
use common\models\TrainTimetable;
use common\models\TrainStations;

class TrainNumbersController extends Controller
{
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    public function actionUpdateExtData()
    {
        $trains = TrainNumbers::findTrainsWithoutExtData();
        echo 'Найдено '.count($trains)." поездов без дополнительной информации\n";
        foreach($trains as $train)
        {
            echo "Поезд $train->id\n";
            $uids = TrainUid::findTrainUids($train->id);
            echo count($uids)." расписаний\n";

            $timetables_array = [];
            $min_duration = null;
            $max_duration = null;
            $departure_id = null;
            $arrival_id   = null;

            if (!empty($uids)) {

                foreach ($uids as $uid)
                {
                    $timetables_array[] = TrainTimetable::findByUid($uid->uid);
                }
            }

            if (!empty($timetables_array)) {
                foreach ($timetables_array as $timetable)
                {
                    $count = count($timetable);

                    $last_row = $timetable[$count-1];
                    $duration = $last_row['duration'];

                    if($duration <= $min_duration || $min_duration === null) {
                        $min_duration = $duration;
                    }
                    if($duration >= $max_duration || $max_duration === null) {
                        $max_duration = $duration;
                    }

                    $departure_id = $timetable[0]['station_id'];
                    $arrival_id = $last_row['station_id'];
                }

                if (isset($min_duration, $max_duration, $arrival_id, $departure_id)) {
                    $trainExtData = new TrainNumbersExtData();
                    $trainExtData->train_id = $train->id;
                    $trainExtData->departure_id = $departure_id;
                    $trainExtData->arrival_id = $arrival_id;
                    $trainExtData->duration_min = $min_duration;
                    $trainExtData->duration_max = $max_duration;
                    $trainExtData->save();
                }

            }

        }
    }


}