<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 12.09.2018
 * Time: 0:31
 */

namespace console\controllers;


use common\classes\api\partners\BusforApi;
use common\helpers\ComparatorHelper;
use common\models\BusBusforCities;
use common\models\City;
use yii\console\Controller;
use yii\helpers\Console;

class BusforController extends Controller
{
    public function actionAdd(){
        $bus = new BusforApi();
        $bus->getCities("933044A0E9E3ED96E040A8C01E022741");
        foreach($bus->response->CITY as $city_name){
            $full_name = explode(',', $city_name->NAME_FULL);
            $subject = explode(' ', $full_name[count($full_name)-1]);
            var_dump($city_name->NAME);
            $cities = City::find()->where(['city' => $city_name->NAME])->all();
            if(count($cities) == 0){
                $new_city = new City();
                $new_city->city = (string)$city_name->NAME;
                $new_city->region = $subject[1] . ' ' . $subject[2];
                $new_city->country = 'Россия';
                $new_city->population = 500;
                $new_city->save();
                $busfor = new BusBusforCities();
                $busfor->lowtrip_city_id = $new_city->id;
                $busfor->busfor_city_id = (string)$city_name->ID;
                $busfor->save();
                echo "add new city : {$new_city->city}, {$new_city->region}\n";
            }else {
                $is_exist = false;
                echo 'count cities = '.count($cities)."\n";
                foreach ($cities as $city) {
                    $region = str_replace('г.', '', $city->region);
                    $region = explode(' ', $region);
                    if(ComparatorHelper::compare($region[0], 'республика') == 1 || ComparatorHelper::compare($region[0], 'Республика') == 1){
                        $region = $region[1];
                    } else {
                        $region = $region[0];
                    }

                    if(count($subject) == 3){
                        $reg = $subject[1];
                    } else {
                        $reg = $subject[0];
                    }
                    $compare = ComparatorHelper::compare($region, $reg);
                    echo "compare = $compare\n";
                    if($compare < 0.5) {
                        $is_exist = $is_exist || false;
                    } else {
                        $is_exist = $is_exist || true;
                        $busfor = BusBusforCities::find()->where(['lowtrip_city_id' => $city->id])->one();
                        if (is_null($busfor)) {
                            $busfor = new BusBusforCities();
                            $busfor->lowtrip_city_id = $city->id;
                            $busfor->busfor_city_id = (string)$city_name->ID;
                            $busfor->save();
                            echo "city name : {$city->city}, {$city->region} | lowtrip id : {$busfor->lowtrip_city_id}, busfor id : {$busfor->busfor_city_id}\n";
                            break;
                        } else {
                            echo "already exist lw = {$busfor->lowtrip_city_id}, bf= {$busfor->busfor_city_id}\n";
                        }
                    }
                }
                if(!$is_exist){
                    $new_city = new City();
                    $new_city->city = (string)$city_name->NAME;
                    $new_city->region = $subject[1] . ' ' . $subject[2];
                    $new_city->country = 'Россия';
                    $new_city->population = 500;
                    $new_city->save();
                    var_dump($new_city->id);
                    echo "add city : {$new_city->city}, {$new_city->region}\n";
                    $busfor = new BusBusforCities();
                    $busfor->lowtrip_city_id = $new_city->id;
                    $busfor->busfor_city_id = (string)$city_name->ID;
                    $busfor->save();
                    echo "lowtrip id : {$busfor->lowtrip_city_id}, busfor id : {$busfor->busfor_city_id}\n";
                }
            }
        }
    }
    public function actionDeleteAll(){
        $all = BusBusforCities::find()->all();
        foreach($all as $one){
            $one->delete();
        }
    }
    public function actionCount(){
        $all = BusBusforCities::find()->all();
        foreach ($all as $item) {
            $city = City::find()->where(['id' => $item->lowtrip_city_id])->one();
            echo "{$city->city}, {$city->region}\n";
        }
    }
}