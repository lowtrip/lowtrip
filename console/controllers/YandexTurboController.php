<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\City;
use common\models\TrainPopularRoutes;
use common\models\TrainPopularCities;
use common\models\PageTrainTypeCity;

use yii\helpers\Url;

use DOMDocument;

class YandexTurboController extends Controller
{
    public function getContentTrainMain()
    {
        $popular_routes = TrainPopularRoutes::getPopularRoutesWithPrices(10);
        $popular_cities = TrainPopularCities::getPopularCities(10);

        $content = $this->renderPartial('@frontend/views/amp/train/main.php', [
            'popular_routes' => $popular_routes,
            'popular_cities' => $popular_cities,
        ]);

        return $content;
    }

    public function getContentTrainCity($cityId)
    {
        $first = City::findCityById($cityId);

        $page = PageTrainTypeCity::findPage($first->id);

        $popular_from = $first->findPopularRoutesFromWithMinPrice();
        $popular_to   = $first->findPopularRoutesToWithMinPrice();

        $topCitiesCountry = $first->topCitiesCountry('rossiya', 5);

        $content = $this->renderPartial('@frontend/views/amp/train/from', [
            'first' => $first,
            'page' => $page,
            'topCities' => $topCitiesCountry,
            'popular_from' => $popular_from,
            'popular_to' => $popular_to
        ]);

        return $content;
    }

    public function actionTrainMain()
    {
        $xml = $this->createXML();

        $channel = $this->createXMLElement($xml, 'channel');
        $rss = $this->createRSS($xml);

        $turboContent = $this->createTurboContent($xml, $this->getContentTrainMain());
        $link = $this->createXMLElement($xml, 'link', false, 'https://lowtrip.ru/train');
        $title = $this->createXMLElement($xml, 'title', false, 'РЖД официальный сайт купить билеты');
        $description = $this->createXMLElement($xml, 'description', false, 'Подбор недорогих билетов на РЖД, ищем акции и скидки, проверяем наличие мест в поезде. Официальный партнер РЖД. Можно купить билеты на РЖД со скидкой');

        $item = $this->createItem($xml);
        $item->appendChild($link);
        $item->appendChild($title);
        $item->appendChild($description);
        $item->appendChild($turboContent);

        $channel->appendChild($item);

        $rss->appendChild($channel);
        $xml->appendChild($rss);

        $xml->save($this->getFilePath());
    }

    public function actionTrainCities()
    {
        $xml = $this->loadXML($this->getFilePath());

        if (!$xml) {
            echo "shit happens";
            return;
        }

        $channelNodes = $xml->getElementsByTagName('channel');
        $channel = $channelNodes[0];

        $popularCitiesIds = TrainPopularCities::find()
            ->select('city_id')
            ->orderBy(['counter' => SORT_DESC])
            ->asArray()
            ->all();

        $getCityId = function($row) {
            return $row['city_id'];
        };

        $cityIds = array_map($getCityId, $popularCitiesIds);

        foreach($cityIds as $cityId) {
            $city = City::findCityById($cityId);
            $url =  Url::toRoute(['train/city', 'from' => $city->city_slug]);
            $page = PageTrainTypeCity::findPage($cityId);

            $turboContent = $this->createTurboContent($xml, $this->getContentTrainCity($cityId));
            $link = $this->createXMLElement($xml, 'link', false, $url);
            $title = $this->createXMLElement($xml, 'title', false, $page->title);
            $description = $this->createXMLElement($xml, 'description', false, $page->description);

            $item = $this->createItem($xml);
            $item->appendChild($link);
            $item->appendChild($title);
            $item->appendChild($description);
            $item->appendChild($turboContent);

            $channel->appendChild($item);
        }

        $xml->save($this->getFilePath());
    }

    protected function getFilePath() {
        $filename = 'train-turbo.rss';
        $basePath = Yii::getAlias( '@frontend' ).'/web/';

        return $basePath.$filename;
    }

    protected function createXML()
    {
        $xml = new DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;

        return $xml;
    }

    protected function loadXML($filepath) {
        $xml = new DOMDocument();
        $xml->load($filepath);

        return $xml;
    }

    protected function createXMLAttribute($xml, $name, $value) {
        $attribute = $xml->createAttribute($name);
        $attribute->value = $value;

        return $attrubute;
    }

    protected function createRSS($xml)
    {
        return $this->createXMLElement($xml, 'rss', [
            'xmlns:yandex' => 'http://news.yandex.ru',
            'xmlns:media' => 'http://search.yahoo.com/mrss/',
            'xmlns:turbo' => 'ttp://turbo.yandex.ru',
            'version' => '2.0',
        ]);
    }

    protected function createItem($xml, $pubDate = false)
    {
        $item = $this->createXMLElement($xml, 'item', [
            'turbo' => 'true',
        ]);

        if (!empty($pubDate)) {
            $item->appendChild($this->createXMLElement($xml, 'pubDate', false, $pubDate));
        }

        return $item;
    }

    protected function createTurboContent($xml, $content)
    {
        $cdata = $xml->createCDATASection($content);
        $turboContent = $this->createXMLElement($xml, 'turbo:content', false, $cdata);

        return $turboContent;
    }

    protected function createXMLElement($xml, $name = '', $attributes = [], $content = false)
    {
        $element = $xml->createElement($name);

        if (!empty($attributes)) {
            foreach($attributes as $key => $value) {
                $attribute = $xml->createAttribute($key);
                $attribute->value = $value;
                $element->appendChild($attribute);
            }
        }

        if (!empty($content)) {
            $node = is_string($content) ? $xml->createTextNode($content) : $content;
            $element->appendChild($node);
        }

        return $element;
    }
}