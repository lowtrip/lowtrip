<?php

//namespace app\commands;
namespace console\controllers;

use common\models\CityStations;
use yii\console\Controller;

use common\models\City;
use common\models\PageTrainTypeRoute;
use common\models\TrainRoutes;

class PageTrainTypeRouteController extends Controller
{

    protected function getCities()
    {
        $cities = City::find()
            ->innerJoinWith('stations b')
            ->where('b.stations_id <> 0')
            ->andWhere(['b.has_stations' => 1])
            ->all();
        return $cities;
    }

    // Получить все записи CityStations со станциями
    protected function getCitiesWithoutMark()
    {
        return CityStations::find()
            ->where('stations_id <> 0')
            ->andWhere(['has_stations' => 0])
            ->all();
    }

    protected function initCityRoutes($city_id)
    {
        $cities = $this->getCities();

        $new_city = City::find()->where(['id' => $city_id])->one();

        foreach ($cities as $city)
        {
            if ($city->id !== $new_city->id)
            {
                $row = new PageTrainTypeRoute();
                $row->first_id = $city->id;
                $row->second_id = $new_city->id;
                $row->save();

                $row = new TrainRoutes();
                $row->first_city_id = $city->id;
                $row->second_city_id = $new_city->id;
                $row->save();

                $row = new PageTrainTypeRoute();
                $row->first_id = $new_city->id;
                $row->second_id = $city->id;
                $row->save();

                $row = new TrainRoutes();
                $row->first_city_id = $new_city->id;
                $row->second_city_id = $city->id;
                $row->save();
            }
        }
    }

    // Сгенерировать новые страницы
    public function actionNewPages()
    {
        $newCities = $this->getCitiesWithoutMark();

        foreach ($newCities as $k => $newCity)
        {
            echo $newCity->city_id."started\n";

            $this->initCityRoutes($newCity->city_id);
            $newCity->has_stations = 1;
            $newCity->save();

            echo $newCity->city_id."completed\n";
        }
    }

    public function actionNewCityRoutesPages($city_id)
    {
        $this->initCityRoutes($city_id);
    }

    public function actionShowCities()
    {
        $cities = $this->getCities();
        foreach ($cities as $key => $city)
        {
            echo $key.": ".$city->id." ".$city->city."\n";
        }
    }

    public function actionBuildPages()
    {
        $cities = $this->getCities();

        foreach($cities as $c1 => $first)
        {
            foreach($cities as $c2 => $second)
            {
                if ($first->id !== $second->id)
                {
                    $row = new PageTrainTypeRoute();
                    $row->first_id = $first->id;
                    $row->second_id = $second->id;
                    $row->save();
                }
            }
        }
    }

    public function actionGenerateTitle($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['title' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('цена', 'стоимость', 'низкая цена', 'низкая стоимость',),
            array('купе, плацкарт', 'купе и плацкарт', 'купейные и плацкартные места', 'места в плацкарте и купе', 'плацкарт и купе'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    $seorecord  = $pages[$count-1];
                    $word = [$v[0][$k0],$v[1][$k1]];
                    $text = implode(";", $word);
                    $seorecord->title = $text;
                    //$seorecord->update();
                    if ($seorecord->update() === false) {
                        echo  $seorecord->id." update failed";
                        exit();
                    }
                    else {
                        echo $seorecord->id."title updated\n";
                    }
                    $count = $count - 1;
                }
            }
        }
    }

    public function actionGenerateDescription($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['description' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('Есть сортировка', 'Сортируем', 'Разделяем', 'Выводим', 'Показываем', 'Находим'),
            array('самый дешевый', 'самый недорогой', 'дешевый'),
            array('есть отзывы', 'отзывы', 'отзывы покупателей', 'живые отзывы', 'живые отзывы покупателей'),
            array('недорогие', 'дешевые', 'дешевый'),
            array('Рассказываем', 'Показываем', 'Отображаем', 'Выводим информацию'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4){
                                $seorecord  = $pages[$count-1];
                                $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2],$v[3][$k3],$v[4][$k4]];
                                $text = implode(";", $word);
                                $seorecord->description = $text;
                                if ($seorecord->update() === false) {
                                    echo  $seorecord->id." update failed";
                                    exit();
                                }
//                                else {
//                                    echo $seorecord->id." description updated\n";
//                                }
                                $count = $count - 1;
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionGenerateText1($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['text1' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('Наш сервис', 'Lowtrip', 'Сервис Lowtrip'),
            array('только', 'лишь'),
            array('варианты', 'билеты'),
            array('насыщены', 'интересны'),
            array('специальный', 'уникальный'),
            array('питание', 'обед'),
            array('замечания', 'идеи'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4)
                            {
                                foreach ($v[5] as $k5 => $v5)
                                {
                                    foreach ($v[6] as $k6 => $v6)
                                    {
                                        $seorecord  = $pages[$count-1];
                                        $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2],$v[3][$k3],$v[4][$k4],$v[5][$k5],$v[6][$k6]];
                                        $text = implode(";", $word);
                                        $seorecord->text1 = $text;
                                        if ($seorecord->update() === false) {
                                            echo  $seorecord->id." update failed";
                                            exit();
                                        }
//                                        else {
//                                            echo $seorecord->id." text1 updated\n";
//                                        }
                                        $count = $count - 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionGenerateText2($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['text2' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('выгоднее', 'лучше', 'дешевле'),
            array('старте', 'начале'),
            array('стоимость', 'цена'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        $seorecord  = $pages[$count-1];
                        $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2]];
                        $text = implode(";", $word);
                        $seorecord->text2 = $text;
                        if ($seorecord->update() === false) {
                            echo  $seorecord->id." update failed";
                            exit();
                        }
//                        else {
//                            echo $seorecord->id." text2 updated\n";
//                        }
                        $count = $count - 1;
                    }
                }
            }
        }
    }

    public function actionGenerateText3($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['text3' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('маршруту', 'направлению'),
            array('выводится', 'показывается', 'отображается'),
            array('посмотреть', 'увидеть'),
            array('поменяйте', 'измените'),
            array('находится', 'располагается'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4)
                            {
                                $seorecord  = $pages[$count-1];
                                $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2],$v[3][$k3],$v[4][$k4]];
                                $text = implode(";", $word);
                                $seorecord->text3 = $text;
                                if ($seorecord->update() === false) {
                                    echo  $seorecord->id." update failed";
                                    exit();
                                }
//                                else {
//                                    echo $seorecord->id." text3 updated\n";
//                                }
                                $count = $count - 1;
                            }
                        }
                    }
                }
            }
        }
    }

    public function actionGenerateText4($limit = 0)
    {
        $pages = PageTrainTypeRoute::find()
            ->where(['text4' => ''])
            ->limit($limit)
            ->all();

        $count = count($pages);

        $v = array(
            array('подбирает', 'составляет'),
            array('недорогие', 'дешевые', 'отображается'),
            array('ищем', 'находим', 'показываем'),
            array('жилье', 'проживание'),
            array('гиды', 'аудиогиды'),
        );

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4)
                            {
                                $seorecord  = $pages[$count-1];
                                $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2],$v[3][$k3],$v[4][$k4]];
                                $text = implode(";", $word);
                                $seorecord->text4 = $text;
                                if ($seorecord->update() === false) {
                                    echo  $seorecord->id." update failed";
                                    exit();
                                }
//                                else {
//                                    echo $seorecord->id." text4 updated\n";
//                                }
                                $count = $count - 1;
                            }
                        }
                    }
                }
            }
        }
    }
}