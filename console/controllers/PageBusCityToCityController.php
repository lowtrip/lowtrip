<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 24.09.2018
 * Time: 22:03
 */

namespace console\controllers;


use common\models\PageBusCityToCity;
use yii\console\Controller;
use common\models\City;

class PageBusCityToCityController extends Controller
{
    protected function getCitiesWithoutPages()
    {
        return City::find()
            ->leftJoin('page_bus_city_to_city', 'page_bus_city_to_city.from_id = city.id')
            ->where('page_bus_city_to_city.from_id IS NULL')
            ->all();
    }

    public function actionCreateNewRecords()
    {
        $cities = $this->getCitiesWithoutPages();

        foreach ($cities as $city) {
            $row = new PageBusCityToCity();
            $row->from_id = $city->id;
            $row->save();
            echo $city->id."\n";
        }
    }

    public function actionMainText()
    {
        $v = array(
            array('находить', 'подбирать', 'искать',),
            array('недорогие', 'выгодные',),
            array('площадках', 'сервисах', 'сайтах',),
            array('добраться', 'доехать',),
            array('авто', 'машин', 'автомобилей'),
            array('идет', 'осуществляется', 'происходит',),
            array('находить', 'искать',),
            array('недорогих', 'дешевых',),
        );

        $all = PageBusCityToCity::find()->where('main_text IS NULL')->all();
        $count = count($all);

        while ($count > 0) {

            foreach ($v[0] as $k0 => $v0) {
                foreach ($v[1] as $k1 => $v1) {
                    foreach ($v[2] as $k2 => $v2) {
                        foreach ($v[3] as $k3 => $v3) {
                            foreach ($v[4] as $k4 => $v4) {
                                foreach ($v[5] as $k5 => $v5) {
                                    foreach ($v[6] as $k6 => $v6) {
                                        foreach ($v[7] as $k7 => $v7) {
                                            $page  = $all[$count-1];
                                            $word = [$v[0][$k0], $v[1][$k1], $v[2][$k2], $v[3][$k3], $v[4][$k4], $v[5][$k5], $v[6][$k6], $v[7][$k7]];
                                            $text = implode(";", $word);
                                            $page->main_text = $text;
                                            $page->update();
                                            $count = $count - 1;
                                            echo $count . "\n";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



    }

    public function actionTips() {

        $v = array(
            array('поспать', 'выспаться'),
            array('упускать', 'упустить',),
            array('проверить', 'взять'),
        );

        $all = PageBusCityToCity::find()->where('tips IS NULL')->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        $page  = $all[$count-1];
                        $word = [$v[0][$k0], $v[1][$k1], $v[2][$k2]];
                        $text = implode(";", $word);
                        $page->tips = $text;
                        $page->update();
                        $count = $count - 1;
                        echo $count . "\n";
                    }
                }
            }
        }

    }

    public function actionAdvantages() {

        $v = array(
            array('ищем', 'находим', 'подбираем'),
            array('местах', 'сайтах', 'площадках'),
            array('сервисом', 'сайтом'),
            array('альтернативных', 'разных')
        );

        $all = PageBusCityToCity::find()->where('advantages IS NULL')->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach($v[3] as $k3 => $v3) {
                            $page = $all[$count - 1];
                            $word = [$v[0][$k0], $v[1][$k1], $v[2][$k2], $v[3][$k3]];
                            $text = implode(";", $word);
                            $page->advantages = $text;
                            $page->update();
                            $count = $count - 1;
                            echo $count . "\n";
                        }
                    }
                }
            }
        }
    }
}