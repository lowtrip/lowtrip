<?php

namespace console\controllers;

use yii\console\Controller;
use yii\helpers\Console;

use common\models\City;
use common\models\CityStations;

use common\classes\api\OnetwotripTrainApi;

class CityStationsController extends Controller
{
    public function actionFindStations()
    {
        $cities = City::find()
            ->innerJoinWith('stations b')
            ->where(['b.has_stations' => 0])
            ->andWhere(['b.stations_id' => 0])
            ->andWhere('b.city_id > 1872')
            ->all();

        $api = new OnetwotripTrainApi();

        foreach ($cities as $k => $city)
        {
            $result = $api->suggestStations($city->city);
            $result = json_decode($result);
            if (!empty($result->result)) {
                $variants = $result->result;
                foreach ($variants as $variant) {
                    if ($variant->name->ru == $city->city) {
                        echo $city->city_slug."\n";
                        echo var_dump($variant)."\n";
                        if (Console::confirm("Add to database this station id?\n")) {
                            $this->updateCityStation($city->id, $variant->id, false);
                            break;
                        }
                    }
                }
            }
        }

    }

    protected function getCitiesWithoutRecords()
    {
        return City::find()
            ->leftJoin('city_stations', 'city_stations.city_id = city.id')
            ->where('city_stations.has_stations IS NULL')
            ->all();
    }

    public function actionCreateNewRecords()
    {
        $cities = $this->getCitiesWithoutRecords();

        foreach ($cities as $city) {
            $row = new CityStations();
            $row->city_id = $city->id;
            $row->save();
            echo $city->id."\n";
        }
    }

    protected function updateCityStation($city_id, $stations_id, $has_stations = true)
    {
        $row = CityStations::find()
            ->where(['city_id' => $city_id])
            ->one();

        $row->stations_id = $stations_id;
        if ($has_stations) {
            $row->has_stations = 1;
        }
        $row->save();

        echo $city_id." updated\n";
    }
}