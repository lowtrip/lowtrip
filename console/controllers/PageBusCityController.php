<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 09.09.2018
 * Time: 14:01
 */

namespace console\controllers;


use common\models\City;
use common\models\PageBusCity;
use yii\console\Controller;

class PageBusCityController extends Controller
{
    protected function getCitiesWithoutPages()
    {
        return City::find()
            ->leftJoin('page_bus_city', 'page_bus_city.city_id = city.id')
            ->where('page_bus_city.city_id IS NULL')
            ->all();
    }

    public function actionCreateNewRecords()
    {
        $cities = $this->getCitiesWithoutPages();

        foreach ($cities as $city) {
            $row = new PageBusCity();
            $row->city_id = $city->id;
            $row->save();
            echo $city->id."\n";
        }
    }

    public function actionDeleteAll(){
        $records = PageBusCity::find()->all();

        foreach($records as $record){
            $id = $record->id;
            $record->delete();
            echo 'record '. $id . 'deleted'."\n";
        }
    }
    public function actionDescription() {

        $v = array(
            array('Наша команда', 'Lowtrip'),
            array('дешево', 'недорого', 'экономно'),
            array('подгружаем', 'находим', 'подбираем'),
            array('идеология', 'задача', 'миссия'),
            array('дешевые', 'выгодные', 'классные'),
        );

        $all = PageBusCity::find()->where('description IS NULL')->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4)
                            {
                                $seorecord  = $all[$count-1];

                                $city = City::find()->where(['id' => $seorecord->city_id])->one();

                                $description = $v[0][$k0] . ' помогает ' . $v[1][$k1] . ' путешествовать по всей России. Мы ' . $v[2][$k2] . ' все рейсы на наземном транспорте. Наша ' . $v[3][$k3] . ' - искать самые ' . $v[4][$k4] . ' маршруты. На странице ' . $city->city . ' можно найти доступный трансфер. ';
                                $seorecord->description = $description;
                                $seorecord->update();
                                if ($seorecord->update() === false) {
                                    echo  $seorecord->id." update failed";
                                    exit();
                                }
                                $count = $count - 1;
                            }
                        }
                    }
                }
            }
        }

    }

    public function actionTitle() {
        $v = array(
            array('автовокзалы', 'вокзалы' , 'станции')
        );

        $all = PageBusCity::find()->where('title IS NULL')->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                    $seorecord  = $all[$count-1];

                    $city = City::find()->where(['id' => $seorecord->city_id])->one();

                    $title = 'Все ' . $v[0][$k0] . ' и рейсы ' . $city->city;
                    $seorecord->title = $title;
                    $seorecord->update();
                    if ($seorecord->update() === false) {
                        echo  $seorecord->id." update failed";
                        exit();
                    }
                    $count = $count - 1;
            }
        }
    }
}