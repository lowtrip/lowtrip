<?php

namespace console\controllers;

use Yii;
use yii\db\Exception;
use yii\helpers\Console;
use yii\console\Controller;
use yii\db\Query;

use common\models\TrainNumbers;
use common\models\TrainStations;
use common\models\TrainUid;
use common\models\TrainCarriers;
use common\models\TrainCalendars;
use common\models\TrainSchedules;
use common\models\TrainTimetable;

use common\models\TrainList;
use common\models\TrainParse;

use common\models\City;
use common\models\CityStations;

use common\classes\api\YandexRaspApi;

class TrainParserController extends Controller
{
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    public function actionAddTrains()
    {
        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = 'trains-russia.csv';

        $list = $this->getTrains($path.$filename);

        foreach ($list as $key => $item)
        {
            $train = array(
                'code' => $item[0],
                'from_city' => $item[1],
                'to_city' => $item[2],
                'from_code' => $this->findCityCode($item[1]),
                'to_code' => $this->findCityCode($item[2])
            );

            $this->saveTrain($train);
        }
    }

    protected function getTrains($filename)
    {
        $list = [];

        if (($fp = fopen($filename, "r")) !== FALSE) {
            while (($data = fgetcsv($fp, 0, ",")) !== FALSE) {
                $list[] = $data;
            }
            fclose($fp);
            //print_r($list);
        }

        return $list;
    }

    protected function findCityCode($name)
    {
        $subquery = (new Query())
            ->select(['city.id'])
            ->from('city')
            ->where(['city.city' => $name])
            ->one();

        $query = (new Query())
            ->select(['yandex'])
            ->from('city_stations')
            ->where(['city_id' => $subquery])
            ->one();

        if (empty($query['yandex'])) {
            $query = (new Query())
                ->select(['yandex'])
                ->from('train_stations')
                ->where(['name' => $name])
                ->one();
        }

        return $query['yandex'];
    }

    protected function saveTrain($train)
    {
        $row = TrainList::find()
            ->where(['code' => $train['code']])
            ->one();

        if (empty($row)) {
            $row = new TrainList();
            $row->code = $train['code'];
            $row->from_city = $train['from_city'];
            $row->to_city = $train['to_city'];
            $row->from_code = $train['from_code'];
            $row->to_code = $train['to_code'];

            if ($row->save()) {
                print_r($train);
                echo "Saved\n\n";
            }
            else {
                print_r($row->errors);
            }
        }
    }

    public function actionPrepareUniqueRequest()
    {
        $query = (new Query())
            ->select('*')
            ->from(TrainList::tableName())
            ->groupBy(['from_code', 'to_code'])
            ->all();

        if (!empty($query)) {
            foreach ($query as $train)
            {
                $model = new TrainParse();
                $model->from_city = $train['from_city'];
                $model->to_city = $train['to_city'];
                $model->from_code = $train['from_code'];
                $model->to_code = $train['to_code'];

                if ($model->save()) {
                    echo "Saved\n\n";
                }
                else {
                    print_r($model->errors);
                }
            }
        }
    }



    // PARSING FROM YANDEX RASP

    // Берем направления для парсинга из заготовленной заранее таблицы
    public function actionParseFromTable()
    {
        $rows = TrainParse::find()
            ->where(['parsed' => 0])
            ->all();

        if (!empty($rows)) {
            foreach ($rows as $row)
            {
                $this->parseTrainsByRouteFromYandex($row->from_code, $row->to_code, 0);
                $this->setTrainParsed($row->id);
                echo "Отдыхаем 3 секунды\n";
                sleep(3);
            }
        }
    }

    protected function setTrainParsed($row_id)
    {
        $model = TrainParse::find()
            ->where(['id' => $row_id])
            ->one();
        $model->parsed = 1;
        $model->save();
    }


    public function actionParseRoutes($from_code, $to_code)
    {
        $this->parseTrainsByRouteFromYandex($from_code, $to_code, 0);
    }

    protected function parseTrainsByRouteFromYandex($from_code, $to_code, $offset)
    {
        echo "Парсинг поездов от $from_code до $to_code;\n";

        $api = new YandexRaspApi();
        $api->setOffset($offset);

        $json = $api->searchTrains($from_code, $to_code);

        $content = json_decode($json);

        //echo "Результат запроса к API Yandex Rasp:\n";
        //print_r($content);

        $total = $content->pagination->total;
        $limit = $api->getLimit();
        $offset = $api->getOffset();

        echo "Найдено $total результатов на все дни;\n";

        if (!empty($content->segments)) {
            $this->addTrainNumbers($content->segments);
        }

        if ($total > ($limit + $offset)) {
            echo "Осталось спарсить ".$total-$limit-$offset." результатов\n";
            $this->parseTrainsByRouteFromYandex($from_code, $to_code, $offset + $limit);
        }
    }

    protected function addTrainNumbers($trains)
    {
        echo "Найдено ".count($trains)." различных расписаний поездов\n";

        foreach ($trains as $train)
        {
            // Если нет поезда с таким номером, создаем
            $train_number_model = $this->checkTrainNumber($train->thread->number);

            if (empty($train_number_model)) {
                echo "Поезда с номером ".$train->thread->number." еще не существует, создаем;\n";
                $train_number_model = $this->createNewTrain($train);
                //print_r($train_number_model);
            }

            // Если нет уникального расписания поезда, создаем
            $train_uid = $this->checkTrainUid($train->thread->uid);

            if (empty($train_uid)) {
                echo "Расписания с номером ".$train->thread->uid." еще не существует, создаем;\n";
                $this->createTrainUidWithCalendar($train_number_model->id, $train);
            }

            // Если расписание Неделю не обновлялось, нужно обновить
            if (!empty($train_uid) && (time() - strtotime($train_uid->updated_at)) > 3600 * 24 * 7) {
                // Написать обновление
                $this->updateTrainUidWithCalendar($train_uid, $train);
            }
        }
    }

    protected function createNewTrain($train)
    {
        $model = new TrainNumbers();
        $model->train_number = $train->thread->number;
        $model->type = $train->thread->transport_type;
        $model->title = $train->thread->title;
        $model->carrier_code = $this->checkCarrierExisted($train->thread->carrier);

        if ($model->save()) {
            return $model;
        }
        return NULL;
    }

    // Создать расписание
    protected function createTrainUidWithCalendar($train_id, $train_data)
    {
        $model = $this->createTrainUid($train_id, $train_data->thread->uid);

        if (!empty($model)) {
            $this->createCalendarWithRelations($model->uid, $train_data->schedule);
        }
    }

    // Обновить расписание
    protected function updateTrainUidWithCalendar($train_uid, $train_data)
    {
        // Обновить календари по году месяцу
        // Апдейт расписания с новой датой последнего изменения
        // Запрос на получение маршрутного листа (наверно удалить старый лист, создать новый)
    }

    protected function createTrainUid($train_id, $yandex_uid)
    {
        echo "Создание для поезда id:$train_id, yandex_uid:$yandex_uid модели TrainUid;\n";

        $model = new TrainUid();
        $model->train_id = $train_id;
        $model->yandex_uid = $yandex_uid;

        if ($model->save()) {
            return $model;
        }
        else {
            echo "Ошибка при создании модели TrainUid:\n";
            print_r($model->errors);
        }
        return NULL;
    }

    protected function createCalendarWithRelations($uid, $schedules)
    {
        echo "Создание календарей для расписания $uid;\n";
        if (!empty($schedules)) {
            $calendar_ids = [];

            foreach ($schedules as $schedule)
            {
                $calendar_ids[] = $this->createCalendar($schedule);
            }

            if (!empty($calendar_ids)) {
                foreach ($calendar_ids as $calendar_id)
                {
                    echo "Для календаря $calendar_id создаем связь с расписанием $uid;\n";
                    $this->createUidCalendarRelation($uid, $calendar_id);
                }
            }
        }
        else {
            echo "Нет календарей!\n";
            print_r($schedules);
        }
    }

    protected function createCalendar($schedule)
    {
        echo "Создание календаря; ";
        $model = new TrainCalendars();
        $model->year = $schedule->year;
        $model->month = (int) $schedule->month;
        $model->days = json_encode($schedule->days);

        if ($model->save()) {
            echo "Календарь сохранен;\n";
            return $model->id;
        }
        else {
            print_r($model->errors);
            return NULL;
        }
    }

    protected function createUidCalendarRelation($uid, $calendar_id)
    {
        $model = new TrainSchedules();
        $model->train_uid = $uid;
        $model->calendar_id = $calendar_id;

        if ($model->save()) {
            return $model;
        }
        return NULL;
    }

    protected function checkTrainNumber($number)
    {
        $model = TrainNumbers::find()
            ->where(['train_number' => $number])
            ->one();

        return !empty($model) ? $model : NULL;
    }

    protected function checkTrainUid($yandex_uid)
    {
        $model = TrainUid::find()
            ->where(['yandex_uid' => $yandex_uid])
            ->one();

        return !empty($model) ? $model : NULL;
    }

    protected function checkCarrierExisted($carrier)
    {
        $model = TrainCarriers::find()
            ->where(['code' => $carrier->code])
            ->one();

        if (empty($model)) {
            $model = new TrainCarriers();
            $model->code = $carrier->code;
            $model->contacts = $carrier->contacts;
            $model->url = $carrier->url;
            $model->title = $carrier->title;
            $model->phone = $carrier->phone;
            $model->address = $carrier->address;
            $model->logo = $carrier->logo;
            $model->email = $carrier->email;

            $model->save();
        }

        return $carrier->code;
    }


    protected function findStationId($code)
    {
        $station = TrainStations::find()
            ->where(['yandex' => $code])
            ->one();

        return !empty($station) ? $station->id : NULL;
    }



    // Парсинг маршрутного листа

    public function actionParseTimetables()
    {
        $rows = TrainUid::find()
            ->where(['has_timetable' => 0])
            ->all();

        foreach ($rows as $row)
        {
            $this->parseTimetableForUid($row);
            echo "Отдыхаем 1 секунду;\n\n";
            sleep(1);
        }
    }

    protected function parseTimetableForUid($trainUidModel)
    {
        echo "Запрос на получение маршрутного листа для расписания $trainUidModel->uid;\n";

        $api = new YandexRaspApi();
        $content = $api->trainTimetable($trainUidModel->yandex_uid);

        if ($content !== FALSE) {

            $content = json_decode($content);

            $status = false;

            if (!empty($content->stops)) {
                echo "Ответ получен. Сохранение маршрутного листа для расписания $trainUidModel->uid;\n";
                $status = $this->addTimetables($trainUidModel->uid, $content->stops);
            }

            if ($status == true) {
                echo "Весь маршрутный лист сохранен; Отмечаем модель $trainUidModel->uid как пройденную;\n";
                $this->saveTrainUidModelParsed($trainUidModel->uid);
            }

        }
    }

    protected function addTimetables($uid, $stops)
    {
        $status = false;

        foreach($stops as $stop)
        {
            $status = $this->createTimetable($uid, $stop);
            if (!$status) {
                echo "Произошла ошибка при сохранении маршрутного листа для расписания $uid;\n";
            }
        }

        return $status;
    }

    protected function createTimetable($uid, $stop)
    {
        $model = new TrainTimetable();
        $model->train_uid = $uid;
        $model->station_id = $this->findStationId($stop->station->code);
        $model->departure = $stop->departure;
        $model->arrival = $stop->arrival;
        $model->stop_time = $stop->stop_time;
        $model->duration = $stop->duration;

        if ($model->save()) {
            return true;
        }
        else {
            print_r($model->errors);
            return false;
        }
    }

    protected function saveTrainUidModelParsed($uid)
    {
        $model = TrainUid::find()
            ->where(['uid' => $uid])
            ->one();
        $model->has_timetable = 1;
        $model->save();
    }

    public function actionResetEmptyUid()
    {
        $query = (new Query())
            ->select('train_uid')
            ->from(TrainTimetable::tableName())
            ->where('station_id IS NULL')
            ->groupBy('train_uid')
            ->all();

        $list = [];

        foreach($query as $row)
        {
            $list[] = $row['train_uid'];

            $trainUid = TrainUid::find()
                ->where(['uid' => $row['train_uid']])
                ->one();
            $trainUid->has_timetable = 0;
            $trainUid->save();
        }

        foreach ($query as $row)
        {
            TrainTimetable::deleteAll(['IN', 'train_uid', $list]);
        }
    }




}