<?php

namespace console\controllers;


use common\classes\api\partners\BusforApi;
use common\models\BusBusforCities;
use common\helpers\ComparatorHelper;
use common\models\BusStations;
use Yii;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Console;

use common\classes\api\YandexRaspApi;

use common\models\City;
use common\models\CityStations;
use common\models\TrainStations;

class YandexRaspController extends controller
{

    public function actionSaveStations()
    {
        $api = new YandexRaspApi();
        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = 'stations_list.json';

        $fp = fopen($path.$filename, "w+");

        $responce = $api->stationsList();

        fwrite($fp, $responce);
        fclose($fp);
        echo $path.$filename." write end\n";
    }

    public function actionCountryStations($country_code)
    {
        $country_name = $this->getCountryName($country_code);

        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = 'stations_list.json';

        $json = file_get_contents($path.$filename);

        $content = json_decode($json);

        foreach ($content->countries as $country)
        {
            //echo "$country->title\n";
            if ($country->title == $country_name)
            {
                echo "$country_name is finded\n";
                $country_file = $path.$country_code.'.json';
                $fp = fopen($country_file, "w+");
                $json_country = json_encode($country, JSON_UNESCAPED_UNICODE);
                fwrite($fp, $json_country);
                fclose($fp);
                echo "$country_file write end\n";
            }
        }
    }

    public function actionCountryTrains($country_code)
    {
        $country_name = $this->getCountryName($country_code);

        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = $country_code.'.json';

        $json = file_get_contents($path.$filename);

        $content = json_decode($json);

        $country = array(
            'country' => $content->title,
        );

        $regions = $content->regions;

        echo count($content)."\n";

        $new_regions = [];

        foreach ($regions as $region)
        {
            $new_settlements = [];

            foreach ($region->settlements as $settlement)
            {
                $settlement_train_stations = [];

                foreach ($settlement->stations as $station)
                {
                    if ($station->transport_type == 'train')
                    {
                        $settlement_train_stations[] =  $station;
                    }
                }

                if (!empty($settlement_train_stations)) {
                    $new_settlements[] = array(
                        'title' => $settlement->title,
                        'codes' => $settlement->codes,
                        'stations' => $settlement_train_stations
                    );
                }
            }

            if (!empty($new_settlements)) {
                $new_regions[] = array(
                    'title' => $region->title,
                    'codes' => $region->codes,
                    'settlements' => $new_settlements
                );
            }
        }

        if (!empty($new_regions)) {
            $country['regions'] = $new_regions;

            $new_file = $path.$country_code.'_train_stations.json';

            $fp = fopen($new_file, "w+");
            $content = json_encode($country, JSON_UNESCAPED_UNICODE);
            fwrite($fp, $content);
            fclose($fp);
            echo "$country_code train_stations.json write end\n";
        }
    }

    public function actionCountryBus($country_code)
    {
        $country_name = $this->getCountryName($country_code);

        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = $country_code.'.json';

        $json = file_get_contents($path.$filename);

        $content = json_decode($json);

        $country = array(
            'country' => $content->title,
        );

        $regions = $content->regions;

        echo count($content)."\n";

        $new_regions = [];

        foreach ($regions as $region)
        {
            $new_settlements = [];

            foreach ($region->settlements as $settlement)
            {
                $settlement_bus_stations = [];

                foreach ($settlement->stations as $station)
                {
                    if ($station->transport_type == 'bus')
                    {
                        $settlement_bus_stations[] =  $station;
                    }
                }

                if (!empty($settlement_bus_stations)) {
                    $new_settlements[] = array(
                        'title' => $settlement->title,
                        'codes' => $settlement->codes,
                        'stations' => $settlement_bus_stations
                    );
                }
            }

            if (!empty($new_settlements)) {
                $new_regions[] = array(
                    'title' => $region->title,
                    'codes' => $region->codes,
                    'settlements' => $new_settlements
                );
            }
        }

        if (!empty($new_regions)) {
            $country['regions'] = $new_regions;

            $new_file = $path.$country_code.'_bus_stations.json';

            $fp = fopen($new_file, "w+");
            $content = json_encode($country, JSON_UNESCAPED_UNICODE);
            fwrite($fp, $content);
            fclose($fp);
            echo "$country_code bus_stations.json write end\n";
        }
    }

    public function actionAddBusStations($country_code)
    {
        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = $country_code.'_bus_stations.json';
        $json = file_get_contents($path.$filename);

        $content = json_decode($json);

        $country = $content->country;

        echo "started script\n";

        $count = 0;

        foreach ($content->regions as $region)
        {
            echo "region: ".$region->title."\n";

            foreach ($region->settlements as $settlement)
            {
                echo "settlement: ".$settlement->title."\n";

                $city_id = NULL;
                if ($settlement->title != '') {
                    //$city_id = $this->findOrAddCity($settlement->title, $region->title, $country);
                    $city_id = $this->findCity($settlement->title, $region->title, $country);
                    //$this->addCityYandexCode($settlement->title, $region->title, $settlement->codes->yandex_code);
                    $cities = City::find()->where(['city' => $settlement->title])->all();
                    $city_id = null;
                    $find = false;
                    foreach($cities as $city){
                        $ya_region = str_replace(' край','', $region->title);
                        $ya_region = str_replace('Республика ', '', $ya_region);
                        $ya_region = str_replace(' область','', $ya_region);
                        $lowtrip_region = str_replace(' край', '', $city->region);
                        $lowtrip_region = str_replace(' область', '', $lowtrip_region);
                        $lowtrip_region = str_replace(' республика', '', $lowtrip_region);
                        $lowtrip_region = str_replace('Республика ', '', $lowtrip_region);
                        if(ComparatorHelper::compare($ya_region, $lowtrip_region) > 0.5 || strcasecmp($city->city, 'Москва') == 0){
                            $city_id = $city;
                            $find = true;
                            break;
                        }
                    }
                    if(!$find){
                        echo "Город не найден. Ya: {$settlement->title} reg: {$region->title}\n";
                        Console::confirm('');
                        continue;
                    }
                }

                foreach ($settlement->stations as $station)
                {
                    if ($region->title == "Санкт-Петербург и Ленинградская область") {
                        $region_title = "Ленинградская область";
                    }
                    elseif ($region->title == "Москва и Московская область") {
                        $region_title = "Московская область";
                    }
                    else {
                        $region_title = $region->title;
                    }

                    if(!is_null($city_id)) {
                        $row = new BusStations();

                        $row->city_id = $city_id->id;
                        $row->type = $station->station_type;
                        $row->name = $station->title;
                        $row->region = $region_title;
                        $row->country = $country;
                        $row->lat = $station->latitude;
                        $row->lng = $station->longitude;
                        $row->esr = $station->codes->esr_code;
                        $row->yandex = $station->codes->yandex_code;

                        if ($row->save()) {
                            echo $count . ": city_id:" . $city_id->id . " " . $settlement->title . " " . $region->title . " " . $country . " saved\n";
                        } else {
                            echo $count . "error\n";
                        }

                        $count++;
                    }
                }
            }
        }

    }


    public function actionAddStations($country_code)
    {
        $path = Yii::getAlias( '@frontend' ).'/web/templates/docs/';
        $filename = $country_code.'_train_stations.json';

        $json = file_get_contents($path.$filename);

        $content = json_decode($json);

        $country = $content->country;

        echo "started script\n";

        $count = 0;

        foreach ($content->regions as $region)
        {
            echo "region: ".$region->title."\n";

            foreach ($region->settlements as $settlement)
            {
                echo "settlement: ".$settlement->title."\n";

                $city_id = NULL;
                if ($settlement->title != '') {
                    $city_id = $this->findOrAddCity($settlement->title, $region->title, $country);
                    $this->addCityYandexCode($settlement->title, $region->title, $settlement->codes->yandex_code);
                }

                foreach ($settlement->stations as $station)
                {
                    if ($region->title == "Санкт-Петербург и Ленинградская область") {
                        $region_title = "Ленинградская область";
                    }
                    elseif ($region->title == "Москва и Московская область") {
                        $region_title = "Московская область";
                    }
                    else {
                        $region_title = $region->title;
                    }


                    $row = new TrainStations();

                    $row->city_id = $city_id;
                    $row->type = $station->station_type;
                    $row->name = $station->title;
                    $row->region = $region_title;
                    $row->country = $country;
                    $row->lat = $station->latitude;
                    $row->lng = $station->longitude;
                    $row->esr = $station->codes->esr_code;
                    $row->yandex = $station->codes->yandex_code;

                    if ($row->save()){
                        echo $count.": city_id:".$city_id." ".$settlement->title." ".$region->title." ".$country." saved\n";
                    }
                    else {
                        echo $count."error\n";
                    }

                    $count++;
                }
            }
        }

    }

    protected function findOrAddCity($city, $region, $country)
    {
        if ($region == "Санкт-Петербург и Ленинградская область") {
            $region = "Ленинградская область";
        }
        if ($region == "Москва и Московская область") {
            $region = "Московская область";
        }

        $search = $this->findCity($city, $region, $country);

        echo "record in DB: ".$search->id." ".$search->city." ".$search->region." ".$search->country."\n\n";

        if (!empty($search)) {
            return $search->id;
        }
        else {
            echo $city." ".$region." ".$country."\n";
            //if (Console::confirm("Add to database this city?\n")) {
            if(true){
                $row = new City();
                $row->city = $city;
                $row->region = $region;
                $row->country = $country;
                if ($row->save()) {
                    echo "saved\n\n";
                    return $row->id;
                }
                else {
                    echo "error with saving".var_dump($row->errors)."\n\n";
                }
            }
        }

        return NULL;
    }

    protected function findCity($city, $region, $country)
    {
        return City::find()
            ->where(['city' => $city])
            ->andWhere(['region' => $region])
            ->andWhere(['country' => $country])
            ->one();
    }

    protected function addCityYandexCode($title, $region, $code) {
        if ($region == "Санкт-Петербург и Ленинградская область") {
            $region = "Ленинградская область";
        }
        if ($region == "Москва и Московская область") {
            $region = "Московская область";
        }

        $city_station = CityStations::find()
            ->innerJoinWith('city', 'city.id = city_stations.city_id')
            ->where(['city.city' => $title])
            ->andWhere(['city.region' => $region])
            ->one();

        if (!empty($city_station) && empty($city_station->yandex)) {
            $city_station->yandex = $code;
            $city_station->save();
        }
    }
    public function beforeAction($action)
    {
        if (Console::isRunningOnWindows()) {
            shell_exec('chcp 65001');
        }
        return parent::beforeAction($action);
    }

    protected function getCountryName($code)
    {
        $countries = array(
            'aze' => 'Азербайджан',
            'arm' => 'Армения',
            'bel' => 'Беларусь',
            'ger' => 'Германия',
            'ita' => 'Италия',
            'kz' => 'Казахстан',
            'krg' => 'Киргизия',
            'mng' => 'Монголия',
            'pol' => 'Польша',
            'ru' => 'Россия',
            'ukr' => 'Украина',
            'fra' => 'Франция',
            'est' => 'Эстония',
            'fin' => 'Финляндия',
            'lva' => 'Латвия',
            'chn' => 'Китай',
            'kndr' => 'Северная Корея',
            'uz' => 'Узбекистан',
            'tad' => 'Таджикистан',
            'aus' => 'Австрия',
            'che' => 'Чехия',
            'abh' => 'Абхазия',
            'lit' => 'Литва',

            'mon' => 'Монако',
            'mld' => 'Молдова',
            'rum' => 'Румыния'
        );

        return $countries[$code];
    }
}