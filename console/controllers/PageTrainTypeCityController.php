<?php

namespace console\controllers;

use yii\console\Controller;

use common\models\City;
use common\models\PageTrainTypeCity;
use common\models\CityStations;

class PageTrainTypeCityController extends Controller
{

    protected function getCitiesWithoutPages()
    {
        return City::find()
            ->leftJoin('page_train_type_city', 'page_train_type_city.city_id = city.id')
            ->where('page_train_type_city.city_id IS NULL')
            ->all();
    }

    public function actionCreateNewRecords()
    {
        $cities = $this->getCitiesWithoutPages();

        foreach ($cities as $city) {
            $row = new PageTrainTypeCity();
            $row->city_id = $city->id;
            $row->save();
            echo $city->id."\n";
        }
    }

    public function actionSeoText()
    {
        $v = array(
            array('Сервис', 'Сервис Lowtrip', 'Lowtrip',),
            array('подбирать', 'находить', 'отыскивать', 'выбирать',),
            array('дешевые', 'недорогие', 'классные',),
            array('недорогие', 'дешевые',),
            array('оформления', 'покупки',),
            array('Цена', 'Стоимость',),
            array('Проверено', 'Проверено нами', 'Проверено лично',),
            array('боковушках', 'боковых местах',),
        );

        $all = PageTrainTypeCity::find()->where(['seotext' => ''])->all();
        $count = count($all);

        while ($count > 0) {

            foreach ($v[0] as $k0 => $v0) {
                foreach ($v[1] as $k1 => $v1) {
                    foreach ($v[2] as $k2 => $v2) {
                        foreach ($v[3] as $k3 => $v3) {
                            foreach ($v[4] as $k4 => $v4) {
                                foreach ($v[5] as $k5 => $v5) {
                                    foreach ($v[6] as $k6 => $v6) {
                                        foreach ($v[7] as $k7 => $v7) {
                                            $page  = $all[$count-1];
                                            $word = [$v[0][$k0], $v[1][$k1], $v[2][$k2], $v[3][$k3], $v[4][$k4], $v[5][$k5], $v[6][$k6], $v[7][$k7]];
                                            $text = implode(";", $word);
                                            $page->seotext = $text;
                                            $page->update();
                                            $count = $count - 1;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



    }

    public function actionDescription() {

        $v = array(
            array('умный', 'быстрый'),
            array('подбора', 'поиска', 'нахождения'),
            array('советы', 'советы по экономии'),
            array('Ищем', 'Находим', 'Подбираем'),
            array('ОАО “РЖД”', 'оф. сайте ОАО “РЖД”',),
        );

        $all = PageTrainTypeCity::find()->where(['description' => ''])->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    foreach ($v[2] as $k2 => $v2)
                    {
                        foreach ($v[3] as $k3 => $v3)
                        {
                            foreach ($v[4] as $k4 => $v4)
                            {
                                    $seorecord  = $all[$count-1];

                                    $city = City::find()->where(['id' => $seorecord->city_id])->one();

                                    $description = 'ЖД билеты из г. '.$city->city.' на РЖД. Актуальные цены на весь 2018 год. Удобный интерфейс сайта, '.$v[0][$k0].' алгоритм '.$v[1][$k1].' недорогих билетов, классные '.$v[2][$k2].' для путешественников. '.$v[3][$k3].' билеты на '.$v[4][$k4];
                                    $seorecord->description = $description;
                                    $seorecord->update();
                                    if ($seorecord->update() === false) {
                                        echo  $seorecord->id." update failed";
                                        exit();
                                    }
                                    $count = $count - 1;
                            }
                        }
                    }
                }
            }
        }

    }

    public function actionTitle() {

        $v = array(
            array('поездов', 'на поезд'),
            array('билеты', 'недорогие билеты', 'дешевые билеты'),
        );

        $all = PageTrainTypeCity::find()->where(['title' => ''])->all();
        $count = count($all);

        while ($count > 0) {
            echo "start while; remaining records:".$count."\n";

            foreach ($v[0] as $k0 => $v0)
            {
                foreach ($v[1] as $k1 => $v1)
                {
                    $seorecord  = $all[$count-1];

                    $city = City::find()->where(['id' => $seorecord->city_id])->one();

                    $title = 'ЖД билеты '.$city->city.' - Расписание '.$v[0][$k0].', '.$v[1][$k1].', цены на 2018-ый год';
                    $seorecord->title = $title;
                    $seorecord->update();
                    if ($seorecord->update() === false) {
                        echo  $seorecord->id." update failed";
                        exit();
                    }
                    $count = $count - 1;
                }
            }
        }
    }

}