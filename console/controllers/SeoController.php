<?php

//namespace app\commands;
namespace console\controllers;

use yii\console\Controller;

use common\models\City;
use common\models\Seo;

class SeoController extends Controller
{

    public function actionIndex()
    {
        $v = array(
            array('Думаете', 'Xотите', 'Решили',),
            array('Отлично', 'Правильно', 'Замечательно',),
            array('считаем', 'подтягиваем', 'подбираем',),
            array('доступные', 'возможные',),
            array('Сервис', 'Площадка', 'Сайт',),
            array('собирает', 'подбирает', 'находит',),
            array('тур', 'путешествие', 'поездку',),
            array('сегодня или завтра', 'ближайшие 2 дня', 'ближайшие дни',),
            array('по адресу', 'на почту', 'на емейл',),
            array('по адресу', 'на почту', 'на емейл',),
            array('Ну коль', 'А так как', 'Ну раз',),
            array('иногда', 'часто', 'периодически',),
            array('классные', 'крутые', 'клевые',),
            array('Им будет полезно', 'Друзьям будет полезно', 'Друзьям понадобится',),
        );

        $cities = City::find()->all();
        $cortage = [];

        foreach($cities as $c1 => $first)
        {
            foreach($cities as $c2 => $second)
            {
                if ($first->id !== $second->id)
                {
                    $cortage[] = array(
                        'first_id' =>$first->id,
                        'first_name' => $first->city,
                        'second_id' =>$second->id,
                        'second_name' => $second->city,
                    );
                }
            }
        }

        $iteration = 0;

        foreach ($v[0] as $k0 => $v0)
        {
            foreach ($v[1] as $k1 => $v1)
            {
                foreach ($v[2] as $k2 => $v2)
                {
                    foreach ($v[3] as $k3 => $v3)
                    {
                        foreach ($v[4] as $k4 => $v4)
                        {
                            foreach ($v[5] as $k5 => $v5)
                            {
                                foreach ($v[6] as $k6 => $v6)
                                {
                                    foreach ($v[7] as $k7 => $v7)
                                    {
                                        foreach ($v[8] as $k8 => $v8)
                                        {
                                            foreach ($v[9] as $k9 => $v9)
                                            {
                                                foreach ($v[10] as $k10 => $v10)
                                                {
                                                    foreach ($v[11] as $k11 => $v11)
                                                    {
                                                        foreach ($v[12] as $k12 => $v12)
                                                        {
                                                            foreach ($v[13] as $k13 => $v13)
                                                            {
                                                                $word = [$v[0][$k0],$v[1][$k1],$v[2][$k2],$v[3][$k3],$v[4][$k4],$v[5][$k5],$v[6][$k6],$v[7][$k7],$v[8][$k8],$v[9][$k9],$v[10][$k10],$v[11][$k11],$v[12][$k12],$v[13][$k13]];
                                                                //$text = $this->fillText($word, $cortage[$iteration]);
                                                                $text = implode(";", $word);
                                                                $seo = new Seo();
                                                                $seo->first_id = $cortage[$iteration]['first_id'];
                                                                $seo->second_id = $cortage[$iteration]['second_id'];
                                                                $seo->text = $text;
                                                                $seo->save();
                                                                echo $iteration." ".$cortage[$iteration]['first_id']."-".$cortage[$iteration]['second_id']."\n";

                                                                $iteration = $iteration + 1;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }



    }
}