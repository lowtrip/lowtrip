<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'statisticsTrain' => [
            'class' => 'common\components\StatisticsTrain'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => 'https://lowtrip.ru',
            'rules' => [
                '/' => 'site/index',
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'trip',
                    'routePrefix' => 'trip',
                    'rules'  => [
                        '<action:(search)>'   => 'search',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from:\S+>'          => 'city'
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'car',
                    'routePrefix' => 'car',
                    'rules'  => [
                        ''                    => 'main',
                        '<action:(search)>'   => 'search',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from:\S+>'          => 'city',
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'train',
                    'routePrefix' => 'train',
                    'rules'  => [
                        '' => 'main',
                        '<action:(search)>'                  => 'search',
                        '<action:(raspisanie)>'              => 'timetable-main',
                        '<action:(raspisanie)>/<number:\S+>' => 'timetable-train',
                        '<from:\S+>/<to:\S+>/<type:\S+>'     => 'build-type',
                        '<from:\S+>/<to:\S+>'                => 'build',
                        '<from:\S+>'                         => 'city',
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'svaha',
                    'routePrefix' => 'svaha',
                    'rules'  => [
                        '' => 'index',
                        '<action:(search)>'     => 'search',
                        '<action:(rating)>'     => 'rating',
                        '<from:\S+>/<to:\S+>'   => 'build',
                    ]
                ],
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'bus',
                    'routePrefix' => 'bus',
                    'rules' => [
                        '' => 'main',
                        '<action:(search)>' => 'search',
                        '<from_slug:\S+>/stations/<station_slug:\S+>' => 'station',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from_slug:\S+>' => 'city',
                    ]
                ],

                '<controller:(redirect)>/<action:(onetwotrip)>' => 'redirect/onetwotrip',
            ],
        ],
    ],
    'params' => $params,
];
