<?php

include 'services/onetwotrip.php';
include 'class/house.php';
include 'db.php';

if ( isset($_POST['city']) && isset($_POST['region']) && isset($_POST['date']) && isset($_POST['adults']) ) {

	$city = $_POST['city'];
	$region = $_POST['region'];
	$date_start = $_POST['date'];
	$date_end = date("Y-m-d", strtotime("$date_start +1 days"));
	$adults = $_POST['adults'];

	$house = new House($city, $region, $date_start, $date_end, $adults);
	$builder = new HouseBuilder($house);
	$builder->find();
}