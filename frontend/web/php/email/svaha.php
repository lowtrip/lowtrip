<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <style>
        body {margin: 0; padding: 0;}
    </style>
</head>
<body style="">
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#f5f5f5" style="">
    <tr>
        <td>
            <center>
                <table width="600" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="70">&nbsp;</td>
                        <td align="center" valign="top">
                            <table width="460" cellpadding="0" cellspacing="0" border="0" style="margin: auto;"> <!-- wrapper -->
                                <tr><!-- Header -->
                                    <td align="center" valign="center">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="">
                                            <tr>
                                                <td height="25" style="font-size:25px; line-height:25px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="center">
                                                    <a href="https://lowtrip.ru/svaha" target="_blank">
                                                        <img src="https://lowtrip.ru/img/logo/lowtrip/lowtrip.png" width="50" style="margin:0; padding:0; border:none; display:block;" border="0" class="img" alt="logo">
                                                    </a>
                                                </td>
                                                <td align="right" valign="center" style="color: #1b1b1b;font-size: 14px;font-weight: bold;line-height: 20px;font-family: 'Trebuchet MS', Helvetica, sans-serif;">
                                                    ЖД Сваха<br>
                                                    знакомства в поезде с помощью нейросети
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="35" style="font-size:35px; line-height:35px;">&nbsp;</td>
                                            </tr>
                                        </table>

                                    </td>
                                </tr><!-- /Header -->
                                <tr><!-- Main block -->
                                    <td align="left" valign="center">
                                        <div style="box-shadow: 0 5px 15px rgba(0, 0, 0, 0.1);border-radius: 5px;background-color: #ffffff;border-bottom: 5px solid; border-color: #a4433c;">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td colspan="3" height="51" style="font-size:51px; line-height:51px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="36px;">&nbsp;</td>
                                                    <td>
                                                        <div style="font-size: 30px;font-weight: bold;color: #1b1b1b;font-family: 'Trebuchet MS', Helvetica, sans-serif;">Спасибо за участие в проекте «Ж/Д Сваха» </div>
                                                        <div style="height: 30px;">&nbsp;</div>
                                                        <div style="font-size: 18px;color: #1b1b1b;font-family: 'Trebuchet MS', Helvetica, sans-serif;">
                                                            <p>Что нужно сделать после покупки билета или как встретиться с попутчиками? </p>
                                                            <p>
                                                                Скачиваем картинку (Прикреплена к письму)<br>
                                                                Идем в вагон ресторан в этот промежуток времени (Время: 12:00-13:00; 16:00-17:00; 20:00-21:00)<br>
                                                                Открываем скаченную картинку на телефоне и кладем включенный телефон на стол экраном вверх, чтобы другие лоутрипперы смогли понять что Вы ищите попутчика.<br>
                                                                Перед третьим шагом, проверь, может тебя уже кто-то ждет с включенным телефоном? Если да, просто садись и начинай разговор.
                                                            </p>
                                                            <p>
                                                                О чем говорить с лоутрипперами?
                                                            </p>
                                                            <p>
                                                                Лоутрипперы — такие же люди. У них те же проблемы и интересы что и у обычных людей, поэтому не стоит начинать разговор, например, с категорического императива в философии Канта.
                                                                Хотя…
                                                            </p>
                                                            <p>
                                                                Лучше скажите, привет, меня зовут (Ваше имя), я никогда так не знакомился, это мой первый раз и круто, ребята из Лоутрип придумали, да? Молодцы вообще. Дальше можете похвалить нас еще несколько раз, ну, а дальше все как по маслу пойдет.
                                                            </p>
                                                            <p>
                                                                Не бойся. Знакомиться с новыми людьми это очень приятно и весело. Удачной поездки. У тебя все получится!
                                                            </p>
                                                        </div>
                                                        <div style="height: 30px;">&nbsp;</div>
                                                        <div style="font-size: 14px;color: #1b1b1b;font-family: 'Trebuchet MS', Helvetica, sans-serif;">

                                                        </div>
                                                    </td>
                                                    <td width="63px;">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" height="60" style="font-size:60px; line-height:60px;">&nbsp;</td>
                                                </tr>
                                            </table>

                                        </div>
                                    </td>
                                </tr><!-- /Main block -->
                                <tr><!-- Social -->
                                    <td align="center" valign="center">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td colspan="7" align="center" valign="center" style="color: #1b1b1b;font-size: 16px;font-weight: bold;font-family: 'Trebuchet MS', Helvetica, sans-serif;">
                                                    Мы в социальных сетях
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" height="30" style="font-size:30px; line-height:30px;">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td width="166">&nbsp;</td>
                                                <td align="center" valign="center">
                                                    <a href="https://vk.com/low_trip" target="_blank" style="">
                                                        <img src="https://ledel.ru/email_img/vk.png" width="29" height="29" style="margin:0; padding:0; border:none; display:block;" border="0" alt="" />
                                                    </a>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                                <td align="center" valign="center">
                                                    <a href="https://t.me/rzdticket" target="_blank" style="">
                                                        <img src="https://lowtrip.ru/img/logo/telegram-logo.png" width="29" height="29" style="margin:0; padding:0; border:none; display:block;" border="0" alt="" />
                                                    </a>
                                                </td>
                                                <td width="20">&nbsp;</td>
                                                <td align="center" valign="center">
                                                    <a href="https://www.instagram.com/lowtrip.bro/" target="_blank" style="">
                                                        <img src="https://ledel.ru/email_img/instagram.png" width="30" height="30" style="margin:0; padding:0; border:none; display:block;" border="0" alt="" />
                                                    </a>
                                                </td>
                                                <td width="166">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" height="65" style="font-size:65px; line-height:65px;">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr><!-- /Social -->
                            </table><!-- /wrapper -->

                        </td>
                        <td width="70">&nbsp;</td>
                    </tr>
                </table>
            </center>
        </td>
    </tr>
</table>
</body>
</html>