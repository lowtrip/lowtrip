<?php
include "services/google_places.php";
include 'db.php';
include 'class/place.php';

if ( isset($_POST['lat']) && isset($_POST['lng']) && isset($_POST['type']) && isset($_POST['city_id']) ) {

	//$key = 'AIzaSyDDsCgHAikp3Q_B1ybx5JIDn-V2vRjg38E';
	$lat = $_POST['lat'];
	$lng = $_POST['lng'];
	$type = $_POST['type'];
	$city_id = $_POST['city_id'];



	$places = PlacesHandler::getSavedPlaces($city_id, $type);
	if ( !empty($places) ) {
		echo json_encode($places);
	}
	else {
		$api = new PlacesAPI();
		$json = $api->nearbySearch($lat, $lng, $type);
		$handler = new PlacesHandler($json, $city_id, $type);
		$handler->parse();
		$places = $handler->getSavedPlaces($city_id, $type);
		echo json_encode($places);
	}

}
?>