<?php
// Класс для обращения к API Onetwotrip
// Публичные методы класса
class OnetwotripAPI
{
	public $city;
	public $date_start;
	public $date_end;
	public $city_id;
	public $request_id;
	public $hotel_id;
	
	public function suggestRequest($city) 
	{
		$params = array(
			'query' => $city,
			'limit' => 10,
		);

		//$url = 'https://hapi.onetwotrip.com/api/suggestRequest';
		$url = 'http://partner.onetwotrip.com/wl_hapi/canon/api/suggestRequest';
		$url = $url. '?' . http_build_query($params);
		$resp_json = file_get_contents($url);

		return $resp_json;
	}

	public function searchRequest($city_id, $date_start, $date_end, $adults)
	{
		$params = array(
			'object_id' => $city_id,
			'object_type' => 'geo',
			'date_start' => $date_start,
			'date_end' => $date_end,
			'adults' => $adults,
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',
		);

		//$url = 'https://hapi.onetwotrip.com/api/searchRequest';
		$url = 'http://partner.onetwotrip.com/wl_hapi/canon/api/searchRequest';
		$url = $url. '?' . http_build_query($params);
		$resp_json = file_get_contents($url);

		return $resp_json;
	}

	public function searchPolling($request_id) 
	{
		$params = array(
			'request_id' => $request_id,
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',
		);

		//$url = 'https://hapi.onetwotrip.com/api/searchPolling';
		$url = 'http://partner.onetwotrip.com/wl_hapi/canon/api/searchPolling';
		$url = $url. '?' . http_build_query($params);
		$resp_json = file_get_contents($url);
		
		return $resp_json;
	}

	public function hotelRequest($hotel_id, $adults)
	{
		$params = array(
			'id' => $hotel_id,
            'adults' => $adults
		);

		//$url = 'https://hapi.onetwotrip.com/api/hotelRequest';
		$url = 'http://partner.onetwotrip.com/wl_hapi/canon/api/hotelRequest';
		$url = $url. '?' . http_build_query($params);
		$resp_json = file_get_contents($url);
		
		return $resp_json;
	}

	public function getTypes() 
	{
		$params = array(
			'currency' => 'RUB',
			'lang' => 'ru',
			'locale' => 'ru',

		);

		$url = 'https://hapi.onetwotrip.com/api/getTypes';
		$url = $url. '?' . http_build_query($params);
		$resp_json = file_get_contents($url);
		
		return $resp_json;
	}

}


class OnetwotripHandler extends OnetwotripAPI
{

	public function __construct($city, $region, $date_start, $date_end, $adults)
	{
		$this->city = $city;
		$this->region = $region;
		$this->date_start = $date_start;
		$this->date_end = $date_end;
        $this->adults = $adults;
		$this->city_id = null;
		$this->request_id = null;
		$this->status = null;
		$this->hotels_in_city = null;
		$this->hotels_with_price = null;
		$this->hotel_id = null;
		$this->hotel_price = null;
		$this->hotel_img = null;
		$this->hotel_description = null;
		$this->hotel_url = null;
        $this->lat = null;
        $this->lng = null;
		$this->error = null;
	}

	public function suggestRequestHandler($response)
	{
		$response = json_decode($response);
		$results = $response->result;
		if (!$results) {
			$this->error = 'There are no results; suggestRequest() response';
			return false;
		}
		else {
			$city_name = mb_strtolower($this->city);

			foreach ($results as $result) {
				if ( mb_strtolower($result->city_name) == $city_name ||  mb_strtolower($result->name) == $city_name ) {
					$city_id = $result->city_id;
					break;
				}
			}

			if (isset($city_id)) {
				$this->city_id = $city_id;
				return true;
			}
			else {
				$this->error = 'There are no this city; suggestRequest() response';
				return false;
			}
		}
	}

	public function searchRequestHandler($response)
	{
		$response = json_decode($response);
		if (isset($response->error)) {
			$this->error = $response->error->msg.'; searchRequest() response';
			return false;
		}
		else {
			$this->request_id = $response->result->request_id;
			$hotels = $response->result->hotels;

			if (!isset($hotels)) {
				$this->error = 'There are no hotels; searchRequest() response';
				return false;
			}
			else {
				$hotels_in_city = [];

				foreach ($hotels as $key=>$hotel) {
					if (mb_strtolower($hotel->translated_city) == mb_strtolower($this->city)) {
						$hotels_in_city[] = $hotel;
					}
				}

				if (!isset($hotels_in_city)) {
					$this->error = 'There are no hotels in the city; searchRequest() response';
					return false;
				}
				else {
					$this->hotels_in_city = json_encode($hotels_in_city);
					return true;
				}
			}
		}
	}

	public function searchPollingHandler($response)
	{
		$response = json_decode($response);
		if (isset($response->error)) {
			$this->error = $response->error->msg.'; searchPolling() response';
			return false;
		}
		else {
			$this->status = $response->result->status;
			$offers = $response->result->offers;

			// Delete this then
			// $this->searchPolling = $offers;

			if (!isset($offers)) {
				$this->error = 'There are no offers; searchPolling() response';
				return false;
			}
			else {
				$hotels_with_price = [];
				$hotels_in_city = json_decode($this->hotels_in_city);

				foreach ($offers as $offer) {

					foreach ($hotels_in_city as $key => $hotel) {
						if ( $hotel->id == $offer[0] ) {
							$hotel->price = $offer[2];
							$hotels_with_price[] = $hotel;
						}
					}
				}

				if (empty($hotels_with_price)) {
					$this->error = 'There are no hotels with price in this city; searchPolling() response';
					return false;
				}
				else {
					$this->hotels_with_price = json_encode($hotels_with_price);
					return true;
				}
			}
		}
	}

	public function sortByTypes()
	{
		$this->getTypesHandler();
		$types = $this->types;
		$hotels = json_decode($this->hotels_with_price);

		foreach ($hotels as $hotel) {

			if ( !in_array($hotel->type, [2, 13, 14]) ) {
				continue;
			}
			else {
				$type_name = $this->getTypeName($hotel->type, $types);
				$hotel->type_name = $type_name;
				switch ($type_name) {
					case "Гостиницы" :
						if ( !isset($group[$type_name.$hotel->stars]) ) {
							$group[$type_name.$hotel->stars] = $hotel;
						}
						else {
							if ( $hotel->price < $group[$type_name.$hotel->stars]->price ) {
								$group[$type_name.$hotel->stars] = $hotel;
							}
						}
						break;

					default :
						if ( !isset($group[$type_name]) ) {
							$group[$type_name] = $hotel;
						}
						else {
							if ( $hotel->price < $group[$type_name]->price ) {
								$group[$type_name] = $hotel;
							}
						}
				}
			}
		}

		//$this->groups = $group;
		$this->groupsToArray($group);
	}

	public function groupsToArray($groups)
	{
		$rich = [];
		$poor = [];
		$all = [];

		if ( !empty($groups) ) {
			foreach ($groups as $key => $group) {
				
				switch ($group->stars) {
					case "3" :
						$rich[] = $group;
						break;
					case "4" :
						$rich[] = $group;
						break;
					case "5" :
						$rich[] = $group;
						break;
					default:
						$poor[] = $group;
						break;
				}

				$all[] = $group;

			}
		}

		if ( empty($rich) ) {
			$this->rich = $this->findMax($all);
		}
		else {
			$this->rich = $this->findMin($rich);
		}

		$this->poor = $this->findMin($all);
		
		// $this->rich = $rich;
		// $this->poor = $poor;
		$this->groups = $all;
	}

	public function getTypeName($type_code, $types)
	{
		foreach ($types as $key => $name) {
			if ( $key == $type_code ) {
				return $name;
			}
		}
	}

	public function hotelRequestHandler($response)
	{
		$response = json_decode($response);
		if (isset($response->error)) {
			$this->error = $response->error->msg.'; hotelRequest() response';
			return false;
		}
		else {
			$hotel = $response->result->hotel;
			$this->hotel_img = $hotel->img;
			$this->hotel_description = $hotel->description;
			$this->lat = $hotel->lat;
            $this->lng = $hotel->lng;

            $this->hotel_url = $hotel->url.'&date_start='.$this->date_start.'&date_end='.$this->date_end.'&scp=60,affiliate,4368-14624-0-2';
			//$this->hotel_url = $hotel->url.'&adults='.$this->adults.'&date_start='.$this->date_start.'&date_end='.$this->date_end.'&marker=lowtrip&scp=60,affiliate,4368-14624-0-2';
			return true;
		}
	}

	public function getTypesHandler()
	{
		$response = json_decode($this->getTypes());
		if (isset($response->error)) {
			$this->error = $response->error->msg.'; getTypes() response';
			return false;
		}
		else {
			$types = $response->result;
			$this->types = $types;
			return true;
		}
	}

	public function sortOffers()
	{
		$offers = json_decode($this->hotels_with_price);
		if (!isset($offers)) {
			$this->error = 'There are no offers, nothing to sorting; sortOffers()';
			return false;
		}
		else {
			$best = $offers[0];
			foreach ($offers as $offer) {
				if ($offer->price < $best->price) {
					$best = $offer;
				}
			}
			$this->hotel_id = $best->id;
			$this->hotel_price = $best->price;
			return true;
		}
	}

	public function findMin($offers)
	{
		$min = null;

		if ( !empty($offers) ) {
			$min = $offers[0];
			foreach ($offers as $offer) {
				if ($offer->price < $min->price) {
					$min = $offer;
				}
			}
		}

		return $min;
	}

	public function findMax($offers)
	{
		$max = null;
		
		if ( !empty($offers) ) {
			$max = $offers[0];
			foreach ($offers as $offer) {
				if ($offer->price > $max->price) {
					$max = $offer;
				}
			}
		}

		return $max;
	}

}

class HouseBuilder
{
	public $house;
	public $handler;

	public function __construct($house)
	{
		$this->house = $house;
		$this->handler = $this->createHandler($this->house);
	}

	public function createHandler($house)
	{
		$handler = new OnetwotripHandler($house->city, $house->region, $house->date_start, $house->date_end, $house->adults);
		return $handler;
	}

	public function build()
	{
		$this->handler->city_id = $this->cityID($this->handler->city, $this->handler->region);

		if (!isset($this->handler->city_id)) {
			$suggestRequestResponse = $this->handler->suggestRequest($this->handler->city);
			$suggestRequestHandler = $this->handler->suggestRequestHandler($suggestRequestResponse);
		}

		if (isset($this->handler->city_id)) {
			$searchRequestResponse = $this->handler->searchRequest($this->handler->city_id, $this->handler->date_start, $this->handler->date_end, $this->handler->adults);
			$searchRequestHandler = $this->handler->searchRequestHandler($searchRequestResponse);

			if ($searchRequestHandler) {
				$searchPollingResponse = $this->handler->searchPolling($this->handler->request_id);
				$searchPollingHandler = $this->handler->searchPollingHandler($searchPollingResponse);

				if ($searchPollingHandler) {
					// $this->handler->sortByTypes();
					$sortOffers = $this->handler->sortOffers();
					$hotelRequestResponse = $this->handler->hotelRequest($this->handler->hotel_id, $this->handler->adults);
					$hotelRequestHandler = $this->handler->hotelRequestHandler($hotelRequestResponse);
				}
			}
		}
	}

	public function houseData()
	{
		$this->house->status = $this->handler->status;
		$this->house->city_id = $this->handler->city_id;
		$this->house->price = $this->handler->hotel_price;
		$this->house->hotel_id = $this->handler->hotel_id;
		$this->house->img = $this->handler->hotel_img;
		$this->house->url = $this->handler->hotel_url;
		$this->house->description = $this->handler->hotel_description;
        $this->house->lat = $this->handler->lat;
        $this->house->lng = $this->handler->lng;
		$this->house->error = $this->handler->error;

		// $this->house->groups = $this->handler->groups;
		// $this->house->rich = $this->handler->rich;
		// $this->house->poor = $this->handler->poor;
		
		// $this->house->types = $this->handler->types;
		// $this->house->hotels_in_city = json_decode($this->handler->hotels_in_city);
		// $this->house->hotels_with_price = json_decode($this->handler->hotels_with_price);
		// $this->house->searchPolling = $this->handler->searchPolling;
		// $this->house->request_id = $this->handler->request_id;

	}



	public function sendHouse()
	{
		$response = json_encode($this->house);
		echo $response;
	}

	public function sendGroups()
	{
		$response = json_encode($this->handler->groups);
		echo $response;
	}

	public function find()
	{
		$this->build();
		//$this->sendGroups();
		$this->houseData();
		$this->sendHouse();
	}

	public function cityID($city, $region)
	{
		global $db;

		$sql = 'SELECT `city_id` FROM `city` WHERE `city` = :city AND `region` = :region';
		$result = $db->prepare($sql);
		$result->execute(
			array(
				'city' => $city,
				'region' => $region
			)
		);
		$result = $result->fetch(PDO::FETCH_OBJ);
		return $result->city_id;
	}
}
?>