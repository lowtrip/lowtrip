<?php

class PlacesAPI
{

	public function __construct()
	{
		$this->key = 'AIzaSyDL9Y3TSB_MR_HoaJAPaNECUDC5Qc2zNvY';
	}

	public function nearbySearch($lat, $lng, $type)
	{
		$params = array(
			'location' => $lat.','.$lng,
			'radius' => 5000,
			'rankby' => 'prominence',
			'type' => $type,
			'language' => 'ru',
			'key' => $this->key
		);

		$url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?';
		$url = $url.http_build_query($params);
		$resp_json = file_get_contents($url);

		return $resp_json;
	}

	public function radarSearch($lat, $lng, $type)
	{
		$params = array(
			'location' => $lat.','.$lng,
			'radius' => 5000,
			'type' => $type,
			'language' => 'ru',
			'key' => $this->key
		);

		$url = 'https://maps.googleapis.com/maps/api/place/radarsearch/json?';
		$url = $url.http_build_query($params);
		$resp_json = file_get_contents($url);

		return $resp_json;
	}

	public function getPhoto($photoreference, $width)
	{
		$params = array(
			'photoreference' => $photoreference,
			'maxwidth' => $width,
			'key' => $this->key
		);

		$url = 'https://maps.googleapis.com/maps/api/place/photo?';
		$url = $url.http_build_query($params);
		$image_data = file_get_contents($url);
		return $image_data;
	}

	public function placeData()
	{
		$params = array(
			'location' => $place_id,
			'language' => 'ru',
			'key' => $this->key
		);

		$url = 'https://maps.googleapis.com/maps/api/place/details/json?';
		$url = $url.http_build_query($params);
		$resp_json = file_get_contents($url);

		return $resp_json;
	}
}



class PlacesHandler
{
	public function __construct($content, $city_id, $type)
	{
		$this->table = 'places';
		$this->content = $content;
		$this->city_id = $city_id;
		$this->type = $type;
	}

	public function parse()
	{
		$content = json_decode($this->content);
		$results = $content->results;

		$saved_places = $this->getSavedPlacesID($this->city_id);

		if ( isset($results) ) {
			foreach ($results as $result) {
				if ( !in_array($result->place_id, $saved_places) ) {
					if ( isset($result->photos[0]->photo_reference) ) {
						$place = new Place($result, $this->city_id, $this->type);
						$place->save();

						$ref = $result->photos[0]->photo_reference;
						$image_data = $this->getImage($ref, 320);
						$this->saveImage($place->place_id, $image_data);
					}
				}
			}
		}
	}

	public function getImage($ref, $width) {
		$api = new PlacesAPI();
		$image_data = $api->getPhoto($ref, $width);
		return $image_data;
	}

	public function saveImage($name, $data)
	{
		$name = '../images/places/'.$name.'.jpg';
		file_put_contents($name, $data);
	}

	public function getSavedPlacesID($city_id)
	{
		global $db;
		$data = [];

		$sql = 'SELECT `place_id` FROM `places` WHERE `city_id` = :city_id';
		$result = $db->prepare($sql);
		$result->execute(
			array(
				'city_id' => $city_id,
			)
		);
		while ( $row = $result->fetch(PDO::FETCH_OBJ) ) {
			$data[] = $row->place_id;
		}

		return $data;
	}

	public function getSavedPlaces($city_id, $type)
	{
		global $db;
		$data = [];

		$sql = 'SELECT * FROM `places` WHERE `city_id` = :city_id AND `type` = :type';
		$result = $db->prepare($sql);
		$result->execute(
			array(
				'city_id' => $city_id,
				'type' => $type
			)
		);
		while ( $row = $result->fetch(PDO::FETCH_OBJ) ) {
			$data[] = $row;
		}

		return $data;
	}

}

?>