<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

if( isset($_POST['email']) ) {

	$destination = $_POST['email'];

	$data = $_POST['data'];
	$data = json_decode($data);

	$type = $_POST['type'];

    switch ($type) {
        case 'car':
            $link = 'email/email2.php';
            break;
        case 'train':
            $link = 'email/train.php';
            break;
        default:
            $link = 'email/email2.php';
    }

	ob_start();
	include $link;
	$message = ob_get_contents();
	ob_end_clean();

	$email = new PHPMailer();

	$email->IsSMTP();
	$email->SMTPAuth = true;
	$email->SMTPSecure = "tls";
	$email->Host = "smtp.gmail.com";
	$email->Port = 587;

	// $email->SMTPDebug = 2;

	$email->Username = 'lowtripkzn@gmail.com';
	$email->Password = 'Qaz123456'; 

	$email->From      = 'lowtripkzn@gmail.com';
	$email->FromName  = 'Lowtrip';
	$email->Subject   = 'Путешествие в '.$data->second->city;
	$email->isHTML(true);
	$email->Body      = $message;
	$email->AddAddress( $destination );

	$email->CharSet = 'UTF-8';

	if(!$email->Send()) {
	   echo 'Mailer Error: ' . $email->ErrorInfo;
	   exit;
	}
	else {
		addEmail($destination);
		addTrip($destination, $data);
	}

}


// Добавление путешествия в базу
function addTrip($email, $data) {
	include "db.php";
	global $db;

	$sql = 'INSERT INTO `trips` (`email`, `trip_date`, `adults`, `departure_name`, `departure_id`, `arrival_name`, `arrival_id`, `car_link`, `house_link`, `car_price`, `house_price`, `total_price`) VALUES (:email,:trip_date,:adults,:departure_name,:departure_id,:arrival_name,:arrival_id,:car_link,:house_link,:car_price,:house_price,:total_price)';
	$res = $db->prepare($sql);
	$res->execute(
		array(
			'email' => 			$email,
			'trip_date' => 		$data->date,
			'adults' => 		$data->adults,
			'departure_name' => $data->first->city,
			'departure_id'	=>	$data->first->id,
			'arrival_name' =>	$data->second->city,
			'arrival_id' =>		$data->second->id,
			'car_link' =>		$data->car->link,
			'house_link' =>		$data->house->url,
			'car_price' =>		$data->car->trips[0]->price->value,
			'house_price' =>	$data->house->price,
			'total_price' =>	$data->price,
		)
	);
}


function addEmail($email) {
	include "db.php";
    global $db;

	$sql = 'SELECT `email` FROM `lowtrip_email` WHERE `email` = :email';
	$result = $db->prepare($sql);
	$result->execute(
		array(
			'email' => $email
		)
	);
	$row = $result->fetch(PDO::FETCH_OBJ);
	if ( empty($row->email) ) {
		$sql = 'INSERT INTO `lowtrip_email` (`email`) VALUES (:email)';
		$res = $db->prepare($sql);
		$res->execute(
			array(
				'email' => $email
			)
		);
	}
}
?>