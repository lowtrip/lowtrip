<?php

class Place
{
	public $place_id;
	public $city_id;
	public $name;
	public $vicinity;
	public $type;
	public $lat;
	public $lng;
	public $photo_reference;

	public function __construct($place, $city_id, $type)
	{
		$this->place_id = $place->place_id;
		$this->city_id = $city_id;
		$this->name = $place->name;
		$this->vicinity = $place->vicinity;
		$this->type = $type;
		$this->lat = $place->geometry->location->lat;
		$this->lng = $place->geometry->location->lng;
		$this->photo_reference = $place->photos[0]->photo_reference;
	}

	public function save()
	{
		global $db;
		$sql = 'INSERT INTO `places` (`place_id`, `city_id`, `name`, `vicinity`, `type`, `lat`, `lng`, `photo_reference`) VALUES (:place_id, :city_id, :name, :vicinity, :type, :lat, :lng, :photo_reference)';
		$result = $db->prepare($sql);
		$result->execute(
			array(
				'place_id' => $this->place_id,
				'city_id' => $this->city_id,
				'name' => $this->name,
				'vicinity' => $this->vicinity,
				'type' => $this->type,
				'lat' => $this->lat,
				'lng' => $this->lng,
				'photo_reference' => $this->photo_reference,
			)
		);
	}
}

?>