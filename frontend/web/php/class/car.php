<?php

class Car
{
	public $date_start;
	public $date_end;
	public $city_start;
	public $city_end;
	public $price;
	public $url;

	public function __construct($city_start, $city_end, $date_start, $date_end)
	{
		$this->city_start = $city_start;
		$this->city_end = $city_end;
		$this->date_start = $date_start;
		$this->date_end = $date_end;
		$this->price = null;
		$this->url = null;
	}
}

?>