<?php

class House
{
	public $city;
	public $city_id;
	public $region;
	public $date_start;
	public $date_end;
	public $adults;
	public $price;
	public $img;
	public $url;
	public $description;

	public function __construct($city, $region, $date_start, $date_end, $adults)
	{
		$this->city = $city;
		$this->city_id = $city_id;
		$this->region = $region;
		$this->date_start = $date_start;
		$this->date_end = $date_end;
		$this->adults = $adults;
		$this->price = null;
		$this->img = null;
		$this->url = null;
		$this->description = null;
	}

}