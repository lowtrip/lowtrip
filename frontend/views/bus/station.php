<?php
use common\widgets\Breadcrumbs;

$this->title = $page->title;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $page->description,
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $page->title,
]);

$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $page->description,
]);?>

<div class="bus-page">
    <!--Breadcrumbs-->
    <section class="bus-page__breadcrumbs breadcrumbs_block breadcrumbs-transparent">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'Билеты на автобус',
                        'url' => ['bus/main'],
                    ],
                    [
                        'label' => $city->city,
                        'url' => ['bus/city', 'from_slug' => $city->city_slug],
                    ],
                    [
                        'label' => $stationName,
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section class="bus-page__header">
        <div class="container">
            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-form.php', [
                'module' => 'bus/search',
                'style' => 'header__form bus-form bus-form--black',
                'first' => $city,
            ]); ?>
        </div>
    </section>

    <section class="bus-page__content">
        <div class="container">

            <h1 class="bus-page__title heading--h1"><?= $stationName ?>: Расписание автобусов</h1>

            <div class="station-timetable">
                <div class="station-timetable__head">
                    <div class="station-timetable__row">
                        <div class="station-timetable__col station-timetable__col--route">Маршрут</div>
                        <div class="station-timetable__col station-timetable__col--days">Дни</div>
                        <div class="station-timetable__col station-timetable__col--time">Отправление</div>
                    </div>
                </div>
                <div class="station-timetable__body">
                    <? if(!is_null($schedules)) :?>
                        <? foreach($schedules as $schedule) : ?>
                            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-schedule-item.php',['schedule' => $schedule]);?>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>
        </div>

        <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-add-route.php'); ?>
    </section>
</div>

<section class="site-block site-block--about-service">
    <div class="site-block__mask site-block__mask--grey">
        <div class="container">
            <div class="site-block__content site-block__content--about-service">
                Сервис Lowtrip помогает путешественникам находить недорогие билеты на автобус, ж/д,
                ищет попутные машины, дешевое жилье, бесплатные аудигиды, экономное питание в фаст-фуде.
            </div>
        </div>
    </div>
</section>