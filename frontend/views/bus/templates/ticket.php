<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 01.09.2018
 * Time: 19:30
 */
use yii\helpers\Html;
?>
<tr>
    <td class="departure-time">
        <div class="time">
            <?=Html::encode($trip->START_TIME[0]);?>
        </div>
        <div class="date">
            <?=Html::encode($trip->START_DATE[0]);?>
        </div>
        <div class="timezone">
            <?=Html::encode($trip->START_TIMEZONE[0]);?>
        </div>
        <div class="point">
            <?=Html::encode($trip->STATION_BEGIN_NAME[0])?> <a href="#" class="place-icon"><i
                    class="fa fa-map-marker"></i> </a>
        </div>
        <div class="underground-station">
            <!--<a href="#"><img src="/img/bus/metro_icon.png">Площадь Габдуллы Тукая</a>-->
            <a href="#"><?=Html::encode($trip->STATION_BEGIN_ADDRESS[0])?></a>
        </div>
        <div class="company">
            <?=Html::encode($trip->CARRIER_NAME[0])?>
        </div>
    </td>
    <td class="time-on-line">
        <img src="/img/bus/arrow.png">
        <div class="time">
            <?=Html::encode($trip->TIME_IN_ROAD[0]);?>
        </div>
        <div class="sub">время в пути</div>
    </td>
    <td class="arrival-time">
        <div class="time">
            <?=Html::encode($trip->END_TIME[0]);?>
            <span class="addiction">
                <?=Html::encode($trip->END_DATE[0]);?>
            </span></div>
        <div class="timezone"><?=Html::encode($trip->END_TIMEZONE[0])?> </div>
        <div class="point">
            Автовокзал <a href="#" class="place-icon"><i class="fa fa-map-marker"></i> </a>
        </div>
    </td>
    <td class="days">
        Ежедневно
    </td>
    <td class="ways">
        <a href="#" class="way">Показать <br>маршрут</a>
    </td>
    <td class="cost">
        <div class="amount">
            <?=Html::encode($trip->TARIFF->COST[0])?> руб.
        </div>
        <div class="buy-button">
            Купить билет
        </div>
        <div class="info">
            <a id="mytestid" class="info-link" >Показать информацию</a>
        </div>
    </td>
</tr>
