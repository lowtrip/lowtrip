<section class="site-block site-block--driver" style="margin: 40px 0 20px;">
    <div class="container">
        <div class="site-block__content site-block__content--driver">
            <img src="/img/car/icons/icon_globe.png" class="site-block__image site-block__image--driver-globe"/>

            <div class="site-block__title site-block__title--driver">
                Вы занимаетесь перевозками пассажиров?
            </div>
            <div class="site-block__text site-block__text--driver">
                <p>
                    <span class="site-block__text-action--driver">Добавьте свой маршрут</span> — и получайте больше заявок на ваш рейс.
                </p>
            </div>
            <span class="button button--green site-block__button--driver site-block__button--driver-green">
                Добавить бесплатно
            </span>
            <span class="button button--white site-block__button--driver site-block__button--driver-white">
                Добавить бесплатно
            </span>
        </div>
    </div>

</section>

<?php
$script = <<< JS
$(document).on('click', '.site-block__button--driver', function(){
	if(!openModal('#modalWrapper')) {
	    addIframeModal('https://docs.google.com/forms/d/e/1FAIpQLSdmff5nLFVY2OIA8xt69R9q8Or4BmCdfqwWApFch26DdCCQ7Q/viewform?embedded=true');
	}
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>