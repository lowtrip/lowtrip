<?php
    $time = date('H:i', strtotime($schedule->departure));
?>
<div class="station-timetable__row station-timetable__row--timetable-item">
    <div class="station-timetable__col station-timetable__col--route">
        <?=$schedule->thread->title?>
    </div>
    <div class="station-timetable__col station-timetable__col--days">
        <?=$schedule->days?> <?if(!is_null($schedule->except_days)):?>кроме <?=$schedule->except_days?><?endif?>
    </div>
    <div class="station-timetable__col station-timetable__col--time">
        <img class="station-timetable__icon--time" src="/img/bus/icons/calendar.png" alt="">
        <?=$time?>
    </div>
</div>
