<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 02.09.2018
 * Time: 14:32
 */
use yii\helpers\Html;
?>
<tr class="fullinfo"  style="<?=$style?>">
    <td colspan="6">
        <div class="container">
            <div class="row">
                <div class="col-md-1 col-lg-1 timing">
                    <div class="start">
                        <div class="time">
                            <?=Html::encode($trip->START_TIME[0]);?>
                        </div>
                        <div class="date">
                            <?=Html::encode($trip->START_DATE[0]);?>
                        </div>
                    </div>
                    <div class="finish">
                        <div class="time">
                            <?=Html::encode($trip->END_TIME[0]);?>
                        </div>
                        <div class="date">
                            <?=Html::encode($trip->END_DATE[0]);?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 points">
                    <div class="timeline">
                        <ul class="timeline-list">
                            <li>
                                <div class="content">
                                    <div class="title"><?=Html::encode($trip->START_CITY_NAME[0]);?></div>
                                    <p>
                                        <?=Html::encode($trip->STATION_BEGIN_ADDRESS[0])?>
                                    </p>
                                </div>
                            </li>
                            <li>
                                <div class="content">
                                    <div class="title"><?=Html::encode($trip->END_CITY_NAME[0]);?></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <p class="last-description">
                        <?=Html::encode($trip->STATION_END_ADDRESS[0])?>                    </p>
                    <p class="middle-points">
                        Промежуточные пункты (<?=count($trip->POINT); ?>)
                    </p>
                </div>
                <div class="col-md-3 col-lg-3 additional-info">
                    <div class="title">
                        Дополнительно
                    </div>
                    <div class="info">
                        <?php if($trip->TRIP_MODE[0] == '0'):?>
                        <p> Регулярный рейс</p>
                        <? elseif($trip->TRIP_MODE[0] == '1'): ?>
                        <p> Нерегулярный автобусный
                            рейс</p>
                        <? endif;?>
                    </div>
                    <div class="info">
                        <p>Количество свободных мест</p>
                        <?=Html::encode($trip->FREE_PLACES_COUNT[0]);?>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-lg-offset-1 col-md-offset-1 driver-info">
                    <div class="title">
                        Перевозчик <i class="fa fa-info-circle"></i>
                    </div>
                    <div class="info">
                        <?=Html::encode($trip->CARRIER_NAME[0]);?>
                    </div>

                    <div class="title">
                    </div>
                    <div class="info">
                        
                    </div>
                </div>
            </div>
        </div>
    </td>
</tr>
