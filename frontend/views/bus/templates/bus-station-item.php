<?php
use yii\helpers\Html;
use yii\helpers\Url;

$url = Url::toRoute(['bus/station', 'from_slug' => $city->city_slug, 'station_slug' => $station->name_slug]);
?>

<a class="station-timetable__row station-timetable__row--station-item" href="<?=$url?>">
    <div class="station-timetable__col station-timetable__col--name">
        <?= $station->name ?>
    </div>
</a>