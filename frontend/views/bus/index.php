<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<header class="header header--bus-main header--fullscreen">
    <div class="container">
        <div class="header__content header__content--main">
            <h1 class="header__title heading--h1">Поиск расписаний автобусов</h1>

            <p class="header__text">
                Купить онлайн билет на автобус можно на этой странице - не надо стоять в очереди. Более 60 000 направлений, проверяем цену на нескольких
                сайтах. Расписание выездов: маршрут движения и время выездов автобусов.
            </p>

            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-form.php', [
                'module' => 'bus/search',
                'style' => 'header__form bus-form bus-form--black',
            ]); ?>
        </div>
    </div>
</header>

<section class="site-block site-block--about-service">
    <div class="site-block__mask site-block__mask--grey">
        <div class="container">
            <div class="site-block__content site-block__content--about-service">
                Сервисом Lowtrip ежегодно пользуются более 100 000 человек,<br>
                подбирая самые выгодные билеты для путешествий. Чтобы купить билет, надо выбрать нужный маршрут, выбрать подходящий
                вариант и перейти на страницу оплаты: часть оплат происходит на нашем сайте, часть на стороне партнера.
            </div>
        </div>
    </div>
</section>

<section class="site-block site-block--bus-stations">
    <div class="container">
        <div class="popular-items">
            <h2 class="heading--h1 site-block__title site-block__title--bus-popular-stations">
                Автовокзалы России
            </h2>
            <p>
                Мы помогаем находить актуальное расписание автобусов более чем на 60 000 маршрутов, подбирая самые выгодные
                тарифы.<br>
                Также на нашем сервисе можно купить билет без переплат в своем городе.
            </p>
            <ul class="popular-items-list popular-items-list--bus-stations">
                <?php
                foreach ($cities as $city) {
                    echo Html::beginTag('li', ['class' => 'popular-items-list__item']);
                    echo Html::a(
                        $city->busStations[0]->name,
                        [Url::toRoute(['bus/station', 'from_slug' => $city->city_slug, 'station_slug' => $city->busStations[0]->name_slug])],
                        ['class' =>'popular-items-list__link popular-items-list__link--black']
                    );
                    echo Html::endTag('li');
                }
                ?>
            </ul>
        </div>
    </div>
</section>

<?php if (!empty($popularRoutes)) : ?>
<section class="site-block site-block--city-popular-routes site-block--city-popular-routes-bus">
    <div class="container">
        <div class="popular-items popular-items--black">
            <h2 class="heading--h1 site-block__title site-block__title--city-popular-routes">
                Популярные маршруты
            </h2>
            <ul class="popular-items-list">
            </ul>
        </div>
    </div>
</section>
<?php endif; ?>

<?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-add-route.php'); ?>


<section id="disclaimer" class="disclaimer hidden-sm hidden-xs">
    <div class="container disclaimer__content">
        <img class="disclaimer__img" src="/img/bus/shield.png">
        <div class="text">
            Сервис не передает данные третьим лицам, полностью соблюдая Федеральный закон РФ от 27 июля 2006 года
            №152-Ф3 “О персональных данных”.<br>
            Для защиты данных используются надежные серверы, шифрование данных через SSL-сертификат.
        </div>
    </div>
</section>

<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
