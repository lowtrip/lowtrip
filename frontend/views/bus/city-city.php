<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\widgets\Breadcrumbs;

$this->title = "Автобус {$first->city} - {$second->city}, расписание, маршрут, билет на автобус.";
$description = "Актуальное расписание автобусов из {$first->city} в {$second->city} на 2018-ый год.";
$this->registerMetaTag([
    'name' => 'description',
    'content' => $description,
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => $this->title,
]);

$this->registerMetaTag([
    'property' => 'og:description',
    'content' => $description,
]);
?>

<div class="bus-page bus-page--city-city">
    <!--Breadcrumbs-->
    <section class="bus-page__breadcrumbs breadcrumbs_block breadcrumbs-transparent">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'Билеты на автобус',
                        'url' => ['bus/main'],
                    ],
                    [
                        'label' => $first->city,
                        'url' => ['bus/city', 'from_slug' => $first->city_slug],
                    ],
                    [
                        'label' => $second->city,
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section class="bus-page__header">
        <div class="container">
            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-form.php', [
                'module' => 'bus/search',
                'style' => 'header__form bus-form bus-form--black',
                'first' => $first,
                'second' => $second,
                'date' => $date,
            ]); ?>
        </div>
    </section>

    <section>
        <div class="container advert">
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
                'id' => 'R-A-394883-1',
                'class' => '',
                // 'self' => [
                //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=bus',
                //     'image' => '/img/advert/sgabs_top_2.jpg',
                //     'background' => '#000000',
                // ],
            ]); ?>
        </div>
    </section>

    <section class="bus-page__content">
        <div class="container">

            <h1 class="bus-page__title heading--h1">Билеты на автобус: <?= $first->city ?> - <?= $second->city ?></h1>

            <div id="table-asis"></div>

            <div class="disclaimer">
                <div class="disclaimer__content">
                    <img class="disclaimer__img" src="/img/bus/disclamer.png">
                    Расписание по маршруту <?=Html::encode($from)?>-<?=Html::encode($to)?> регулярно обновляется, но если Вы заметили неточность - сообщите
                    нам об этом.
                    <br>
                    Общая рекомендация - покупайте билеты заранее, но перед покупкой лучше уточняйте время отправления по
                    справочному телефону.
                </div>
            </div>

        </div>

        <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-add-route.php'); ?>
    </section>

</div>



<section class="site-block site-block--about-service">
    <div class="site-block__mask site-block__mask--grey">
        <div class="container">
            <div class="site-block__content site-block__content--about-service">
                <p>Lowtrip позволяет находить самые недорогие варианты на автобус со всего интернета! Мы ищем сразу на нескольких площадках, что позволяет
                Вам экономить до 30 минут личного времени. Также, чтобы помочь Вам быстрее и выгоднее добраться до г.<?= $second->city ?>, мы смотрим доступные
                варианты путешествия на ж/д и с помощью попутных авто - поиск идет на OneTwoTrip, BlaBlaCar, BeepCar.</p>

                <p>Мы постоянно совершенствуем алгоритм, чтобы еще быстрее находить для Вас самые классные варианты недорогих путешествий на автобусе.
                Оплата за билет происходит 2-мя вариантами: либо на нашем сайте, либо на сайте партнера.</p>
            </div>
        </div>
    </div>
</section>

<?php

$script = <<< JS
const busView = new BusView('#table-asis', {
    fromId: $first->id,
    toId: $second->id,
    date: "$date",
});

busView.run();
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>