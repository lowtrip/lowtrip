<?php
use yii\helpers\Html;
use yii\helpers\Url;

use common\widgets\Breadcrumbs;

$this->title = $page->title;

$this->registerMetaTag([
        'name' => 'description',
        'content' => $page->description,
]);

$this->registerMetaTag([
        'property' => 'og:title',
        'content' => $page->title,
]);

$this->registerMetaTag([
        'property' => 'og:description',
        'content' => $page->description,
]);
?>

<div class="bus-page bus-page--city">
    <!--Breadcrumbs-->
    <section class="bus-page__breadcrumbs breadcrumbs_block breadcrumbs-transparent">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'Билеты на автобус',
                        'url' => ['bus/main'],
                    ],
                    [
                        'label' => $city->city,
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section class="bus-page__header">
        <div class="container">
            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-form.php', [
                'module' => 'bus/search',
                'style' => 'header__form bus-form bus-form--black',
                'first' => $city,
            ]); ?>
        </div>
    </section>

    <section class="bus-page__content">
        <div class="container">

            <h1 class="bus-page__title heading--h1"><?= $city->city ?>: автовокзалы и автостанции</h1>

            <div class="station-timetable">
                <div class="station-timetable__head">
                    <div class="station-timetable__row">
                        <div class="station-timetable__col">Название</div>
                    </div>
                </div>
                <div class="station-timetable__body">
                    <? if(!is_null($stations)) :?>
                        <? foreach($stations as $station) : ?>
                            <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-station-item.php',['station' => $station, 'city' => $city]);?>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
            </div>

            <div class="bus-stations-map" id="bus-stations-map">

            </div>
        </div>

        <?php echo \Yii::$app->view->renderFile('@app/views/bus/templates/bus-add-route.php'); ?>
    </section>
</div>

<section class="site-block site-block--about-service">
    <div class="site-block__mask site-block__mask--grey">
        <div class="container">
            <div class="site-block__content site-block__content--about-service">
                Lowtrip помогает недорого путешествовать по всей России.<br>
                Мы подгружаем все рейсы на наземном транспорте. Наша идеология - искать самые классные маршруты.<br>
                На странице можно найти доступный трансфер.
            </div>
        </div>
    </div>
</section>

<?php
$cityJson = json_encode($city->getAttributes());

$cityStations = array_map(function($station) {
    return $station->getAttributes();
}, $stations);

$cityStationsJson = json_encode($cityStations);

$script = <<< JS
function buildMap(element, city, stations) {
    const map = new ymaps.Map(element, {
        center: [city.lat, city.lng],
        zoom: 9,
        controls: []
    });

    stations.forEach(function(item, i) {
        var placemark = new ymaps.Placemark([item.lat, item.lng], {
            balloonContentHeader: item.name,
            balloonContentBody: item.name + ', ' + city.country + ', ' + city.region,
            iconCaption: item.name,
        }, {
            preset: 'islands#redDotIconWithCaption'
        });
        map.geoObjects.add(placemark);
    });

    map.events.add('sizechange', function(e) {
        map.setBounds(map.geoObjects.getBounds());
    });
    
    map.setBounds(map.geoObjects.getBounds());
}

ymaps.ready(() => {
   buildMap('bus-stations-map', $cityJson, $cityStationsJson); 
});
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>