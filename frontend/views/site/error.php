<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<section class="main_bg top" id="main-header">

    <div class="topline container" id="topline">
        <div class="row">
            <div class="col-xs-2 col-md-2 mp_10">
                <div class="logo">
                    <div class="logo_lowtrip">
                        <a href="http://lowtrip.ru"><img src="/img/logo/lowtrip_166.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-8 col-md-8 mp_10">
                <h1 class="center">
                    LOWTRIP<br>ДЕШЕВЫЕ ПУТЕШЕСТВИЯ
                </h1>
            </div>
            <div class="col-xs-2 mp_10">
                <button class="open-form mobile" id="open-form"></button>
            </div>
        </div>
    </div>

    <div class="container content" id="search-form">
        <div class="row">
            <!--START: Cities standart form-->
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'trip/search', 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people]); ?>
            <!--END: Cities standart form-->
        </div>
    </div>

</section>

<section class="after_top">
    <div class="site-error">
        <div class="container">

            <div class="alert alert-danger">
                <?= nl2br(Html::encode($message)) ?>
            </div>

        </div>
    </div>
</section>

<?php
$script = <<< JS
    findBestDate()
    .then(function(offset){
       initDatepicker(6, offset);
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>