<?php

/* @var $this yii\web\View */
/* @var $travel common\models\Travel */
/* @var $form ActiveForm */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//use yii\bootstrap\Nav;
//use common\widgets\NavbarLowtrip;

$this->title = 'Lowtrip.ru - недорогие путешествия по России и за границей';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Проект Lowtrip - сервис позволяющий путешествовать за 1500 рублей. Отдых в России недорого. На нашем сайте Вы можете организовать себе недорогое путешествие и отдохнуть.',
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Недорогие путешествия по России и за границей',
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => 'Учтено всё самое нужное для хорошего путешествия: трансфер, жильё, гиды. Самые дешёвые варианты!',
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'https://lowtrip.ru/img/header/winter.jpg',
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => 'https://lowtrip.ru',
]);
$this->registerMetaTag([
    'property' => 'og:site_name',
    'content' => 'Lowtrip',
]);
$this->registerMetaTag([
    'property' => 'og:locale',
    'content' => 'ru_RU',
]);

?>

<div class="main_page_content">
    <section class="main_bg main_page wide mobile_header" id="main-header">
        <div class="header_cell">
            <div class="mobile_header_content">
                <div class="topline container" id="topline">
                    <div class="row">
                        <div class="col-xs-2 col-md-2 mp_10">
                            <div class="logo">
                                <div class="logo_lowtrip">
                                    <a onclick="yaCounter44396998.reachGoal('lowtrip-logo'); return true;" href="<?=Yii::$app->homeUrl?>"><img src="img/logo/lowtrip_166.png" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-md-8 mp_10">
                            <h1 class="center">
                                LOWTRIP<br>ДЕШЕВЫЕ ПУТЕШЕСТВИЯ
                            </h1>
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="slogan">
                        <div class="name">
                            Lowtrip
                        </div>
                        <div class="title">
                            Реально дешевые путешествия по России
                        </div>
                    </div>
                </div>

                <div class="container content" id="search-form">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'trip/search', 'navigation' => true, 'style' => 'green with-navigation']); ?>
                    <!--END: Cities standart form-->
                </div>
            </div>
        </div>
    </section>

<section class="top_cities" id="top_cities">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="top_cities_text">
                    Рекомендуем поехать
                </div>
            </div>
            <div class="col-md-9">
                <div class="scroll-block top-cities-scroll">
                    <div class="scroll-content">
                        <ul class="top_cities_list" id="top_cities_list">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_lowtrip section_standart">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>LOWTRIP — Сервис умных недорогих путешествий по России.</h2>
                <div class="about_text content-toggle">
                    <p>
                        Сервис автоматически определяет Ваше местоположение (но это не точно), строит маршрут до ближайшего города, подбирает вариант трансфера и жилья, а также предоставляет все самые нужные для туриста вещи: обзор достопримечательностей, бесплатный аудиогид, отзывы жителей.
                    </p>
                    <p>
                        Все предложения уникальные, поэтому Вы можете зайти на сайт в любой день и быть уверенными, что найдете все для классного отдыха. И самое главное – без наценки!
                    </p>
                    <p>
                        Над сервисом работают 4 человека: Руслан, Родион, Саша и Никита. Проект был запущен в апреле 2017 года. Идея создать Lowtrip пришла к нам во время очередного путешествия, когда мы поняли, что времени на подбор маршрутов и проработки нюансов уходит слишком много, что неприемлемо.
                    </p>
                    <p>
                        В соседние города нужно ездить спонтанно, не тратя много времени и денег. Нужно путешествовать ради эмоций! За первые 4 месяца работы сервиса мы собрали 130 000 просмотров и поняли, что проект полезен людям.
                    </p>
                </div>
                <span onclick="yaCounter44396998.reachGoal('about-lowtrip'); return true;" class="fulltext-link toggle-btn">Подробнее</span>
            </div>
        </div>
    </div>
</section>

<!--START: Partners-->
<?php echo \Yii::$app->view->renderFile('@app/views/templates/partners.php'); ?>
<!--END: Partners-->
<!--START: Footer-->
<?php echo \Yii::$app->view->renderFile('@app/views/templates/footer.php'); ?>
<!--END: Footer-->

</div>


<?php
$script = <<< JS
    findBestDate()
    .then(function(offset){
       initDatepicker(6, offset);
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>