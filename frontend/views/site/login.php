<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Войти';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>Пожалуйста заполните данные поля для входа:</p>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                    <?= $form->field($model, 'username')->label('Ваш логин')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($model, 'password')->label('Ваш пароль')->passwordInput() ?>

                    <?= $form->field($model, 'rememberMe')->label('Запомнить меня')->checkbox() ?>

                    <div style="color:#999;margin:1em 0">
                        Если вы забыли ваш пароль, вы можете его <?= Html::a('восстановить', ['site/request-password-reset']) ?>.
                    </div>

                    <div class="form-group">
                        <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
