<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use common\widgets\Breadcrumbs;

$this->title = 'РЖД официальный сайт купить билеты';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Подбор недорогих билетов на РЖД, ищем акции и скидки, проверяем наличие мест в поезде. Официальный партнер РЖД. Можно купить билеты на РЖД со скидкой',
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'РЖД официальный сайт купить билеты',
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => 'Подбор недорогих билетов на РЖД, ищем акции и скидки, проверяем наличие мест в поезде. Официальный партнер РЖД. Можно купить билеты на РЖД со скидкой',
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'https://lowtrip.ru/img/header/train.jpg',
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => 'https://lowtrip.ru'.Url::to(),
]);
$this->registerMetaTag([
    'property' => 'og:site_name',
    'content' => 'Lowtrip',
]);
$this->registerMetaTag([
    'property' => 'og:locale',
    'content' => 'ru_RU',
]);

?>
<div class="train_city_page">
    <!--site navigation-->
    <div class="site-navigation">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
            'style' => 'navigation-nav--white',
            'title' => 'ЖД Билеты',
            'menu'  => [
                [
                    'title' => 'Путешествия',
                    'url' => Url::toRoute('site/index'),
                ],
                [
                    'title' => 'ЖД Билеты',
                    'url' => Url::toRoute('train/main'),
                    'active' => true,
                ],
                [
                    'title' => 'Путешествия на автомобиле',
                    'url' => Url::toRoute('car/main'),
                ],
                [
                    'title' => 'Билеты на автобус',
                    'url' => Url::toRoute('bus/main'),
                ]
            ],
        ]); ?>
    </div>
    <!--site navigation-->

    <section class="main_bg train_header" id="main-header">
            <div class="container">
                <div class="train_header_title">
                    <h1>Ж/Д билеты на поезд без очередей</h1>
                    <h3>
                        Мгновенное бронирование.<br>
                        Ищем самые недорогие билеты на поезд в интернете
                    </h3>
                </div>

                <div class="train-form-wrapper">
                <!--START: Cities standart form-->
                <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'train/search', 'style' => 'black']); ?>
                <!--END: Cities standart form-->
                </div>
            </div>
    </section>

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block shadow">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'ЖД Билеты',
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section class="popular_trains">
        <div class="popular_trains_card popular_trains_card_red">
            <h2 class="popular_trains_title">
                <span>Как купить билет недорого?</span>
            </h2>
            <p>
                1. Покупайте билеты на поезда заранее. ОАО «РЖД» начинает продажи билетов за 90 дней.
            </p>
            <p>
                2. На проходящие направления цены ниже, чем на прямые рейсы.
            </p>
            <p>
                3. Следите за скидками. Мы публикуем их у себя в <a href="https://t.me/rzdticket" target="_blank">группе</a>.
            </p>
            <p>
                4. Самые дешевые билеты на ж/д обычно на боковые верхние места и места возле туалета.
            </p>
        </div>

        <div class="popular_trains_card ">
            <h3 class="popular_trains_title">
                <span>Популярные направления</span>
            </h3>
            <ul class="popular_trains_list">
                <?php
                if (!empty($popular_routes)) {
                    foreach ($popular_routes as $k => $route)
                    {
                        $minPricePart = $route['minPrice'] ? ' от '.$route['minPrice'].' руб.' : '';
                        $url =  Url::toRoute(['train/build', 'from' => $route['departure']['city_slug'], 'to' => $route['arrival']['city_slug']]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$route['departure']['city'].' &#8594; '.$route['arrival']['city'].''.$minPricePart.'</a></li>';
                    }
                }
                ?>
            </ul>
        </div>

        <div class="popular_trains_card">
            <h3 class="popular_trains_title">
                <span>Популярные станции</span>
            </h3>
            <ul class="popular_trains_list">
                <?php
                if (!empty($popular_cities)) {
                    foreach ($popular_cities as $k => $city)
                    {
                        $url =  Url::toRoute(['train/city', 'from' => $city->city->city_slug]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$city->city->city.'</a></li>';
                    }
                }
                ?>
            </ul>
        </div>
    </section>

    <section class="sm_50">
        <div class="trains_block dark_bg">
            <h4 class="trains_block_title">О сервисе Lowtrip</h4>
            <p>
                Сервис Lowtrip автоматически подбирает самые дешевые варианты для путешествий.
            </p>
            <p>
                Мы находим дешевые билеты на поезд, доступные места на авто и автобусах, а также жилье и даем бесплатные аудиогиды по городу.
            </p>
        </div>
    </section>

</div>
<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>