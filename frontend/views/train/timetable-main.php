<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;

use common\widgets\Breadcrumbs;
use common\widgets\NavbarLowtrip;

$this->title = 'Расписание поездов';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Расписание поездов',
]);

?>
    <div class="train_city_page">
        <!--site navigation-->
        <div class="site-navigation">
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
                'style' => 'navigation-nav--white',
                'title' => 'ЖД Билеты',
                'menu'  => [
                    [
                        'title' => 'Путешествия',
                        'url' => Url::toRoute('site/index'),
                    ],
                    [
                        'title' => 'ЖД Билеты',
                        'url' => Url::toRoute('train/main'),
                        'active' => true,
                    ],
                    [
                        'title' => 'Путешествия на автомобиле',
                        'url' => Url::toRoute('car/main'),
                    ],
                    [
                        'title' => 'Билеты на автобус',
                        'url' => Url::toRoute('bus/main'),
                    ]
                ],
            ]); ?>
        </div>
        <!--site navigation-->

        <section class="main_bg train_header" id="main-header">
            <div class="container">
                <div class="train_header_title">
                    <h1>Расписание поездов</h1>
                    <h3>
                        Выберите и оформите ЖД билет<br>
                        в любое направление онлайн
                    </h3>
                </div>

                <div class="train-form-wrapper">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'train/search', 'style' => 'black']); ?>
                    <!--END: Cities standart form-->
                </div>
            </div>
        </section>

        <!--Breadcrumbs-->
        <section class="breadcrumbs_block shadow">
            <div class="container">
                <?php
                echo Breadcrumbs::widget([
                    'links' => [
                        [
                            'label' => 'ЖД Билеты',
                            'url' => ['train/main'],
                        ],
                        [
                            'label' => 'Расписание поездов',
                        ],
                    ],
                ]);
                ?>
            </div>
        </section>
        <!--Breadcrumbs-->

        <section>
            Расписание поездов
        </section>

    </div>
<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>