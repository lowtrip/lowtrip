<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;

use common\widgets\Breadcrumbs;
use common\widgets\NavbarLowtrip;

$date_string = Yii::$app->formatter->asDate($date, 'long');
$trip_url =  Url::toRoute(['trip/build', 'from' => $first->city_slug, 'to' => $second->city_slug]);

$title_string = 'ЖД билеты на поезд '.$first->city.' - '.$second->city.': расписание, цена, купе, плацкарт';
$description_string = ' Ж/д билеты '.$first->city.' - '.$second->city.': сидячие, плацкарт, купе. Есть сортировка: сапсан, самый дешевый, двухэтажный, есть отзывы. Партнер ОАО “РЖД”, находим самые недорогие билеты на ж/д поезд. Рассказываем как доехать из '.$first->city.' до '.$second->city.' на поезде.';

$this->title = $title_string;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description_string,
]);

?>

<div class="train_results_page">

    <div class="offers offers--top">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
            'id' => 'R-A-394883-4',
            'class' => '',
            // 'self' => [
            //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=train',
            //     'image' => '/img/advert/sgabs_top_2.jpg',
            //     'background' => '#000000',
            // ],
        ]); ?>
    </div>

    <!--site navigation-->
    <div class="site-navigation">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
            'style' => 'navigation-nav--white',
            'title' => 'ЖД Билеты',
            'menu'  => [
                [
                    'title' => 'Путешествия',
                    'url' => Url::toRoute('site/index'),
                ],
                [
                    'title' => 'ЖД Билеты',
                    'url' => Url::toRoute('train/main'),
                    'active' => true,
                ],
                [
                    'title' => 'Путешествия на автомобиле',
                    'url' => Url::toRoute('car/main'),
                ],
                [
                    'title' => 'Билеты на автобус',
                    'url' => Url::toRoute('bus/main'),
                ]
            ],
        ]); ?>
    </div>
    <!--site navigation-->

    <!--header-->
    <section class="main_bg train_header" id="main-header">

        <!--desktop header-->
        <div class="content" id="search-form">
            <div class="container">
                <div class="train-form-wrapper">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'train/search', 'style' => 'black', 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people]); ?>
                    <!--END: Cities standart form-->
                </div>
            </div>
        </div>
        <!--desktop header-->

    </section>
    <!--header-->

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block shadow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 t_col-10">
                    <?php
                    echo Breadcrumbs::widget([
                        'links' => [
                            [
                                'label' => 'ЖД Билеты',
                                'url' => ['train/main'],
                            ],
                            [
                                'label' => $first->city,
                                'url' => ['train/city', 'from' => $first->city_slug],
                            ],
                            [
                                'label' => $second->city,
                            ]
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!--Breadcrumbs-->

    <!--Секция для контента результатов поиска-->
    <section class="train-results-container">
        <!--Заголовок-->
        <div class="train-results-header">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 t_col-10 train-results-header__title-block">
                        <h1>
                            <nobr><?= $first->city?> - <?= $second->city?></nobr>&nbsp;<nobr>на поезде</nobr>
                        </h1>
                        <h2>Расписание, билеты, актуальная цена</h2>
                    </div>
                </div>
            </div>
        </div>
        <!--Заголовок-->

        <!--Результаты поиска-->
        <div class="train-results-search-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <div class="no-stations-block">

                            <div class="row">
                                <div class="col-xs-12 t_col-10">
                                    <div class="no-stations-block-header">
                                        <p class="no-stations-block-title">В городе <?= $first->city ?> не найдены пассажирские железнодорожные станции.</p>
                                        <p>Вам нужно доехать до города <b><?= $near_stations[0]['city'] ?></b> (<?= $near_stations[0]['country'] ?>, <?= $near_stations[0]['region'] ?>).</p>
                                        <p>В нём расположен ближайший вокзал от города <b><?= $first->city ?></b> - в <?= $near_stations[0]['distance'] ?> км.</p>
                                    </div>
                                </div>

                                <div class="col-md-6 t_col-10">
                                    <div class="trains-block">
                                        <?php
                                        foreach ($near_stations as $k => $station)
                                        {
                                            $url =  Url::toRoute(['train/build', 'from' => $station['city_slug'], 'to' => $second->city_slug]);
                                            $url_auto = Url::toRoute(['car/build', 'from' => $first->city_slug, 'to' => $station['city_slug']]);
                                            $price = intval($station['distance'] * 1.5);

                                            $block = '
                                    <div class="distance-route-block">
                                        <div class="distance">
                                            <span class="count">'.$station["distance"].'</span>
                                            <span class="unit">км</span>
                                        </div>
                                        <div class="content">
                                            <div><a href="'.$url.'">Поезд '.$station['city'].' &#8594; '.$second->city.'</a></div>
                                        </div>
                                    </div>';
                                            echo $block;
                                        }
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-6 t_col-10">
                                    <div class="map_mini small_height no-stations-map" id="stations-map"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="offers">
                            <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
                                'id' => 'R-A-394883-3',
                                // 'self' => [
                                //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=train',
                                //     'image' => '/img/advert/sgabs_small_2.jpg',
                                // ],
                            ]); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--Результаты поиска-->
    </section>

    <!--Информация о маршруте-->
    <section class="page-train-type-route grey_bg">
        <div class="container">
            <div class="trains_block dark_bg">
                <h3 class="trains_block_title">Как купить ж/д билет на поезд без переплат</h3>
                <p>
                    Идеология <b>Lowtrip</b> - делать все, чтобы Ваши рабочие, туристические, экскурсионные поездки были были максимально недорогими. Потому мы предлагаем на отдельной странице еще и бесплатные экскурсии в г.
                    <a href="<?=$trip_url?>"><?=$second->city?></a> , недорогое жилье, отзывы местных жителей и список достопримечательностей, а также предлагаем промокод на обед в Burger King.
                </p>
                <div class="trains_block_attention">
                    <p>
                        Внимание! Сервис Lowtrip находит самые дешевые билеты на железнодорожный поезд, но вся покупка и оформление проходит у наших партнеров - <b>РЖД</b> или <b>OneTwoTrip</b>. Наш сервис не добавляет к стоимости своей дополнительной комиссии, что помогает путешественникам покупать билеты по самым недорогим ценам.
                    </p>
                    <p class="desicion">
                        Вывод: покупайте только на надежных сайтах, которые являются партнерами РЖД.
                    </p>
                </div>
                <p>
                    Если у Вас есть замечания по работе, либо Вы хотите предложить партнерство, то направляйте письмо на почту
                    <a href="mailto:lowtrip@yandex.ru" target="_blank">lowtrip@yandex.ru</a>
                </p>
            </div>

            <div class="trains_block dark_bg">
                <h3 class="trains_block_title">Как купить самый дешевый билет на ж/д</h3>
                <p>
                    ОАО “РЖД” открывает продажи ж/д билетов за 90 дней до отправления.
                </p>
                <p>
                    Совет: Лучше всего покупать билеты заранее, в самом начале продаж стоимость минимальная.
                </p>
            </div>

            <div class="trains_block dark_bg">
                <h3 class="trains_block_title">О сервисе Lowtrip</h3>
                <p>
                    Сервис Lowtrip автоматически находит самые недорогие варианты для путешествий.
                </p>
                <p>
                    Мы ищем дешевые билеты на поезд, доступные места на авто и автобусах, а также жильё и даем бесплатные аудиогиды по городу.
                </p>
            </div>
        </div>
    </section>
    <!--Информация о маршруте-->
</div>

<?php
$city = json_encode($first->getAttributes());
$stations = json_encode($near_stations);

$script = <<< JS
    noStationsTrainScenario($city, $stations);
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>