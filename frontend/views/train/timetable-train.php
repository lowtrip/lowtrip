<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;

use common\widgets\Breadcrumbs;
use common\widgets\NavbarLowtrip;

$this->title = "Поезд ".$train['train_number']." ".$train['name']." ".$train['title'].". Расписание, маршрут и пути следования поезда ".$train['train_number'];

$this->registerMetaTag([
    'name' => 'description',
    'content' => "Поезд ".$train['train_number']." движется по маршруту ".$train['title'].". Время в пути составляет ".$duration.", расстояние ".$distance." км. По маршруту прописаны все остановки и пути следования, также маршрут дополняется фото. Недорогие билеты можно купить со скидкой до 50%.",
]);

$stations_array = [];
$timetable_count = count($timetable);

?>
<div class="train_timetable_page">
    <!--site navigation-->
    <div class="site-navigation">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
            'style' => 'navigation-nav--white',
            'title' => 'ЖД Билеты',
            'menu'  => [
                [
                    'title' => 'Путешествия',
                    'url' => Url::toRoute('site/index'),
                ],
                [
                    'title' => 'ЖД Билеты',
                    'url' => Url::toRoute('train/main'),
                    'active' => true,
                ],
                [
                    'title' => 'Путешествия на автомобиле',
                    'url' => Url::toRoute('car/main'),
                ],
                [
                    'title' => 'Билеты на автобус',
                    'url' => Url::toRoute('bus/main'),
                ]
            ],
        ]); ?>
    </div>
    <!--site navigation-->

    <!--header-->
    <section class="main_bg train_header" id="main-header">

        <!--desktop header-->
        <div class="content" id="search-form">
            <div class="container">
                <div class="train-form-wrapper">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'train/search', 'style' => 'black', 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people]); ?>
                    <!--END: Cities standart form-->
                </div>
            </div>
        </div>
        <!--desktop header-->

    </section>
    <!--header-->

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block breadcrumbs_block_grey shadow">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'ЖД Билеты',
                        'url' => ['train/main'],
                    ],
                    [
                        'label' => 'Расписание поездов',
                    ],
                    [
                        'label' => 'Поезд '.$train['train_number'],
                    ],
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <!--Timetable results content-->
    <section class="train-results-container">
        <div class="train-results-header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>
                            Поезд
                            <?= $train['train_number'] ?>
                            <?php
                                if (!empty($train['name'])) {

                                    echo Html::beginTag('span', ['class' => 'train-results-header__train-name']);
                                    echo '&laquo;'.$train["name"].'&raquo;';
                                    echo Html::endTag('span');
                                }
                            ?>
                            <nobr><?= $train['title'] ?></nobr>
                        </h1>
                        <h2>Наличие билетов, маршрут, маршрут следования</h2>
                        <p><?= $train['carrier']['title'] ?></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="train-results-search-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">

                        <div class="train-route-timetable">
                            <ul class="nav nav-tabs train-route-timetable__nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#train-route-tab">Маршрут</a></li>
                                <li><a data-toggle="tab" href="#train-map-tab">На карте</a></li>
                            </ul>
                            <div class="tab-content train-route-timetable__tab-content">

                                <div id="train-route-tab" class="tab-pane fade in active">

                                    <h2 class="train-route-timetable__tab-title">Маршрут поезда <b><?= $train['train_number'] ?> <?= $train['title'] ?></b></h2>

                                    <p>Маршрут на
                                        <?php echo Yii::$app->formatter->asDate($timetable[0]['departure']['date'], 'long'); ?>
                                    </p>

                                    <?php
                                        //echo '<pre>' , var_dump($timetable) , '</pre>';
                                    ?>

                                    <div class="train-timetable-table">
                                        <div class="train-timetable-table__head">
                                            <div class="train-timetable-table__row">
                                                <div class="train-timetable-table__column station">Станция</div>
                                                <div class="train-timetable-table__column arrival">Прибытие</div>
                                                <div class="train-timetable-table__column stop-time">Стоянка</div>
                                                <div class="train-timetable-table__column departure">Отправление</div>
                                                <div class="train-timetable-table__column duration">Время в пути</div>
                                            </div>
                                        </div>
                                        <div class="train-timetable-table__body">
                                            <?php
                                                foreach ($timetable as $key => $item) :

                                                    $stations_array[] = $item['station'];

                                                    $cityLink = isset($item['station']['city']['city_slug']) ? "/train/".$item['station']['city']['city_slug'] : "/train";

                                                    echo Html::beginTag('div', ['class' => 'train-timetable-table__row']);
                                                        echo Html::beginTag('div', ['class' => 'train-timetable-table__column station']);
                                                            echo Html::tag('div', $item['station']['name']);
                                                            echo Html::tag('a', $item['station']['city']['city'], ['href' => $cityLink]);
                                                        echo Html::endTag('div');
                                                        echo Html::beginTag('div', ['class' => 'train-timetable-table__column arrival']);
                                                            echo Html::tag('span', $item['arrival']['time'], ['class' => 'train-timetable-table__time']);
                                                            echo Html::tag('span', $item['arrival']['date'], ['class' => 'train-timetable-table__date']);
                                                        echo Html::endTag('div');
                                                        echo Html::tag('div', $item['stop_time'], ['class' => 'train-timetable-table__column stop-time']);
                                                        echo Html::beginTag('div', ['class' => 'train-timetable-table__column departure']);
                                                            echo Html::tag('span', $item['departure']['time'], ['class' => 'train-timetable-table__time']);
                                                            echo Html::tag('span', $item['departure']['date'], ['class' => 'train-timetable-table__date']);
                                                        echo Html::endTag('div');
                                                        echo Html::tag('div', $item['duration'], ['class' => 'train-timetable-table__column duration']);
                                                    echo Html::endTag('div');
                                                endforeach;
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <div id="train-map-tab" class="tab-pane fade">
                                    <h2 class="train-route-timetable__tab-title">Маршрут поезда <b><?= $train['train_number'] ?> <?= $train['title'] ?></b> на карте</h2>

                                    <div id="train-route-map" class="train-route-map"></div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="train-results-search-content">
            <div class="container">
                <div class="trains_block white_bg">
                    <h2 class="trains_block_title">
                        Информация о поезде <b><?= $train['train_number'] ?></b>
                    </h2>
                    <p>
                        Поезд дальнего следования <?= $train['train_number'] ?>  <?= $train['name'] ?> курсирует между городами <?= $train['title'] ?>
                    </p>
                    <ul>
                        <li>
                            Отправление осуществляется со станции <b><?= $timetable[0]['station']['name'] ?></b> в <b><?= $timetable[0]['departure']['time'] ?></b> по местному времени.
                        </li>
                        <li>
                            Прибытие осуществляется на станцию <b><?= $timetable[$timetable_count - 1]['station']['name'] ?></b> в <b><?= $timetable[$timetable_count - 1]['arrival']['time'] ?></b> по местному времени.
                        </li>
                        <li>
                            Время в пути поезда <?= $train['train_number'] ?>  <?= $train['name'] ?> составляет <b><?= $duration ?></b>
                        </li>
                        <li>
                            У поезда <?= $train['train_number'] ?> существует <?= count($train['uids']) ?> различных вариантов расписаний. Дни следования отмечены в графике курсирования.
                        </li>
                        <li>
                            Услуги по перевозке предоставляет компания <?= $train['carrier']['title'] ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </section>
</div>
<?php
$stations_array = json_encode($stations_array);

$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        });

    ymaps.ready(function() {
        routeStations('train-route-map', $stations_array);
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>