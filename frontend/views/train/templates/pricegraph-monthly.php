<?php
$results = json_encode($statistics);
?>

<div id="graphic-monthly"></div>

<?php
$script = <<< JS
const graphicMonthly = Pricegraph('graphic-monthly', 'byMonth', {
    startHeight: 30,
    slideBy: 3,
    slideTime: 1000,
    showSignatures: false,
    data: $results
});

JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>