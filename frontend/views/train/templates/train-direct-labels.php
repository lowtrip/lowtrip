<?php if (isset($direct) && $direct !== NULL) : ?>
    <?php if ($direct) :?>
        <span class="label label-success has_tooltip" data-placement="bottom" title="По данному направлению ходят прямые поезда. Осталось всего лишь найти подходящий билет!">Прямое направление</span>
    <?php else : ?>
        <span class="label label-warning has_tooltip" data-placement="bottom" title="До пункта назначения можно добраться только с пересадками. Прямых поездов не найдено.">С пересадками</span>
    <?php endif; ?>
<?php endif; ?>