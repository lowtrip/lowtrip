<?php

$results = json_encode($statistics);
$min_date = $statistics[0]['date'];
$typeSlug = isset($type) && $type ? '/'.$type->slug : '';
$baseUrl = "/$module/$first->city_slug/$second->city_slug$typeSlug?people=$people";
?>

<div id="graphic-daily"></div>

<?php
$script = <<< JS
const graphicDaily = Pricegraph('graphic-daily', 'byDay', {
    baseUrl: "$baseUrl",
    startHeight: 30,
    slideBy: 3,
    slideTime: 1000,
    minDate: "$min_date",
    currentDate: "$parent_date",
    noDataText: 'Найти билеты',
    data: $results
});

$(document).on('click', '.pricegraph-col-day', function() {
    if (window['yaCounter44396998'] !== undefined) {
        window['yaCounter44396998'].reachGoal('pricegraph-day-click');
    }
});

JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>