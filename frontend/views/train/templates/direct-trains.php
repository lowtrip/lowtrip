<div class="trains_block white_bg">
    <h3 class="trains_block_title">Прямые поезда</h3>
    <ul id="direct-trains">

    </ul>
</div>


<?php
$script = <<< JS
    $.ajax({
        url: '/api/partners/poezd/find-direct-trains',
        dataType: 'json',
        data: {
            from: $from,
            to: $to
        },
        type: 'GET',
        success: function(response) {
            if (response && response.length) {
                var buffer = [];
                response.forEach(function(train){
                    var str = '<li>'+ train.train_number + ' ' + train.title + ' ' + '</li>';
                    buffer.push(str);
                })
                
                $('#direct-trains').empty().append(buffer.join(''));
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>