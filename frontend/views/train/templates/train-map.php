<div class="trains_block trains_map_block dark_bg">
    <h3 class="trains_block_title">Карта пути поезда <?= $first->city?> - <?= $second->city?></h3>
    <div id="car-map" class="map_mini middle_height">
        <div class="trip-loader">
            Построение маршрута <?=$first->city?> - <?=$second->city?> на карте...
        </div>
    </div>
</div>

<?php
$script = <<< JS
    ymaps.ready(function(){
        carmap();
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>