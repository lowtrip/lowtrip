<?php
use yii\helpers\Html;
use yii\helpers\Url;

$type_string = (!isset($type) || !$type) ? '' : 'в '.mb_strtolower($type->name);

?>

<!--Билеты на другие даты-->
<section class="results-statistic-block results-statistic-block-train" id="stat-parent">
    <div class="container-fluid results-statistic-block-container" id="stat-child">
        <div class="row">
            <div class="col-md-4 col-lg-3 results-statistic-block-header">
                <div class="statistic-result">
                    <div class="statistic-result-image green has_tooltip" title="Мы ищем самые выгодные билеты <?= $type_string ?> и выводим их в календаре лучших цен, который обновляется раз в 10 минут">
                        <img src="/img/icons/calendar-green.png" alt="" class="statistic-result-image-icon">
                    </div>
                    <div class="statistic-result-information">
                        <div class="statistic-result-header">
                            <?php if (isset($types)) : ?>
                                <div class="btn-group" style="vertical-align:top;">
                                    <span class="dropdown-toggle pointer" data-toggle="dropdown">Выгодные билеты <?= $type_string ?> <span class="caret"></span></span>
                                    <ul class="dropdown-menu" role="menu">
                                    <?php
                                    foreach($types as $type_to) :
                                        $disabled = ($type_to->id == $type->id);
                                        if (!$disabled) :
                                        echo Html::beginTag('li', ['class' => $disabled]);
                                            echo Html::tag('a', $type_to->name, ['href' => Url::toRoute(['train/build-type', 'from' => $first->city_slug, 'to' => $second->city_slug, 'date' => $parent_date, 'people' => $people, 'type' => $type_to->slug])]);
                                        echo Html::endTag('li');
                                        endif;
                                    endforeach;
                                    ?>
                                    <?php if ($type) : ?>
                                        <li class="divider"></li>
                                        <li><a href="<?= Url::toRoute(['train/build', 'from' => $first->city_slug, 'to' => $second->city_slug, 'date' => $parent_date, 'people' => $people ]); ?>">Все билеты</a></li>
                                    <?php endif; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>

                        </div>
                        <div class="statistic-result-text">На ближайшие дни</div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-lg-9 results-statistic-block-content">
                <div class="train-statistic-offers" id="stat-offers-parent">
                    <div class="train-statistic-offers-container" id="stat-offers-child">
                        <?php
                        $i = 0;
                        foreach ($statistics as $result):
                            $type_slug = (isset($type) && $type) ? '/'.$type->slug : '';
                            $current = $parent_date == $result['date'] ? 'current' : '';
                            $date = $result['date'];
                            $price = $result['price'];

                            echo Html::beginTag('a', ['class' => "train-statistic-offer $current", 'unselectable' => 'on', 'href' => "/$module/$first->city_slug/$second->city_slug$type_slug?date=$date&people=$people"]);
                                if ($price !== 0) :
                                    echo Html::tag('div', "от $price Р", ['class' => 'train-statistic-offer-price']);
                                else :
                                    echo Html::tag('div', "Проданы", ['class' => 'train-statistic-offer-price sold']);
                                endif;
                                echo Html::tag('div', Yii::$app->formatter->asDate($date, 'medium'), ['class' => 'train-statistic-offer-date']);
                            echo Html::endTag('a');

                            if(++$i > 11) break;
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Билеты на другие даты-->

<?php
$script = <<< JS
if ($('#stat-offers-child').outerWidth() > $('#stat-offers-parent').outerWidth()) {
    $('#stat-offers-child').draggable({
        containment: '#stat-offers-parent',
        axis: 'x',
        scroll: false
    });
}
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>