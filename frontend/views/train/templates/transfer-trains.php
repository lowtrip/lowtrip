<div class="trains_block white_bg">
    <h3 class="trains_block_title">С пересадками</h3>
    <?= $from ?>
    <?= $to ?>
    <?= $dateParam ?>
    <button id="find-transfer-trains">Найти</button>
</div>


<?php
$script = <<< JS

    $('#find-transfer-trains').on('click', makeRequest);

    function makeRequest(){
        $.ajax({
            url: '/api/partners/poezd/find-transfer-trains',
            dataType: 'json',
            data: {
                from: trip.data.first.id,
                to: trip.data.second.id,
                date: trip.data.date
            },
            type: 'GET',
            success: function(response) {
                if (response && response.length) {
                    console.log(response);    
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>