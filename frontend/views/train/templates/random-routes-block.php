<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php if (!empty($random)) : ?>
    <div class="popular_trains_card popular_trains_card_wide">
        <h3 class="popular_trains_title">
            <span>Мы ищем поезда по маршрутам</span>
        </h3>
        <ul class="popular_trains_list columns_list">
            <?php
            foreach ($random as $k => $route)
            {
                $url =  Url::toRoute(['train/build', 'from' => $route->departure->city_slug, 'to' => $route->arrival->city_slug]);
                echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$route->departure->city.' &#8594; '.$route->arrival->city.'</a></li>';
            }
            ?>
        </ul>
    </div>
<? endif; ?>