<?php

use yii\helpers\Url;

?>

<div class="trip-block">
    <div class="h2_title">
        <h2>Самые выгодные билеты на поезд <?= $first->city ?> - <?= $second->city ?></h2> <span>в ближайшие дни</span>
    </div>
    <p style="font-style:italic">Стоимость билетов указана в рублях.</p>
    <div class="lowtrip-price-table pricetablesorter">
        <div class="price-table-header">
            <table>
                <thead>
                <tr>
                    <th class="column-date">
                        <span class="name">Дата</span>
                        <span class="down"></span>
                        <span class="up"></span>
                    </th>
                    <th class="column-placetype"><span class="name">Сидячее</span><span class="down"></span><span class="up"></span></th>
                    <th class="column-placetype"><span class="name">Плацкарт</span><span class="down"></span><span class="up"></span></th>
                    <th class="column-placetype"><span class="name">Купе</span><span class="down"></span><span class="up"></span></th>
                    <th class="column-placetype"><span class="name">Люкс</span><span class="down"></span><span class="up"></span></th>
                    <th class="column-placetype"><span class="name">СВ</span><span class="down"></span><span class="up"></span></th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="price-table-body">
            <table>
                <tbody>
                <?php
                foreach ($statistics as $statistic)
                {
                    $row = array(
                        'date' => '',
                        '2' => '',
                        '3' => '',
                        '4' => '',
                        '5' => '',
                        '6' => ''
                    );
                    $row['date'] = $statistic['date'];
                    $date_string = Yii::$app->formatter->asDate($row['date'], 'medium');

                    foreach ($statistic['prices'] as $price) {
                        $type = $price['place_type_id'];
                        $min = $price['min_price'];
                        $row[$type] = $min;
                    }

                    $url = Url::toRoute(['train/build', 'from' => $first->city_slug, 'to' => $second->city_slug, 'date' => $row['date'], 'people' => $people]);

                    $tr = '
                    <tr data-href="'.$url.'">
                        <td class="column-date" data-type="date" data-date="'.$row['date'].'">
                            '.$date_string.'
                        </td>
                        <td class="column-placetype">'.$row['2'].'</td>
                        <td class="column-placetype">'.$row['3'].'</td>
                        <td class="column-placetype">'.$row['4'].'</td>
                        <td class="column-placetype">'.$row['5'].'</td>
                        <td class="column-placetype">'.$row['6'].'</td>
                    </tr>';

                    echo $tr;
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
$script = <<< JS
    $('.pricetablesorter').on('click', 'th', function(){
       
       var _this = $(this);
       var order;
       var table = _this.closest('.pricetablesorter');
       var ix = _this.index('th');
       
       if (_this.hasClass('asc')) {
           _this.removeClass('asc').addClass('desc');
           order = 'desc';
       }
       else {
           _this.removeClass('desc').addClass('asc');
           order = 'asc';
       }
       _this.siblings().removeClass('asc desc');
       
       var container = table.find('.price-table-body').find('tbody');
       
       container.children().sort(function(a,b){
           a = $(a).find('td').eq(ix);
           b = $(b).find('td').eq(ix);
           if (a.attr('data-type') === 'date') {
               a = a.attr('data-date');
               b = b.attr('data-date');
               a = Math.round(new Date(a).getTime()/1000);
               b = Math.round(new Date(b).getTime()/1000);
           }
           else {
               a = parseInt(a.text());
               b = parseInt(b.text());
           }
           
           if (order === 'asc') {
              return (a>b ? 1 : (a === b ? 0 : -1)); 
           }
           else {
              return (a>b ? -1 : (a === b ? 0 : 1));
           }
           
       }).appendTo(container);
    });

    $('.pricetablesorter tbody').on('click', 'tr', function() {
       var url = $(this).attr('data-href');
       document.location.href = url;
    });

    
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>