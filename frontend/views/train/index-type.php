<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;

use common\widgets\Breadcrumbs;
use common\widgets\NavbarLowtrip;

$date_string = Yii::$app->formatter->asDate($date, 'long');
$trip_url =  Url::toRoute(['trip/build', 'from' => $first->city_slug, 'to' => $second->city_slug]);

$type_name = mb_strtolower($type->name);

$titleTypePrice = !empty($lowPrice) ? " от $lowPrice рублей" : '';

if (!empty($page)){
    $title = explode(";", $page->title);
    $description = explode(";", $page->description);
    $text1 = explode(";", $page->text1);
    $text2 = explode(";", $page->text2);
    $text3 = explode(";", $page->text3);
    $text4 = explode(";", $page->text4);
    $title_string = 'ЖД билеты на поезд '.$first->city.' - '.$second->city.' '.$type_name.$titleTypePrice.': расписание, '.$title[0];
    $description_string = ' Ж/д билеты '.$first->city.' - '.$second->city.': '.$type_name.$titleTypePrice.'. '.$description[0].': сапсан, '.$description[1].', двухэтажный, '.$description[2].'. Партнер ОАО “РЖД”, находим самые '.$description[3].' билеты на ж/д поезд. '.$description[4].' как доехать из '.$first->city.' до '.$second->city.' на поезде.';
}
else {
    $title_string = 'ЖД билеты на поезд '.$first->city.' - '.$second->city.': расписание, цена, купе, плацкарт';
    $description_string = ' Ж/д билеты '.$first->city.' - '.$second->city.': сидячие, плацкарт, купе. Есть сортировка: сапсан, самый дешевый, двухэтажный, есть отзывы. Партнер ОАО “РЖД”, находим самые недорогие билеты на ж/д поезд. Рассказываем как доехать из '.$first->city.' до '.$second->city.' на поезде.';
}

$this->title = $title_string;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description_string,
]);

?>

<div class="train_results_page">

    <div class="offers offers--top">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
            'id' => 'R-A-394883-4',
            'class' => '',
            // 'self' => [
            //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=train',
            //     'image' => '/img/advert/sgabs_top_2.jpg',
            //     'background' => '#000000',
            // ],
        ]); ?>
    </div>

    <!--site navigation menu-->
    <div class="site-navigation">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
            'style' => 'navigation-nav--white',
            'title' => \Yii::$app->view->renderFile('@app/views/templates/trip-information.php', ['first' => $first, 'second' => $second, 'people' => $people, 'date' => $date]),
            'menu'  => [
                [
                    'title' => 'Путешествия',
                    'url' => Url::toRoute('site/index'),
                ],
                [
                    'title' => 'ЖД Билеты',
                    'url' => Url::toRoute('train/main'),
                    'active' => true,
                ],
                [
                    'title' => 'Путешествия на автомобиле',
                    'url' => Url::toRoute('car/main'),
                ],
                [
                    'title' => 'Билеты на автобус',
                    'url' => Url::toRoute('bus/main'),
                ]
            ],
            'showSearch' => true,
        ]); ?>
    </div>
    <!--site navigation menu-->

    <!--header-->
    <section class="main_bg train_header" id="main-header">

        <!--desktop header-->
        <div class="content" id="search-form">
            <div class="container">
                <div class="train-form-wrapper">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'train/search', 'style' => 'black', 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people, 'type' => $type]); ?>
                    <!--END: Cities standart form-->
                </div>
            </div>
        </div>
        <!--desktop header-->

    </section>
    <!--header-->

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block shadow">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 t_col-10">
                    <?php
                    echo Breadcrumbs::widget([
                        'links' => [
                            [
                                'label' => 'ЖД Билеты',
                                'url' => ['train/main'],
                            ],
                            [
                                'label' => $first->city,
                                'url' => ['train/city', 'from' => $first->city_slug],
                            ],
                            [
                                'label' => $second->city,
                                'url' => ['train/build', 'from' => $first->city_slug, 'to'=> $second->city_slug],
                            ],
                            [
                                'label' => $type->name,
                            ]
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </section>
    <!--Breadcrumbs-->

    <?php
    if(!empty($statistics)){
        echo \Yii::$app->view->renderFile('@app/views/train/templates/results-statistic-block.php', ['module' => 'train', 'type' => $type, 'types' => $avaliable_types, 'statistics' => $statistics, 'first' => $first, 'second' => $second, 'people' => $people, 'parent_date' => $date]);
    } ?>

    <!--Секция для контента результатов поиска-->
    <section class="train-results-container">
        <!--Заголовок-->
        <div class="train-results-header">
            <div class="container-fluid">
                <div class="row">
                    <?php
                    if (!empty($lowPrice)) :
                        echo \Yii::$app->view->renderFile('@app/views/templates/schemaProduct.php', [
                            'name' => "ЖД билеты на поезд $first->city - $second->city $type_name: расписание, цена",
                            'description' => "$first->city - $second->city: недорогие ж/д билеты в $type_name. Стоимость билета в $type_name от $lowPrice рублей. Находим: самый быстрый, комфортный, недорогой поезда. Выводим расписание, график цен, отправление и прибытие.",
                            'image_url' => 'https://lowtrip.ru/img/train/lowtrip-train.png',
                            'lowPrice' => $lowPrice
                        ]);
                    endif;
                    ?>
                    <div class="col-md-9 col-md-push-3 t_col-10 train-results-header__title-block">
                        <h1>
                            <nobr><?= $first->city?> - <?= $second->city?></nobr>: недорогие ж/д билеты в <?= $type_name ?>
                        </h1>
                        <h2>Расписание, билеты, актуальная цена</h2> на <span class="date-to-string js-current-date"><?=$date_string?></span>

                        <?php echo \Yii::$app->view->renderFile('@app/views/train/templates/train-direct-labels.php', ['direct' => $direct]); ?>

                    </div>
                    <div class="col-md-3 col-md-pull-9 t_col-10">
                        <?php
                        if(!empty($statistics)){
                            echo \Yii::$app->view->renderFile('@app/views/train/templates/pricegraph-daily.php', ['module' => 'train', 'type' => $type, 'statistics' => $statistics, 'first' => $first, 'second' => $second, 'people' => $people, 'parent_date' => $date]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!--Заголовок-->

        <!--Результаты поиска-->
        <div class="train-results-search-content container-fluid">
            <div class="row">
                <div class="col-md-9">
                    <!--Шаблон модуля-->
                    <div class="train-results-timetable row" id="train-view"></div>
                    <!--Шаблон модуля-->
                </div>
                <div class="col-md-3">
                    <div class="offers offers--right">
                        <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
                            'id' => 'R-A-394883-3',
                            // 'self' => [
                            //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=train',
                            //     'image' => '/img/advert/sgabs_small_2.jpg',
                            // ],
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <!--Результаты поиска-->

    </section>
    <!--Секция для контента результатов поиска-->

    <section class="page-train-type-route grey_bg">
        <div class="container">
            <div class="panel-group" id="accordion">
                <?php echo \Yii::$app->view->renderFile('@app/views/templates/seoblocks/trainHowToBuyOverpayment.php', ['text' => $text1, 'firstCityName' => $first->city, 'secondCityName' => $second->city, 'tripUrl' => $trip_url]); ?>

                <?php echo \Yii::$app->view->renderFile('@app/views/templates/seoblocks/trainHowToBuy.php', ['text' => $text2]); ?>

                <?php if ($direct) : ?>
                <?php echo \Yii::$app->view->renderFile('@app/views/templates/seoblocks/trainShedule.php', ['text' => $text3, 'dateString' => $date_string, 'firstCityName' => $first->city, 'secondCityName' => $second->city]); ?>
                <?php endif; ?>

                <?php echo \Yii::$app->view->renderFile('@app/views/templates/seoblocks/about-lowtrip.php', ['text' => $text4]); ?>
            </div>
        </div>
    </section>
</div>
<?php
$script = <<< JS
    trainTypeScenario($type->id);
    if (window.hasOwnProperty('yaCounter44396998')) {
        yaCounter44396998.reachGoal('tickets-page'); 
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>