<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use frontend\assets\LowtripAsset;

LowtripAsset::register($this);

$this->registerMetaTag([
    'name' => 'yandex-verification',
    'content' => '2d50780f54268e13',
]);
$this->registerMetaTag([
    'name' => 'verify-admitad',
    'content' => '060f32bfa8',
]);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/img/favicon/favicon.png'])]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- TradeDoubler site verification 2999555 -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="page-wrapper">

    <div class="page-content">
        <?= $content ?>
    </div>

</div>

<a class="popup_init init_thanks" href="#popup_thanks"></a>

<div class="hidden">

    <!-- Форма для отправки маршрута -->
    <div class="popup_email" id="popup_email">
        <div class="title">
            КУДА ПРИСЛАТЬ МАРШРУТ?
        </div>
        <p>Введи свой email, и мы пришлем тебе твой маршрут со всеми ссылками на покупку билетов, чтобы ты его не забыл.</p>
        <form class="email_form validation" method="post" onsubmit="yaCounter44396998.reachGoal('send-email'); return true;" data-action="sendTrip">
            <div class="row">
                <div class="col-sm-8 relative">
                    <input name="email" type="email" class="email validator" placeholder="Email" required>
                    <div class="status"></div>
                </div>
                <div class="col-sm-4 relative">
                    <button type="submit" class="green_btn">Отправить</button>
                </div>
            </div>
            <div class="personal-agreement">
                Нажимая кнопку отправить, <a href="/templates/docs/soglasie.pdf" target="blank">вы соглашаетесь на обработку персональных данных</a>
            </div>
        </form>
    </div>
    <!-- Форма для отправки маршрута -->

    <!-- Спасибо за использование лоутрип -->
    <div class="popup_thanks" id="popup_thanks">
        <div class="container-fluid">

            <div class="row top">
                <div class="col-xs-12">
                    <div class="title">
                        Спасибо, за то, что пользуетесь lowtrip
                    </div>
                    <p class="text">
                        Сейчас на указанный вами email придет письмо с маршрутом.
                    </p>
                </div>
            </div>

            <div class="row bottom">
                <div class="col-sm-6">
                    <div class="text">
                        Подпишитесь на нас в социальных сетях
                    </div>
                    <img class="arrow" src="/img/icons/red-arrow.png" alt="">
                </div>
                <div class="col-sm-6">
                    <div class="footer-socials">
                        <a onclick="yaCounter44396998.reachGoal('instagram'); return true;" class="social-icon instagram" href="https://www.instagram.com/lowtrip.bro/" target="_blank"></a>
                        <a onclick="yaCounter44396998.reachGoal('vkontakte'); return true;" class="social-icon vk" href="https://vk.com/low_trip" target="_blank"></a>
                        <a onclick="yaCounter44396998.reachGoal('telegram'); return true;" class="social-icon telegram" href="https://t.me/rzdticket" target="_blank"></a>
                    </div>
                </div>
            </div>

        </div>
        <button class="close_btn close_popup">Закрыть</button>
    </div>
    <!-- Спасибо за использование лоутрип -->

    <?php echo \Yii::$app->view->renderFile('@app/views/templates/popups/contact-email-popup.php'); ?>
    <?php echo \Yii::$app->view->renderFile('@app/views/templates/popups/contact-popup.php'); ?>

    <div id="testimonial-full" class="testimonial-full"></div>
</div>

<?php if (!YII_ENV_DEV) {
    echo \Yii::$app->view->renderFile('@app/views/templates/counters.php');
} ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
