<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

use frontend\assets\SvahaAsset;

SvahaAsset::register($this);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/img/favicon/favicon.png'])]);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- TradeDoubler site verification 2999555 -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title)?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div id="svaha-app">
    <?= $content ?>

    <div class="hidden">
        <div class="terminal" id="terminal">
            <div class="terminal__topbar">
                О проекте
            </div>
            <div class="terminal__body">
                <div id="start">
                    <div class="terminal__command">
                        <span>Что такое “ЖД СВАХА”?</span>
                    </div>
                    <br>
                    <div class="terminal__command">

                        <p> Проект Lowtrip - это подбор дешёвых туров по России и СНГ!</p>
                        <p>
                            Недавно мы запустили поиск дешевых билетов на ЖД, и увидели следующую динамику:<br>
                        </p>
                        <p>
                            Многие люди путешествуют на поезде одни.<br>
                        </p>
                        <p>
                            К нам пришла идея: соединить этих людей при помощи нашего сервиса.
                            И мы создали проект - “ЖД СВАХА”.<br>
                            Проект работает на принципах нейронной сети.
                            Мы верим, что наш проект поможет Вам познакомится, возможно, найти свою любовь, или просто скоротать время в поезде!
                        </p>
                        <p>
                            Как же работает наш сервис?
                        </p>
                        <p>
                            Нейросеть анализирует количество проданных билетов по каждому маршруту, на каждый конкретный поезд.
                            <br>
                            Также обрабатываются все запросы на специальной странице Ж/Д Сваха.
                            <br>
                            Полученные результаты система преобразует в массив данных, где идёт распределение между поездами, городами, и интересами пассажиров.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php if (!YII_ENV_DEV) {
    echo \Yii::$app->view->renderFile('@app/views/templates/counters.php');
} ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
