<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Spaceless;
use common\widgets\Alert;

use frontend\assets\TrainAsset;

TrainAsset::register($this);

$this->registerMetaTag([
    'name' => 'yandex-verification',
    'content' => '2d50780f54268e13',
]);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/img/favicon/favicon.png'])]);
?>
<?php Spaceless::begin(); ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <!-- TradeDoubler site verification 2999555 -->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="layout-flex">
    <div class="layout-flex__content">
        <?= $content ?>
    </div>

    <div class="layout-flex__footer">
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/footer.php'); ?>
    </div>
</div>

<div class="hidden">
    <?php echo \Yii::$app->view->renderFile('@app/views/templates/popups/contact-email-popup.php'); ?>
    <?php echo \Yii::$app->view->renderFile('@app/views/templates/popups/contact-popup.php'); ?>
</div>

<?php if (!YII_ENV_DEV) {
    echo \Yii::$app->view->renderFile('@app/views/templates/counters.php');
} ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php Spaceless::end(); ?>
