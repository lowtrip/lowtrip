<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Spaceless;

use frontend\assets\CarAsset;

CarAsset::register($this);

$this->registerMetaTag([
    'name' => 'yandex-verification',
    'content' => '2d50780f54268e13',
]);

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => Url::to(['/img/favicon/favicon.png'])]);
?>
<?php Spaceless::begin(); ?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
    <div class="layout-flex">

        <div class="layout-flex__nav">
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/nav.php', [
                'style' => 'navigation-nav--white',
                'title' => 'Путешествия на автомобиле',
                'menu'  => [
                    [
                        'title' => 'Путешествия',
                        'url' => Url::toRoute('site/index'),
                    ],
                    [
                        'title' => 'ЖД Билеты',
                        'url' => Url::toRoute('train/main'),
                    ],
                    [
                        'title' => 'Путешествия на автомобиле',
                        'url' => Url::toRoute('car/main'),
                        'active' => true,
                    ],
                    [
                        'title' => 'Билеты на автобус',
                        'url' => Url::toRoute('bus/main'),
                    ]
                ],
            ]); ?>
        </div>

        <div class="layout-flex__content">
            <?= $content ?>
        </div>

        <div class="layout-flex__footer">
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/footer.php'); ?>
        </div>

    </div>

    <?php if (!YII_ENV_DEV) {
        echo \Yii::$app->view->renderFile('@app/views/templates/counters.php');
    } ?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>
<?php Spaceless::end(); ?>
