<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Breadcrumbs;

$this->title = 'К оформлению жд билета на Onetwotrip.ru';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Переход на сайт партнера Onetwotrip.ru для оформления жд билета',
]);

?>

<div class="redirect-window">
    <div class="redirect-window-title">
        Спасибо! Почти готово...
    </div>

    <div class="redirect-way">

        <div class="from redirect-block">
            <div class="image-item company">
                <img src="/img/logo/lowtrip_166.png" alt="">
            </div>
            <div class="text">
                Вы выбрали жд билет и перешли к оформлению
            </div>
        </div>

        <div class="center redirect-block">
            <div class="image-item">
                <div id="arrowAnim">
                    <div class="arrowSliding">
                        <div class="arrow"></div>
                    </div>
                    <div class="arrowSliding delay1">
                        <div class="arrow"></div>
                    </div>
                    <div class="arrowSliding delay2">
                        <div class="arrow"></div>
                    </div>
                    <div class="arrowSliding delay3">
                        <div class="arrow"></div>
                    </div>
                </div>
            </div>
            <div class="text">
                Идет переадресация на onetwotrip.ru
            </div>
        </div>

        <div class="to redirect-block">
            <div class="image-item company">
                <img src="/img/logo/onetwotrip.png" alt="">
            </div>
            <div class="text">
                Теперь завершите процесс бронирования на onetwotrip.ru
            </div>
        </div>

    </div>

    <div class="redirect-info">
        Если Вы не будете переадресованы на сайт onetwotrip.ru в течении <span id="time">7</span> секунд, пожалуйста <a href="<?= $url ?>">нажмите здесь</a>.
    </div>

</div>


<?php
$script = <<< JS
    var timer = 7;
    var span = document.getElementById('time');
    
    var interval = setInterval(function(){
       timer--; 
       changeTime(span, timer);
       if (timer === 0) {
           console.log('redirect');
           clearInterval(interval);
           document.location.href = "$url";
       }
    }, 1000);
    
    function changeTime(span, timer) {
        span.innerHTML = timer;
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>