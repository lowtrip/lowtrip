<header class="header header--car-main header--fullscreen">
    <div class="container">
        <div class="header__content header__content--main">
            <h1 class="header__title heading--h1">Поиск оптимального маршрута</h1>

            <p class="header__text">
                Мы разработали для вас удобный поиск маршрутов для поездки, включая детальную информацию о поездке: расстояния между городами,
                расход топлива, время поездки, информацию об остановках в пути, лучших местах для посещения и многое другое!
            </p>

            <?php echo \Yii::$app->view->renderFile('@app/views/car/templates/car-form.php', [
                'module' => 'car/search',
                'style' => 'header__form car-form car-form--black'
            ]); ?>
        </div>
    </div>
</header>