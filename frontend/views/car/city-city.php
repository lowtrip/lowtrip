<?php
use common\widgets\Breadcrumbs;

$year = date('Y');

$this->title = "$first->city - $second->city в $year: как добраться, маршрут, расстояние, поиск попутчиков, актуально на $year год";

$this->registerMetaTag([
    'name' => 'description',
    'content' => "По маршруту $first->city - $second->city можно сэкономить от {$routeData['economy']} рублей. Расстояние по прямой - {$routeData['distance']} км. Полная информация о расстоянии между городами, попутчики из г. $first->city до г. $second->city, изучение маршрутной карты, способы экономить. Актуальная информация на $year год.",
]);
?>

<div class="car-page car-page--city-city">

    <header class="header header--car-city-city">
        <div class="container">
            <div class="header__content header__content--main">
                <?php echo \Yii::$app->view->renderFile('@app/views/car/templates/car-form.php', [
                    'module' => 'car/search',
                    'style' => 'header__form car-form car-form--black',
                    'first' => $first,
                    'second' => $second,
                ]); ?>
            </div>
        </div>
    </header>

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block shadow">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => 'Путешествия на автомобиле',
                        'url' => ['car/main'],
                    ],
                    [
                        'label' => $first->city,
                        'url' => ['car/city', 'from' => $first->city_slug],
                    ],
                    [
                        'label' => $second->city,
                    ]
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section>
        <div class="container advert">
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/advert/yandex-rs.php', [
                'id' => 'R-A-394883-1',
                'class' => '',
                // 'self' => [
                //     'link' => 'https://sgabs.ru/products/horizonts.php?utm_source=lowtrip&utm_medium=banner&utm_campaign=car',
                //     'image' => '/img/advert/sgabs_top_2.jpg',
                //     'background' => '#000000',
                // ],
            ]); ?>
        </div>
    </section>

    <section class="site-block site-block--text-block">
        <div class="container">
            <div class="site-block__content-block">
                <h1 class="heading--h1"><?=$first->city?> - <?=$second->city?> на автомобиле</h1>
                <h2 class="heading--h3">Как добраться, маршрут, расстояние, расход топлива, цены.</h2>
            </div>
        </div>
    </section>

    <?php echo \Yii::$app->view->renderFile('@app/views/car/templates/car-map.php', [
        'first' => $first,
        'second' => $second,
        'date' => $date,
        'routeData' => $routeData,
    ]); ?>

    <section class="site-block site-block--driver">
        <div class="container">
            <div class="site-block__content site-block__content--driver">
                <img src="/img/car/traveler-girl.png" class="site-block__image-fixed site-block__image--driver site-block__image--driver-girl"/>
                <img src="/img/car/traveler-men.png" class="site-block__image-fixed site-block__image--driver site-block__image--driver-men"/>
                <img src="/img/car/icons/icon_globe.png" class="site-block__image site-block__image--driver-globe"/>

                <div class="site-block__title site-block__title--driver">
                    Планируете поездку на своей машине?
                </div>
                <div class="site-block__text site-block__text--driver">
                    <p>
                        <span class="site-block__text-action--driver">Предложите поездку</span> — пассажиры оплатят
                        до 100% ваших дорожных расходов.
                    </p>
                </div>
                <a href="https://c75.travelpayouts.com/click?shmarker=53486&promo_id=1671&source_type=customlink&type=click&custom_url=https%3A%2F%2Fwww.blablacar.ru%2Foffer-seats%2F1" target="_blank" rel="noreferrer noopener" class="button button--green site-block__button--driver site-block__button--driver-green">
                    Подробнее
                </a>
                <a href="https://c75.travelpayouts.com/click?shmarker=53486&promo_id=1671&source_type=customlink&type=click&custom_url=https%3A%2F%2Fwww.blablacar.ru%2Foffer-seats%2F1" target="_blank" rel="noreferrer noopener" class="button button--white site-block__button--driver site-block__button--driver-white">
                    Подробнее
                </a>
            </div>
        </div>
    </section>



    <section class="site-block site-block--text-block">
        <div class="container">
            <div class="site-block__content-block">
                <h3 class="heading--h3">Справочная информация</h3>
                <ul>
                    <li>Расстояние между <?=$first->city?> - <?=$second->city?> по прямой - <?= $routeData['distance'] ?> км.</li>
                    <li>При скорости движения 60км/ч, без остановок Вы потратите - <?= $routeData['time'] ?></li>
                </ul>
            </div>
            <div class="site-block__content-block">
                <h3 class="heading--h3">Как пользоваться калькулятором расхода топлива?</h3>
                <ul>
                    <li>Введите расход топлива Вашего автомобиля</li>
                    <li>Можно поменять цену за 1 л. топлива</li>
                </ul>
            </div>
            <div class="site-block__content-block">
                <h3 class="heading--h3">Как посчитать экономию?</h3>
                <ul>
                    <li>Рассчитайте общую стоимость топлива</li>
                    <li>Вычтите сумму "Можно сэкономить" </li>
                    <li>Сумма "Можно сэкономить" рассчитывается из соображения, что вы можете взять с собой 4 попутчиков и заработать на этом</li>
                </ul>
            </div>
            <div class="site-block__content-block">
                <h3 class="heading--h3">Когда может пригодиться расчет расстояний?</h3>
                <p>Бесплатный рассчет расстояний между городами показывает точное расстояние между городами и считает кратчайший маршрут с расходом топлива. Он может быть востребован в следующих случаях:</p>
                <ul class="list--text-block">
                    <li>Сервис расчета расстояний помогает проложить маршрут автопутешественнику, например, для летнего отдыха с семьей или при планировании деловой поездки на автомобиле. Зная расход бензина и среднюю цену за литр топлива, нетрудно рассчитать обязательные финансовые затраты в поездке.</li>
                    <li>Водителю-дальнобойщику расчет расстояния между городами позволяет проложить маршрут на карте при подготовке к дальнему рейсу.</li>
                    <li>Калькулятор расстояний пригодится грузоотправителю, чтобы определить километраж и в соответствии с тарифами транспортной компании оценить стоимость грузоперевозки.</li>
                </ul>
            </div>
            <div class="site-block__content-block">
                <h3 class="heading--h3">Алгоритм расчета расстояния между городами</h3>
                <p>Расчет маршрута основан на алгоритме поиска кратчайшего пути во взвешенном графе автодорог (алгоритм Дейкстры). Расстояния определены по точным спутниковым координатам дорог и населенных пунктов. Расчет является результатом компьютерного моделирования, а модели не бывают идеальными, поэтому при планировании маршрута поездки не забудьте заложить резерв.</p>
            </div>
        </div>
    </section>

</div>

