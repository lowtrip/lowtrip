<?php
use yii\helpers\Html;

$date_output = Yii::$app->dateFormatter->toDateString($date);
?>

<section class="car-block--car-map">
    <div class="container">
        <div class="car-map">
            <div class="car-map__header car-map-header">
                <button class="button button--green car-map-header__button active">
                    Водителю
                </button>
                <button class="button button--green car-map-header__button">
                    Пассажиру
                </button>
            </div>
            <div class="car-map__body car-map-body">
                <div class="car-map-body__block active car-map-content" id="car-map-driver">


                    <div class="car-map-content car-route-data">
                        <div class="car-route-data__item car-route-data-item">
                            <div class="car-route-data-item__icon car-route-data-item__icon--map"></div>
                            <div class="car-route-data-item__text">
                                <div class="car-route-data-item__title">Расстояние</div>
                                <div class="car-route-data-item__value" id="route-data-distance"></div>
                            </div>
                        </div>

                        <div class="car-route-data__item car-route-data-item">
                            <div class="car-route-data-item__icon car-route-data-item__icon--clock"></div>
                            <div class="car-route-data-item__text">
                                <div class="car-route-data-item__title">Время в пути</div>
                                <div class="car-route-data-item__value" id="route-data-duration"></div>
                            </div>
                        </div>

                        <div class="car-route-data__item car-route-data-item">
                            <div class="car-route-data-item__icon car-route-data-item__icon--oil"></div>
                            <div class="car-route-data-item__text">
                                <div class="car-route-data-item__title">Общий расход топлива</div>
                                <div class="car-route-data-item__value" id="route-data-oil"></div>
                            </div>
                        </div>

                        <div class="car-route-data__item car-route-data-item">
                            <div class="car-route-data-item__icon car-route-data-item__icon--ruble"></div>
                            <div class="car-route-data-item__text">
                                <div class="car-route-data-item__title">Стоимость топлива</div>
                                <div class="car-route-data-item__value" id="route-data-cost"></div>
                            </div>
                        </div>

                        <div class="car-route-data__item car-route-data-item">
                            <div class="car-route-data-item__icon car-route-data-item__icon--earn-money"></div>
                            <div class="car-route-data-item__text">
                                <div class="car-route-data-item__title">Можно сэкономить</div>
                                <div class="car-route-data-item__value" id="route-data-economy">от <?=$routeData['economy']?> Рублей</div>
                            </div>
                        </div>
                    </div>

                    <div class="car-map-content car-map-content--calculator">
                        <div class="car-calculator">
                            <label class="car-calculator__input-wrapper">
                                <div class="car-calculator__input-title">Средний расход, л\100км</div>
                                <input class="car-calculator__input" id="consumption" type="number" min="1" step="0.5" value="">
                            </label>
                            <label class="car-calculator__input-wrapper">
                                <div class="car-calculator__input-title">Цена за 1л, руб.</div>
                                <input class="car-calculator__input" id="fuel-cost" type="number" min="1" step="0.5" value="">
                            </label>

                            <button id="recalculate" class="button button--green car-calculator__button">Пересчитать</button>
                        </div>
                    </div>


                    <div class="car-map-content--column">
                        <div class="car-map-content__header">
                            <h2 class="heading--h3">Маршрут <?=$first->city?> - <?=$second->city?> на карте</h2>
                        </div>
                        <div class="car-map-content__body">
                            <div class="car-map-content__map " id="map"></div>
                        </div>
                    </div>
                    <div class="car-map-content--column">
                        <div class="car-map-content__header">
                            <h2 class="heading--h3">Как добраться <?=$first->city?> - <?=$second->city?> на автомобиле</h2>
                        </div>
                        <div class="car-map-content__body">
                            <div class="car-map-route-table" >
                                <div class="car-map-route-table__header">
                                    <div class="car-map-route-table__row">
                                        <div class="car-map-route-table__col">Место</div>
                                        <div class="car-map-route-table__col">Действие</div>
                                        <div class="car-map-route-table__col">Расстояние</div>
                                        <div class="car-map-route-table__col">Время в пути</div>
                                    </div>
                                </div>
                                <ul class="car-map-route-table__body" id="car-map-route-table"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="car-map-body__block" id="car-map-passenger">
                    <div class="car-map-content">
                        <div class="car-map-content__header">
                            <h2 class="heading--h3">Доехать с попутчиками по маршруту <?=$first->city?> - <?=$second->city?></h2>

                            <div class="cities cities--car-rides">
                                <div class="cities-form">
                                    <div class="form-group-block inputs-group">
                                        <div class="form-input-block form-input-block-medium">
                                            <div class="group has_select">
                                                <?= Html::input('hidden', 'date', $date,['id' => 'date-start', 'readonly' => 'readonly']) ?>
                                                <?= Html::input('text', 'date_formatted', $date_output, ['id' => 'date-input', 'class' => 'with_help pointer form_ui_input', 'readonly' => 'readonly']) ?>
                                                <span class="help top">Когда едем?</span>
                                            </div>
                                        </div>
                                        <div class="form-input-block form-input-block-medium">
                                            <div class="group menu-helper has_ui has_select">
                                                <?= Html::input('hidden', 'people', 1,['id' => 'people', 'class' => 'helper-value', 'readonly' => 'readonly']) ?>
                                                <input readonly="readonly" class="with_help helper-text pointer form_ui_input" type="text" id="people_text" value="1 Пассажир">
                                                <span class="help top">Сколько вас?</span>
                                                <ul class="dropdown-helper ui-element" id="people-select">
                                                    <li data-value="1">1 Пассажир</li>
                                                    <li data-value="2">2 Пассажира</li>
                                                    <li data-value="3">3 Пассажира</li>
                                                    <li data-value="4">4 Пассажира</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group-block">
                                        <button class="btn_go" id="car-find">
                                            Найти поездку
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="car-map-content__progressbar">
                            <div id="progressbar" class="lowtrip-progressbar"></div>
                        </div>

                        <div class="car-map-content__body">


                            <ul class="car-rides" id="car-rides"></ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php
$script = <<< JS

initDatepicker(180, 0);

const progressbar = new Progressbar('progressbar', {
    defaultMessage: '',
});

progressbar.hide();

const stopProgressbar = (runProgressbar) => {
    clearInterval(runProgressbar);
    progressbar.setValue(0);
    progressbar.hide();
};

const searchRides = () => {
    progressbar.show();
    
    let runProgressbar = setInterval(() => {
        progressbar.addValue(100);
    }, 100);
    
    $.ajax({
        url: '/api/partners/blablacar/search',
        dataType: 'json',
        data: {
            fn: "$first->city",
            tn: "$second->city",
            db: $('#date-start').val(),
            seats: $('#people').val(),
            gmt: "$first->GMT",
            limit: 50,
        },
        type: 'GET',
        success: function(response){
            if (response.trips && response.trips.length) {
                buildRidesSuccess(response);
            } else {
                buildRidesFail();
            }
            
            stopProgressbar(runProgressbar);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            
            stopProgressbar(runProgressbar);
        }
   });
};

searchRides();

const carMapData = {
    distance: {},
    duration: {},
    fuelCost: 40,
    fuelCostTotal: 0,
    fuelLitres: 0,
    consuption: 10,
    segments: [],    
};

$('#consumption').val(carMapData.consuption);
$('#fuel-cost').val(carMapData.fuelCost);

const recalculate = () => {
  carMapData.fuelCost = parseFloat($('#fuel-cost').val());  
  carMapData.consuption = parseFloat($('#consumption').val());
  
  calculateRouteData();
};

const calculateRouteData = () => {
    carMapData.fuelLitres = Math.round(carMapData.consuption * carMapData.distance.value / 100000);
    carMapData.fuelCostTotal = Math.round(carMapData.fuelLitres * carMapData.fuelCost);
    
    $('#route-data-distance').text(carMapData.distance.text);
    $('#route-data-duration').text(carMapData.duration.text);
    $('#route-data-oil').text(carMapData.fuelLitres + ' литров');
    $('#route-data-cost').text(carMapData.fuelCostTotal + ' рублей');
};

$(document).on('click', '#car-find', searchRides);

$(document).on('click', '.car-map-header__button', function() {
   let index = $(this).index();
   
   $(this).addClass('active').siblings('.car-map-header__button').removeClass('active');
   
   $('.car-map__body').find('.car-map-body__block').eq(index).addClass('active').siblings().removeClass('active');
});

$(document).on('click', '#recalculate', recalculate);


const coords = {
    0: {
      lat: $first->lat,
      lng: $first->lng,
    },
    1: {
      lat: $second->lat,
      lng: $second->lng,
    },
};

const buildTable = (carMapData) => {
    const table = document.getElementById('car-map-route-table');
    
    table.innerHTML = '';
    
    let routeSegments = [];
    
    carMapData.segments.forEach(segment => {
        let template = `
        <li class="car-map-route-table__row" data-index=`+ segment.index +`>
            <div class="car-map-route-table__col car-map-route-table__street">` + segment.street + `</div>
            <div class="car-map-route-table__col car-map-route-table__action">` + segment.action.text + `</div>
            <div class="car-map-route-table__col car-map-route-table__distance">` + segment.distance.text + `</div>
            <div class="car-map-route-table__col car-map-route-table__duration">` + segment.duration.text + `</div>
        </li>
        `;
        routeSegments.push(template);
    });
    
    table.innerHTML = routeSegments.join('');
};

const buildRidesFail = () => {
    const table = document.getElementById('car-rides');
    
    table.innerHTML = `<div class="car-rides__fail">На выбранную дату водители еще не создали поездки. Попробуйте начать поиск на другую дату.</div>`;
};

const buildRidesSuccess = (data) => {
    const table = document.getElementById('car-rides');
    
    table.innerHTML = '';
    
    let routeSegments = [];
    
    data.trips.forEach(a => {
       let template = `
       <li class="car-rides__item car-rides-item">
            <div class="car-rides-item__info">
                <div class="car-rides-item__date">
                    `+ a.departure_date +`
                </div>
                <div class="car-rides-item__cities">
                    <span class="car-rides-item__city">`+ a.departure_place.city_name +`</span>
                    <span class="car-rides-item__city">`+ a.arrival_place.city_name +`</span>
                </div>
                <div class="car-rides-item__points">
                    <div class="car-rides-item__point">`+ a.departure_place.address +`</div>
                    <div class="car-rides-item__point">`+ a.arrival_place.address +`</div>
                </div>
            </div>
            <div class="car-rides-item__action">
                <div class="car-rides-item__places">
                    <span class="car-rides-item__places-count">`+ a.seats +`</span> свободных мест
                </div>
                <div class="car-rides-item__cost">
                    <span class="car-rides-item__cost-value">`+ a.price.value +`</span> рублей
                </div>
                <a class="button button--green car-rides-item__button" href="`+ data.link +`" target="_blank" rel="nofollow noreferrer noopener">
                    Подробнее
                </a>
            </div>
        </li>
       `; 
       
       routeSegments.push(template);
    });
    
    table.innerHTML = routeSegments.join('');
};

const activeRouteChange = multiRoute => {
    let activeRoute = multiRoute.getActiveRoute();
        
    carMapData.distance = activeRoute.properties.get('distance');
    carMapData.duration = activeRoute.properties.get('durationInTraffic');
    carMapData.segments = [];
    
    let activeRoutePaths = activeRoute.getPaths(); 
    
    activeRoutePaths.each(path => {
        let segments = path.getSegments();
        
        segments.each(segment => {
            carMapData.segments.push({
                self: segment,
                index: segment.properties.get('index'),
                action: segment.properties.get("action"),
                distance: segment.properties.get("distance"),
                duration: segment.properties.get("duration"),
                street: segment.properties.get("street")
            });
        });
        
        buildTable(carMapData);
    });
    
    calculateRouteData();
};

ymaps.ready(() => {
    const myMap = new ymaps.Map('map', {
        center: [coords[0].lat, coords[0].lng],
        zoom: 9,
        controls: [],
    });
    
    let multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: [
            [coords[0].lat, coords[0].lng],
            [coords[1].lat, coords[1].lng]
        ],
        params: {
            results: 2,
            routingMode: 'auto',
        },
    }, {
        boundsAutoApply: true
    });
    
    multiRoute.events
    .add('activeroutechange', () => {
        activeRouteChange(multiRoute);
    });
    
    myMap.geoObjects.add(multiRoute);   
});


JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>