<?php
use common\widgets\Breadcrumbs;
use yii\helpers\Url;
use yii\helpers\Html;
?>

<header class="header header--car-city header--fullscreen">
    <div class="container">
        <div class="header__content header__content--main">
            <h1 class="header__title heading--h1"><?=$city->city?>: путешествия на автомобиле</h1>

            <p class="header__text">
                Рассчет расстояния и затрат средств на путешествия из г.<?=$city->city?> в любую точку России.
            </p>

            <?php echo \Yii::$app->view->renderFile('@app/views/car/templates/car-form.php', [
                'module' => 'car/search',
                'style' => 'header__form car-form car-form--black',
                'first' => $city
            ]); ?>
        </div>
    </div>
</header>

<!--Breadcrumbs-->
<section class="breadcrumbs_block shadow">
    <div class="container">
        <?php
        echo Breadcrumbs::widget([
            'links' => [
                [
                    'label' => 'Путешествия на автомобиле',
                    'url' => ['car/main'],
                ],
                [
                    'label' => $city->city,
                ],
            ],
        ]);
        ?>
    </div>
</section>
<!--Breadcrumbs-->

<section class="site-block site-block--about-service">
    <div class="site-block__mask site-block__mask--grey">
        <div class="container">
            <div class="site-block__content site-block__content--about-service">
                Сервисом Lowtrip ежегодно пользуются более 100 000 человек,<br>
                подбирая самые выгодные билеты для путешествий. Чтобы купить билет, надо выбрать нужный маршрут, выбрать подходящий
                вариант и перейти на страницу оплаты: часть оплат происходит на нашем сайте, часть на стороне партнера.
            </div>
        </div>
    </div>
</section>

<section class="site-block site-block--city-distance-table">
    <div class="container">
        <h2 class="heading--h1 site-block__title site-block__title--city-distance-table">
            <?=$city->city?>: Таблица расстояний
        </h2>
        <p>
            Самые популярные направления, запрашиваемые пользователями за последние 7 дней.
        </p>

        <div class="site-block__tables-container tables-container">
            <div class="city-distance-table">
                <div class="city-distance-table__head city-distance-table__head--yellow city-distance-table__row">
                    <div class="city-distance-table__column city-distance-table__column--headcol">Откуда</div>
                    <div class="city-distance-table__column city-distance-table__column--headcol">Куда</div>
                    <div class="city-distance-table__column city-distance-table__column--headcol">Расстояние</div>
                </div>
                <div class="city-distance-table__body">
                    <?php
                    foreach ($popularFrom as $k => $row)
                    {
                        $url =  Url::toRoute(['car/build', 'from' => $city->city_slug, 'to' => $row['arrival']['city_slug']]);

                        echo Html::beginTag('a', ['class' => 'city-distance-table__row', 'href' => $url]);
                        echo Html::tag('div', $city->city, ['class' => 'city-distance-table__column city-distance-table__column--bodycol']);
                        echo Html::tag('div', $row['arrival']['city'], ['class' => 'city-distance-table__column city-distance-table__column--bodycol']);
                        echo Html::tag('div', "{$row['distance']} км", ['class' => 'city-distance-table__column city-distance-table__column--bodycol']);
                        echo Html::endTag('a');
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="site-block site-block--city-popular-routes">
    <div class="container">
        <div class="popular-items popular-items--black">
            <h2 class="heading--h1 site-block__title site-block__title--city-popular-routes">
                Популярные маршруты из г.<?=$city->city?>
            </h2>
            <ul class="popular-items-list">
                <?php
                foreach ($popularFrom as $k => $row)
                {
                    $url =  Url::toRoute(['car/build', 'from' => $city->city_slug, 'to' => $row['arrival']['city_slug']]);
                    echo '<li class="popular-items-list__item"><a class="popular-items-list__link popular-items-list__link--car-city" href="'.$url.'"><span>'.$city->city.'</span><span>'.$row['arrival']['city'].'<span></a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</section>

<section class="site-block site-block--text-block">
    <div class="container">
        <div class="site-block__content-block">
            <h3 class="heading--h3">Когда может пригодиться расчет расстояний?</h3>
            <p>Бесплатный рассчет расстояний между городами показывает точное расстояние между городами и считает кратчайший маршрут с расходом топлива. Он может быть востребован в следующих случаях:</p>
            <ul class="list--text-block">
                <li>Сервис расчета расстояний помогает проложить маршрут автопутешественнику, например, для летнего отдыха с семьей или при планировании деловой поездки на автомобиле. Зная расход бензина и среднюю цену за литр топлива, нетрудно рассчитать обязательные финансовые затраты в поездке.</li>
                <li>Водителю-дальнобойщику расчет расстояния между городами позволяет проложить маршрут на карте при подготовке к дальнему рейсу.</li>
                <li>Калькулятор расстояний пригодится грузоотправителю, чтобы определить километраж и в соответствии с тарифами транспортной компании оценить стоимость грузоперевозки.</li>
            </ul>
        </div>
        <div class="site-block__content-block">
            <h3 class="heading--h3">Алгоритм расчета расстояния между городами</h3>
            <p>Расчет маршрута основан на алгоритме поиска кратчайшего пути во взвешенном графе автодорог (алгоритм Дейкстры). Расстояния определены по точным спутниковым координатам дорог и населенных пунктов. Расчет является результатом компьютерного моделирования, а модели не бывают идеальными, поэтому при планировании маршрута поездки не забудьте заложить резерв.</p>
        </div>
    </div>
</section>