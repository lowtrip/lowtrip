<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Breadcrumbs;

$this->title = 'Все для путешествия '.$first->city.' - '.$second->city.': самостоятельный тур за копейки';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мы подобрали всё нужное для самостоятельного путешествия '.$first->city.' - '.$second->city.'. Путешествовать по России теперь стало проще. На этой странице есть: как добраться до г.'.$second->city.' из г.'.$first->city.', отзывы путешественников и жителей, жилье и еще подсказываем что посмотреть в г. '.$second->city.' - даем бесплатные аудиогиды.',
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Уау, смотри как дёшево стоит тур '.$first->city.' - '.$second->city,
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => 'Учтено всё самое нужное для хорошего путешествия: трансфер, жильё, гиды. Самые дешёвые варианты!',
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'https://lowtrip.ru/'.$second->info->img_url,
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => 'https://lowtrip.ru'.Url::to(),
]);
$this->registerMetaTag([
    'property' => 'og:site_name',
    'content' => 'Lowtrip',
]);
$this->registerMetaTag([
    'property' => 'og:locale',
    'content' => 'ru_RU',
]);

?>
<section class="main_bg top" id="main-header">

    <div class="topline container" id="topline">
        <div class="row">
            <div class="col-xs-2 col-md-2 mp_10">
                <div class="logo">
                    <div class="logo_lowtrip">
                        <a href="<?=Yii::$app->homeUrl?>"><img src="/img/logo/lowtrip_166.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-10 col-md-8 mp_10">
                <?php echo \Yii::$app->view->renderFile('@app/views/templates/trip-information.php', ['style' => 'white', 'first' => $first, 'second' => $second, 'people' => $people, 'date' => $date]); ?>
            </div>
        </div>
    </div>

    <div class="content" id="search-form">
        <div class="wrapper">
            <!--START: Cities standart form-->
            <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'trip/search', 'logo' => true, 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people]); ?>
            <!--END: Cities standart form-->

            <div class="recommended mobile">
                <div>
                    <div class="top_cities_text">
                        Рекомендуем поехать
                    </div>
                </div>
                <div>
                    <div class="scroll-block top-cities-scroll">
                        <div class="scroll-content">
                            <ul class="top_cities_list" id="top_cities_list">
                                <?php
                                foreach ($cities as $k => $city)
                                {
                                    echo '<li><span class="top_city_btn city_btn_change" data-index="'.$k.'" onclick="yaCounter44396998.reachGoal(\'city-btn-'.$k.'\'); return true;">'.$city->city.'</span></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="after_topline">

    <!--Breadcrumbs-->
    <section class="breadcrumbs_block">
        <div class="container">
            <?php
            echo Breadcrumbs::widget([
                'links' => [
                    [
                        'label' => $first->city,
                        'url' => ['trip/city', 'from' => $first->city_slug],
                    ],
                    [
                        'label' => $second->city,
                        //'url' => ['trip/build', 'from' => $first->city_slug, 'to' => $second->city_slug],
                    ]
                ],
            ]);
            ?>
        </div>
    </section>
    <!--Breadcrumbs-->

    <section class="top_cities white desktop">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="top_cities_text">
                        Рекомендуем поехать
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="scroll-block top-cities-scroll">
                        <div class="scroll-content">
                            <ul class="top_cities_list" id="top_cities_list">
                                <?php
                                    foreach ($cities as $k => $city)
                                    {
                                        echo '<li><span class="top_city_btn city_btn_change" data-index="'.$k.'" onclick="yaCounter44396998.reachGoal(\'city-btn-'.$k.'\'); return true;">'.$city->city.'</span></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="city-review after_top">
        <div class="container">
            <h1>
                Путешествие<br>
                <span><?= $first->city?> - <?= $second->city?></span>
            </h1>

            <div class="trip_page_header">
                <div class="row">
                    <div class="col-md-4">
                        <img class="hello-image" src="/img/emoji/enot.jpg" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="after_image">
                            <p class="title">Приветствуем тебя ЛОУТРИППЕР <img class="emoji_icon" src="/img/emoji/v.png" alt=""></p>
                            <p>Давай расскажем тебе о сервисе. Мы хотим сделать путешествия по России доступными. Средняя цена путешествий у нас составляет 1500 рублей</p>
                            <p>Кстати, мы уже посчитали и подобрали для тебя самые дешевые варианты транспорта, проживания, аудиогидов и квестов.</p>
                            <p>Удачного путешествия!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--START: Trip-->
    <section class="sm_50">
        <div class="container">
            <div class="row offers" id="trip-offers">
                <div class="col-sm-4">

                    <div class="trip-offer multiple">
                        <h3>САМАЯ ДЕШЁВАЯ ПОЕЗДКА</h3>

                        <div class="switcher">
                            <div class="switcher-btn active" data-item="car">
                                Авто
                            </div>
                            <div class="switcher-btn" data-item="train">
                                РЖД
                            </div>
                        </div>

                        <a id="car" class="trip-offer-item active" href="" target="_blank" onclick="yaCounter44396998.reachGoal('blablacar'); yaCounter44396998.reachGoal('blablacar-trip'); return true;">

                            <div class="content">
                            </div>

                            <div class="trip-loader">
                                Поиск поездки
                                <div class="cssload-progress cssload-float cssload-shadow">
                                    <div class="cssload-progress-item"></div>
                                </div>
                            </div>
                        </a>

                        <a id="train" class="trip-offer-item" href="" target="_blank" onclick="yaCounter44396998.reachGoal('train'); return true;">

                            <div class="content">
                            </div>

                            <div class="trip-loader">
                                Поиск ЖД билетов
                                <div class="cssload-progress cssload-float cssload-shadow">
                                    <div class="cssload-progress-item"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="trip-offer" id="house">
                        <h3>САМОЕ ДЕШЁВОЕ ЖИЛЬЁ</h3>
                        <a class="trip-offer-item" href="" target="_blank" onclick="yaCounter44396998.reachGoal('onetwotrip'); return true;">

                            <div class="content">
                            </div>

                            <div class="trip-loader">
                                <span id="loader-status">Поиск жилья</span>
                                <div class="cssload-progress cssload-float cssload-shadow">
                                    <div class="cssload-progress-item"></div>
                                </div>
                            </div>

                        </a>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="trip-offer" id="audiogid">
                        <h3>БЕСПЛАТНЫЕ АУДИОГИДЫ</h3>
                        <a class="trip-offer-item" href="https://izi.travel/ru/search" target="_blank" onclick="yaCounter44396998.reachGoal('izitravel'); return true;">
                            <div class="content">
                            </div>

                            <div class="trip-loader">
                                <span id="loader-status">Поиск аудиогидов</span>
                                <div class="cssload-progress cssload-float cssload-shadow">
                                    <div class="cssload-progress-item"></div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="offer-check" id="price" style="display: none;">
            <div class="pricelist">
                <table class="pricelist-table">
                    <tr>
                        <td class="icon_td">
                            <img src="/img/icons/car-compact.png" alt="">
                        </td>
                        <td class="price_td">
                            <div class="big">
                                <span id="price-car"></span> рублей
                            </div>
                            <div class="small">
                                поездка
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="icon_td">
                            <img src="/img/icons/house.png" alt="">
                        </td>
                        <td class="price_td">
                            <div class="big">
                                <span id="price-house"></span> рублей
                            </div>
                            <div class="small">
                                жильё
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="icon_td">
                            <img src="/img/icons/coin.png" alt="">
                        </td>
                        <td class="price_td">
                            <div class="big">
                                <span id="total-price"></span> рублей
                            </div>
                            <div class="small">
                                стоимость
                            </div>
                        </td>
                    </tr>
                </table>
                <button class="save_trip to_email_popup" onclick="yaCounter44396998.reachGoal('save-trip'); return true;">
                    <span>Сохранить маршрут</span>
                </button>
            </div>
        </div>
    </div>
    <!--END: Trip-->

    <!--START: Quests-->
    <section class="places quests sm_50" id="quests" style="display:none;">
        <div class="container">
            <h3 class="section_title">ТЕБЕ НЕ БУДЕТ СКУЧНО! ПОПРОБУЙ КВЕСТ-ЭКСКУРСИИ ПО ГОРОДУ</h3>
            <div class="row">
                <div class="col-xs-12">
                    <div class="places_block">
                        <div class="owl-carousel places_container slider2" id="quests_container">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END: Quests-->

    <div class="section_information">

    <section class="city-review sm_50">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="section_title">НУ КАК? РЕШИЛСЯ?<br>
                        ПОЧИТАЙ ИНФОРМАЦИЮ О ГОРОДЕ, ВДРУГ ПРИГОДИТСЯ
                    </h3>
                </div>
            </div>

            <div class="city-block">
                <div class="row">
                    <div class="col-md-6">
                        <div class="city-infoblock">
                            <img class="city-image" src="/<?= $second->info->img_url?>" alt="">
                            <div class="city-text-block parent">
                                <div class="city-text item-content">
                                    <?php
                                    echo $text = $second->info->text;
                                    $text_link = '';
                                    if (strlen($text) > 300) {
                                        $text_link = '<span onclick="yaCounter44396998.reachGoal(\'about-city\'); return true;" class="fulltext-link show-full-content">Подробнее<span>';
                                    }
                                    ?>
                                </div>
                                <?= $text_link ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <?php
                        if (!empty($second->reviews)) {
                            echo \Yii::$app->view->renderFile('@app/views/templates/reviews.php', ['reviews' => $second->reviews]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--START: Places-->
    <section class="places sm_50">
        <div class="content">
            <div class="container">
                <h3 class="section_title">ДОСТОПРИМЕЧАТЕЛЬНОСТИ</h3>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="places_block" id="places_block">
                            <div class="owl-carousel places_container slider2" id="places_container"></div>

                            <div class="cssload-progress cssload-float cssload-shadow" id="place_loader">
                                <div class="cssload-progress-item"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END: Places-->
</div>

    <?php
        if (!empty($seotext->text)) {
            echo \Yii::$app->view->renderFile('@app/views/templates/seotext.php', ['seotext' => $seotext->text, 'first' => $first->city, 'second' => $second->city]);
        }
    ?>

    <!--START: Other trips-->
    <section class="top_cities white tours">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="top_cities_text">
                        Куда поехать дальше?
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="scroll-block top-cities-scroll">
                        <div class="scroll-content">
                            <ul class="top_cities_list">
                                <?php
                                foreach ($next as $k => $city)
                                {
                                    $url =  Url::toRoute(['trip/build', 'from' => $second->city_slug, 'to' => $city->city_slug]);
                                    $name = $city->city;
                                    echo '<li><a class="top_city_btn" href="'.$url.'">'.$name.'</a></li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--END: Other trips-->

    <!--START: Partners-->
    <?php echo \Yii::$app->view->renderFile('@app/views/templates/partners.php'); ?>
    <!--END: Partners-->
    <!--START: Footer-->
    <?php echo \Yii::$app->view->renderFile('@app/views/templates/footer.php'); ?>
    <!--END: Footer-->
</div>

<!--START: Find Trip-->
<?php
$script = <<< JS
    tripScenario();
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>