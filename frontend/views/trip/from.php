<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\Breadcrumbs;

$this->title = 'Куда поехать из г.'.$first->city;

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Мы подобрали для вас список всех вариантов путешествий из г. '.$first->city.' в ближайшие города.',
]);

?>
    <section class="main_bg top" id="main-header">

        <div class="topline container" id="topline">
            <div class="row">
                <div class="col-xs-2 col-md-2 mp_10">
                    <div class="logo">
                        <div class="logo_lowtrip">
                            <a href="<?=Yii::$app->homeUrl?>"><img src="/img/logo/lowtrip_166.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10 col-md-8 mp_10">
                    <?php echo \Yii::$app->view->renderFile('@app/views/templates/trip-information.php', ['first' => $first, 'second' => $second, 'people' => $people]); ?>
                </div>
            </div>
        </div>

        <div class="content" id="search-form">
            <div class="wrapper">
                <!--START: Cities standart form-->
                <?php echo \Yii::$app->view->renderFile('@app/views/templates/cities-standart.php', ['module' => 'trip/search', 'logo' => true, 'first' => $first, 'second' => $second, 'date' => $date, 'people' => $people]); ?>
                <!--END: Cities standart form-->

                <div class="recommended mobile">
                    <div>
                        <div class="top_cities_text">
                            Рекомендуем поехать
                        </div>
                    </div>
                    <div>
                        <div class="scroll-block top-cities-scroll">
                            <div class="scroll-content">
                                <ul class="top_cities_list" id="top_cities_list">
                                    <?php
                                    foreach ($cities as $k => $city)
                                    {
                                        echo '<li><span class="top_city_btn city_btn_change" data-index="'.$k.'" onclick="yaCounter44396998.reachGoal(\'city-btn-'.$k.'\'); return true;">'.$city->city.'</span></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="after_topline">
        <!--Breadcrumbs-->
        <section class="breadcrumbs_block">
            <div class="container">
                <?php
                echo Breadcrumbs::widget([
                    'links' => [
                        [
                            'label' => $first->city,
                        ],
                    ],
                ]);
                ?>
            </div>
        </section>
        <!--Breadcrumbs-->

        <div class="trip_page_header">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <p class="title">Тур выходного дня из г. <?=$first->city?></p>
                        <p>
                            На этой странице Вы можете собрать свой тур выходного дня и сэкономить на путешествии.
                        </p>
                        <p>
                            В обычном экскурсионном бюро Вы платите за: трансфер, жилье и гида. Но все это можно найти отдельно, экономя до 30%. Никаких агентских сборов.
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <section class="top_cities white desktop">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="top_cities_text">
                            Рекомендуем поехать
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="scroll-block top-cities-scroll">
                            <div class="scroll-content">
                                <ul class="top_cities_list" id="top_cities_list">
                                    <?php
                                    foreach ($topCities as $k => $city)
                                    {
                                        echo '<li><span class="top_city_btn city_btn_change" data-index="'.$k.'" onclick="yaCounter44396998.reachGoal(\'city-btn-'.$k.'\'); return true;">'.$city->city.'</span></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="city-review after_top">
            <div class="container">
                <h1>Список недорогих маршрутов из г.<?= $first->city?></h1>

                <div class="row">
                    <ul class="city-route-list">
                        <?php
                        foreach ($cities as $city)
                        {
                            $url =  Url::toRoute(['trip/build', 'from' => $first->city_slug, 'to' => $city->city_slug]);
                            $name = $first->city.' - '.$city->city;
                            echo '<li class="col-sm-4"><a class="" href="'.$url.'">'.$name.'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </section>

        <!--START: Partners-->
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/partners.php'); ?>
        <!--END: Partners-->
        <!--START: Footer-->
        <?php echo \Yii::$app->view->renderFile('@app/views/templates/footer.php'); ?>
        <!--END: Footer-->
    </div>
    <!--START: Find Trip-->
<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(6, offset);
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>