<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TravelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Путешествия';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="travel-index wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'travel_id',
                        //'user_id',
                        'first_city',
                        'second_city',
                        'car_price',
                        [
                            'attribute' => 'car_link',
                            'value' => function ($model) {
                                return '<a href="'.$model->car_link.'" target="_blank">Перейти</a>';
                            },
                            'format' => 'raw'
                        ],

                        'house_price',
                        [
                            'attribute' => 'house_link',
                            'value' => function ($model) {
                                return '<a href="'.$model->house_link.'" target="_blank">Перейти</a>';
                            },
                            'format' => 'raw'
                        ],
                        'food_price',
                        'total_price',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{delete}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>