<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */
/* @var $form ActiveForm */

$this->title = 'Мой профиль';
?>
<div class="site-profile wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'first_name') ?>
                <?= $form->field($model, 'second_name') ?>
                <?= $form->field($model, 'middle_name') ?>

                <div class="form-group">
                    <?= Html::submitButton('Редактировать', ['class' => 'btn btn-primary']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div><!-- site-profile -->