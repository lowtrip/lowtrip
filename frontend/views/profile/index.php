<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Profile */

/* @var $searchModel common\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мой профиль';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1><?= Html::encode($this->title) ?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'user_id',
                        //'avatar',
                        'first_name',
                        'second_name',
                        'middle_name',
                    ],
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <p>
                    <?= Html::a('Редактировать', ['edit'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
        </div>
    </div>






</div>
