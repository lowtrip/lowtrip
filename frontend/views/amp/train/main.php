<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use common\widgets\Breadcrumbs;
?>

<header>
    <h1>Ж/Д билеты на поезд без очередей</h1>
    <h3>
        Мгновенное бронирование.<br>
        Ищем самые недорогие билеты на поезд в интернете
    </h3>
    <figure>
        <img src="https://lowtrip.ru/img/header/train-dark.jpg">
    </figure>
    <menu>
        <a href="https://lowtrip.ru">Путешествия</a>
        <a href="https://lowtrip.ru/train">ЖД Билеты</a>
        <a href="https://lowtrip.ru/car">Путешествия на автомобиле</a>
        <a href="https://lowtrip.ru/bus">Билеты на автобус</a>
    </menu>
</header>

<section class="popular_trains">
    <div class="popular_trains_card popular_trains_card_red">
        <h2 class="popular_trains_title">
            <span>Как купить билет недорого?</span>
        </h2>
        <p>
            1. Покупайте билеты на поезда заранее. ОАО «РЖД» начинает продажи билетов за 90 дней.
        </p>
        <p>
            2. На проходящие направления цены ниже, чем на прямые рейсы.
        </p>
        <p>
            3. Следите за скидками. Мы публикуем их у себя в <a href="https://t.me/rzdticket" target="_blank">группе</a>.
        </p>
        <p>
            4. Самые дешевые билеты на ж/д обычно на боковые верхние места и места возле туалета.
        </p>
    </div>

    <div class="popular_trains_card ">
        <h3 class="popular_trains_title">
            <span>Популярные направления</span>
        </h3>
        <ul class="popular_trains_list">
            <?php
            if (!empty($popular_routes)) {
                foreach ($popular_routes as $k => $route)
                {
                    $minPricePart = $route['minPrice'] ? ' от '.$route['minPrice'].' руб.' : '';
                    $url =  Url::toRoute(['train/build', 'from' => $route['departure']['city_slug'], 'to' => $route['arrival']['city_slug']]);
                    echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$route['departure']['city'].' &#8594; '.$route['arrival']['city'].''.$minPricePart.'</a></li>';
                }
            }
            ?>
        </ul>
    </div>

    <div class="popular_trains_card">
        <h3 class="popular_trains_title">
            <span>Популярные станции</span>
        </h3>
        <ul class="popular_trains_list">
            <?php
            if (!empty($popular_cities)) {
                foreach ($popular_cities as $k => $city)
                {
                    $url =  Url::toRoute(['train/city', 'from' => $city->city->city_slug]);
                    echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$city->city->city.'</a></li>';
                }
            }
            ?>
        </ul>
    </div>
</section>

<section class="sm_50">
    <div class="trains_block dark_bg">
        <h4 class="trains_block_title">О сервисе Lowtrip</h4>
        <p>
            Сервис Lowtrip автоматически подбирает самые дешевые варианты для путешествий.
        </p>
        <p>
            Мы находим дешевые билеты на поезд, доступные места на авто и автобусах, а также жилье и даем бесплатные аудиогиды по городу.
        </p>
    </div>
</section>