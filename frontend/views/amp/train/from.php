<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use common\widgets\Breadcrumbs;

?>
<div class="train_city_page">

    <header>
        <h1>РЖД билеты из г. <?=$first->city?></h1>
        <figure>
            <img src="https://lowtrip.ru/img/header/train-dark.jpg">
        </figure>
        <menu>
            <a href="https://lowtrip.ru">Путешествия</a>
            <a href="https://lowtrip.ru/train">ЖД Билеты</a>
            <a href="https://lowtrip.ru/car">Путешествия на автомобиле</a>
            <a href="https://lowtrip.ru/bus">Билеты на автобус</a>
        </menu>
    </header>

    <section class="popular_trains">
        <h3>
            Выберите и оформите ЖД билет<br>
            в любое направление онлайн
        </h3>

        <div class="popular_trains_card popular_trains_card_main">
            <h2 class="popular_trains_title">
                <span>Популярные направления</span>
            </h2>
            <p>
                Хотите отправиться в путешествие на поезде из г.<?=$first->city?>?
            </p>
            <p>
                Мы отобрали самые популярные направления движения поездов дальнего следования, чтобы вы смогли легко найти расписание поездов и цены на жд билеты!
            </p>
        </div>

        <div class="popular_trains_card">
            <h3 class="popular_trains_title">
                <span>Из г. <?=$first->city?></span>
            </h3>
            <ul class="popular_trains_list">
                <?php
                if (!empty($popular_from)) {
                    foreach ($popular_from as $k => $row)
                    {
                        $minPricePart = $row['minPrice'] ? ' от '.$row['minPrice'].' руб.' : '';
                        $url =  Url::toRoute(['train/build', 'from' => $first->city_slug, 'to' => $row['arrival']['city_slug']]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$first->city.' &#8594; '.$row['arrival']['city'].''.$minPricePart.'</a></li>';
                    }
                }
                else {
                    foreach ($topCities as $k => $city)
                    {
                        $url =  Url::toRoute(['train/build', 'from' => $first->city_slug, 'to' => $city->city_slug]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$first->city.' &#8594; '.$city->city.'</a></li>';
                    }
                }
                ?>
            </ul>
        </div>

        <div class="popular_trains_card">
            <h3 class="popular_trains_title">
                <span>В г. <?=$first->city?>
            </h3>
            <ul class="popular_trains_list">
                <?php
                if (!empty($popular_to)) {
                    foreach ($popular_to as $k => $row)
                    {
                        $minPricePart = $row['minPrice'] ? ' от '.$row['minPrice'].' руб.' : '';
                        $url =  Url::toRoute(['train/build', 'from' => $row['departure']['city_slug'], 'to' => $first->city_slug]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$row['departure']['city'].' &#8594; '.$first->city.''.$minPricePart.'</a></li>';
                    }
                }
                else {
                    foreach ($topCities as $k => $city)
                    {
                        $url =  Url::toRoute(['train/build', 'from' => $city->city_slug, 'to' => $first->city_slug]);
                        echo '<li><a class="popular_trains_list_link" href="'.$url.'">'.$city->city.' &#8594; '.$first->city.'</a></li>';
                    }
                }
                ?>
            </ul>
        </div>
    </section>

    <button
        formaction="<?php echo Url::toRoute(['train/city', 'from' => $first->city_slug]); ?>"
        data-background-color="#5cb85c"
        data-color="#FFFFFF"
        data-turbo="false"
    >
        Найти билеты
    </button>

    <?php
    $txt = explode(";", $page->seotext);
    ?>

    <section class="sm_50">
        <div class="trains_block">
            <h4 class="trains_block_title">Как купить недорогой билет из г. <?=$first->city?>. Советы путешественников.</h4>
            <p>
                Больше не нужно стоять у кассы в очереди, всю информацию о ж/д билетах на поезд можно узнать онлайн. Наша задача находить самые <?=$txt[3]?> билеты на наземный транспорт, работаем через официальных партнеров ОАО “РЖД” и OneTwoTrip. Для <?=$txt[4]?> билета Вы будете перенаправлены на сайт партнера.
            </p>
            <p class="advice">
                <b>Совет №1.</b><br>
                Чтобы найти самый недорогой билет старайтесь брать заранее, ОАО “РЖД” начинает продажи за 90 дней до отправления.
            </p>
            <p class="advice">
                <b>Совет №2.</b><br>
                При покупке обращайте внимание не только на цену, но и на тип поезда - прямые направления, как правило, более комфортны. Но если для Вас в первую очередь важна цена, то берите на проходящие поезда (обычно цена меньше на 10-20%)
            </p>
            <p class="advice">
                <b>Совет №3.</b><br>
                <?=$txt[5]?> при покупке в интернете дешевле, чем на кассах. <?=$txt[6]?>.
            </p>
            <p class="advice">
                <b>Совет №4.</b><br>
                Самые дешевые билеты продаются в конце вагона и на верхних <?=$txt[7]?>.
            </p>
        </div>
    </section>

    <section class="sm_50">
        <div class="trains_block">
            <h4 class="trains_block_title">О сервисе Lowtrip</h4>
            <p>
                <?=$txt[0]?> помогает <?=$txt[1]?> самые <?=$txt[2]?> билеты на железную дорогу, автобусы, машины на BlaBlaCar, недорогое жилье, дает бесплатные гиды по городу.
            </p>
            <p>
                Мы делаем все, чтобы путешествие было недорогим.
            </p>
        </div>
    </section>
</div>