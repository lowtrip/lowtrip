<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;
use common\widgets\NavbarLowtrip;

$this->title = 'Рейтинг городов и маршрутов для знакомств согласно данным Ж/д- свахи';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Рейтинг городов и маршрутов для знакомств согласно данным Ж/д- свахи',
]);

?>

    <div class="svaha-body">

        <div class="svaha-topbar">
            <div class="container">
                <div class="svaha-topbar__container">
                    <a class="svaha-topbar__logo" href="/svaha"><img class="lowtrip-logo" src="/img/logo/lowtrip/lowtrip-white.svg" alt=""></a>

                    <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/trip-information.php', ['style' => 'white']) ?>
                </div>
            </div>
        </div>

        <!--Контент страницы результатов поиска билетов-->
        <div class="svaha-content train_results_page">

            <!--header-->
            <section class="main_bg train_header svaha_train_header" id="main-header">

                <!--desktop header-->
                <div class="content" id="search-form">
                    <div class="container">
                        <div class="train-form-wrapper">
                            <!--START: Cities standart form-->
                            <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/search-form.php', ['module' => 'svaha/search', 'style' => 'white-opacity']); ?>
                            <!--END: Cities standart form-->
                        </div>
                    </div>
                </div>
                <!--desktop header-->

            </section>
            <!--header-->

            <!--Секция для контента результатов поиска-->
            <section class="train-results-container background-transparent">

                <div class="container">

                    <div class="train-route-rating">
                        <div class="row">
                            <div class="col-xs-12">
                                <h1><span class="green">Топ-50</span> направлений поездов, где пассажиры ищут легкие знакомства без обязательств</h1>
                                <div class="train-route-rating__block">
                                    <img class="responsive-img" src="/img/svaha/svaha.jpg" alt="ЖД Сваха">
                                </div>
                                <div class="train-route-rating__block">
                                    <p>Проект Lowtrip специализируется на подборе дешевых туров и ЖД билетов. Недавно в рамках проекта создатели запустили сервис “ЖД СВАХА”, позволяющий находить одиноким пассажирам себе спутника или спутницу во время поездки в поезде.</p>
                                    <p>Проанализировав покупки билетов на ЖД, создатели поняли, что многие путешествуют одни. И создали сервис, который работает на принципах нейронной сети и подбирает пару во время поездки в поезде.</p>
                                    <p>По первым результатам сервис составил список ТОП-50 городов и направлений, где люди ищут легкие знакомства в поезде.</p>
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <h2 class="train-route-rating__title">
                                    <span class="green">Топ-50</span>
                                    поездов
                                </h2>
                                <div class="train-route-rating__content">

                                    <ol class="train-route-rating__rating-list">
                                        <?php
                                        if (!empty($popular_routes)) {
                                            foreach ($popular_routes as $k => $route)
                                            {
                                                echo '<li><span class="black">'.$route['departure']['city'].' &#8594; '.$route['arrival']['city'].'</span></li>';
                                            }
                                        }
                                        ?>
                                    </ol>

                                </div>

                            </div>

                            <div class="col-md-6">

                                <h2 class="train-route-rating__title">
                                    <span class="green">Топ-50</span>
                                    городов
                                </h2>
                                <div class="train-route-rating__content">

                                    <ol class="train-route-rating__rating-list">
                                        <?php
                                        if (!empty($popular_cities)) {
                                            foreach ($popular_cities as $k => $city)
                                            {
                                                echo '<li><span class="black">'.$city->city->city.'</span></li>';
                                            }
                                        }
                                        ?>
                                    </ol>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </section>
            <!--Секция для контента результатов поиска-->

        </div>
        <!--Контент страницы результатов поиска билетов-->

        <!--START: Footer-->
        <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/footer.php', ['style' => 'background-transparent']); ?>
        <!--END: Footer-->

    </div>

    <div class="svaha-bg svaha-bodylayer">
        <div class="svaha-bodylayer background-grey"></div>
    </div>

<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        });
    if (window.hasOwnProperty('yaCounter44396998')) {
        yaCounter44396998.reachGoal('svaha-rating-page'); 
    }
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>