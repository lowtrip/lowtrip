<?php

use yii\helpers\Html;

$this->title = 'Ж/Д - СВАХА: знакомства в поездке с помощью нейросети';

$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Для знакомства в поездке наши инженеры разработали специальный проект, который с помощью нейросети ищет варианты путешествий и варианты знакомств. Ж/д сваха знакомит людей на любых направлениях поездов.',
]);

$this->registerMetaTag([
    'property' => 'og:title',
    'content' => 'Ж/Д - СВАХА: знакомства в поездке с помощью нейросети',
]);
$this->registerMetaTag([
    'property' => 'og:description',
    'content' => 'Cпециальный проект, который с помощью нейросети ищет варианты путешествий и варианты знакомств в поезде.',
]);
$this->registerMetaTag([
    'property' => 'og:type',
    'content' => 'website',
]);
$this->registerMetaTag([
    'property' => 'og:image',
    'content' => 'https://lowtrip.ru/img/header/svaha-bg.jpg',
]);
$this->registerMetaTag([
    'property' => 'og:url',
    'content' => 'https://lowtrip.ru',
]);
$this->registerMetaTag([
    'property' => 'og:site_name',
    'content' => 'Lowtrip',
]);
$this->registerMetaTag([
    'property' => 'og:locale',
    'content' => 'ru_RU',
]);

?>



<div class="svaha-body">

    <div class="svaha-topbar">
        <div class="container">
            <div class="svaha-topbar__container">
                <a class="svaha-topbar__logo" href="/svaha"><img class="lowtrip-logo" src="/img/logo/lowtrip/lowtrip-white.svg" alt=""></a>
            </div>
        </div>
    </div>

    <div class="svaha-content flex-center">
        <div class="svaha-content__header" id="main-header">
            <div class="container">
                <h1 class="svaha-content__title text-white smooth-opacity">ЖД Сваха</h1>

                <h2 class="svaha-content__title text-white smooth-opacity">Знакомства в поезде с помощью нейросети</h2>
            </div>
        </div>
        <div class="svaha-content__body">
            <div class="container">
                <div class="svaha-content__block">
                    <!--START: Cities standart form-->
                    <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/search-form.php', ['module' => 'svaha/search', 'style' => 'white-opacity']); ?>
                    <!--END: Cities standart form-->
                </div>
                <div class="svaha-content__block">

                </div>
            </div>
        </div>
    </div>

    <!--START: Footer-->
    <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/footer.php', ['style' => 'background-transparent svaha-footer']); ?>
    <!--END: Footer-->

</div>

<div class="svaha-bg svaha-bodylayer">
    <div class="svaha-bodylayer red-gradient"></div>
</div>

<?php
$script = <<< JS

    if ($(window).width() > 767) {
        var headerCanvas = new CanvasHeader('svaha-app');
        headerCanvas.init();
    }
    
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
