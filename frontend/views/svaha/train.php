<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;

use yii\bootstrap\Nav;

use common\widgets\Breadcrumbs;
use common\widgets\NavbarLowtrip;

$date_string = Yii::$app->formatter->asDate($date, 'long');
$trip_url =  Url::toRoute(['trip/build', 'from' => $first->city_slug, 'to' => $second->city_slug]);

if (!empty($page)){
    $title = explode(";", $page->title);
    $description = explode(";", $page->description);
    $text1 = explode(";", $page->text1);
    $text2 = explode(";", $page->text2);
    $text3 = explode(";", $page->text3);
    $text4 = explode(";", $page->text4);
    $title_string = 'ЖД билеты на поезд '.$first->city.' - '.$second->city.': расписание, '.$title[0].', '.$title[1];
    $description_string = ' Ж/д билеты '.$first->city.' - '.$second->city.': сидячие, плацкарт, купе. '.$description[0].': сапсан, '.$description[1].', двухэтажный, '.$description[2].'. Партнер ОАО “РЖД”, находим самые '.$description[3].' билеты на ж/д поезд. '.$description[4].' как доехать из '.$first->city.' до '.$second->city.' на поезде.';
}
else {
    $title_string = 'ЖД билеты на поезд '.$first->city.' - '.$second->city.': расписание, цена, купе, плацкарт';
    $description_string = ' Ж/д билеты '.$first->city.' - '.$second->city.': сидячие, плацкарт, купе. Есть сортировка: сапсан, самый дешевый, двухэтажный, есть отзывы. Партнер ОАО “РЖД”, находим самые недорогие билеты на ж/д поезд. Рассказываем как доехать из '.$first->city.' до '.$second->city.' на поезде.';
}

$this->title = $title_string;

$this->registerMetaTag([
    'name' => 'description',
    'content' => $description_string,
]);

?>

    <div class="svaha-body">

        <div class="svaha-topbar">
            <div class="container">
                <div class="svaha-topbar__container">
                    <a class="svaha-topbar__logo" href="/svaha"><img class="lowtrip-logo" src="/img/logo/lowtrip/lowtrip-white.svg" alt=""></a>

                    <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/trip-information.php', ['style' => 'white', 'first' => $first, 'second' => $second, 'date' => $date]) ?>
                </div>
            </div>
        </div>

        <!--Контент страницы результатов поиска билетов-->
        <div class="svaha-content train_results_page">

            <!--header-->
            <section class="main_bg train_header svaha_train_header" id="main-header">

                <!--desktop header-->
                <div class="content" id="search-form">
                    <div class="container">
                        <div class="train-form-wrapper">
                            <!--START: Cities standart form-->
                            <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/search-form.php', ['module' => 'svaha/search', 'style' => 'white-opacity', 'first' => $first, 'second' => $second, 'date' => $date, 'goal' => $goal, 'self' => $self, 'find' => $find]); ?>
                            <!--END: Cities standart form-->
                        </div>
                    </div>
                </div>
                <!--desktop header-->

            </section>
            <!--header-->

            <!--Секция для контента результатов поиска-->
            <section class="train-results-container background-transparent">
                <!--Заголовок-->
                <div class="train-results-header">
                    <div class="container">
                        <div class="row">

                            <div class="col-md-4 t_col-10">
                                <div class="train-route-rating">
                                    <div class="train-route-rating__title">
                                        <span class="train-route-rating__number"><?= $routeRating ?></span>
                                        место
                                    </div>
                                    <div class="train-route-rating__content">
                                        <a class="train-route-rating__link" href="<?= Url::toRoute(['svaha/rating']) ?>">
                                            <span>В рейтинге маршрутов для знакомств</span>
                                            <span class="icon__arrow-right train-route-rating__icon"></span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8 t_col-10">
                                <h1>
                                    Выберите нужный рейс на поезд <nobr><?= $first->city?> - <?= $second->city?></nobr> и купите билет
                                </h1>
                                <br>
                                <h2>Время и место встречи в поезде с попутчиками, интересы которых совпадают с Вашими, будет указано в email письме</h2>
                            </div>

                        </div>
                    </div>
                </div>
                <!--Заголовок-->

                <!--Результаты поиска-->
                <div class="train-results-search-content">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-push-4 t_col-10">
                                <div class="train-results-search-title"><h2>Расписание</h2><span> на </span><span class="date-to-string js-current-date"><?=$date_string?></span></div>
                            </div>
                        </div>
                    </div>

                    <!--Шаблон модуля-->
                    <div class="train-results-timetable" id="train-view"></div>
                    <!--Шаблон модуля-->

                </div>
                <!--Результаты поиска-->
            </section>
            <!--Секция для контента результатов поиска-->

        </div>
        <!--Контент страницы результатов поиска билетов-->

        <!--START: Footer-->
        <?php echo \Yii::$app->view->renderFile('@app/views/svaha/components/footer.php', ['style' => 'background-transparent']); ?>
        <!--END: Footer-->

    </div>

    <div class="svaha-bg svaha-bodylayer">
        <div class="svaha-bodylayer background-grey"></div>
    </div>

    <div class="hidden">
        <!-- Форма для отправки маршрута -->
        <div class="popup_email" id="popup_email_train">
            <div class="title">
                Где место встречи в поезде?
            </div>
            <p>Введи свой email, и мы пришлем тебе информацию о времени и месте встречи в поезде с людьми, интресы которых совпадают с твоими!</p>
            <form class="email_form validation" method="post" onsubmit="yaCounter44396998.reachGoal('svaha-send-email'); return true;" data-action="sendSvaha">
                <div class="row">
                    <div class="col-sm-8 relative">
                        <input name="email" type="email" class="email validator" placeholder="Email" required>
                        <div class="status"></div>
                    </div>
                    <div class="col-sm-4 relative">
                        <button type="submit" class="green_btn">Отправить</button>
                    </div>
                </div>
                <div class="personal-agreement">
                    <div>
                        Или <a id="continue" href="#" class="continue">Перейти к покупке без отправки письма</a>
                    </div>
                    Нажимая кнопку отправить, <a href="/templates/docs/soglasie.pdf" target="blank">вы соглашаетесь на обработку персональных данных</a>
                </div>
            </form>
        </div>
        <!-- Форма для отправки маршрута -->
    </div>

<?php
$script = <<< JS
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        })
        .then(function(){
            trip.init()
                .then(function(){
                    var train_view = new TrainView('#train-view', {
                        friends: true,
                        friendType: '$find',
                        friendGoal: '$goal'
                    });
                });
        });
    if (window.hasOwnProperty('yaCounter44396998')) {
        yaCounter44396998.reachGoal('svaha-tickets-page'); 
    }
    
    window.svaha = {
        self: '$self',
        find: '$find',
        goal: '$goal'
    };
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>