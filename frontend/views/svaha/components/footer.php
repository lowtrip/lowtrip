<section class="page-footer section_standart <?= $style ?>">
    <div class="container">
        <div class="footer-container">
            <div class="footer-menu">
                <a class="popup_init link white" href="#terminal">О проекте</a>
            </div>
            <div class="footer-socials">
                <a onclick="yaCounter44396998.reachGoal('svaha-instagram'); return true;" class="social-icon instagram" href="https://www.instagram.com/lowtrip.bro/" target="_blank"></a>
                <a onclick="yaCounter44396998.reachGoal('svaha-vkontakte'); return true;" class="social-icon vk" href="https://vk.com/low_trip" target="_blank"></a>
                <a onclick="yaCounter44396998.reachGoal('svaha-telegram'); return true;" class="social-icon telegram" href="https://t.me/rzdticket" target="_blank"></a>
            </div>
        </div>
    </div>
</section>