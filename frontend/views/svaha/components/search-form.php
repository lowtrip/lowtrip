<?php
use yii\helpers\Html;

$helpTop1 = $helpTop2 = '';
$isValid1 = $isValid2 = 'invalid';
$first_value = '';
$second_value = '';
$self_men = '';
$self_woman = '';
$find_men = '';
$find_woman = '';
$goal_value = 'Поговорить';

// Если в view формы переданы переменные городов, то сразу заполняем форму
if (isset($first)) {
    $first_value = $first->city;
    $isValid1 = 'valid';
    $helpTop1 = 'top';
}
if (isset($second)) {
    $second_value = $second->city;
    $isValid2 = 'valid';
    $helpTop2 = 'top';
}

if (isset($date)) {
    $date_output = Yii::$app->dateFormatter->toDateString($date);
}
else {
    $date = $date_output = '';
}

if (isset($self)) {
    if ($self == 'men') {
        $self_men = "checked";
    }
    else {
        $self_woman = 'checked';
    }
}

if (isset($find)) {
    if ($find == 'men') {
        $find_men = "checked";
    }
    else {
        $find_woman = 'checked';
    }
}

if (isset($goal)) {
    switch ($goal) {
        case '1' :
            $goal_value = 'Поговорить';
            break;
        case '2' :
            $goal_value = 'Пофлиртовать';
            break;
        case '3' :
            $goal_value = 'Выпить горячего';
            break;
    }
}

?>

    <div class="cities <?=$style?>" id="search-trip">

        <?= Html::beginForm([$module], 'post', ['class' => 'cities-form']) ?>
        <div class="form-group-block form-top-block">
            <div class="form-input-block form-input-block-switch no-border">
                <div class="group switch-block">
                    <div class="switch-block__item">
                        <input required class="switch-block__input" type="radio" name="self" id="self-men" value="men" <?= $self_men ?>>
                        <label class="switch-block__label men" for="self-men" onclick="yaCounter44396998.reachGoal('svaha-self-man'); return true;">
                            <img class="switch-block__icon" src="/img/icons/men.svg" alt="">
                        </label>
                    </div>
                    <div class="switch-block__title">
                        <div class="switch-block__title-content">
                            я
                        </div>
                    </div>
                    <div class="switch-block__item">
                        <input class="switch-block__input" type="radio" name="self" id="self-woman" value="woman" <?= $self_woman ?>>
                        <label class="switch-block__label woman" for="self-woman" onclick="yaCounter44396998.reachGoal('svaha-self-woman'); return true;">
                            <img class="switch-block__icon" src="/img/icons/girl.svg" alt="">
                        </label>
                    </div>

                    <!--Всплывающее окошко-->
                    <div class="city_notification error help-window">
                        <span class="text">Чтобы продолжить, выберите, кто Вы:</span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
            </div>
            <div class="form-input-block form-input-block-switch no-border">
                <div class="group switch-block">
                    <div class="switch-block__item">
                        <input class="switch-block__input" type="radio" name="find" id="find-men" value="men" <?= $find_men ?>>
                        <label class="switch-block__label men" for="find-men" onclick="yaCounter44396998.reachGoal('svaha-find-man'); return true;">
                            <img class="switch-block__icon" src="/img/icons/men.svg" alt="">
                        </label>
                    </div>
                    <div class="switch-block__title">
                        <div class="switch-block__title-content">
                            ищу
                        </div>
                    </div>
                    <div class="switch-block__item">
                        <input class="switch-block__input" type="radio" name="find" id="find-woman" value="woman" <?= $find_woman ?>>
                        <label class="switch-block__label woman" for="find-woman" onclick="yaCounter44396998.reachGoal('svaha-find-woman'); return true;">
                            <img class="switch-block__icon" src="/img/icons/girl.svg" alt="">
                        </label>
                    </div>

                    <!--Всплывающее окошко-->
                    <div class="city_notification error help-window">
                        <span class="text">Чтобы продолжить, выберите, кого Вы хотите найти:</span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
            </div>
        </div>

        <div class="form-group-block inputs-group">
            <div class="form-input-block form-input-block-big">
                <div class="group input_wrapper has_select">
                    <?= Html::input('text', 'from[value]', $first_value,['class' => 'with_help with_clear has_hint city_input form_ui_input', 'id' => 'first', 'data-valid' => $isValid1]) ?>
                    <?= Html::input('hidden', 'from[id]', $first->id,['id' => 'first-id']) ?>
                    <?= Html::input('hidden', 'from[city]', $first->city,['id' => 'first-city']) ?>
                    <?= Html::input('hidden', 'from[city_slug]', $first->city_slug,['id' => 'first-city_slug']) ?>
                    <?= Html::input('hidden', 'from[region]', $first->region,['id' => 'first-region']) ?>
                    <?= Html::input('hidden', 'from[region_slug]', $first->region_slug,['id' => 'first-region_slug']) ?>
                    <?= Html::input('hidden', 'from[country]', $first->country,['id' => 'first-country']) ?>
                    <?= Html::input('hidden', 'from[country_slug]', $first->country_slug,['id' => 'first-country_slug']) ?>
                    <?= Html::input('hidden', 'from[lat]', $first->lat,['id' => 'first-lat']) ?>
                    <?= Html::input('hidden', 'from[lng]', $first->lng,['id' => 'first-lng']) ?>
                    <?= Html::input('hidden', 'from[gmt]', $first->GMT,['id' => 'first-gmt']) ?>
                    <span class="help <?= $helpTop1 ?>">Откуда едем?</span>

                    <!--Всплывающее окошко-->
                    <div id="notification_city_1" class="city_notification error help-window">
                        <span class="text"></span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
                <div class="group button_wrapper">
                    <!--Поменять города местами-->
                    <div title="Поменять города местами" class="btn_change switch has_hint has_tooltip" id="switch_city">
                        <img class="" src="/img/icons/switch.svg" alt="">
                    </div>
                    <!--Поменять города местами-->
                </div>
            </div>

            <div class="form-input-block form-input-block-big">
                <div class="group input_wrapper has_select">
                    <?= Html::input('text', 'to[value]', $second_value,['class' => 'with_help with_clear has_hint city_input form_ui_input', 'id' => 'second', 'data-index' => -1, 'data-valid' => $isValid2]) ?>
                    <?= Html::input('hidden', 'to[id]', $second->id,['id' => 'second-id']) ?>
                    <?= Html::input('hidden', 'to[city]', $second->city,['id' => 'second-city']) ?>
                    <?= Html::input('hidden', 'to[city_slug]', $second->city_slug,['id' => 'second-city_slug']) ?>
                    <?= Html::input('hidden', 'from[region]', $second->region,['id' => 'second-region']) ?>
                    <?= Html::input('hidden', 'to[region_slug]', $second->region_slug,['id' => 'second-region_slug']) ?>
                    <?= Html::input('hidden', 'from[country]', $second->country,['id' => 'second-country']) ?>
                    <?= Html::input('hidden', 'to[country_slug]', $second->country_slug,['id' => 'second-country_slug']) ?>
                    <?= Html::input('hidden', 'to[lat]', $second->lat,['id' => 'second-lat']) ?>
                    <?= Html::input('hidden', 'to[lng]', $second->lng,['id' => 'second-lng']) ?>
                    <?= Html::input('hidden', 'to[gmt]', $second->GMT,['id' => 'second-gmt']) ?>
                    <span class="help <?= $helpTop2 ?>">Куда едем?</span>

                    <!--Всплывающее окошко-->
                    <div id="notification_city_2" class="city_notification error help-window">
                        <span class="text"></span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
                <div class="group button_wrapper">
                    <!--Сменить город-->
                    <div title="Другой город" class="btn_change change_city has_hint has_tooltip" id="change_city" onclick="yaCounter44396998.reachGoal('change-city'); return true;">
                        <img class="" src="/img/icons/rotate.png" alt="">
                    </div>
                    <!--Сменить город-->
                    <!--Всплывающее окошко-->
                    <div id="notification_change" class="city_notification hint help-window">
                        <span class="text">Не знаешь куда поехать?<br>Мы подскажем!<br>Нажми, чтобы сменить город</span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
            </div>

            <div class="form-input-block form-input-block-medium">
                <div class="group has_select">
                    <?= Html::input('hidden', 'date', $date,['id' => 'date-start', 'readonly' => 'readonly']) ?>
                    <?= Html::input('text', 'date_formatted', $date_output, ['id' => 'date-input', 'class' => 'with_help pointer form_ui_input', 'readonly' => 'readonly']) ?>
                    <span class="help top">Когда едем?</span>
                </div>
            </div>

            <div class="form-input-block form-input-block-medium">
                <div class="group menu-helper has_ui has_select">
                    <?= Html::input('hidden', 'goal', 1,['id' => 'goal', 'class' => 'helper-value', 'readonly' => 'readonly']) ?>
                    <input readonly="readonly" class="with_help helper-text pointer form_ui_input" type="text" value="<?= $goal_value ?>">
                    <span class="help top">Хочу познакомится и</span>

                    <ul class="dropdown-helper ui-element" id="goal-select">
                        <li data-value="1" onclick="yaCounter44396998.reachGoal('svaha-goal-1'); return true;">Поговорить</li>
                        <li data-value="2" onclick="yaCounter44396998.reachGoal('svaha-goal-2'); return true;">Пофлиртовать</li>
                        <li data-value="3" onclick="yaCounter44396998.reachGoal('svaha-goal-3'); return true;">Выпить горячего</li>
                    </ul>
                </div>
            </div>

        </div>

        <div class="form-group-block">
            <button class="btn_go btn-red" id="start_find" onclick="yaCounter44396998.reachGoal('svaha-search-btn'); return true;">
                Найти друзей
            </button>
        </div>
        <?= Html::endForm() ?>
    </div>

<?php
$script = <<< JS
    $(document).on('click', '#start_find', function(e) {
        var form = $(this).closest('form');
        
        e.preventDefault();
        if (validation() && svahaFormValidation(form)) {
            form.submit();
            form[0].reset();
        }
    });

    function svahaFormValidation(form){
        var self = form.find('input[name="self"]');
        var finding = form.find('input[name="find"]');
        
        if (self.is(":checked") && finding.is(":checked")) {
            return true;
        }
        else if (!self.is(":checked")) {
            validateRadioInput(self);
            return false;
        }
        else {
            validateRadioInput(finding);
            return false;
        }
    }
    
    function validateRadioInput(input) {
        input.closest('.group').find('.help-window').fadeIn(400);
    }
    
    $(document).on('click', 'input[type="radio"]', function() {
       $(this).closest('form').find('.help-window').fadeOut(400); 
    });
    
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>