<?php
$bk_cities = ['Саратов', 'Самара', 'Нижний Новгород', 'Чебоксары', 'Ульяновск', 'Йошкар-Ола', 'Тольятти', 'Дзержинск', 'Пенза', 'Казань', 'Нижнекамск', 'Набережные Челны'];
if (in_array($second->city, $bk_cities)) :
?>

    <div id="burgerking-offer" class="burgerking_container slider owl-carousel owl-theme">
        <!--whopper junior dinner-->
        <div class="special-offer">
            <div class="left">
                <div class="gift-bk">
                    <div class="text">
                        <span>СПЕЦПРЕДЛОЖЕНИЕ</span><br>
                        <span>от LOWTRIP</span><br>
                        <span>и BURGER KING</span>
                    </div>
                </div>
                <div class="bk-form">
                    <p class="bk-form-text">Получи промокод на специальное предложение от BURGER KING!</p>
                    <p class="bk-form-text">Промокод высылается путешественнику, только при сохранении маршрута</p>
                </div>
            </div>
            <div class="right">
                <img src="/img/trip/whopper-obed.jpg" alt="">
            </div>
        </div>

        <!--chicken junior dinner-->
        <div class="special-offer">
            <div class="left">
                <div class="gift-bk">
                    <div class="text">
                        <span>СПЕЦПРЕДЛОЖЕНИЕ</span><br>
                        <span>от LOWTRIP</span><br>
                        <span>и BURGER KING</span>
                    </div>
                </div>
                <div class="bk-form">
                    <p class="bk-form-text">Получи промокод на специальное предложение от BURGER KING!</p>
                    <p class="bk-form-text">Промокод высылается путешественнику, только при сохранении маршрута</p>
                </div>
            </div>
            <div class="right">
                <img src="/img/trip/chicken-obed.jpg" alt="">
            </div>
        </div>
    </div>

<?php
$script = <<< JS
    $('#burgerking-offer').owlCarousel({
        items: 1,
        navText: ['',''],
        mouseDrag: false,
        responsive: {
            0: {
                nav: false,
                dots: true,
            },
            768: {
                nav: true,
                dots: false,
            },
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>
<?php
endif;
?>
