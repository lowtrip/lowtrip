<?php
    $navStyle = isset($style) ? $style : '';
    $navTitle = isset($title) ? $title : '';
    $navMenu = isset($menu) ? $menu : [];
    $showSearch = isset($showSearch) && $showSearch;
    $titleWidthClass = $showSearch ? 'navigation-nav__title--width-calc' : '';
?>

<div class="navigation-nav <?= $navStyle ?>">
    <div class="container">
        <div class="navigation-nav__content">

            <div class="navigation-nav__logo">
                <a href="/">
                    <img class="navigation-nav__logotype" src="/img/logo/lowtrip/lowtrip.png" alt="Lowtrip Car">
                </a>
            </div>

            <div class="navigation-nav__title <?= $titleWidthClass ?>">
                <?= $navTitle ?>
            </div>

            <div class="navigation-nav__buttons">
                <?php if($showSearch) : ?>
                <div class="navigation-nav__button navigation-nav__button--search">
                    <button class="button button-search" id="nav-search-btn">
                        <svg class="button-search__icon" version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="446.25px" height="446.25px" viewBox="0 0 446.25 446.25" style="enable-background:new 0 0 446.25 446.25;"
                             xml:space="preserve">
                            <g>
                                <path d="M318.75,280.5h-20.4l-7.649-7.65c25.5-28.05,40.8-66.3,40.8-107.1C331.5,73.95,257.55,0,165.75,0S0,73.95,0,165.75
                                    S73.95,331.5,165.75,331.5c40.8,0,79.05-15.3,107.1-40.8l7.65,7.649v20.4L408,446.25L446.25,408L318.75,280.5z M165.75,280.5
                                    C102,280.5,51,229.5,51,165.75S102,51,165.75,51S280.5,102,280.5,165.75S229.5,280.5,165.75,280.5z"/>
                            </g>
                        </svg>
                    </button>
                </div>
                <?php endif; ?>

                <div class="navigation-nav__button navigation-nav__button--menu">
                    <button class="button button-menu" id="nav-menu-btn">
                        <span class="button-menu__line"></span>
                    </button>
                </div>
            </div>

            <div class="navigation-nav__menu" id="nav-menu">
                <?php
                if (empty($navMenu)) return;

                echo '<ul class="navigation-nav-list">';
                foreach ($navMenu as $navMenuItem) {
                    $active = isset($navMenuItem['active']) && $navMenuItem['active'] ? 'active' : '';

                    echo '<li class="navigation-nav-list__item">';
                        echo '<a class="navigation-nav-list__link '.$active.'" href="'.$navMenuItem["url"].'">';
                            echo $navMenuItem["title"];
                        echo '</a>';
                    echo '</li>';
                }
                echo '</ul>';
                ?>
            </div>

        </div>
    </div>
</div>


<?php
$script = <<< JS
    const navMenuBtn = $('#nav-menu-btn');
    const navSearchBtn = $('#nav-search-btn');
    const navMenu = $('#nav-menu');
    const searchForm = $('#search-form');
    
    const closeAllExcept = elem => {
       $('.navigation-nav__button').removeClass('active').find('.button').removeClass('active');
       navMenu.removeClass('active');
       searchForm.removeClass('mobile-open');
    };
    
    navMenuBtn.on('click', function() {
        $(this).toggleClass('active').closest('.navigation-nav__button').toggleClass('active');
        navMenu.toggleClass('active');
    });
    
    navSearchBtn.on('click', function(){
        $(this).toggleClass('active').closest('.navigation-nav__button').toggleClass('active');
        searchForm.toggleClass('mobile-open');
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>