<?php
use yii\helpers\Html;

$helpTop1 = $helpTop2 = '';
$isValid1 = $isValid2 = 'invalid';
$first_value = '';
$second_value = '';

// Если в view формы переданы переменные городов, то сразу заполняем форму
if (isset($first)) {
    $first_value = $first->city;
    $isValid1 = 'valid';
    $helpTop1 = 'top';
}
if (isset($second)) {
    $second_value = $second->city;
    $isValid2 = 'valid';
    $helpTop2 = 'top';
}

(!isset($people)) ? $people = 1 : $people;

if (isset($date)) {
    $date_output = Yii::$app->dateFormatter->toDateString($date);
}
else {
    $date = $date_output = '';
}

if ($module == 'train/search') {
    $people_text = 'Пассажир';
    $button_text = 'Найти билеты';
}
else {
    $people_text = 'Лоутрипер';
    $button_text = 'Поехали';
}

if (intval($people) > 1) {
    $people_input = $people_text.'a';
}
else {
    $people_input = $people_text;
}


?>

<div class="cities <?=$style?>" id="search-trip">

    <? if (isset($navigation) && $navigation) : ?>
        <ul class="site-navigation-form">
            <li class="active"><?= Html::a('Путешествия', ['site/index']) ?></li>
            <li><?= Html::a('ЖД Билеты', ['train/main']) ?></li>
            <li><?= Html::a('Путешествия на автомобиле', ['car/main']) ?></li>
            <li><?= Html::a('Билеты на автобус', ['bus/main']) ?></li>
        </ul>
    <? endif; ?>


    <?= Html::beginForm([$module], 'post', ['class' => 'cities-form']) ?>
        <? if (isset($type)) : ?>
        <?= Html::input('hidden', 'type', $type->slug, ['id' => 'place-type']) ?>
        <? endif; ?>
        <? if (isset($logo) && $logo) : ?>
            <a href="/"><img class="lowtrip-form-logo" src="/img/logo/lowtrip_166.png" alt=""></a>
        <? endif; ?>
        <div class="form-group-block inputs-group">
            <div class="form-input-block form-input-block-big">
                <div class="group input_wrapper has_select">
                    <?= Html::input('text', 'from[value]', $first_value,['class' => 'with_help with_clear has_hint city_input form_ui_input', 'id' => 'first', 'data-valid' => $isValid1]) ?>
                    <?= Html::input('hidden', 'from[id]', $first->id,['id' => 'first-id']) ?>
                    <?= Html::input('hidden', 'from[city]', $first->city,['id' => 'first-city']) ?>
                    <?= Html::input('hidden', 'from[city_slug]', $first->city_slug,['id' => 'first-city_slug']) ?>
                    <?= Html::input('hidden', 'from[region]', $first->region,['id' => 'first-region']) ?>
                    <?= Html::input('hidden', 'from[region_slug]', $first->region_slug,['id' => 'first-region_slug']) ?>
                    <?= Html::input('hidden', 'from[country]', $first->country,['id' => 'first-country']) ?>
                    <?= Html::input('hidden', 'from[country_slug]', $first->country_slug,['id' => 'first-country_slug']) ?>
                    <?= Html::input('hidden', 'from[lat]', $first->lat,['id' => 'first-lat']) ?>
                    <?= Html::input('hidden', 'from[lng]', $first->lng,['id' => 'first-lng']) ?>
                    <?= Html::input('hidden', 'from[gmt]', $first->GMT,['id' => 'first-gmt']) ?>
                    <span class="help <?= $helpTop1 ?>">Откуда ты?</span>

                    <!--Всплывающее окошко-->
                    <div id="notification_city_1" class="city_notification error help-window">
                        <span class="text"></span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
                <div class="group button_wrapper">
                    <!--Поменять города местами-->
                    <div title="Поменять города местами" class="btn_change switch has_hint has_tooltip" id="switch_city">
                        <img class="" src="/img/icons/switch.svg" alt="">
                    </div>
                    <!--Поменять города местами-->
                </div>
            </div>

            <div class="form-input-block form-input-block-big">
                <div class="group input_wrapper has_select">
                    <?= Html::input('text', 'to[value]', $second_value,['class' => 'with_help with_clear has_hint city_input form_ui_input', 'id' => 'second', 'data-index' => -1, 'data-valid' => $isValid2]) ?>
                    <?= Html::input('hidden', 'to[id]', $second->id,['id' => 'second-id']) ?>
                    <?= Html::input('hidden', 'to[city]', $second->city,['id' => 'second-city']) ?>
                    <?= Html::input('hidden', 'to[city_slug]', $second->city_slug,['id' => 'second-city_slug']) ?>
                    <?= Html::input('hidden', 'from[region]', $second->region,['id' => 'second-region']) ?>
                    <?= Html::input('hidden', 'to[region_slug]', $second->region_slug,['id' => 'second-region_slug']) ?>
                    <?= Html::input('hidden', 'from[country]', $second->country,['id' => 'second-country']) ?>
                    <?= Html::input('hidden', 'to[country_slug]', $second->country_slug,['id' => 'second-country_slug']) ?>
                    <?= Html::input('hidden', 'to[lat]', $second->lat,['id' => 'second-lat']) ?>
                    <?= Html::input('hidden', 'to[lng]', $second->lng,['id' => 'second-lng']) ?>
                    <?= Html::input('hidden', 'to[gmt]', $second->GMT,['id' => 'second-gmt']) ?>
                    <span class="help <?= $helpTop2 ?>">Куда едем?</span>

                    <!--Всплывающее окошко-->
                    <div id="notification_city_2" class="city_notification error help-window">
                        <span class="text"></span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
                <div class="group button_wrapper">
                    <!--Сменить город-->
                    <div title="Другой город" class="btn_change change_city has_hint has_tooltip" id="change_city" onclick="yaCounter44396998.reachGoal('change-city'); return true;">
                        <img class="" src="/img/icons/rotate.png" alt="">
                    </div>
                    <!--Сменить город-->
                    <!--Всплывающее окошко-->
                    <div id="notification_change" class="city_notification hint help-window">
                        <span class="text">Не знаешь куда поехать?<br>Мы подскажем!<br>Нажми, чтобы сменить город</span>
                        <span class="close">×</span>
                    </div>
                    <!--Всплывающее окошко-->
                </div>
            </div>

            <div class="form-input-block form-input-block-medium">
                <div class="group has_select">
                    <?= Html::input('hidden', 'date', $date,['id' => 'date-start', 'readonly' => 'readonly']) ?>
                    <?= Html::input('text', 'date_formatted', $date_output, ['id' => 'date-input', 'class' => 'with_help pointer form_ui_input', 'readonly' => 'readonly']) ?>
                    <span class="help top">Когда едем?</span>
                </div>
            </div>

            <div class="form-input-block form-input-block-medium">
                <div class="group menu-helper has_ui has_select">
                    <?= Html::input('hidden', 'people', $people,['id' => 'people', 'class' => 'helper-value', 'readonly' => 'readonly']) ?>
                    <input readonly="readonly" class="with_help helper-text pointer form_ui_input" type="text" id="people_text" value="<?= $people ?> <?= $people_input ?>">
                    <span class="help top">Сколько <?= $people_text ?>ов?</span>

                    <ul class="dropdown-helper ui-element" id="people-select">
                        <li data-value="1">1 <?= $people_text ?></li>
                        <li data-value="2">2 <?= $people_text ?>a</li>
                        <li data-value="3">3 <?= $people_text ?>a</li>
                        <li data-value="4">4 <?= $people_text ?>a</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="form-group-block">
            <button class="btn_go" id="start_find" onclick="yaCounter44396998.reachGoal('go-btn'); return true;">
                <?= $button_text ?>
            </button>
        </div>
    <?= Html::endForm() ?>
</div>

<?php
$script = <<< JS
    $(document).on('click', '#start_find', function(e) {
        e.preventDefault();
        if (validation() === true) {
            var form = $(this).closest('form');
            form.submit();
            form[0].reset();
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>