<?php
if (empty($text)) {
    $text = ['лучше', 'начале', 'стоимость'];
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 data-toggle="collapse" data-parent="#accordion" href="#train-how-to-buy" class="panel-title expand">
            <div class="right-arrow pull-right">+</div>
            Как купить самый дешевый билет на ж/д?
        </h3>
    </div>
    <div id="train-how-to-buy" class="panel-collapse collapse">
        <div class="panel-body">
        <p>ОАО “РЖД” открывает продажи ж/д билетов за 90 дней до отправления.</p>
        <p>Совет: <?=$text[0]?> всего покупать билеты заранее, в самом <?=$text[1]?> продаж <?=$text[2]?> минимальная.</p>
        </div>
    </div>
</div>