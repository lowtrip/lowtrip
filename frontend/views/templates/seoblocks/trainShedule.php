<div class="panel panel-default">
    <div class="panel-heading">
        <h3 data-toggle="collapse" data-parent="#accordion" href="#train-shedule" class="panel-title expand">
            <div class="right-arrow pull-right">+</div>
            Расписание движения на <span class="date-to-string js-current-date"><?=$dateString?></span>
        </h3>
    </div>
    <div id="train-shedule" class="panel-collapse collapse">
        <div class="panel-body">
        <p>Мы вывели на странице расписание железнодорожных поездов по <?=$text[0]?> <?=$firstCityName?> - <?=$secondCityName?>, которое <?=$text[1]?> на <nobr><span class="date-to-string js-current-date"><?=$dateString?></span></nobr></p>
        <p>Если Вы хотите <?=$text[2]?> другие даты, то <?=$text[3]?> их в календаре, который <?=$text[4]?> в шапке сайта.</p>
        </div>
    </div>
</div>