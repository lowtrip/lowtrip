<?php
if (empty($text)) {
    $text = ['подбирает', 'выгодные', 'находим', 'достопримечательности', 'аудиогиды'];
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 data-toggle="collapse" data-parent="#accordion" href="#about-lowtrip" class="panel-title expand">
            <div class="right-arrow pull-right">+</div>
            О сервисе Lowtrip
        </h3>
    </div>
    <div id="about-lowtrip" class="panel-collapse collapse">
        <div class="panel-body">
            <p>Сервис Lowtrip автоматически <?=$text[0]?> самые <?=$text[1]?> варианты для путешествий.</p>
            <p>Мы <?=$text[2]?> дешевые билеты на поезд, доступные места на авто и автобусах, а также <?=$text[3]?> и даем бесплатные <?=$text[4]?> по городу.</p>
        </div>
    </div>
</div>