<?php
if (empty($text)) {
    $text = ['Сервис', 'Lowtrip', 'билеты', 'интересными'];
}
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 data-toggle="collapse" data-parent="#accordion" href="#train-how-to-buy-overpayment" class="panel-title expand">
            <div class="right-arrow pull-right">+</div>
            Как купить ж/д билет на поезд без переплат?
        </h3>
    </div>
    <div id="train-how-to-buy-overpayment" class="panel-collapse collapse">
        <div class="panel-body">
            <p>
                Идеология <b>Lowtrip</b> - делать все, чтобы Ваши рабочие, туристические, экскурсионные поездки были были максимально <?=$text[3]?>. Потому мы предлагаем на отдельной странице еще и бесплатные экскурсии в г.
                <a href="<?=$tripUrl?>"><?=$secondCityName?></a>, недорогое жилье, отзывы местных жителей и список достопримечательностей. Также мы рассказываем, как можно добраться из г.<?=$firstCityName?> до г.<?=$secondCityName?> на поезде и других видах транспорта.
            </p>
            <div class="trains_block_attention">
                <p>
                    Внимание! <?=$text[0]?> <?=$text[1]?> находит самые дешевые <?=$text[2]?> на железнодорожный поезд, но вся покупка и оформление проходит у наших партнеров - <b>РЖД</b> или <b>OneTwoTrip</b>. Наш сервис не добавляет к стоимости своей дополнительной комиссии, что помогает путешественникам покупать билеты по самым недорогим ценам.
                </p>
                <p class="desicion">
                    Вывод: покупайте только на надежных сайтах, которые являются партнерами РЖД.
                </p>
            </div>
            <p>
                Если у Вас есть замечания по работе, либо Вы хотите предложить партнерство, то направляйте письмо на почту
                <a href="mailto:lowtrip@yandex.ru" target="_blank">lowtrip@yandex.ru</a>
            </p>
        </div>
    </div>
</div>