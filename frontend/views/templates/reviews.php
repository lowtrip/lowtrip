<h3 class="section_title">ОТЗЫВЫ О ГОРОДЕ</h3>
<div class="testimonials-block owl-carousel owl-theme slider2" id="testimonials-block">
    <?php
    foreach ($reviews as $review) {
        $item = '
            <div class="testimonial-item parent">
                <div class="text item-content">
                    ' . $review->text . '
                </div>
                <div class="read-full">
                    <span onclick="yaCounter44396998.reachGoal(\'show-review\'); return true;" class="fulltext-link show-full-content">Подробнее<span>
                </div>
            </div>
        ';
        echo $item;
    }
    ?>
</div>
<?php
$script = <<< JS
    $('#testimonials-block').owlCarousel({
        margin: 10,
        navText: ['', ''],
        responsive: {
            0: {
                items: 1,
                dots: true,
                nav: false,
            },
            768: {
                items: 2,
                dots: false,
                nav: true,
            }
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>


