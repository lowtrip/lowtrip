<?php
(!isset($people)) ? $people = 1 : $people;
if (isset($date)) {
    $date_string = Yii::$app->formatter->asDate($date, 'long');
}
else {
    $date_string = '';
}

?>
<div class="trip-data-information">
    <div>
        <?=$first->city?>
        <?php
        if (isset($second->city)) {
            echo '-';
        }
        ?>
        <?=$second->city?>
    </div>
    <div>
        <span id="data-information_date" class="date-to-string js-current-date"><?= $date_string ?></span>
        <span id="data-information_people"><?= $people ?></span> чел.
    </div>
</div>
