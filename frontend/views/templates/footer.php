<section class="page-footer section_standart <?= $class ?>">
    <div class="container">
        <div class="footer-container">
            <div class="footer-menu">
                <a onclick="yaCounter44396998.reachGoal('popup-lowtrip'); return true;" class="footer-menu-item popup_init" href="#popup_lowtrip">ООО "Лоутрип" <?=date("Y")?></a>
            </div>
            <div class="footer-socials">
                <a onclick="yaCounter44396998.reachGoal('instagram'); return true;" class="social-icon instagram" href="https://www.instagram.com/lowtrip.bro/" target="_blank"></a>
                <a onclick="yaCounter44396998.reachGoal('vkontakte'); return true;" class="social-icon vk" href="https://vk.com/low_trip" target="_blank"></a>
                <a onclick="yaCounter44396998.reachGoal('telegram'); return true;" class="social-icon telegram" href="https://t.me/rzdticket" target="_blank"></a>
            </div>
        </div>
    </div>
</section>