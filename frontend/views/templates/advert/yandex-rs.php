<div class="offer-block <?=$class?>">
    <div id="<?=$id?>" class="offer-block-content"></div>
</div>


<script type="text/javascript">
    (function(w, d, n, s, t) {
        w[n] = w[n] || [];
        w[n].push(function() {
            Ya.Context.AdvManager.render({
                blockId: "<?=$id?>",
                renderTo: "<?=$id?>",
                async: true
            },function altCallback () {
            <?php if(!empty($self)): ?>
                const block = document.getElementById("<?=$id?>");
                const link = document.createElement("a");
                const image = document.createElement("img");
                const close = document.createElement('img');

                link.setAttribute('href', '<?=$self['link'];?>');
                link.setAttribute('target', 'blank');
                link.classList.add('offer-block__link');
                link.style.background = '<?=$self['background'];?>';

                image.setAttribute('src', '<?=$self['image'];?>');
                image.classList.add('offer-block__image');

                close.setAttribute('src', '/img/icons/close.svg');
                close.classList.add('offer-block__close');

                link.appendChild(image);
                block.appendChild(link);
                block.appendChild(close);

                document.addEventListener('click', event => {
                    if (!event.target.closest('.offer-block__close')) return;

                    const container = event.target.closest('.offer-block');
                    const parent = container.parentElement;

                    if (parent && container) {
                        parent.removeChild(container);
                    }
                });
            <?php endif; ?>
            });
        });
        t = d.getElementsByTagName("script")[0];
        s = d.createElement("script");
        s.type = "text/javascript";
        s.src = "//an.yandex.ru/system/context.js";
        s.async = true;
        t.parentNode.insertBefore(s, t);
    })(this, this.document, "yandexContextAsyncCallbacks");
</script>