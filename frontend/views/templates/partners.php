<section class="partners white in_news section_standart">
    <div class="container">
        <h3 class="section_title">О НАС ПИШУТ</h3>
        <div class="list partners_news owl-carousel owl-theme slider" id="partners_slider">
            <a class="img_boxed" href="https://incrussia.ru/concoct/lowtrip-pridumat-servis-nedorogih-puteshestvij-po-rossii-v-kotoryj-zahochetsya-investirovat/" target="_blank">
                <img src="/img/logo/inc.jpg" alt="">
            </a>
            <a class="img_boxed" href="http://entermedia.io/people/sozdateli-lowtrip-o-vnutrennem-turizme-i-umnyh-puteshestviyah/" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/entermedia.jpg" alt="">
            </a>
            <a class="img_boxed" href="https://vc.ru/p/lowtrip" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/vc.png" alt="">
            </a>
            <a class="img_boxed" href="https://www.dp.ru/a/2017/06/07/Komu_pomogat_v_turizme" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/delpet.png" alt="">
            </a>
            <a class="img_boxed" href="https://weekend.rambler.ru/interest/kak-sekonomit-na-puteshestvii-poleznye-servisy-dlya-turistov-2017-05-29/" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/rambler.png" alt="">
            </a>
            <a class="img_boxed" href="http://rusrep.ru/" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/russian-reporter.png" alt="">
            </a>
            <a class="img_boxed" href="https://rb.ru/tag/lowtrip/" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/rusbase.svg" alt="">
            </a>
            <a class="img_boxed" href="https://www.kommersant.ru/doc/3338141" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/kommersant.jpg" alt="">
            </a>
            <a class="img_boxed" href="https://riamo.ru/article/214372/kak-sekonomit-na-puteshestvii-poleznye-servisy-dlya-turistov.xl" target="_blank" rel="nofollow noopener noreferrer">
                <img src="/img/logo/riamo.jpeg" alt="">
            </a>
        </div>
    </div>
</section>

<?php
$script = <<< JS
    $('#partners_slider').owlCarousel({
        navText: ['', ''],
        responsive: {
            320: {
                items: 3,
                dots: true,
                nav: false,
            },
            768: {
                items: 4,
                dots: false,
                nav: true,
            },
            992: {
                items: 5,
                dots: false,
                nav: true,
            }
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);
?>

<section class="partners partners_company section_standart">
    <div class="container">
        <h3 class="section_title">НАШИ ПАРТНЕРЫ</h3>
        <div class="row">
            <div class="col-md-10">
                <div class="list">
                    <a onclick="yaCounter44396998.reachGoal('onetwotrip'); return true;" href="https://www.onetwotrip.com/ru/" target="_blank" rel="nofollow noopener noreferrer" class="img_boxed">
                        <img src="/img/logo/onetwotrip.png" alt="">
                    </a>
                    <a onclick="yaCounter44396998.reachGoal('blablacar'); return true;" href="https://www.blablacar.ru/?utm_source=WID-FREE&utm_medium=other&utm_content=widget&utm_campaign=RU_WID-FREE_ALL_LOWTRIP&comuto_cmkt=RU_WID-FREE_ALL_LOWTRIP" target="_blank" rel="nofollow noopener noreferrer" class="img_boxed">
                        <img src="/img/logo/blablacar-logo.svg" alt="">
                    </a>
                    <a onclick="yaCounter44396998.reachGoal('burgerking'); return true;" href="https://burgerking.ru/" target="_blank" rel="nofollow noopener noreferrer" class="img_boxed">
                        <img src="/img/logo/burgerking.png" alt="">
                    </a>
                    <a onclick="yaCounter44396998.reachGoal('surprizeme'); return true;" href="https://surprizeme.ru/" target="_blank" rel="nofollow noopener noreferrer" class="img_boxed">
                        <img src="/img/logo/surprizeme-logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-md-2">
                <a class="be_partner popup_init" href="#popup_contact">Стать партнером</a>
            </div>
        </div>

    </div>
</section>