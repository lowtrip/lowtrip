<!--schemaProduct-->
<script type="application/ld+json">
{
    "@context":"http://schema.org/",
    "@type":"Product",
    "name": "<?= $name ?>",
    "description": "<?= $description ?>",
    "offers": {
        "@type":"AggregateOffer",
        <?php if (isset($lowPrice)) : ?>
        "lowPrice":"<?= $lowPrice ?>",
        <?php endif; ?>
        <?php if (isset($highPrice)) : ?>
        "highPrice":"<?= $highPrice ?>",
        <?php endif; ?>
        "priceCurrency":"RUB"
    }
}
</script>
<!--schemaProduct-->