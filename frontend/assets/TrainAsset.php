<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class TrainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $jsOptions = [
        //'async' => 'async',
    ];
    public $css = [
        'css/train.min.css'
    ];
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'https://cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/56724342b5934e18f8301e958867f182_1.js',
        //'https://vk.com/js/api/openapi.js?149',
        //'https://maps.googleapis.com/maps/api/js?key=AIzaSyDL9Y3TSB_MR_HoaJAPaNECUDC5Qc2zNvY',
        'js/train-bundle.min.js',
    ];
    // public $jsOptions = ['position' => \yii\web\View::POS_HEAD];
    public $depends = [
        //'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}