<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class SvahaAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $css = [
        'css/svaha.min.css',
    ];
    public $js = [
        'https://api-maps.yandex.ru/2.1/?lang=ru_RU',
        'https://cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/56724342b5934e18f8301e958867f182_1.js',
        'js/svaha-bundle.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];
}