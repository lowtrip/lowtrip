<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class RedirectAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $publishOptions = ['forceCopy' => true];
    public $jsOptions = [
        //'async' => 'async',
    ];
    public $css = [
        'css/redirect.min.css',
        'css/loader.min.css',
    ];
//    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
//    ];
}