var TrainView = function(element, options) {

    var _this = this;

    var DAYS = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
    var MONTHS = ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];

    this.$container = null;
    this.$topbar = null;
    this.$loader = null;
    this.$trainResults = null;
    this.$offers = null;
    this.$timetable = null;

    this.tickets_stats = {
        departure: {
            min: 1440,
            max: 0
        },
        types: {
            0: {
                typeName: "Все типы",
                minPrice: Infinity,
                maxPrice: 0
            }
        }
    };

    this.sorting = {
        $el: null,
        value: 'departure'
    };

    // Структура данных хранящихся в фильтрах
    this.filters = {
        $el: null,
        types: {
            $el: null,
            value: null
        },
        price: {
            $el: null,
            value: {
                min: null,
                max: null
            }
        },
        departure: {
            $el: null,
            value: {
                min: null,
                max: null
            }
        }
    };

    this.buffer = [];

    this.svaha = {
        places: 900,
        coefficient: 0.08,
        men: 0.58,
        woman: 0.42,
        goals: {
            men: {
                1: 0.33,
                2: 0.40,
                3: 0.27
            },
            woman: {
                1: 0.68,
                2: 0.23,
                3: 0.09
            }
        }
    };

    var defaults = {
        api: TRAIN,
        source: trip,
        metrika: 'yaCounter44396998'
    };

    this.run = function() {
        if (this instanceof TrainView) {
            this.setViewElement(element);
            this.setOptions(options);
            this.getDataFromSource();

            this.renderBaseTemplate();
            this.initProgressbar();

            this.findTickets();
        }
    };

    this.init = function() {
        this.renderBaseTemplate();
        this.initProgressbar();

        this.findTickets();
    };

    this.setOptions = function(options) {
        this.options = $.extend(true, {}, defaults, options);
    };

    // Задать контейнер для отображения
    this.setViewElement = function(element) {
        this.$container = $(element);
    };

    // Базовый шаблон модуля
    this.renderBaseTemplate = function() {
        var viewBaseTemplate =
            '<div class="container">' +
            '<div class="row">' +
            '<div class="col-xs-12 col-md-8 col-md-offset-4 t_col-10">' +
            '<div class="train-loader" id="train-loader"></div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-xs-6 col-sm-8 col-sm-offset-4 t_col-10">' +
            '<div class="train-results-topbar" id="train-topbar"></div>' +
            '</div>' +
            '<div class="col-xs-6 col-sm-4 t_col-10">' +
            '<div class="train-results-filters" id="train-filters"></div>' +
            '</div>' +
            '<div class="col-xs-12 col-sm-8 t_col-10">' +
            '<div class="train-timetable-container" id="train-results">' +
            '<div class="train-results-offers" id="train-offers"></div>' +
            '<div class="train-timetable-items" id="train-timetable"></div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        this.$container.empty().append(viewBaseTemplate);
        this.$topbar = this.$container.find('#train-topbar');
        this.$loader = this.$container.find('#train-loader');
        this.$filters = this.$container.find('#train-filters');
        this.$trainResults = this.$container.find('#train-results');
        this.$offers = this.$container.find('#train-offers');
        this.$timetable = this.$trainResults.find('#train-timetable');
    };

    // Создаем прогрессбар и добавляем в шаблон
    this.initProgressbar = function() {
        this.progressbar = {
            $el: undefined,
            value: 0
        };
        this.renderProgressBarTemplate(this.$loader);
    };

    // Изменяем значения прогрессбара, добавляя value
    this.changeProgressbar = function(value) {
        this.progressbar.value += +value;
        this.renderProgressbarValue();
    };

    // Отрисовка значения (в процентах) прогрессбара
    this.renderProgressbarValue = function() {
        this.progressbar.$el.css('width', this.progressbar.value + '%');
    };

    // Шаблон прогрессбара добавляется к переданному контейнеру
    this.renderProgressBarTemplate = function($container) {
        var template =
            '<div id="loader-text">Выполняется поиск жд билетов...</div>' +
            '<div class="progress lowtrip-progress-bar">' +
            '<div id="progressbar" class="progress-bar" role="progressbar" style="width:0;"></div>' +
            '</div>';

        $container.append(template);

        this.progressbar.$el = $container.find('#progressbar');
    };

    // Задать апи для получения данных
    this.setTrainApi = function(api) {
        this.options.api = api;
    };

    // Задать источник для получения данных
    this.setSource = function(source) {
        this.options.source = source;
    };

    // Получить данные для запроса из источника данных
    this.getDataFromSource = function() {
        this.searchData = this.options.source.getData();
    };

    // Обертка для поиска станций
    // Изменение значение прогрессбара
    this.findStation = function(object) {
        this.changeProgressbar(20);
        return this.options.api.findStation(object);
    };

    // Получить данные о билетах через апи
    // Изменение значение прогрессбара
    this.findTickets = function() {
        Promise.all([
            this.findStation(this.searchData.first),
            this.findStation(this.searchData.second),
        ]).then(stations => {
            this.changeProgressbar(20);
            return this.options.api.findTrains(stations[0].stations_id, stations[1].stations_id, this.searchData.date);
        }).then(results => {
            this.changeProgressbar(40);

            // ВОТ ЭТУ ЧАСТЬ ПЕРЕПИСАТЬ!!
            this.saveTickets(results);
            this.analyzeTickets();
            this.analyzeBestTickets();

            this.renderTicketsToCache()
                .then(this.renderResults());

        }).catch(e => {
            this.handleError(e);
        });
    };

    // Сохраняем билеты в свойство this.tickets
    // Добавляем билету свойство position - для соответствия с его отображением
    this.saveTickets = function(tickets) {
        this.tickets = tickets.map(function(ticket, i) {
            ticket.position = i;
            return ticket;
        });
    };

    // В свойство tickets_stats будем записывать max и min значения для фильтров
    this.analyzeTickets = function() {

        for(var i = 0; i < this.tickets.length; i++) {
            //for(var i = this.tickets.length-1; i--;) {
            this.collectTypesWithPrices(this.tickets[i]);
            this.parseDataTime(this.tickets[i], 'departure');
            this.parseDataTime(this.tickets[i], 'arrival');
            this.parseDuration(this.tickets[i]);
            this.analyzeMovement(this.tickets[i], 'departure');
            this.analyzePlaces(this.tickets[i]);
        }
    };

    // Анализ занятых мест
    // Для ЖД СВАХА
    this.analyzePlaces = function(ticket) {
        var freePlaces = 0,
            employed = 0,
            men = 0,
            woman = 0;

        ticket.places.forEach(function(place) {
            freePlaces += place.freeSeats;
        });

        employed = this.svaha.places - freePlaces;
        men = Math.ceil(employed * this.svaha.coefficient * this.svaha.men);
        woman = Math.ceil(employed * this.svaha.coefficient * this.svaha.woman);

        if (this.options.friendGoal) {
            var k = this.options.friendGoal;

            men = Math.ceil(men * this.svaha.goals.men[k]);
            woman = Math.ceil(woman * this.svaha.goals.woman[k]);
        }

        ticket.friends = {
            all: employed,
            men: men,
            woman: woman
        };

    };

    // Функция проводит анализ билетов по 3м типам, добавляя билету поле marks
    // Самый дешевый, самый быстрый, комфортный (дешевый среди дорогого типа; максим. - СВ)
    // Выполняется после функции analyzeTickets !!
    this.analyzeBestTickets = function() {
        var cheap = {},
            fast = {},
            comfort = {},
            i,
            prop,
            pos,
            type = 0;

        // Находим комфортный тип и запоминаем его минимальную стоимость
        for (prop in this.tickets_stats.types) {
            if (prop > type && this.tickets_stats.types[prop].typeName !== 'Люкс') {
                type = parseInt(prop);
                comfort.type = type;
                comfort.price = this.tickets_stats.types[prop].minPrice;
            }
        }

        // Находим лучшие билеты и запоминаем их позиции
        for(i = 0; i < this.tickets.length; i++) {
            if (this.tickets[i].minPrice < cheap.price || cheap.price === undefined) {
                cheap.price = this.tickets[i].minPrice;
                cheap.duration = this.tickets[i].duration;
                cheap.position = i;
            }

            if (this.tickets[i].durationInMinutes < fast.durationInMinutes || fast.durationInMinutes === undefined) {
                fast.price = this.tickets[i].minPrice;
                fast.durationInMinutes = this.tickets[i].durationInMinutes;
                fast.duration = this.tickets[i].duration;
                fast.position = i;
            }

            this.tickets[i].places.forEach(function(place, idx) {
                if (place.type === type && place.cost === comfort.price) {
                    comfort.position = i;
                    comfort.duration = _this.tickets[i].duration;
                }
            });
        }

        this.tickets_stats.offers = {
            cheap: cheap,
            fast: fast,
            comfort: comfort
        };

        // Добавляем лучшим билетам отметки marks
        for(prop in this.tickets_stats.offers) {
            pos = this.tickets_stats.offers[prop].position;
            if (this.tickets[pos].marks === undefined) {
                this.tickets[pos].marks = [];
            }
            this.tickets[pos].marks.push(prop);
        }

        // Очищаем локальные переменные для сборщика мусора
        cheap = fast = comfort = i = prop = type = pos = null;
    };

    // Сбор типов и диапазонов цен
    // Принимает в качестве аргумента билет
    // Заодно добавляем билету свойство минимальная стоимость minPrice
    this.collectTypesWithPrices = function(ticket) {
        var minPrice;

        ticket.places.forEach(function(place, idx) {
            var cost;

            // Приводим стоимость типа места к int
            cost = ticket.places[idx].cost = parseInt(place.cost);

            if (cost < minPrice || minPrice === undefined) {
                minPrice = cost;
            }

            // Если типа в статистике не существует, добавляем его
            if (_this.tickets_stats.types[place.type] === undefined) {
                _this.tickets_stats.types[place.type] = {
                    typeName: place.typeName,
                    minPrice: Infinity,
                    maxPrice: 0
                };
            }

            // Анализ диапазона цен
            if (_this.tickets_stats.types[place.type].minPrice > cost) {
                _this.tickets_stats.types[place.type].minPrice = cost;
            }

            if (_this.tickets_stats.types[0].minPrice > cost) {
                _this.tickets_stats.types[0].minPrice = cost;
            }

            if (_this.tickets_stats.types[place.type].maxPrice < cost) {
                _this.tickets_stats.types[place.type].maxPrice = cost;
            }

            if (_this.tickets_stats.types[0].maxPrice < cost) {
                _this.tickets_stats.types[0].maxPrice = cost;
            }

        });

        ticket.minPrice = minPrice;

        // Очищаем локальные переменные для сборщика мусора
        minPrice = null;
    };

    // Сбор min max значений времени отправления и прибытия (в минутах)
    this.analyzeMovement = function(ticket, movement) {
        if (ticket[movement].minutes < this.tickets_stats[movement].min) {
            this.tickets_stats[movement].min = ticket[movement].minutes;
        }
        if (ticket[movement].minutes > this.tickets_stats[movement].max) {
            this.tickets_stats[movement].max = ticket[movement].minutes;
        }
    };

    // Приводим данные time об arrival или departure к человеческому виду
    // date: YYYY-MM-DD; time: hh-mm
    // movement = [arrival, departure];
    this.parseDataTime = function(ticket, movement) {
        var old,
            dat;

        old = ticket[movement].time;
        old = old + '.000+03:00';
        dat = new Date(Date.parse(old));

        ticket[movement].string = old;
        ticket[movement].day = old.slice(8,10);
        ticket[movement].date = old.slice(0,10);
        ticket[movement].time = old.slice(11,16);
        ticket[movement].day_name = DAYS[dat.getDay()];
        ticket[movement].month = MONTHS[dat.getMonth()];
        ticket[movement].minutes = dat.getHours() * 60 + dat.getMinutes();

        // Очищаем локальные переменные для сборщика мусора
        old = dat = null;
    };

    // Продолжительность пути в минутах преобразуем в строку
    this.parseDuration = function(ticket) {
        var duration,
            minutes,
            hours,
            days;

        duration = parseInt(ticket.durationInMinutes);
        minutes = duration % 60;
        hours = (duration - minutes) / 60;
        if (hours > 24) {
            days = parseInt(hours / 24);
            hours = hours - days * 24;
            ticket.duration =  days + 'д ' + hours + 'ч ' + minutes + 'м';
        }
        else {
            ticket.duration =  hours + 'ч ' + minutes + 'м';
        }

        // Очищаем локальные переменные для сборщика мусора
        duration = minutes = hours = days = null;
    };

    // Количество минут преобразуем в строку hh:mm
    this.parseMinutesToTime = function(value) {
        var hours,
            minutes;

        hours = Math.floor(value / 60);
        minutes = value - (hours * 60);
        if(hours.toString().length === 1) hours = '0' + hours;
        if(minutes.toString().length === 1) minutes = '0' + minutes;

        return hours+':'+minutes;
    };


    // Функция рендера html отображения всех билетов,
    // которое сохраняется в свойство tickets_views
    this.renderTicketsToCache = function() {
        return new Promise(function(resolve, reject) {
            _this.tickets_views = {};
            _this.tickets.forEach(function(ticket, idx) {
                _this.tickets_views[idx] = _this.renderTicketView(ticket, idx);
            });
            resolve();
        });
    };

    // Рендеринг билета и return его view
    this.renderTicketView = function(ticket, idx) {
        var template,
            classes = '',
            marks = '',
            places = '',
            name,
            i;

        var friends,
            friendsView = '';

        var lowtrip_marker = '&marker=lowtrip&scp=180,affiliate,4368-14624-0-2';
        var adults = '&adults=' + this.searchData.adults;

        name = (ticket.name !== undefined) ? '&#171;'+ticket.name+'&#187;' : '';

        if (ticket.class !== undefined) {
            for (i = 0; i < ticket.class.length; i++) {
                classes += '<span class="train-type">' + ticket.class[i] + '</span>';
            }
        }

        if (ticket.marks !== undefined) {
            for (i = 0; i < ticket.marks.length; i++) {
                var str;
                switch (ticket.marks[i]) {
                    case "fast":
                        str = "Самый быстрый";
                        break;
                    case "cheap":
                        str = "Самый дешёвый";
                        break;
                    case "comfort":
                        str = "Комфортный";
                        break;
                }
                marks += '<div class="marker ' + ticket.marks[i] + '">' + str + '</div>';
            }
        }

        // для жд сваха
        // выводить друзей
        if (this.options.friends && this.options.friendType) {
            var friendType = this.options.friendType;
            var friendTypeName = '';

            switch (friendType) {
                case 'men' :
                    friendTypeName = "Парней";
                    break;
                case 'woman':
                    friendTypeName = "Девушек";
                    break;
            }

            friends = {
                type: friendType,
                typeName: friendTypeName,
                count: ticket.friends[friendType],
            };

            friendsView =
                '<div class="train-friends">' +
                    '<div class="has_tooltip train-friends-sex '+ friends.type +'" title="Уже купили билет, и ваши интересы совпадают">' +
                        '<span class="train-friends__count">'+ friends.count +' </span>' +
                        '<span class="train-friends__typename">'+ friends.typeName +'</span>' +
                    '</div>' +
                '</div>';
        }

        for(i = 0; i < ticket.places.length; i++) {
            var offers = '';
            var type = '&carType=' + ticket.places[i].type;
            var link = ticket.deeplink + adults + type + lowtrip_marker;
            var redirect = '/redirect/onetwotrip?url=' + encodeURIComponent(link);


            if (this.tickets_stats.offers.comfort.position === idx) {
                if (this.tickets_stats.offers.comfort.type === ticket.places[i].type) {
                    offers += 'comfort';
                }
            }
            if (this.tickets_stats.offers.cheap.position === idx) {
                if (this.tickets_stats.offers.cheap.price === ticket.places[i].cost) {
                    offers += ' cheap';
                }
            }

            places +=
                '<a class="train-place '+ offers +'" href="'+ redirect +'" target="_blank" rel="nofollow" onclick="yaCounter44396998.reachGoal(\'train\'); yaCounter44396998.reachGoal(\'svaha-train\'); return true;">' +
                '<div class="place-type">'+ ticket.places[i].typeName +'</div>' +
                '<div class="place-price">от <span class="price-value">'+ ticket.places[i].cost +' Р</span></div>' +
                '<div class="places-count">Мест '+ ticket.places[i].freeSeats +'</div>' +
                '</a>';
        }

        template =
            '<div class="train-ticket">' +
            '<div class="train-ticket-marks">'+ marks +'</div>' +
            '<div class="train-ticket-header">' +
            '<div class="train-ticket-header-info">' +
            '<div class="train-number">' +
            '<span class="has_tooltip" title="Номер поезда">'+ ticket.trainNumber +'</span>' +
            '</div>' +
            '<div class="train-types">' +
            classes +
            '</div>' +
            '<div class="train-name">' +
            '<span class="has_tooltip" title="Название поезда">'+ name +'</span>' +
            '</div>' +
                friendsView +
            '</div>' +
            '<div class="train-ticket-header-time">' +
            '<div class="train-stations-times">' +
            '<div class="train-station-column">' +
            '<div class="column-name">Отправление</div>' +
            '<div class="station-timing">' +
            '<div class="value has_tooltip" title="Время отправления (по мск)">' +
            '<span class="time">'+ ticket.departure.time +'</span><span class="type">Мск</span>' +
            '</div>' +
            '<div class="date">'+ ticket.departure.day + ' ' + ticket.departure.month +', '+ ticket.departure.day_name +'</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-timeline-column">' +
            '<div class="train-timeline"><span class="has_tooltip" title="Время в пути">'+ ticket.duration +'</span></div>' +
            '</div>' +
            '<div class="train-station-column">' +
            '<div class="column-name">Прибытие</div>' +
            '<div class="station-timing">' +
            '<div class="value has_tooltip" title="Время прибытия (по мск)">' +
            '<span class="time">'+ ticket.arrival.time +'</span><span class="type">Мск</span>' +
            '</div>' +
            '<div class="date">'+ ticket.arrival.day + ' ' + ticket.arrival.month +', '+ ticket.arrival.day_name +'</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-stations-names">' +
            '<div class="station-name has_tooltip dep" title="Вокзал отправления">'+ ticket.from.station +'</div>' +
            '<div class="station-name has_tooltip arr" title="Вокзал прибытия">'+ ticket.to.station +'</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-ticket-separator">' +
            '<div class="left-corner"></div><div class="separator-line"></div><div class="right-corner"></div>' +
            '</div>' +
            '<div class="train-ticket-body">' +
            places +
            '</div>' +
            '</div>';

        return template;
    };

    // ПЕРЕПИСАТЬ !!!! УБРАТЬ ДАЛЬНЕЙШУЮ ЛОГИКУ!!!
    this.renderResults = function() {

        var results = '',
            i;

        this.tickets.forEach(function(ticket, idx) {
            results += _this.tickets_views[ticket.position];
        });

        // Нужно добавить рендер количества найденных поездов и сортировки
        this.$loader.hide();
        this.renderTopBar();

        this.renderOffers();
        this.renderFilters();
        this.clearTimetable();
        this.fillTimetable(results);
    };

    // Рендер билетов с вызовом рендера каждого билета
    this.renderTicketsArray = function(tickets) {
        var results = '';
        tickets.forEach(function(ticket, idx) {
            results += _this.renderTicketView(ticket);
        });

        this.clearTimetable();
        this.fillTimetable(results);
    };

    // Рендер одного билета с позицией position в контейнер
    this.renderOfferTicket = function(position) {
        var results = '';

        this.tickets.forEach(function(ticket, idx) {
            if (ticket.position === position) {
                results += _this.tickets_views[position];
            }
        });

        this.clearTimetable();
        this.fillTimetable(results);
    };


    // Рендеринг лучших предложений
    this.renderOffers = function() {
        var offers = '',
            prop,
            prop_name,
            title,
            style,
            type,
            i = 0;

        this.$offers.empty();

        offers = '<div class="offers-container">';

        for(prop in this.tickets_stats.offers) {
            switch (prop) {
                case 'cheap':
                    prop_name = 'Самый дешёвый';
                    title = 'Показать самый дешёвый билет';
                    break;
                case 'fast':
                    prop_name = 'Самый быстрый';
                    title = 'Показать билет на самый быстрый поезд';
                    break;
                case 'comfort':
                    prop_name = 'Комфортный';
                    title = 'Самое комфортное место, по разумной цене. Учитываются такие параметры, как тип вагона, стоимость места.';
                    break;
            }

            type = (i === 0) ? 'state-hover' : '';
            style = (i < 2) ? 'border-right' : '';

            offers +=
                '<div class="train-offer-item has_tooltip '+ prop +' '+ type+'" data-offer="'+ prop +'" data-prop="'+ prop_name +'" title="'+ title +'">' +
                '<div class="offer-item-content '+ style +'">' +
                '<div class="offer-title">'+ prop_name +'</div>' +
                '<div class="offer-cost">'+ this.tickets_stats.offers[prop].price +' Р</div>' +
                '<div class="offer-duration">'+ this.tickets_stats.offers[prop].duration +'</div>' +
                '</div>' +
                '</div>';

            i++;
        }

        offers +=
            '</div>' +
            '<div class="show-all-tickets not-visible">Показать все билеты</div>';

        this.$offers.append(offers);
        this.initTooltips(this.$offers);

        // Навешиваем обработчик события на предложения
        this.$offers.on('click', '.train-offer-item', this._onClickOfferItem.bind(this));
        this.$offers.on('click', '.show-all-tickets', this._onClickShowAllTickets.bind(this));
    };

    // Рендер верхней панели с количеством результатов и сортировкой
    this.renderTopBar = function() {
        var template;

        template =
            '<div class="train-results-topbar">' +
            '<div class="train-topbar-content">' +
            '<div class="train-results-count">Найдено поездов: '+ this.tickets.length +'</div>' +
            '<div class="train-results-sorting" id="train-sorting">' +
            '<div class="train-mobile filter-btn" id="sorting-mobile-btn">Сортировка</div>' +
            '<div class="sorting-content" id="sorting-content">' +
            '<span class="sorting-title">Сортировать</span>' +
            '<div class="sorting-select-wrapper">' +
            '<select class="train-sorting-select" name="sorting" id="sorting-select">' +
            '<option value="departure">По времени отправления</option>' +
            '<option value="arrival">По времени прибытия</option>' +
            '<option value="duration">По времени в пути</option>' +
            '<option value="cost">По стоимости</option>' +
            '</select>' +
            '</div>' +
            '<div class="apply-btn" id="sorting-apply">Применить сортировку</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        this.$topbar.append(template);

        this.$sorting = this.$topbar.find('#train-sorting');
        this.sorting.$el = this.$sorting.find('#sorting-select');
        this.sorting.$el.on('change', this._onChangeSorting.bind(this));
        this.$sorting.on('click', '#sorting-mobile-btn' , this._onClickSortingMobileBtn.bind(this));
        this.$sorting.on('click', '#sorting-apply', this._onClickSortingApplyBtn.bind(this));
    };

    // Рендер всех фильтров и начального состояния значений
    this.renderFilters = function() {
        this.$filters.append(
            '<div class="train-mobile filter-btn" id="filter-mobile-btn">Фильтры</div>' +
            '<div class="filters-content" id="filters-content">' +
            '<div class="apply-btn" id="filters-apply">Применить фильтры</div>' +
            '</div>');
        this.filters.$el = this.$filters.find('#filters-content');

        this.renderFilterTypes();
        this.renderFilterPrice();
        this.renderFilterDeparture();

        this.setInitialStateFilters();

        this.filters.$el.on('click', '.filter-item-header', this._onClickFilterHeader.bind(this));
        this.$filters.on('click', '#filter-mobile-btn', this._onClickFilterMobileBtn.bind(this));
        this.$filters.on('click', '#filters-apply', this._onClickFilterApplyBtn.bind(this));
    };

    // Функция инициализации начального состояния фильтров
    this.setInitialStateFilters = function() {
        this.filters.types.value = 0;
        this.filters.price.value.min = this.tickets_stats.types[0].minPrice;
        this.filters.price.value.max = this.tickets_stats.types[0].maxPrice;
        this.filters.departure.value.min = 0;
        this.filters.departure.value.max = 1440;
    };

    this.setSlidersInitialValues = function() {
        this.filters.price.$slider.slider('value', this.tickets_stats.types[0].maxPrice);
        this.changeFilterPriceValues(this.filters.price, this.tickets_stats.types[0].maxPrice);
        this.filters.departure.$slider.slider('values', [0, 1440]);
        this.changeDepartureOutputValues(this.filters.departure, 0, 1440);
        this.filters.types.$el.find('input#type0').prop("checked", true);
    };

    this.renderFilterTypes = function() {
        var template,
            prop,
            checked,
            i = 0;

        template =
            '<div class="filter-item opened" id="filter-types">' +
            '<div class="filter-item-header">' +
            '<span class="filter-item-icon"></span>' +
            '<span class="filter-item-title">Тип вагона</span>' +
            '<span class="filter-item-reset"></span>' +
            '</div>' +
            '<div class="filter-item-body">';

        for(prop in this.tickets_stats.types) {
            checked = (i === 0) ? "checked" : "";
            template +=
                '<div class="filter-label-block">' +
                '<input class="hidden_input for-filter-checkbox" type="radio" name="type" '+ checked +' value="'+ prop +'" id="type'+ prop +'">' +
                '<label class="filter-checkbox" for="type'+ prop +'">' +
                '<span class="filter-name">'+ this.tickets_stats.types[prop].typeName +'</span>' +
                '<span class="filter-from-price">от '+ this.tickets_stats.types[prop].minPrice +' Р</span>' +
                '</label>' +
                '</div>';
            i++;
        }

        template +=
            '</div></div>';

        this.filters.$el.append(template);
        this.filters.types.$el = this.filters.$el.find('#filter-types');

        this.filters.types.$el.on('change', 'input:radio', this._onChangeFilterType.bind(this));
    };

    this.renderFilterPrice = function() {
        var template;

        template =
            '<div class="filter-item opened" id="filter-price">' +
            '<div class="filter-item-header">' +
            '<span class="filter-item-icon"></span>' +
            '<span class="filter-item-title">Цена билета</span>' +
            '<span class="filter-item-reset"></span>' +
            '</div>' +
            '<div class="filter-item-body">' +
            '<div class="filter-range-block">' +
            '<div class="range-output-block">' +
            '<div class="range-output-label">' +
            'от <span id="price-range-from" class="output-input">'+ this.tickets_stats.types[0].minPrice +'</span> Р' +
            '</div>' +
            '<div class="range-output-label">' +
            'до <span id="price-range-to" class="output-input">'+ this.tickets_stats.types[0].maxPrice +'</span> Р' +
            '</div>' +
            '</div>' +
            '<div id="price-range" class="filter-range"></div>' +
            '</div>' +
            '</div>' +
            '</div>';

        this.filters.$el.append(template);
        this.filters.price.$el = this.filters.$el.find('#filter-price');
        this.filters.price.$slider = this.filters.price.$el.find('#price-range');
        this.filters.price.$from = this.filters.price.$el.find('#price-range-from');
        this.filters.price.$to = this.filters.price.$el.find('#price-range-to');

        this.filters.price.$slider.slider({
            range: "min",
            min: _this.tickets_stats.types[0].minPrice,
            max: _this.tickets_stats.types[0].maxPrice,
            step: 10,
            value: _this.tickets_stats.types[0].maxPrice,
            change: function(event, ui) {
                _this._onChangeFilterPrice(event, ui.value);
            },
            slide: function(event, ui) {
                _this.changeFilterPriceValues(_this.filters.price, ui.value);
            }
        });
    };

    this.renderFilterDeparture = function() {
        var template;

        template =
            '<div class="filter-item opened" id="filter-departure">' +
            '<div class="filter-item-header">' +
            '<span class="filter-item-icon"></span>' +
            '<span class="filter-item-title">Время отправления</span>' +
            '<span class="filter-item-reset"></span>' +
            '</div>' +
            '<div class="filter-item-body">' +
            '<div class="filter-range-block">' +
            '<div class="range-output-block">' +
            '<div class="range-output-label">' +
            'c <span id="departure-range-from" class="output-input">00:00</span>' +
            '</div>' +
            '<div class="range-output-label">' +
            'до <span id="departure-range-to" class="output-input">24:00</span>' +
            '</div>' +
            '</div>' +
            '<div id="departure-range" class="filter-range"></div>' +
            '</div>' +
            '</div>' +
            '</div>';

        this.filters.$el.append(template);
        this.filters.departure.$el = this.filters.$el.find('#filter-departure');
        this.filters.departure.$slider = this.filters.departure.$el.find('#departure-range');
        this.filters.departure.$from = this.filters.departure.$el.find('#departure-range-from');
        this.filters.departure.$to = this.filters.departure.$el.find('#departure-range-to');

        this.filters.departure.$slider.slider({
            range: true,
            min: 0,
            max: 1440,
            step: 15,
            values: [ 0, 1440 ],
            change: function(event, ui) {
                _this._onChangeFilterDeparture(event, ui.values);
            },
            slide: function(event, ui) {
                _this.changeDepartureOutputValues(_this.filters.departure, ui.values[0], ui.values[1]);
            }
        });
    };

    // Изменение значений вывода в фильтре времени отправления
    this.changeDepartureOutputValues = function(filter_name, min, max) {
        var start,
            end;

        start = this.parseMinutesToTime(min);
        end = this.parseMinutesToTime(max);
        filter_name.$from.text(start);
        filter_name.$to.text(end);

        start = end = null;
    };

    // Изменение значений вывода в фильтре filter_name
    this.changeFilterPriceValues = function(filter_name, max) {
        filter_name.$to.text(max);
    };

    // Выбор, откуда рендерить билеты. Из buffer или из tickets
    this.smartRenderTickets = function() {
        if (this.buffer !== null && this.buffer.length > 0) {
            this.renderTicketsArray(this.buffer);
        }
        else {
            this.setInitialStateFilters();
            this.setSlidersInitialValues();
            this.processing();
        }
    };

    // Обработчик события при клике на предложение
    this._onClickOfferItem = function(e) {
        var $offer,
            $btn,
            data,
            position;

        $offer = $(e.target).closest('.train-offer-item');
        $btn = $offer.closest('.train-results-offers').find('.show-all-tickets');

        data = $offer.attr('data-offer');

        if ($offer.hasClass('active')) {
            $offer.removeClass('active');
            $btn.removeClass('visible').addClass('not-visible');
            this.smartRenderTickets();
        }
        else {
            $offer.addClass('active').removeClass('state-hover').siblings('.train-offer-item').removeClass('active state-hover');
            $btn.removeClass('not-visible').addClass('visible');
            position = this.tickets_stats.offers[data].position;
            this.renderOfferTicket(position);
        }

        if (window[this.options.metrika] !== undefined) {
            console.log('Metrika is exist');
            window[this.options.metrika].reachGoal('train-'+ data);
        }
        else {
            console.log('Metrika not exists');
        }
    };

    // Обработчик события при нажатии показать все билеты
    this._onClickShowAllTickets = function(e) {
        this._resetOffersState();
        this.smartRenderTickets();
    };

    // Вернуть начальное состояние лучшим предложениям
    this._resetOffersState = function() {
        var $btn,
            $offers;

        $btn = this.$offers.find('.show-all-tickets');
        $offers = this.$offers.find('.train-offer-item');

        $offers.removeClass('active');
        $btn.removeClass('visible').addClass('not-visible');
    };

    // Обработчик события при клике на заголовок фильтра
    this._onClickFilterHeader = function(e) {
        var $filter_header,
            $filter_item;

        $filter_header = $(e.target).closest('.filter-item-header');
        $filter_item = $filter_header.closest('.filter-item');

        if ($filter_item.hasClass('opened')) {
            $filter_item.removeClass('opened').addClass('closed');
        }
        else {
            $filter_item.removeClass('closed').addClass('opened');
        }
    };

    // Обработчик события при нажатии мобильной кнопки фильтры
    this._onClickFilterMobileBtn = function(e) {
        this.filters.$el.addClass('mobile-open');
        $('html, body').css('overflow-y', 'hidden');
    };

    // Обработчик события при нажатии мобильной кнопки Применить фильтры
    this._onClickFilterApplyBtn = function(e) {
        this.filters.$el.removeClass('mobile-open');
        $('html, body').css('overflow-y', 'auto');
    };

    // Обработчик события при нажатии мобильной кнопки сортировки
    this._onClickSortingMobileBtn = function(e) {
        this.$sorting.find('#sorting-content').addClass('mobile-open');
        $('html, body').css('overflow-y', 'hidden');
    };

    // Обработчик события при нажатии мобильной кнопки Применить сортировку
    this._onClickSortingApplyBtn = function(e) {
        this.$sorting.find('#sorting-content').removeClass('mobile-open');
        $('html, body').css('overflow-y', 'auto');
    };

    // Обработчик события при изменении типа в фильтре
    this._onChangeFilterType = function (e) {
        this.filters.types.value = +this.filters.types.$el.find('input:checked').val();
        this._resetOffersState();
        this.processing();
    };

    // Обработчик события при изменении времени отправления в фильтре
    // values = [min, max];
    this._onChangeFilterDeparture = function(e, values) {
        this.filters.departure.value.min = values[0];
        this.filters.departure.value.max = values[1];
        this._resetOffersState();
        if (e.originalEvent !== undefined) {
            this.processing();
        }
    };

    // Обработчик собития при изменении стоимости в фильтре
    this._onChangeFilterPrice = function(e, value) {
        this.filters.price.value.max = value;
        this._resetOffersState();
        if (e.originalEvent !== undefined) {
            this.processing();
        }
    };

    // Обработчик события при изменении сортировки
    this._onChangeSorting = function(e) {
        this.sorting.value = this.sorting.$el.val();
        this._resetOffersState();
        this.processing();
    };

    // Процесс фильтрации и сортировки, единая цепочка
    this.processing = function() {
        this.filterTickets()
            .then(function(tickets) {
                return _this.sortByProperty(tickets, _this.sorting.value);
            })
            .then(function(tickets) {
                if (tickets.length !== 0) {
                    _this.renderTicketsArray(tickets);
                }
                else {
                    _this.renderNoResults();
                }
            })
            .catch(function(e) {
                console.log(e);
            });
    };

    // Сортировка массива билетов tickets по свойству property
    this.sortByProperty = function(tickets, property) {
        if (property === 'departure') {
            tickets.sort(function(a,b) {
                a = Date.parse(a.departure.string);
                b = Date.parse(b.departure.string);
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'arrival') {
            tickets.sort(function(a,b) {
                a = Date.parse(a.arrival.string);
                b = Date.parse(b.arrival.string);
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'duration') {
            tickets.sort(function(a,b) {
                a = a.durationInMinutes;
                b = b.durationInMinutes;
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'cost') {
            if (this.filters.types.value === 0) {
                tickets.sort(function(a,b) {
                    a = a.minPrice;
                    b = b.minPrice;
                    return (a>b ? 1 : (a === b ? 0 : -1));
                });
            }
            else {
                tickets.sort(function(a,b) {
                    a = a.places[0].cost;
                    b = b.places[0].cost;
                    return (a>b ? 1 : (a === b ? 0 : -1));
                });
            }
        }
        return tickets;
    };

    // Фильтрация билетов по всем фильтрам с добавлением результата в буфер
    this.filterTickets = function() {
        return new Promise(function(resolve, reject) {
            var type,
                maxPrice,
                minTime,
                maxTime;

            type = _this.filters.types.value;
            maxPrice = _this.filters.price.value.max;
            minTime = _this.filters.departure.value.min;
            maxTime = _this.filters.departure.value.max;

            _this.buffer = null;
            _this.buffer = [];

            _this.buffer = _this.filterByAllFilters(_this.tickets, {
                type: type,
                maxPrice: maxPrice,
                minTime: minTime,
                maxTime: maxTime
            });

            _this.buffer = _this.buffer.map(function(item) {
                return Object.assign({}, item);
            });

            // Убираем места не подходящие по типу и стоимости
            _this.buffer = _this.buffer.filter(function(ticket, i) {
                var temp = [];
                ticket.places.forEach(function(place, idx) {
                    if (place.cost <= maxPrice && (type === place.type || type === 0)) {
                        temp.push(place);
                    }
                });
                if (temp.length !== 0) {
                    ticket.places = temp;
                    return true;
                }
                return false;
            });

            resolve(_this.buffer);
        });
    };

    // Функция фильтрации tickets по filters
    this.filterByAllFilters = function(tickets, filters) {
        return tickets.filter(function(ticket) {
            return ticket.places.some(function(place) {
                return filters.type === 0 || +place.type === filters.type;
            });
        })
            .filter(function(ticket) {
                return (ticket.departure.minutes >= filters.minTime) && (ticket.departure.minutes <= filters.maxTime);
            })
            .filter(function(ticket) {
                return ticket.minPrice <= filters.maxPrice;
            });
    };

    this.initTooltips = function(container) {
        if ($(window).width() > 767) {
            container.find('.has_tooltip').tooltip();
        }
    };

    // Очистка билетов
    this.clearTimetable = function() {
        this.$timetable.find('.has_tooltip').tooltip('destroy');
        this.$timetable.empty();
    };

    // Заполнение контейнера билетов
    this.fillTimetable = function(results) {
        this.$timetable.append(results);
        this.initTooltips(this.$timetable);
    };

    // Вывод сообщения, что нет результатов
    this.renderNoResults = function() {
        var template;

        template =
            '<div class="train-results-information-block">' +
            '<div class="information-content">' +
            'Нет билетов, соответствующих вашим фильтрам.' +
            '</div>' +
            '</div>';

        this.clearTimetable();
        this.fillTimetable(template);
        this.$offers.find('.show-all-tickets').removeClass('not-visible').addClass('visible');
    };


    this.renderTrainCalendar = function() {
        var startDate,
            template,
            i;

        var tutu = 'https://c45.travelpayouts.com/click?shmarker=53486&promo_id=1770&source_type=customlink&type=click&custom_url=https%3A%2F%2Fwww.tutu.ru%2F';

        var url = '/train/' + this.searchData.first.city_slug + '/' + this.searchData.second.city_slug;

        startDate= new Date(this.searchData.date);

        template =
            '<div class="train-results-information-block" id="train-calendar">' +
            '<div class="information-content">' +
            '<div class="calendar-title">На '+ startDate.getDate() +' ' + MONTHS[startDate.getMonth()] + ' нет билетов</div>' +
            '<p class="calendar-text">Начать поиск ближайших рейсов по маршруту<br>'+ this.searchData.first.city +' - '+ this.searchData.second.city +'</p>' +
            '<div class="calendar-dates">';

        for (i = 1; i <= 5; i++) {
            var newDate = new Date(this.searchData.date);
            newDate.setDate(newDate.getDate() + i);

            var date_string = (newDate.toISOString()).slice(0,10);

            var searchUrl = url + '?date=' + date_string + '&project=svaha' + '&self=' + window.svaha.self + '&find=' + window.svaha.find + '&goal=' + window.svaha.goal;
            template +=
                '<a class="date-item" href="'+ searchUrl +'" data-date="'+ date_string +'">' +
                '<div class="date-item-weekday">'+ DAYS[newDate.getDay()] +'</div>' +
                '<div class="date-item-date">'+ newDate.getDate() +'</div>' +
                '<div class="date-item-month">'+ MONTHS[newDate.getMonth()] +'</div>' +
                '</a>';
        }

        template +=
            '</div>' +
            '<div class="calendar-special-offer">' +
                '<div class="calendar-special-offer-text">' +
                    'Сейчас мы делаем сервис лучше, и вы сможете находить прямые поезда по данному маршруту, или добраться с пересадками. Пока мы работаем, вы можете найти нужный вам поезд здесь.' +
                '</div>' +
                '<div class="calendar-special-offer-partner">' +
                    '<a href="'+ tutu +'" target="_blank" onclick="yaCounter44396998.reachGoal(\'tutu\');"><img class="partner-logo" src="/img/logo/tutu.svg"></a>' +
                    '<a href="'+ tutu +'" target="_blank" class="partner-btn" onclick="yaCounter44396998.reachGoal(\'tutu\');">Перейти</a>' +
                '</div>' +
            '</div>' +
            '</div></div>';

        this.$loader.empty().append(template);

        this.$calendar = this.$loader.find('#train-calendar');
        this.$calendar.on('click', '.date-item', this._onClickDateItem.bind(this));
    };

    // Обработчик события При нажатии на дату календаря
    this._onClickDateItem = function(e) {
        var $el,
            href,
            date,
            url;

        e.preventDefault();
        $el = $(e.target).closest('.date-item');
        href = $el.attr('href');

        if ( window.history && history.pushState ) {
            date = $el.attr('data-date');
            trip.data.date = date;
            this.init();
            $('body').find('.js-current-date').text(this.formatDateString(date));

            url = window.location.pathname + '?date=' + date + '&project=svaha' + '&self=' + window.svaha.self + '&find=' + window.svaha.find + '&goal=' + window.svaha.goal;
            window.history.replaceState(null, document.title, url);
        }
        else {
            window.location.href = href;
        }
    };

    this.formatDateString = function(str) {
        var date;

        date = new Date(str);
        return date.getDate() + ' ' + MONTHS[date.getMonth()] + ' ' + date.getFullYear() + ' г.';
    };

    this.renderErrorMessage = function() {
        this.$loader.empty().append(this.error.text);
    };


    // Обработчик ошибки
    this.handleError = function(e) {
        this.error = e;

        if (this.error.type === 1) {
            this.renderTrainCalendar();
        }
        else {
            this.renderErrorMessage();
        }
    };

    this.run();
};