// Открыть попап выслать информацию о встрече в поезде
$(document).on('click', '.train-place', function(e){
    e.preventDefault();

    var link = $(this).attr('href');

    var sended = cookie.get('svaha-email');

    console.log(sended);

    if (!sended) {
        cookie.set('train-link', link, {
            expires: 60 * 60 * 24
        });

        $.magnificPopup.open({
            items: {
                src: $('#popup_email_train'),
            },
            callbacks: {
                open: function() {
                    $('html, body').addClass('fixed');
                },
                close: function() {
                    $('html, body').removeClass('fixed');
                }
            }
        });
    }
    else {
        document.location.href = link;
    }
});

$(document).on('click', '#popup_email_train #continue', function(e) {
    var link = cookie.get('train-link');

    cookie.delete('train-link');

    setTimeout(function(){
        document.location.href = link;
    }, 0);
});

