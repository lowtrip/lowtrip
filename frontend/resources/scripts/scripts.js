var cookie = {

  get: function (name) {
    var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  },


  // устанавливает cookie с именем name и значением value
  // options - объект с свойствами cookie (expires, path, domain, secure)
  set: function (name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires === "number" && expires) {
      var d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
      updatedCookie += "; " + propName;
      var propValue = options[propName];
      if (propValue !== true) {
        updatedCookie += "=" + propValue;
      }
    }

    document.cookie = updatedCookie;
  },

  delete: function (name) {
    cookie.set(name, "", {
      expires: -1
    })
  }

};

function reverseFormat(date){
    date = date.split(/-/);
    date = date[2] + date[1] + date[0];
    return date;
}

function getFormattedDate(date, divider) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return year + divider + month + divider + day;
}

// Определение даты и времени
var time = {
	timestamp: undefined,
	date: undefined,
	formatted: undefined,
	string: undefined,
	locale: 'ru',
	timeRequest: function(offset) {
		offset === undefined ? offset = 0 : offset;

		return new Promise(function(resolve, reject) {
			$.ajax({
				url: '/php/getDate.php',
				type: 'GET',
				dataType: 'json',
				success: function(a) {
					resolve((parseInt(a) + offset * 24 * 3600) * 1000);
				},
				error: function (xhr, ajaxOptions, thrownError) {
			        reject(thrownError);
			    }
			});
		})
	},
	setTimestamp: function(a) {
		time.timestamp = a;
	},
	timestampToDate: function() {
		time.date = new Date(time.timestamp);
	},
	dateToFormat: function() {
		var timezone = new Date().getTimezoneOffset();
		var date = new Date(time.timestamp - timezone * 60 * 1000);
		time.formatted = date.toISOString().slice(0,10);
	},
	dateToString: function() {
		var day = time.date.getDate();
		var month = time.date.getMonth();
		var year = time.date.getFullYear();
		var month_ru = ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
		time.string = day + ' ' + month_ru[month] + ' ' + year;
	},
	getTime: function(offset) {
		return new Promise (function(resolve, reject) {
			time.timeRequest(offset)
				.then(function(timestamp) {
					time.setTimestamp(timestamp);
					time.timestampToDate();
					time.dateToFormat();
					time.dateToString();
					resolve(time);
				});
		});
	}
};

function findBestDate() {
    return new Promise(function(resolve, reject){
        var date_form = $('#date-start').val();
        var date = new Date();

        if ( date_form === '' ) {
            var hours = date.getHours();
            var offset = 0;
            if ( hours >= 16 ) {
                offset = 1;
            }
            resolve(offset);
        }
        else {
            time.getTime()
                .then(function(){
                	var current = new Date(time.formatted);
                	var in_form = new Date(date_form);
                    var timeDiff = (in_form.getTime() - current.getTime()) / (1000 * 3600 * 24);
                    console.log(timeDiff);
                    if (timeDiff < 0) {
                    	updatePageDates(time.string);
					}
                    var offset = (timeDiff >= 0) ? timeDiff : 0;
                    resolve(offset);
                });
        }
    });
}

function updatePageDates(str) {
	$(document).find('.date-to-string').text(str);
}

// offset - максимальное количество доступных дней от сегодняшней даты, в днях
// start - сдвиг по старту
function initDatepicker(offset, start) {
	var $date_input = $('#date-input');
	var $date_start = $('#date-start');


	var minDate = new Date();
	var maxDate = new Date();
	var startDate = new Date();
    maxDate.setDate(minDate.getDate() + offset);
    startDate.setDate(minDate.getDate() + start);

    var myDatepicker = $date_input.datepicker({
        minDate: minDate,
        maxDate: maxDate,
        startDate: startDate,
		dateFormat: 'dd M yyyy',
        showOtherYears: false,
        selectOtherYears: false,
        autoClose: true,
        toggleSelected: false,
        position: 'bottom left',
		onShow: function(dp, animationCompleted) {
            if(!animationCompleted){
                var iFits = false;
                // Loop through a few possible position and see which one fits
                $.each(['bottom left', 'top left', 'right center'], function (i, pos) {
                    if (!iFits) {
                        dp.update('position', pos);
                        var fits = isElementInViewport(dp.$datepicker[0]);
                        if (fits.all) {
                            iFits = true;
                        }
                    }
                });
                if (!iFits) {
                    dp.update('position', 'bottom left');
				}
            }
            if (animationCompleted) {
            	dp.$el.addClass('datapicker-visible');
			}
		},
		onHide: function(dp, animationCompleted) {
        	if (animationCompleted) {
                dp.$el.removeClass('datapicker-visible');
			}
		},
		onSelect: function(formattedDate, date, inst) {
        	$date_start.val( getFormattedDate(date, '-') );
		}
	}).data('datepicker');

    $date_input.on('click', function(){
        if ( $(this).hasClass('datapicker-visible') && myDatepicker.visible ) {
            myDatepicker.hide();
		}
	});

    myDatepicker.selectDate(startDate);
}

function dateToString(dateObj) {
    var day = dateObj.getDate();
    var month = dateObj.getMonth();
    var year = dateObj.getFullYear();
    var month_ru = ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
	return day + ' ' + month_ru[month] + ' ' + year;
}


// A function to check wether the element fits inside the viewport:
function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    var fitsLeft = (rect.left >= 0 && rect.left <= $(window).width());
    var fitsTop = (rect.top >= 0 && rect.top <= $(window).height());
    var fitsRight = (rect.right >= 0 && rect.right <= $(window).width());
    var fitsBottom = (rect.bottom >= 0 && rect.bottom <= $(window).height());
    return {
        top: fitsTop,
        left: fitsLeft,
        right: fitsRight,
        bottom: fitsBottom,
        all: (fitsLeft && fitsTop && fitsRight && fitsBottom)
    };
}


// Выпадающие меню
$(document).on('click', '.has_ui', function(event){
	if ( $(event.target).closest('.ui-element').length > 0 ) {
        $(this).removeClass('active');
		return false;
	}
	if ( $(this).find('.ui-element').hasClass('show_this') ) {
		$(this).find('.ui-element').removeClass('show_this');
        $(this).removeClass('active');
	}
	else {
		$('.has_ui .ui-element.show_this').removeClass('show_this');
		$(this).find('.ui-element').addClass('show_this');
		$(this).addClass('active');
	}
});

// Если клик не по элементу интерфеса а по документу, скрываем открытые элементы
$(document).on('click', function(event){
	if ( $(event.target).closest('.has_ui').length > 0 ) {
		return;
	}
	$('.has_ui').removeClass('active');
	$('.ui-element.show_this').removeClass('show_this');
});

// Выбор значения в выпадающем меню
$(document).on('click', '.menu-helper li', function(){
	var $menu = $(this).closest('.menu-helper');
	var value = $(this).attr('data-value');
	var text = $(this).text();

	$menu.find('.helper-value').val(value);
	$menu.find('.helper-text').val(text);
	$menu.find('.dropdown-helper').removeClass('show_this');
});


// Плейсходер на инпутах
$(document).on('blur change', '.with_help', function(){
	if ($(this).val().length > 0) {
		$(this).siblings('.help').addClass('top');
	}
	else {
		$(this).siblings('.help').removeClass('top');
	}
});

//При выборе города сбрасываем фокус с input
$(document).on("autocompleteselect", ".city_input", function(event, ui){
	setTimeout(function(){
		$(':focus').blur();
	}, 0);
} );

$(document).on('click', '.city_input.form_ui_input', function(){
	$(this).select();
});

$(document).on('focusin', '.city_input.form_ui_input', function(){
    $(this).attr('placeholder', 'Название города...');
});

$(document).on('focusout', '.city_input.form_ui_input', function(){
    $(this).attr('placeholder', '');
});

$(document).on('focusin', '.form_ui_input', function(){
	$(this).closest('.group').addClass('focused');
});

$(document).on('focusout', '.form_ui_input', function(){
    $(this).closest('.group').removeClass('focused');
});

$(document).on('focusin click', '.has_hint', function(){
	$(this).closest('.group').find('.help-window').fadeOut(400);
});

// Анимация Кнопки Вертушка
$(document).on('click', '.btn_change.change_city', function(){
	var _this = $(this);
	_this.removeClass('rotate');
	setTimeout(function(){
		_this.addClass('rotate');
	}, 10);
});

// Закрытие всплывающего окна
// .close класс кнопки "закрыть", .help-window информационное окно
$(document).on('click', '.help-window .close', function(){
	$(this).closest('.help-window').hide();
});

// Открытие попапа
$('.popup_init').magnificPopup({
	fixedContentPos: false,
	callbacks: {
		open: function() {
			$('html, body').addClass('fixed');
		},
		close: function() {
			$('html, body').removeClass('fixed');
		}
	}
});

// Закрытие попапа
$(document).on('click', '.close_popup', function() {
	$.magnificPopup.close();
});

// Открыть попап с отзывом о городе
$(document).on('click', '.show-full-content', function(){
	var item = $(this).closest('.parent').find('.item-content').clone();
	var popup = $('#testimonial-full');
	popup.empty().append(item);
	popup.append('<button class="close_btn close_popup">Закрыть</button>');

	$.magnificPopup.open({
		fixedContentPos: false,
		items: {
			src: $('#testimonial-full'),
		},
		callbacks: {
			open: function() {
				$('html, body').addClass('fixed');
			},
			close: function() {
				$('html, body').removeClass('fixed');
			}
		}
	});
});

// Открыть попап сохранить маршрут
$(document).on('click', '.to_email_popup', function(){
    $.magnificPopup.open({
        items: {
            src: $('#popup_email'),
        },
        callbacks: {
            open: function() {
                $('html, body').addClass('fixed');
            },
            close: function() {
                $('html, body').removeClass('fixed');
            }
        }
    });
});

// Развернуть полный текст
$(document).on('click', '.toggle-btn', function(){
	var content = $(this).parent().find('.content-toggle');
	content.toggleClass('full');
	$(this).text() === 'Подробнее' ? $(this).text('Скрыть') : $(this).text('Подробнее');
});


$(document).on('click', '.switcher-btn', function(){
	var switcher = $(this).closest('.switcher');
	var parent = $(this).closest('.trip-offer');

	switcher.find('.switcher-btn').removeClass('active');
	$(this).addClass('active');

    var item = $(this).attr('data-item');
    parent.find('.trip-offer-item').removeClass('active');
    parent.find('#' + item).addClass('active');

    trip.price.show(item);
});

// Кнопка открыть скрытую форму поиска на мобилках
$(document).on('click', '#open-form', function(){
	if ( $('#search-form').is(':hidden') ) {
		$(this).addClass('open');
        $('#search-form').addClass('mobile-open');
		$('html, body').css('overflow-y', 'hidden');
	}
	else {
        $(this).removeClass('open');
        $('#search-form').removeClass('mobile-open');
        $('html, body').css('overflow-y', 'auto');
	}
});

$(document).on('click', function(event){
	if ( $(event.target).closest('form').length > 0 ) {
		return;
	}
	$('.form-el').removeClass('show_this');
});

function validateEmail(input) {
	var form = input.closest('form');
	var value = input.val();
	var msg = form.find('.status');

	if ( value == '' ) {
		input.addClass('invalid');
		msg.text('Введите email!');
		return false;
	}

	else if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value))  
  	{  	
  		msg.removeClass('show_this');
  		input.removeClass('invalid').addClass('valid');
    	return true; 
  	}  

  	else {
	  	input.removeClass('valid').addClass('invalid');
	    msg.text('Введите email в правильном формате!');
	    return false;
    }

}

function isValid(input) {
	var status;
	var type = input.attr('type');

	switch (type) {
		case 'email':
			status = validateEmail(input);
			break;
	}

	return status;
}

function formValidation(form) {
	var status = false;
	var msg = form.find('.status');

	$('input.validator', form).each(function(){
		status = isValid( $(this) );
		if ( status == false ) {
			$(this).focus();
			return;
		}
	});

	if (status == false) {
		console.log('inputs not valid');
		msg.addClass('show_this');
	}
	else {
		console.log('inputs valid');
		msg.removeClass('show_this');
	}

	return status;
}

$(document).on('click', 'form.validation button[type=submit]', function(e){
	e.preventDefault();
	var form = $(this).closest('form');
	var status = formValidation(form);

	if ( status === false ) {
		return false;
	}
	else {
		console.log('form is ok, submit');
		var action = form.attr('data-action');
		sendEmail(action, form);	
	}
});


function sendEmail(action, form) {
	switch (action) {
		case 'sendBk':
			sendBk(form);
			break;

		case 'sendTrip':
			sendTrip(form);
			break;
		case 'sendSvaha':
			sendSvaha(form);
			break;
	}
}

function sendBk(form) {
	var email = form.find('input[type=email]').val();
	console.log(email);
	$.ajax({
		type: "POST",
		url: "/php/send-bk.php",
		data: {
			email: email,
		},
		success: function(response) {
			console.log(response);
			yaCounter44396998.reachGoal('send-code');
		}
	});
	reset_form(form);
	alert('Промокод отправлен вам на почту. Спасибо за использование сервиса!');
	return false;
}

function sendTrip(form) {
	var data = getLowtripData();
	var email = form.find('input[type=email]').val();
	var type = form.attr('data-type');
	console.log(email);
	$.ajax({
		type: "POST",
		url: "/php/send.php",
		data: {
			email: email,
			data: data,
			type: type,
		},
		success: function(response) {
			console.log(response);
			yaCounter44396998.reachGoal('send-email');
		}
	});
	$('.popup_init').magnificPopup('close');
	reset_form(form);
	setTimeout(function(){
		$.magnificPopup.open({
			items: {
				src: '#popup_thanks'
			}
		});
	}, 500);
	return false;
}

function sendSvaha(form) {
    var email = form.find('input[type=email]').val();
    console.log(email);
    $.ajax({
        type: "POST",
        url: "/php/send-svaha.php",
        data: {
            email: email,
        },
        success: function(response) {
            console.log(response);
            cookie.set('svaha-email', true, {
                expires: 60 * 60 * 24 * 7
            });

            document.location.href = cookie.get('train-link');

            yaCounter44396998.reachGoal('send-svaha');
        }
    });
    reset_form(form);
    setTimeout(function(){
        $.magnificPopup.close();
    }, 500);
    return false;
}

function getLowtripData() {
	return JSON.stringify(trip.data);
}

function reset_form(form) {
	var inputs = form.find('input');
	$(inputs).each(function(){
		$(this).removeClass('valid');
	});
	$(form)[0].reset();
}

// Все доступные ближайшие города
var avaliableCities = [];

var first = '#first';
var second = '#second';

// Местоположение определено?
var locationFinded = false;

// Начал вводить город
var startedTyping = false;

//Сразу навешиваем обработчик на первый город - если ввод начался, startedTyping = true
$('#first').on('input keyup', function(){
    startedTyping = true;
});

// Валидация поисковой формы
function validation(){
    if ($(first).attr('data-valid') === 'valid' && $(second).attr('data-valid') === 'valid') {
        return true;
    }
    else if ($(first).attr('data-valid') === 'invalid') {
        validateCityInput($(first));
    }
    else {
        validateCityInput($(second));
    }
    return false;
}

// Валидация поля ввода города
function validateCityInput(input) {
    var help = input.closest('.group').find('.help-window');
    if (input.attr('data-valid') === 'invalid' && input.val().length > 0) {
        help.find('.text').text('Город с таким названием не найден. Начните ввод заново, и выберите город из выпадающего списка.');
        help.fadeIn(400);
    }
    if (input.attr('data-valid') === 'invalid' && input.val().length === 0) {
        help.find('.text').text('Поле не заполнено. Пожалуйста, введите название города.');
        help.fadeIn(400);
    }
}

// Сбор исходных данных для путешествия
function getTripData() {
    return {
        first: {
            label: $('#first').val(),
            id: $('#first-id').val(),
            city: $('#first-city').val(),
            city_slug: $('#first-city_slug').val(),
            region: $('#first-region').val(),
            region_slug: $('#first-region_slug').val(),
            country: $('#first-country').val(),
            country_slug: $('#first-country_slug').val(),
            lat: $('#first-lat').val(),
            lng: $('#first-lng').val(),
            gmt: $('#first-gmt').val()
        },
        second: {
        	label: $('#second').val(),
            id: $('#second-id').val(),
            city: $('#second-city').val(),
            city_slug: $('#second-city_slug').val(),
            region: $('#second-region').val(),
            region_slug: $('#second-region_slug').val(),
            country: $('#second-country').val(),
            country_slug: $('#second-country_slug').val(),
            lat: $('#second-lat').val(),
            lng: $('#second-lng').val(),
            gmt: $('#second-gmt').val()
        },
        placeType: 'museum',
        date: $('#date-start').val(),
        adults: $('#people').val()
    };
}

//Определение координат средствами API Yandex Maps
function yandexSearch(){
	var myCoords,
		myCityName;

	ymaps.ready(function(){
	    ymaps.geolocation.get({
		    provider: 'yandex',
		    autoReverseGeocode: true
		}).then(function (result) {
			var item = result.geoObjects.get(0)
			myCityName = item.properties.get('name');
			myCoords = item.geometry.getCoordinates();
			console.log('Город:' + myCityName + '; Координаты: ' + myCoords);
		    yandexAfterFind(myCityName, myCoords);
		    //yandexAfterFind("Кукуево", [0,0]);
		}).catch(function(e){
			console.log(e);
			notFoundNotification();
		});
	});
}

function yandexAfterFind(myCityName, myCoords) {
	$.ajax({
		url: '/city/find-city-yandex',
        type: 'GET',
        data: {
			city: myCityName,
			lat: myCoords[0],
			lng: myCoords[1]
		},
        dataType: 'json',
		success: function(value) {
			if (value !== false && startedTyping === false) {
                var obj = {
                    label:value.city,
                    city: value.city,
                    city_slug: value.city_slug,
                    region: value.region,
                    region_slug: value.region_slug,
                    country: value.country,
                    country_slug: value.country_slug,
                    lat: value.lat,
                    lng: value.lng,
                    id: value.id,
                    gmt: value.GMT
                };

                $(first).blur();
                setAttr(first, obj);
                findCities(first);
                locationFinded = true;
			}
            notFoundNotification();
		}
	});
}

function notFoundNotification() {
    if ( locationFinded === false && startedTyping === false) {
    	var $notification = $('#notification_city_1');
        $notification
            .find('.text')
            .text('Не удалось определить Ваше местоположение. Пожалуйста, введите название вашего города.');
        $notification.fadeIn(400);
    }
}

function setAttr(city, obj) {
	$(city).val(obj.label);
	$(city+'-id').val(obj.id);
	$(city+'-city').val(obj.city);
	$(city+'-city_slug').val(obj.city_slug);
	$(city+'-region').val(obj.region);
	$(city+'-region_slug').val(obj.region_slug);
	$(city+'-country').val(obj.country);
	$(city+'-country_slug').val(obj.country_slug);
	$(city+'-lat').val(obj.lat);
	$(city+'-lng').val(obj.lng);
	$(city+'-gmt').val(obj.gmt);

	$(city).attr('data-index', obj.index);

	if ($(city+'-id').val().length > 0 && $(city+'-city_slug').val().length > 0 ){
        $(city).attr('data-valid', 'valid');
	}
	else {
        $(city).attr('data-valid', 'invalid');
	}

	$(city).trigger('change');
}

// Функция для навешивания автокомплита на инпут city
function autocompleteList(city){
	var another = '#first';
	if (city === '#first'){
        another = '#second';
	}

	$(city).autocomplete({
		minLength: 2,
        autoFocus: true,
		appendTo: $(city).closest('.form-input-block'),
		source: function(request, response) {
			$.ajax({
                url: '/city/find-cities-by-term',
                type: 'GET',
                data: {
                    term: request.term,
                    lat: $(another+'-lat').val(),
            		lng: $(another+'-lng').val(),
					except: $(another+'-id').val()
                },
                dataType: 'json',
				success: function(data) {
                    response($.map( data, function(value){
                        return {
                            label:value.city,
                            city: value.city,
                            city_slug: value.city_slug,
                            region: value.region,
                            region_slug: value.region_slug,
                            country: value.country,
                            country_slug: value.country_slug,
                            lat: value.lat,
                            lng: value.lng,
                            id: value.id,
                            gmt: value.GMT,
							distance: value.distance,
						};
                    }));
				}
			});
		},
	})
    .autocomplete( "instance" )._renderItem = function( ul, item ) {
        var distance_string;
		if (item.distance === undefined) {
			distance_string = '';
		}
		else {
            distance_string = item.distance + ' км';
		}

        return $( "<li>" )
            .append(
            	'<div class="listbox">' +
					'<div class="cityname">'+ item.city +'<span class="distance">'+ distance_string +'</span></div>' +
					'<div class="region">'+ item.region +', '+ item.country +'</div>' +
				'</div>')
            .appendTo( ul );
    };

	var menu = $(city).autocomplete("widget");

    $(city).on('autocompletechange', function(event,ui) {
        if ( ui.item === null || ui.item  === undefined ){
        	if ( menu.children.length > 0 ) {
                $(menu[0].children[0]).click();
			}
			else {
                $(city).attr('data-index', -1).attr('data-valid', 'invalid');
			}
        }
        else if (city === '#first') {
            setAttr(city, ui.item);
            findCities(city);
        }
        else {
            setAttr(city, ui.item);
		}
    });
}

// Функция поиска ближайших городов
function findCities(city) {
	$.ajax({
		url: '/city/find-close-cities',
		type: 'POST',
		data: {
			lat: $(city + '-lat').val(),
			lng: $(city + '-lng').val(),
			id: $(city + '-id').val(),
		},
		dataType: 'json',
		error: function (xhr, ajaxOptions, thrownError) {
	        console.log(xhr.status);
	        console.log(thrownError);
	    },
		success: function(response){
			avaliableCities = [];

			$.each(response, function(key, value){
				avaliableCities.push({
					index: key,
					label:value.city,
					city: value.city,
					city_slug: value.city_slug,
					region: value.region,
					region_slug: value.region_slug,
					country: value.country,
					country_slug: value.country_slug,
					lat: value.lat,
					lng: value.lng, 
					id: value.id,
					gmt: value.GMT
				});
			});

            topCities(avaliableCities);
		}
	});
}

// Заполнение блока топ 5 городов
function topCities(list) {
	var i = 0;
	var $list = $('#top_cities_list');

    $list.empty();
	while ( (i < list.length) && (i < 5) ) {
		var item = '<li><span class="top_city_btn city_btn_change" onclick="yaCounter44396998.reachGoal(\'city-btn-'+ i +'\'); return true;" data-index="'+ i +'">'+ list[i].city +'</span></li>';
        $list.append(item);
		i++;
	}
}

// При нажатии на кнопку города из топ5, меняем его в поле второй город
$(document).on('click', '.city_btn_change', function(){
	var index = $(this).attr('data-index');
	setAttr(second, avaliableCities[index]);
	if ( $(window).width() < 991 ) {
		$('#search-form').addClass('mobile-open').show();
	}
});

// Нажатие кнопки сменить город
$(document).on('click', '#change_city', function(){
	var index = parseInt( $(second).attr('data-index') );

	index = index + 1 >= avaliableCities.length ? 0 : index + 1;
	setAttr(second, avaliableCities[index]);			
});

// Нажатие кнопки поменять города местами
$(document).on('click', '#switch_city', function(){
    var formData = getTripData();
    setAttr('#first', formData.second);
    setAttr('#second', formData.first);
    findCities('#first');
});

$(document).ready(function(){
    autocompleteList(first);
    autocompleteList(second);

	if ( $(first).val().length === 0 ) {
		yandexSearch();
	}
	else {
	   findCities(first);
	}
});