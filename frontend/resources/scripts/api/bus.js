//Модуль поиска билетов на автобус
var BUS = {
    findTickets: function(url) {
        return new Promise(function(resolve, reject){
            $.ajax({
                url,
                type: 'GET',
                dataType: 'json',
                success: function(a) {
                    resolve(a);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log("error test\n");
                    console.log(xhr.status);
                    console.log(thrownError);
                    reject();
                }
            });
        });
    },

    searchUrl: function(fromId, toId, date, people) {
        var errObj = {};

        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/bus-tickets/search-url',
                type: 'GET',
                dataType: 'json',
                data: {
                    fromId,
                    toId,
                    date,
                    people,
                },
                success: function(a) {
                    resolve(a);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    console.log("error test\n");
                    console.log(xhr.status);
                    console.log(thrownError);
                    errObj.type = 0;
                    errObj.text = 'Произошла ошибка. Попробуйте сменить направление.';
                    reject(errObj);
                }
            });
        });
    }
}