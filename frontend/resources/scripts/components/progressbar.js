class Progressbar {
    constructor(element, options) {
        this.el = document.getElementById(element);

        let defaults = {
            startValue: 0,
            maxValue: 100,
            defaultMessage: 'Выполняется поиск билетов...'
        };

        if (!this.el || !(this instanceof Progressbar)) {
            throw new Error('Progressbar element is not defined');
        }

        this.opts = Object.assign({}, defaults, options);
        this.init();
    }

    init() {
        this.value = this.opts.startValue;
        this.elems = {};
        this.isVisible = false;

        this.createView();
    }

    addValue(value) {
        let newValue = this.value + value;

        this.value = newValue <= this.opts.maxValue ? newValue : this.opts.maxValue;
        this.renderValue();
    }

    setValue(value) {
        this.value = value <= this.opts.maxValue ? value : this.opts.maxValue;
        this.renderValue();
    }

    message(text) {
        this.elems.message.textContent = text;
    }

    createView() {
        this.el.innerHTML = `
            <div class="progressbar">
                <div class="progressbar-message">${this.opts.defaultMessage}</div>
                <div class="lowtrip-progressbar">
                    <div class="progress-bar" role="progressbar"></div>
                </div>
            </div>
        `;

        this.elems.message = this.el.querySelector('.progressbar-message');
        this.elems.bar     = this.el.querySelector('.progress-bar');

        this.renderValue();
    }

    renderValue() {
        this.elems.bar.style.maxWidth = `${this.value}%`;
    }

    show() {
        this.isVisible = true;
        this.el.style.display = 'block';
    }

    hide() {
        this.isVisible = false;
        this.el.style.display = 'none';
    }

    toggle() {
        this.isVisible = !this.isVisible;
        this.el.style.display = this.isVisible ? 'block' : 'none';
    }

}