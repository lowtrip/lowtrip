// Выпадающие меню
$(document).on('click', '.has_ui', function(event){
    if ( $(event.target).closest('.ui-element').length > 0 ) {
        $(this).removeClass('active');
        return false;
    }
    if ( $(this).find('.ui-element').hasClass('show_this') ) {
        $(this).find('.ui-element').removeClass('show_this');
        $(this).removeClass('active');
    }
    else {
        $('.has_ui .ui-element.show_this').removeClass('show_this');
        $(this).find('.ui-element').addClass('show_this');
        $(this).addClass('active');
    }
});

// Если клик не по элементу интерфеса а по документу, скрываем открытые элементы
$(document).on('click', function(event){
    if ( $(event.target).closest('.has_ui').length > 0 ) {
        return;
    }
    $('.has_ui').removeClass('active');
    $('.ui-element.show_this').removeClass('show_this');
});

// Выбор значения в выпадающем меню
$(document).on('click', '.menu-helper li', function(){
    var $menu = $(this).closest('.menu-helper');
    var value = $(this).attr('data-value');
    var text = $(this).text();

    $menu.find('.helper-value').val(value);
    $menu.find('.helper-text').val(text);
    $menu.find('.dropdown-helper').removeClass('show_this');
});
