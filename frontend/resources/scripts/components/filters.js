class Filters {
    constructor(element, data, options) {
        this.$el = document.getElementById(element);

        if (!this.$el || !(this instanceof Filters)) {
            throw new Error('Filters element is not defined');
        }

        let defaultOptions = {
            price: {
                unit: 'Р',
                from: 'от',
                to: 'до'
            },
            time: {
                unit: '',
                from: 'c',
                to: 'до'
            },
            checkedType: 0
        };

        this.options = Object.assign({}, defaultOptions, options);
        this.data = data;
        this.init();
    }

    /**
     * Инициализация компонента
     */
    init() {
        // Структура данных хранящихся в фильтрах
        this.filters = {
            $el: null,
            types: {
                $el: null,
                value: null
            },
            price: {
                $el: null,
                value: {
                    min: null,
                    max: null
                }
            },
            departure: {
                $el: null,
                value: {
                    min: null,
                    max: null
                }
            },
            sorting: {
                $el: null,
                value: null
            }
        };

        this.events = {};

        try {
            this.initFilters();
            this.initEvents();
        } catch(e) {
            console.log(e);
        }
    }

    /**
     * Инициализация всех фильтров
     */
    initFilters() {
        this.$el.innerHTML = this.constructor.getFiltersContainerView();

        this.filters.$el = this.$el.querySelector('#filters-content');

        this.initSorting();
        this.initTypeFilter(this.data.types, this.options.checkedType);
        this.initPriceFilter(this.data.types[0].minPrice, this.data.types[0].maxPrice);
        this.initDepartureFilter();

        this.resetFilters();
    }

    initSorting() {
        let node = this.getSortingView();

        this.filters.$el.appendChild(node);

        this.filters.sorting.$el = $('#sorting-select');

        this.filters.sorting.$el.on('change', this.onChangeSortingFilter.bind(this));
    }

    getSortingView() {
        let node       = document.createElement('div');
        let nodeHeader = this.getFilterItemHeader('Сортировка');
        let nodeBody   = this.getFilterItemBodySelect();

        node.id = 'filter-sorting';
        node.className = 'filter-item opened';

        node.appendChild(nodeHeader);
        node.appendChild(nodeBody);

        return node;
    }

    /**
     * Инициализация фильтра "Время отправления"
     */
    initDepartureFilter() {
        let node = this.getDepartureFilterView();

        this.filters.$el.appendChild(node);
        
        this.filters.departure.$el     = this.filters.$el.querySelector('#filter-departure');

        this.filters.departure.$slider = $('#departure-range');

        this.filters.departure.$from   = this.filters.departure.$el.querySelector('#departure-range-from');
        this.filters.departure.$to     = this.filters.departure.$el.querySelector('#departure-range-to');

        this.filters.departure.$slider.slider({
            range: true,
            min: 0,
            max: 1440,
            step: 15,
            values: [ 0, 1440 ],
            change: (event, ui) => {
                this.onChangeDepartureFilter(ui.values);
            },
            slide: (event, ui) => {
                this.changeSliderTimeOutputValues(this.filters.departure, ui.values[0], ui.values[1]);
            }
        });
    }


    /**
     * Получить шаблон фильтра "Время отправления"
     * @return {Node}
     */
    getDepartureFilterView() {
        let node       = document.createElement('div');
        let nodeHeader = this.getFilterItemHeader('Время отправления');
        let nodeBody   = this.getFilterItemBodySlider('00:00', '24:00', 'time', 'departure-range');

        node.id = 'filter-departure';
        node.className = 'filter-item opened';

        node.appendChild(nodeHeader);
        node.appendChild(nodeBody);

        return node;
    }

    /**
     * Инициализация фильтра "Цена"
     */
    initPriceFilter(minPrice, maxPrice) {
        let node = this.getPriceFilterView(minPrice, maxPrice);

        this.filters.$el.appendChild(node);

        this.filters.price.$el     = this.filters.$el.querySelector('#filter-price');

        //this.filters.price.$slider = this.filters.price.$el.find('#price-range');
        this.filters.price.$slider = $('#price-range');

        this.filters.price.$from   = this.filters.price.$el.querySelector('#price-range-from');
        this.filters.price.$to     = this.filters.price.$el.querySelector('#price-range-to');

        this.filters.price.$slider.slider({
            range: "min",
            min: minPrice,
            max: maxPrice,
            step: 10,
            value: maxPrice,
            change: (event, ui) =>{
                this.onChangePriceFilter(ui.value);
            },
            slide: (event, ui) => {
                this.changeSliderPriceOutputValues(this.filters.price, ui.value);
            }
        });
    }


    /**
     * Получить шаблон фильтра "Цена"
     * @param  {integer} minPrice 
     * @param  {integer} maxPrice 
     * @return {Node}
     */
    getPriceFilterView(minPrice, maxPrice) {
        let node       = document.createElement('div');
        let nodeHeader = this.getFilterItemHeader('Цена билета');
        let nodeBody   = this.getFilterItemBodySlider(minPrice, maxPrice, 'price', 'price-range');

        node.id = 'filter-price';
        node.className = 'filter-item opened';

        node.appendChild(nodeHeader);
        node.appendChild(nodeBody);

        return node;
    }

    initTypeFilter(types, checkedType) {
        let node = this.getTypeFilterView(types, checkedType);

        this.filters.$el.appendChild(node);

        //this.filters.types.$el = this.filters.$el.querySelector('#filter-types');
        this.filters.types.$el = $('#filter-types');

        this.filters.types.$el.on('change', 'input:radio', this.onChangeTypeFilter.bind(this));
    }

    getTypeFilterView(types, checkedType) {
        let node       = document.createElement('div');
        let nodeHeader = this.getFilterItemHeader('Тип вагона');
        let nodeBody   = this.getFilterItemBodyTypes(types, checkedType);

        node.id = 'filter-types';
        node.className = 'filter-item opened';

        node.appendChild(nodeHeader);
        node.appendChild(nodeBody);

        return node;
    }

    /**
     * Получить шаблон контейнера для фильтров
     * @returns {string}
     */
    static getFiltersContainerView() {
        return `
        <div class="train-mobile filter-btn filter-btn__filtering" id="filter-mobile-btn"></div>
        <div class="filters-content" id="filters-content">
            <div class="apply-btn" id="filters-apply">Применить фильтры</div>
        </div>`;
    }

    /**
     * Получить узел шапки фильтра
     * @param {string} title 
     * @return {Node}
     */
    getFilterItemHeader(title) {
        let node = document.createElement('div');

        node.className = 'filter-item-header';

        node.innerHTML = `
        <span class="filter-item-icon"></span>
        <span class="filter-item-title">${title}</span>
        <span class="filter-item-reset"></span>`;

        return node;
    }

    /**
     * Получить узел тела фильтра - слайдера
     * @param {integer} min 
     * @param {integer} max 
     * @param {string}  type this.options[type]
     * @param {string}  sliderId 
     * @return {Node}
     */
    getFilterItemBodySlider(min, max, type, sliderId) {
        let node = document.createElement('div');

        node.className = 'filter-item-body';

        node.innerHTML = `
        <div class="filter-range-block">
            <div class="range-output-block">
                <div class="range-output-label">
                    ${this.options[type].from} <span id="${sliderId}-from" class="output-input">${min}</span> ${this.options[type].unit}
                </div>
                <div class="range-output-label">
                    ${this.options[type].to} <span id="${sliderId}-to" class="output-input">${max}</span> ${this.options[type].unit}
                </div>
            </div>
            <div id="${sliderId}" class="filter-range"></div>
        </div>`;

        return node;
    }

    /**
     * Получить узел тела фильтра - типы
     * @param {object} types 
     * @param {integer} checkedType 
     * @return {Node}
     */
    getFilterItemBodyTypes(types, checkedType) {
        let node = document.createElement('div'),
            template = '';

        node.className = 'filter-item-body';

        for(let prop in types) {
            let checked = +prop === checkedType ? "checked" : "";

            template += `
            <div class="filter-label-block">
                <input class="hidden_input for-filter-checkbox" type="radio" name="type" ${checked} value="${prop}" id="type${prop}">
                <label class="filter-checkbox" for="type${prop}">
                    <span class="filter-name">${types[prop].typeName}</span>
                    <span class="filter-from-price">от ${types[prop].minPrice} Р</span>
                </label>
            </div>`;
        }

        node.innerHTML = template;

        return node;
    }

    /**
     * Получить узел тела фильтра - сортировки
     */
    getFilterItemBodySelect() {
        let node = document.createElement('div');

        node.className = 'filter-item-body';

        node.innerHTML = `
        <div class="sorting-select-wrapper">
            <select class="train-sorting-select" name="sorting" id="sorting-select">
                <option value="departure">По времени отправления</option>
                <option value="arrival">По времени прибытия</option>
                <option value="duration">По времени в пути</option>
                <option value="cost">По стоимости</option>
            </select>
        </div>`;

        return node;
    }

    /**
     * Преобразует минуты в строку ЧЧ:ММ
     * @param {integer} minutes 
     */
    static minutesToHHMM(minutes) {
        let h = Math.floor(minutes / 60),
            m = minutes - (h * 60);

        if(h.toString().length === 1) h = '0' + h;
        if(m.toString().length === 1) m = '0' + m;

        return `${h}:${m}`;
    }


    /**
     * Изменить отображение значений в слайдере времени
     * @param {object}  filterName 
     * @param {integer} min 
     * @param {integer} max 
     */
    changeSliderTimeOutputValues(filterName, min, max) {
        filterName.$from.textContent = this.constructor.minutesToHHMM(min);
        filterName.$to.textContent   = this.constructor.minutesToHHMM(max);
    }

    /**
     * Изменить отображение значений в слайдере стоимости
     * @param {object} filterName 
     * @param {integer} max 
     */
    changeSliderPriceOutputValues(filterName, max) {
        filterName.$to.textContent = max;
    }


    /**
     * Создание событий
     */
    initEvents() {
        let el = $(this.$el);

        el.on('click', '.filter-item-header', this.onClickFilterHeader.bind(this));

        el.on('click', '#filter-mobile-btn', this.onClickFilterMobileBtn.bind(this));
        el.on('click', '#filters-apply', this.onClickFilterApplyBtn.bind(this));
    }

    /**
     * Вызывает событие, что фильтры изменились
     */
    onChangeFilters() {
        let event = new Event('filtersChange', {
            bubbles: true,
            cancelable: true
        });

        this.$el.dispatchEvent(event);
    }

    /**
     * Действия, при изменении фильтра стоимости
     * @param {Event} e 
     * @param {integer} value 
     */
    onChangePriceFilter(value) {
        this.filters.price.value.max = value;
        this.onChangeFilters();
    }

    /**
     * Действия, при изменении фильтра времени отправления
     * @param {Event} e 
     * @param {array} values 
     */
    onChangeDepartureFilter(values) {
        this.filters.departure.value.min = values[0];
        this.filters.departure.value.max = values[1];
        this.onChangeFilters();
    }

    onChangeTypeFilter(e) {
        this.filters.types.value = +e.target.getAttribute('value');
        this.onChangeFilters();
    }

    onChangeSortingFilter() {
        this.filters.sorting.value = this.filters.sorting.$el.val();
        this.onChangeFilters();
    }

    // Обработчик события при клике на заголовок фильтра
    onClickFilterHeader(e) {
        let $filterHeader = $(e.target).closest('.filter-item-header'),
            $filterItem   = $filterHeader.closest('.filter-item');

        if ($filterItem.hasClass('opened')) {
            $filterItem.removeClass('opened').addClass('closed');
        } else {
            $filterItem.removeClass('closed').addClass('opened');
        }
    }

    // Обработчик события при нажатии мобильной кнопки фильтры
    onClickFilterMobileBtn() {
        $(this.filters.$el).addClass('mobile-open');
        $('html, body').css('overflow-y', 'hidden');
    }

    // Обработчик события при нажатии мобильной кнопки Применить фильтры
    onClickFilterApplyBtn() {
        $(this.filters.$el).removeClass('mobile-open');
        $('html, body').css('overflow-y', 'auto');
    }


    resetFiltersState() {
        this.resetFilters();
        this.resetSlidersValues();
    }

    // Функция инициализации начального состояния фильтров
    resetFilters() {
        this.filters.types.value = this.options.checkedType;
        this.filters.price.value.min = this.data.types[0].minPrice;
        this.filters.price.value.max = this.data.types[0].maxPrice;
        this.filters.departure.value.min = 0;
        this.filters.departure.value.max = 1440;
    }

    resetSlidersValues() {
        this.filters.price.$slider.slider('value', this.data.types[0].maxPrice);
        this.changeSliderPriceOutputValues(this.filters.price, this.data.types[0].maxPrice);

        this.filters.departure.$slider.slider('values', [0, 1440]);
        this.changeSliderTimeOutputValues(this.filters.departure, 0, 1440);

        this.filters.types.$el.find('input#type0').prop("checked", true);
    }


    getType() {
        return this.filters.types.value;
    }

    getPrice(type) {
        return this.filters.price.value[type];
    }

    getDeparture(type) {
        return this.filters.departure.value[type];
    }

    getSorting() {
        return this.filters.sorting.value;
    }

}