function TrainView (element, options) {

    const DAYS = ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'];
    const MONTHS = ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];

    this.$container = null;
    this.$progressbarContainer = null;
    this.$messages = null;
    this.$trainResults = null;
    this.$offers = null;
    this.$timetable = null;

    this.tickets_stats = {
        departure: {
            min: 1440,
            max: 0
        },
        types: {
            0: {
                typeName: "Все типы",
                minPrice: Infinity,
                maxPrice: 0
            }
        }
    };

    this.buffer = [];

    this.points = {
        departure: null,
        arrival: null,
        all: {}
    };

    let defaults = {
        api: TRAIN,
        source: trip,
        metrika: 'yaCounter44396998',
        tutuUrl: 'https://c45.travelpayouts.com/click?shmarker=53486&promo_id=1770&source_type=customlink&type=click&custom_url=https%3A%2F%2Fwww.tutu.ru%2F',
        checkedType: 0,
        advert: {
            filters: null,
        },
    };

    this.run = function() {
        if (this instanceof TrainView) {
            this.setViewElement(element);
            this.setOptions(options);
            this.getDataFromSource();

            this.init();
        }
    };

    this.init = function() {
        this.renderBaseTemplate();
        this.initProgressbar();
        this.findTickets();
        this.renderAdvert();
    };

    this.setOptions = function(options) {
        this.options = $.extend(true, {}, defaults, options);
    };

    // Задать контейнер для отображения
    this.setViewElement = function(element) {
        this.$container = $(element);
    };

    // Базовый шаблон модуля
    this.renderBaseTemplate = function() {
        let template = `
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-4 t_col-10">
                    <div id="progressbar-container"></div>
                    <div id="train-messages"></div>
                </div>
                <div class="col-xs-12 col-sm-4 t_col-10">
                    <div class="train-results-filters" id="train-filters"></div>
                    <div class="advert desktop" id="advert-filters"></div>
                </div>
                <div class="col-xs-12 col-sm-8 t_col-10">
                    <div class="train-timetable-container" id="train-results">
                        <div class="train-results-offers" id="train-offers"></div>
                        <div class="train-timetable-items" id="train-timetable"></div>
                    </div>
                </div>
            </div>
        </div>`;

        this.$container.empty().append(template);

        this.$progressbarContainer = this.$container.find('#progressbar-container');
        this.$messages = this.$container.find('#train-messages');
        this.$filters = this.$container.find('#train-filters');
        this.$trainResults = this.$container.find('#train-results');
        this.$offers = this.$container.find('#train-offers');
        this.$timetable = this.$trainResults.find('#train-timetable');

        this.$advertFilters = this.$container.find('#advert-filters');
        this.$advertRight = this.$container.find('#advert-right');
    };

    // Создаем прогрессбар и добавляем в шаблон
    this.initProgressbar = function() {
        this.progressbar = new Progressbar(this.$progressbarContainer.attr('id'));
    };


    this.initFilters = function() {
        this.filters = new Filters(this.$filters.attr('id'), this.tickets_stats);
        this.$container.on('filtersChange', this.onFiltersChange.bind(this));
    };

    this.resetFilters = function() {
        this.filters.resetFiltersState();
    };

    this.onFiltersChange = function() {
        this._resetOffersState();
        this.processing();
    };


    this.renderAdvert = function() {
        if (!this.options.hasOwnProperty('advert')) return;

        const advertFilters = this.options.advert.filters;

        if (Array.isArray(advertFilters) && advertFilters.length) {
            advertFilters.forEach(advert => this.$advertFilters.append(advert));
        }
    };


    // Задать апи для получения данных
    this.setTrainApi = function(api) {
        this.options.api = api;
    };

    // Задать источник для получения данных
    this.setSource = function(source) {
        this.options.source = source;
    };

    // Получить данные для запроса из источника данных
    this.getDataFromSource = function() {
        this.searchData = this.options.source.getData();
    };

    // Обертка для поиска станций
    // Изменение значение прогрессбара
    this.findStation = function(object) {
        this.progressbar.addValue(20);
        return this.options.api.findStation(object);
    };

    // Получить данные о билетах через апи
    // Изменение значение прогрессбара
    this.findTickets = function() {
        Promise.all([
            this.findStation(this.searchData.first),
            this.findStation(this.searchData.second),
        ]).then(stations => {
            this.progressbar.addValue(10);

            this.points.departure = stations[0];
            this.points.arrival   = stations[1];
            this.points.all[stations[0].yandex] = stations[0].stations_id;
            this.points.all[stations[1].yandex] = stations[1].stations_id;

            return this.options.api.findTrains(this.points.departure.stations_id, this.points.arrival.stations_id, this.searchData.date);
        }).then(results => {
            this.progressbar.addValue(50);

            this.saveTickets(results);
            this.analyzeTickets();
            this.analyzeBestTickets();

            this.renderTicketsToCache()
                .then(this.renderResults());

        }).catch(e => {
            this.handleError(e);
        });
    };

    /**
     * Отправка запроса к Yandex Rasp
     */
    this.findTrainsYandex = function() {
        let first_code  = this.points.departure.yandex,
            second_code = this.points.arrival.yandex,
            date        = this.searchData.date;

        return this.options.api.findTrainsYandex(first_code, second_code, date)
            .then(result => {
                if (!result.segments) return Promise.reject();

                return Promise.resolve(result);
            })
            .catch(() => {
                return Promise.reject();
            });
    };

    /**
     * Найти код Onetwotrip по коду Яндекс
     * @param {string} yandex_code 
     */
    this.findOnetwotripCodeByYandex = function(yandex_code) {
        if (this.points.all[yandex_code]) return Promise.resolve(this.points.all[yandex_code]);

        return this.options.api.findStationByYandex(yandex_code)
            .then(result => {
                if (!result || !result.stations_id) return Promise.reject(null);

                this.points.all[yandex_code] = result.stations_id;

                return Promise.resolve(result.stations_id);
            })
            .catch(() => {
                return Promise.reject(null);
            });
    };

    /**
     * Обработать ответ от яндекса. Логика действий
     * @param {object} response 
     */
    this.handleYandexResponse = function(response) {
        let segments = [];

        this.transferSegments = response;
        this.progressbar.message('Поиск поездов с пересадками...');

        this.transferSegments.segments.forEach(segment => {
            if (segment.details) {
                let filtered =  segment.details.filter(detail => {
                    return detail.thread && detail.start_date && detail.from && detail.to;
                });

                segments.push(filtered);
            }
        });

        segments.forEach(segment => {
            segment.forEach(detail => {
                this.points.all[detail.from.code] = null;
                this.points.all[detail.to.code]   = null;
            });
        });

        return this.options.api.findAllStationsByYandex(Object.keys(this.points.all))
            .then(stations => {
                stations.forEach(station => {
                    if (station.stations_id) this.points.all[station.yandex] = station.stations_id;
                });
            })
            .then(() => {
                let segmentsLength = segments.length,
                    rejects = 0;

                return Promise.all(

                    segments.map((segment, segmentIndex) => {
                        return Promise.all(segment.map(detail => {
                            if (!this.points.all[detail.from.code] || !this.points.all[detail.to.code]) return Promise.reject();

                            return this.options.api.findTrains(
                                this.points.all[detail.from.code],
                                this.points.all[detail.to.code],
                                detail.departure.substr(0,10)
                            );
                        }))
                        .then(arrayResults => {
                            return arrayResults.map((trainResults, detailIndex) => {
                                return trainResults.find(train => {
                                    return train.trainNumber === segments[segmentIndex][detailIndex].thread.number;
                                });
                            });
                        })
                        .then(trains => {
                            if (trains.some(train => train === undefined)) return Promise.reject();

                            return trains;
                        })
                        .then(trains => {
                            this.fillTimetable(this.renderTicketsGroup(trains));
                        })
                        .catch(error => {
                            console.log('Segment error:', error);
                            rejects ++;
                        })
                        .then(() => {
                            this.progressbar.addValue(50 / segmentsLength);

                            return Promise.resolve();
                        });
                    })

                ).then(() =>  {
                    this.progressbar.hide();
                    return segmentsLength === rejects ? Promise.reject() : Promise.resolve();
                });
            })
            .catch((e) => {
                console.log(e);
                return Promise.reject();
            });
    };

    // Сохраняем билеты в свойство this.tickets
    // Добавляем билету свойство position - для соответствия с его отображением
    this.saveTickets = function(tickets) {
        this.tickets = tickets.map((ticket, i) => {
            ticket.position = i;
            return ticket;
        });
    };

    // В свойство tickets_stats будем записывать max и min значения для фильтров
    this.analyzeTickets = function() {
        this.tickets.forEach(ticket => {
            this.collectTypesWithPrices(ticket);
            this.parseDataTime(ticket, 'departure');
            this.parseDataTime(ticket, 'arrival');
            this.parseDuration(ticket);
            this.analyzeMovement(ticket, 'departure');
        });
    };

    // Функция проводит анализ билетов по 3м типам, добавляя билету поле marks
    // Самый дешевый, самый быстрый, комфортный (дешевый среди дорогого типа; максим. - СВ)
    // Выполняется после функции analyzeTickets !!
    this.analyzeBestTickets = function() {
        let cheap = {},
            fast = {},
            comfort = {},
            type = 0;

        // Находим комфортный тип и запоминаем его минимальную стоимость
        for (let prop in this.tickets_stats.types) {
            if (prop > type && this.tickets_stats.types[prop].typeName !== 'Люкс') {
                type = parseInt(prop);
                comfort.type = type;
                comfort.price = this.tickets_stats.types[prop].minPrice;
            }
        }

        // Находим лучшие билеты и запоминаем их позиции
        for(let i = 0; i < this.tickets.length; i++) {
            if (this.tickets[i].minPrice < cheap.price || cheap.price === undefined) {
                cheap.price = this.tickets[i].minPrice;
                cheap.duration = this.tickets[i].duration;
                cheap.position = i;
            }

            if (this.tickets[i].durationInMinutes < fast.durationInMinutes || fast.durationInMinutes === undefined) {
                fast.price = this.tickets[i].minPrice;
                fast.durationInMinutes = this.tickets[i].durationInMinutes;
                fast.duration = this.tickets[i].duration;
                fast.position = i;
            }

            this.tickets[i].places.forEach(place => {
                if (place.type === type && place.cost === comfort.price) {
                    comfort.position = i;
                    comfort.duration = this.tickets[i].duration;
                }
            });
        }

        this.tickets_stats.offers = {
            cheap: cheap,
            fast: fast,
            comfort: comfort
        };

        // Добавляем лучшим билетам отметки marks
        for(let prop in this.tickets_stats.offers) {
            let pos = this.tickets_stats.offers[prop].position;
            if (this.tickets[pos].marks === undefined) {
                this.tickets[pos].marks = [];
            }
            this.tickets[pos].marks.push(prop);
        }
    };

    // Сбор типов и диапазонов цен
    // Принимает в качестве аргумента билет
    // Заодно добавляем билету свойство минимальная стоимость minPrice
    this.collectTypesWithPrices = function(ticket) {
        let minPrice;

        ticket.places.forEach((place, idx) => {
            // Приводим стоимость типа места к int
            let cost = ticket.places[idx].cost = parseInt(place.cost);

            if (cost < minPrice || minPrice === undefined) {
                minPrice = cost;
            }

            // Если типа в статистике не существует, добавляем его
            if (this.tickets_stats.types[place.type] === undefined) {
                this.tickets_stats.types[place.type] = {
                    typeName: place.typeName,
                    minPrice: Infinity,
                    maxPrice: 0
                };
            }

            // Анализ диапазона цен
            if (this.tickets_stats.types[place.type].minPrice > cost) {
                this.tickets_stats.types[place.type].minPrice = cost;
            }

            if (this.tickets_stats.types[0].minPrice > cost) {
                this.tickets_stats.types[0].minPrice = cost;
            }

            if (this.tickets_stats.types[place.type].maxPrice < cost) {
                this.tickets_stats.types[place.type].maxPrice = cost;
            }

            if (this.tickets_stats.types[0].maxPrice < cost) {
                this.tickets_stats.types[0].maxPrice = cost;
            }

        });

        ticket.minPrice = minPrice;
    };

    // Сбор min max значений времени отправления и прибытия (в минутах)
    this.analyzeMovement = function(ticket, movement) {
        if (ticket[movement].minutes < this.tickets_stats[movement].min) {
            this.tickets_stats[movement].min = ticket[movement].minutes;
        }
        if (ticket[movement].minutes > this.tickets_stats[movement].max) {
            this.tickets_stats[movement].max = ticket[movement].minutes;
        }
    };

    // Приводим данные time об arrival или departure к человеческому виду
    // date: YYYY-MM-DD; time: hh-mm
    // movement = [arrival, departure];
    this.parseDataTime = function(ticket, movement) {
        let old = ticket[movement].time + '.000+03:00',
            dat = new Date(Date.parse(old));

        ticket[movement].string = old;
        ticket[movement].day = old.slice(8,10);
        ticket[movement].date = old.slice(0,10);
        ticket[movement].time = old.slice(11,16);
        ticket[movement].day_name = DAYS[dat.getDay()];
        ticket[movement].month = MONTHS[dat.getMonth()];
        ticket[movement].minutes = dat.getHours() * 60 + dat.getMinutes();

    };

    // Продолжительность пути в минутах преобразуем в строку
    this.parseDuration = function(ticket) {
        let duration = parseInt(ticket.durationInMinutes),
            minutes  = duration % 60,
            hours    = (duration - minutes) / 60;

        if (hours > 24) {
            let days = parseInt(hours / 24);
            hours = hours - days * 24;
            ticket.duration =  days + 'д ' + hours + 'ч ' + minutes + 'м';
        }
        else {
            ticket.duration =  hours + 'ч ' + minutes + 'м';
        }
    };


    // Функция рендера html отображения всех билетов,
    // которое сохраняется в свойство tickets_views
    this.renderTicketsToCache = function() {
        this.ticketsViews = {};

        this.tickets.forEach((ticket, idx) => {
            this.ticketsViews[idx] = this.renderTicketView(ticket, idx);
        });

        return Promise.resolve();
    };

    // Рендеринг билета и return его view
    this.renderTicketView = function(ticket, idx) {
        let template,
            classes = '',
            marks = '',
            places = '';

        // Временно убрал
        // let lowtrip_marker = '&scp=60,affiliate,4368-14624-0-2';
        let lowtrip_marker = '';
        let adults = '&adults=' + this.searchData.adults;

        let name = ticket.name ? '&#171;'+ticket.name+'&#187;' : '';
        let trainNumberLink = `/train/raspisanie/${encodeURIComponent(ticket.trainNumber)}`;

        if (ticket.class) {
            for (let i = 0; i < ticket.class.length; i++) {
                classes += '<span class="train-type">' + ticket.class[i] + '</span>';
            }
        }

        if (ticket.marks && ticket.marks.length < 3) {
            ticket.marks.forEach(mark => {
                let str;
                switch (mark) {
                    case "fast":
                        str = "Самый быстрый";
                        break;
                    case "cheap":
                        str = "Самый дешёвый";
                        break;
                    case "comfort":
                        str = "Комфортный";
                        break;
                }
                marks += `<div class="marker ${mark}">${str}</div>`;
            });
        }

        for(let i = 0; i < ticket.places.length; i++) {
            var offers = '';
            var type = '&carType=' + ticket.places[i].type;
            // onetwotrip
            //var link = ticket.deeplink + adults + type + lowtrip_marker;
            //var redirect = '/redirect/onetwotrip?url=' + encodeURIComponent(link);
            // tutu
            var link = this.options.tutuUrl;

            if (this.tickets_stats.offers && this.tickets_stats.offers.comfort.position === idx) {
                if (this.tickets_stats.offers.comfort.type === ticket.places[i].type) {
                    offers += 'comfort';
                }
            }
            if (this.tickets_stats.offers && this.tickets_stats.offers.cheap.position === idx) {
                if (this.tickets_stats.offers.cheap.price === ticket.places[i].cost) {
                    offers += ' cheap';
                }
            }

            places +=
                '<a class="train-place '+ offers +'" href="'+ link +'" target="_blank" rel="nofollow" onclick="yaCounter44396998.reachGoal(\'train\'); return true;">' +
                '<div class="place-type">'+ ticket.places[i].typeName +'</div>' +
                '<div class="place-price">от <span class="price-value">'+ Math.ceil(ticket.places[i].cost) +' Р</span></div>' +
                '<div class="places-count">Мест '+ ticket.places[i].freeSeats +'</div>' +
                '</a>';
        }

        template =
            '<div class="train-ticket">' +
            '<div class="train-ticket-marks">'+ marks +'</div>' +
            '<div class="train-ticket-header">' +
            '<div class="train-ticket-header-info">' +
            '<div class="train-number">' +
            '<a class="has_tooltip" href="'+ trainNumberLink +'" traget="_blank" title="Посмотреть маршрут следования поезда '+ ticket.trainNumber +'">'+ ticket.trainNumber +'</a>' +
            '</div>' +
            '<div class="train-types">' +
            classes +
            '</div>' +
            '<div class="train-name">' +
            '<span class="has_tooltip" title="Название поезда">'+ name +'</span>' +
            '</div>' +
            '</div>' +
            '<div class="train-ticket-header-time">' +
            '<div class="train-stations-times">' +
            '<div class="train-station-column">' +
            '<div class="column-name">Отправление</div>' +
            '<div class="station-timing">' +
            '<div class="value has_tooltip" title="Время отправления (по мск)">' +
            '<span class="time">'+ ticket.departure.time +'</span><span class="type">Мск</span>' +
            '</div>' +
            '<div class="date">'+ ticket.departure.day + ' ' + ticket.departure.month +', '+ ticket.departure.day_name +'</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-timeline-column">' +
            '<div class="train-timeline"><span class="has_tooltip" title="Время в пути">'+ ticket.duration +'</span></div>' +
            '</div>' +
            '<div class="train-station-column">' +
            '<div class="column-name">Прибытие</div>' +
            '<div class="station-timing">' +
            '<div class="value has_tooltip" title="Время прибытия (по мск)">' +
            '<span class="time">'+ ticket.arrival.time +'</span><span class="type">Мск</span>' +
            '</div>' +
            '<div class="date">'+ ticket.arrival.day + ' ' + ticket.arrival.month +', '+ ticket.arrival.day_name +'</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-stations-names">' +
            '<div class="station-name has_tooltip dep" title="Вокзал отправления">'+ ticket.from.station +'</div>' +
            '<div class="station-name has_tooltip arr" title="Вокзал прибытия">'+ ticket.to.station +'</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="train-ticket-separator">' +
            '<div class="left-corner"></div><div class="separator-line"></div><div class="right-corner"></div>' +
            '</div>' +
            '<div class="train-ticket-body">' +
            places +
            '</div>' +
            '</div>';

        return template;
    };

    // Рендер группы билетов...пока что
    this.renderTicketsGroup = function(trains) {
        let results = '';

        trains.forEach(ticket => {
            this.parseDataTime(ticket, 'departure');
            this.parseDataTime(ticket, 'arrival');
            this.parseDuration(ticket);
            results += this.renderTicketView(ticket);
        });

         return `
            <div class="tickets-group">${results}</div>
         `;
    };

    // ПЕРЕПИСАТЬ !!!! УБРАТЬ ДАЛЬНЕЙШУЮ ЛОГИКУ!!!
    this.renderResults = function() {
        this.progressbar.hide();

        this.renderOffers(this.tickets_stats.offers);

        this.initFilters();

        this.clearTimetable();

        this.processing();

        this.$messages.empty().append(`<div class="train-results-count">Найдено: ${this.tickets.length} поездов</div>`);
    };

    // Рендер билетов с вызовом рендера каждого билета
    this.renderTicketsArray = function(tickets) {
        let results = '';

        tickets.forEach(ticket => {
            results += this.renderTicketView(ticket);
        });

        this.clearTimetable();
        this.fillTimetable(results);
    };

    // Рендер одного билета с позицией position в контейнер
    this.renderOfferTicket = function(position) {
        let ticket = this.tickets.find(ticket => ticket.position === position);

        if (!ticket) return;

        this.clearTimetable();
        this.fillTimetable(this.ticketsViews[position]);
    };


    // Рендеринг лучших предложений
    this.renderOffers = function(offers) {
        let template = '',
            propName,
            title,
            i = 0;

        this.$offers.empty();

        template = `
        <div class="offers-container">`;

        for(let prop in offers) {
            switch (prop) {
                case 'cheap':
                    propName = 'Самый дешёвый';
                    title = 'Показать самый дешёвый билет';
                    break;
                case 'fast':
                    propName = 'Самый быстрый';
                    title = 'Показать билет на самый быстрый поезд';
                    break;
                case 'comfort':
                    propName = 'Комфортный';
                    title = 'Самое комфортное место, по разумной цене. Учитываются такие параметры, как тип вагона, стоимость места.';
                    break;
            }

            let type  = (i === 0) ? 'state-hover' : '';
            let style = (i < 2) ? 'border-right' : '';

            template += `
            <div class="train-offer-item has_tooltip ${prop} ${type}" data-offer="${prop}" data-prop="${propName}" title="${title}">
                <div class="offer-item-content ${style}">
                    <div class="offer-title">${propName}</div>
                    <div class="offer-cost">${offers[prop].price} Р</div>
                    <div class="offer-duration">${offers[prop].duration}</div>
                </div>
            </div>`;

            i++;
        }

        template += `
        </div>
        <div class="show-all-tickets not-visible">Показать все билеты</div>`;

        this.$offers.append(template);
        this.initTooltips(this.$offers);

        // Навешиваем обработчик события на предложения
        this.$offers.on('click', '.train-offer-item', this._onClickOfferItem.bind(this));
        this.$offers.on('click', '.show-all-tickets', this._onClickShowAllTickets.bind(this));
    };

    // Выбор, откуда рендерить билеты. Из buffer или из tickets
    this.smartRenderTickets = function() {
        if (this.buffer && this.buffer.length) {
            this.renderTicketsArray(this.buffer);
        } else {
            this.resetFilters();

            this.processing();
        }
    };

    // Обработчик события при клике на предложение
    this._onClickOfferItem = function(e) {
        let $offer = $(e.target).closest('.train-offer-item'),
            $btn   = $offer.closest('.train-results-offers').find('.show-all-tickets'),
            data   = $offer.attr('data-offer');

        if ($offer.hasClass('active')) {
            $offer.removeClass('active');
            $btn.removeClass('visible').addClass('not-visible');
            this.smartRenderTickets();
        } else {
            $offer.addClass('active').removeClass('state-hover').siblings('.train-offer-item').removeClass('active state-hover');
            $btn.removeClass('not-visible').addClass('visible');

            let position = this.tickets_stats.offers[data].position;
            this.renderOfferTicket(position);
        }

        if (window[this.options.metrika] !== undefined) {
            window[this.options.metrika].reachGoal('train-'+ data);
        }
    };

    // Обработчик события при нажатии показать все билеты
    this._onClickShowAllTickets = function(e) {
        this._resetOffersState();
        this.smartRenderTickets();
    };

    // Вернуть начальное состояние лучшим предложениям
    this._resetOffersState = function() {
        let $btn    = this.$offers.find('.show-all-tickets'),
            $offers = this.$offers.find('.train-offer-item');

        $offers.removeClass('active');
        $btn.removeClass('visible').addClass('not-visible');
    };

    // Процесс фильтрации и сортировки, единая цепочка
    this.processing = function() {
        this.filterTickets()
            .then(tickets => {
                return this.sortByProperty(tickets, this.filters.getSorting());
            })
            .then(tickets => {
                tickets.length ? this.renderTicketsArray(tickets) : this.renderNoResults();
            })
            .catch(e => {
                console.log(e);
            });
    };

    // Сортировка массива билетов tickets по свойству property
    this.sortByProperty = function(tickets, property) {
        if (property === 'departure') {
            tickets.sort(function(a,b) {
                a = Date.parse(a.departure.string);
                b = Date.parse(b.departure.string);
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'arrival') {
            tickets.sort(function(a,b) {
                a = Date.parse(a.arrival.string);
                b = Date.parse(b.arrival.string);
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'duration') {
            tickets.sort(function(a,b) {
                a = a.durationInMinutes;
                b = b.durationInMinutes;
                return (a>b ? 1 : (a === b ? 0 : -1));
            });
        }
        else if (property === 'cost') {
            if (this.filters.getType() === 0) {
                tickets.sort(function(a,b) {
                    a = a.minPrice;
                    b = b.minPrice;
                    return (a>b ? 1 : (a === b ? 0 : -1));
                });
            }
            else {
                tickets.sort(function(a,b) {
                    a = a.places[0].cost;
                    b = b.places[0].cost;
                    return (a>b ? 1 : (a === b ? 0 : -1));
                });
            }
        }
        
        return tickets;
    };

    // Фильтрация билетов по всем фильтрам с добавлением результата в буфер
    this.filterTickets = function() {
        this.buffer = this.filterTicketsToBuffer(this.tickets);

        return Promise.resolve(this.buffer);
    };

    this.filterTicketsToBuffer = function(tickets) {
        let type     = this.filters.getType(),
            maxPrice = this.filters.getPrice('max'),
            minTime  = this.filters.getDeparture('min'),
            maxTime  = this.filters.getDeparture('max');

        let buffer = this.filterByAllFilters(tickets, {
            type,
            maxPrice,
            minTime,
            maxTime
        });

        return buffer
            .map(item => Object.assign({}, item))
            .filter(ticket => {
                ticket.places = ticket.places.filter(place => {
                    return place.cost <= maxPrice && (type === place.type || type === 0);
                });
                return !!ticket.places.length;
            });
    };

    this.filterByAllFilters = (tickets, { type, maxPrice, minTime, maxTime }) => {
        return tickets.filter(ticket => {
            return ticket.places.some(place => {
                return type === 0 || +place.type === type;
            });
        }).filter(ticket => {
            return (ticket.departure.minutes >= minTime) && (ticket.departure.minutes <= maxTime);
        }).filter(ticket => {
            return ticket.minPrice <= maxPrice;
        });
    };

    this.initTooltips = function(container) {
        if ($(window).width() > 767) container.find('.has_tooltip').tooltip();
    };

    // Очистка билетов
    this.clearTimetable = function() {
        this.$timetable.find('.has_tooltip').tooltip('destroy');
        this.$timetable.empty();
    };

    // Заполнение контейнера билетов
    this.fillTimetable = function(results) {
        this.$timetable.append(results);
        this.initTooltips(this.$timetable);
    };

    // Вывод сообщения, что нет результатов
    this.renderNoResults = function() {
        let template = `
        <div class="train-results-information-block">
            <div class="information-content">
                Нет билетов, соответствующих вашим фильтрам.
            </div>
        </div>`;

        this.clearTimetable();
        this.fillTimetable(template);
        this.$offers.find('.show-all-tickets').removeClass('not-visible').addClass('visible');
    };


    this.renderTrainCalendar = function() {
        var url = '/train/' + this.searchData.first.city_slug + '/' + this.searchData.second.city_slug;

        let startDate= new Date(this.searchData.date);

        let template =
            '<div class="train-results-information-block" id="train-calendar">' +
            '<div class="information-content">' +
            '<div class="calendar-title">На '+ startDate.getDate() +' ' + MONTHS[startDate.getMonth()] + ' нет билетов</div>' +
            '<p class="calendar-text">Начать поиск ближайших рейсов по маршруту<br>'+ this.searchData.first.city +' - '+ this.searchData.second.city +'</p>' +
            '<div class="calendar-dates">';

        for (let i = 1; i <= 5; i++) {
            var newDate = new Date(this.searchData.date);
            newDate.setDate(newDate.getDate() + i);

            var date_string = (newDate.toISOString()).slice(0,10);

            var searchUrl = url + '?date=' + date_string + '&people=' + this.searchData.adults;
            template +=
                '<a class="date-item" href="'+ searchUrl +'" data-date="'+ date_string +'">' +
                '<div class="date-item-weekday">'+ DAYS[newDate.getDay()] +'</div>' +
                '<div class="date-item-date">'+ newDate.getDate() +'</div>' +
                '<div class="date-item-month">'+ MONTHS[newDate.getMonth()] +'</div>' +
                '</a>';
        }

        template +=
            '</div>' +
            '<div class="calendar-special-offer">' +
                '<div class="calendar-special-offer-text">' +
                    'Мы проверили <b>прямые поезда и поезда с пересадками</b>, но, к сожалению, билетов не осталось. Вы можете перепроверить информацию здесь.' +
                '</div>' +
                '<div class="calendar-special-offer-partner">' +
                    '<a href="'+ this.options.tutuUrl +'" target="_blank" onclick="yaCounter44396998.reachGoal(\'tutu\');"><img class="partner-logo" src="/img/logo/tutu.svg"></a>' +
                    '<a href="'+ this.options.tutuUrl +'" target="_blank" class="partner-btn" onclick="yaCounter44396998.reachGoal(\'tutu\');">Перейти</a>' +
                '</div>' +
            '</div>' +
            '</div></div>';

        this.progressbar.hide();
        this.$messages.empty().append(template);

        this.$calendar = this.$messages.find('#train-calendar');
        this.$calendar.on('click', '.date-item', this._onClickDateItem.bind(this));
    };

    this.renderTransferMessage = function() {
        let message = `
        <div class="alert alert-info" role="alert">
            <p>По данному маршруту можно добраться только с пересадками.</p>
            <p>Выберите подходящий для вас маршрут, и перейдите к оформлению билетов, выбрав тип места в каждом поезде (оформление будет в отдельном окне).</p>
        </div>`;

        this.$messages.empty().append(message);
    };

    // Обработчик события При нажатии на дату календаря
    this._onClickDateItem = function(e) {
        e.preventDefault();
        let $el  = $(e.target).closest('.date-item'),
            href = $el.attr('href');

        if ( window.history && history.pushState ) {
            let date = $el.attr('data-date');
            trip.data.date = date;
            this.init();
            $('body').find('.js-current-date').text(this.formatDateString(date));

            let url = window.location.pathname + '?date=' + date + '&people=' + this.searchData.adults;
            window.history.replaceState(null, document.title, url);
        } else {
            window.location.href = href;
        }
    };

    this.formatDateString = function(str) {
        let date = new Date(str);

        return `${date.getDate()} ${MONTHS[date.getMonth()]} ${date.getFullYear()} г.`;
    };

    this.renderErrorMessage = function() {
        this.$messages.empty().append(this.error.text);
    };


    // Обработчик ошибки
    this.handleError = function(e) {
        this.error = e;

        if (this.error.type === 0 || this.error.type === 1) {
            // Теперь если нет прямых поездов, ищем пересадки на yandex
            // this.findTrainsYandex()
            //     .then(result => {
            //         return this.handleYandexResponse(result);
            //     })
            //     .then(() => {
            //         this.renderTransferMessage();
            //     })
            //     .catch(() => {
                    this.renderTrainCalendar();
                // });
        }
        else {
            this.renderErrorMessage();
        }
    };

    this.run();
}