class PricegraphDay {
    constructor(element, defaults, options) {
        this.el = document.getElementById(element);

        if (!this.el || !(this instanceof PricegraphDay)) {
            throw new Error('PricegraphDay element is not defined');
        }

        this.opts = Object.assign({}, defaults, options);
        this.init();
    }

    init() {
        if (this.opts.data === null || this.opts.data === undefined || typeof this.opts.data !== 'object') {
            throw new Error('Pricegraph data is not defined');
        }

        this.sizes      = {};
        this.elems      = {};
        this.controls   = {};
        this.data       = {};
        this.graphData  = {};
        this.isSliding  = false;

        try {
            this.initPolyfills();

            this.analyzeOptions();

            this.calcGraphData();
            this.parseData();

            this.createView();

            this.calculateSizes();

            this.centerContent();

            this.initEventListeners();
        } catch(error) {
            console.log('Pricegraph error: ', error);
        }
    }

    analyzeOptions() {
        if (this.opts.minDate) this.opts.startDate = new Date(Date.parse(this.opts.minDate));
        if (this.opts.maxDate) this.opts.endDate   = new Date(Date.parse(this.opts.maxDate));
    }

    createBodyView() {
        let elBody = `
            <div class="pricegraph-body-container">
                <div class="pricegraph-body">
                    <div class="pricegraph-handlers">
                        <div class="pricegraph-priceline">
                            <div class="pricegraph-priceview"></div>
                            <div class="pricegraph-pricearrow"></div>
                        </div>
                        <ul class="pricegraph-lines">
                        </ul>
                        <div class="pricegraph-signatures">
                            <div class="pricegraph-signatures__text"></div>
                        </div>
                    </div>
                    <div class="pricegraph-table">
                        <div class="pricegraph-table-scroll">
                            <div class="pricegraph-table-columns"></div>
                        </div>
                    </div>
                </div>
            </div>
        `;

        let controls = this.constructor.createControlsView();

        this.el.classList.add('pricegraph');
        this.el.innerHTML = elBody;
        this.el.appendChild(controls);

        this.elems.bodyCont   = this.el.querySelector('.pricegraph-body-container');
        this.elems.body       = this.el.querySelector('.pricegraph-body');
        this.elems.handlers   = this.el.querySelector('.pricegraph-handlers');
        this.elems.lines      = this.el.querySelector('.pricegraph-lines');
        this.elems.priceview  = this.el.querySelector('.pricegraph-priceview');
        this.elems.pricearrow = this.el.querySelector('.pricegraph-pricearrow');
        this.elems.signature  = this.el.querySelector('.pricegraph-signatures__text');
        this.elems.table      = this.el.querySelector('.pricegraph-table');
        this.elems.columns    = this.el.querySelector('.pricegraph-table-columns');
        this.controls.body    = this.el.querySelector('.pricegraph-controls');
        this.controls.prev    = this.controls.body.querySelector('.btn-prev');
        this.controls.next    = this.controls.body.querySelector('.btn-next');

        this.elems.body.style.transition    = this.opts.slideTime / 1000 + 's';
        this.elems.columns.style.transition = this.opts.slideTime / 1000 + 's';
        // Это хардкод значение нужно вывести в опцию
        this.elems.lines.style.height       = 120 * ((100 - this.opts.startHeight) / 100) + 'px';
    }

    createLinesView() {
        let max = `${this.graphData.max} ${this.opts.currency}`,
            ave = `${this.graphData.average} ${this.opts.currency}`;

        if (this.graphData.average === this.graphData.max) {
            max = '';
            ave = '';
        }

        this.elems.lines.innerHTML = `
            <li class="pricegraph-lines__item line-max">
                <span class="line-value">${max}</span>
            </li>
            <li class="pricegraph-lines__item line-average">
                <span class="line-value">${ave}</span>
            </li>
        `;
    }

    createColumnView(day, formatted, price, weekend) {
        let cost         = +price,
            diff         = cost - this.graphData.min,
            classData    = cost > 0 ? 'has-data' : 'no-data',
            classWeekend = weekend  ? 'weekend'  : '',
            height       = diff >= 0 ? diff * this.graphData.step + this.opts.startHeight : this.opts.minHeight,
            url          = this.opts.baseUrl ? this.opts.baseUrl + `&date=${formatted}` : '#';

        return `
            <a class="pricegraph-col pricegraph-col-day" data-day="${day}" data-price="${cost}" href="${url}">
                <div class="pricegraph-col__graph">
                    <div class="pricegraph-col__value ${classData}" style="height:${height}%"></div>
                </div>
                <div class="pricegraph-col__date ${classWeekend}">${day}</div>
            </a>
        `;
    }

    createView() {
        this.createBodyView();
        this.createLinesView();

        let fragment = document.createDocumentFragment();

        for (let year in this.data) {
            if (this.data.hasOwnProperty(year) && !isNaN(year)) {
                let yearFragment = this.constructor.createYearView(year);

                for (let month in this.data[year]) {
                    if (this.data[year].hasOwnProperty(month) && !isNaN(month)) {
                        let monthFragment = this.constructor.createMonthView(month),
                            daysBuffer = '';

                        for (let day in this.data[year][month]) {
                            if (this.data[year][month].hasOwnProperty(day) && !isNaN(day)) {
                                let date    = new Date(Date.UTC(+year, +month, +day)),
                                    dateDay = date.getUTCDay(),
                                    weekend = dateDay === 0 || dateDay === 6,
                                    format  = this.constructor.datePartsFormatted(year, +month + 1, day);

                                if ((!this.opts.startDate || this.opts.startDate <= date) &&
                                    (!this.opts.endDate || this.opts.endDate >= date)) {
                                    daysBuffer += this.createColumnView(day, format, this.data[year][month][day], weekend);
                                }
                            }
                        }

                        monthFragment.innerHTML = daysBuffer;
                        yearFragment.appendChild(monthFragment);
                    }
                }

                fragment.appendChild(yearFragment);
            }
        }

        this.elems.columns.appendChild(fragment);

        this.elems.columnsMonth = Object.values(this.el.querySelectorAll('.pricegraph-col-month'));
    }

    initEventListeners() {
        this.el.addEventListener('mousemove', this._onMouseMove.bind(this));
        this.el.addEventListener('mouseover', this._onMouseOver.bind(this));
        this.el.addEventListener('mouseout', this._onMouseOut.bind(this));

        this.controls.prev.addEventListener('click', this.slideContent.bind(this, 0));
        this.controls.next.addEventListener('click', this.slideContent.bind(this, 1));

        window.addEventListener('resize', this._onWindowResize.bind(this));
    }

    _onMouseMove(e) {
        e.preventDefault();
        let col  = e.target.closest('.pricegraph-col');

        if (col && col.hasAttribute('data-price')) {
            let price = col.getAttribute('data-price');

            let table = this.elems.table.getBoundingClientRect(),
                width = this.elems.priceview.offsetWidth,
                left  = e.clientX - table.left - width / 2,
                right = table.right - e.clientX - width / 2;

            if (left < 0) {
                this.elems.priceview.style.left = 0 + 'px';
            } else if (right < 0) {
                this.elems.priceview.style.left = (table.width - width) + 'px';
            } else {
                this.elems.priceview.style.left = left + 'px';
            }

            if (price && +price !== 0) {
                this.elems.priceview.textContent = `от ${price} ${this.opts.currency}`;
            } else {
                this.elems.priceview.textContent = this.opts.noDataText;
            }

            this.elems.priceview.style.opacity = 1;
        }
    }

    _onMouseOver(e) {
        e.preventDefault();
        let col = e.target.closest('.pricegraph-col');

        if (col && col.hasAttribute('data-price')) {
            let parentPosition = this.elems.table.getBoundingClientRect(),
                colPosition    = col.getBoundingClientRect();

            this.elems.pricearrow.style.left    = (Math.abs(colPosition.left) - Math.abs(parentPosition.left) + col.offsetWidth / 2) + 'px';
            this.elems.pricearrow.style.opacity = 1;
        }
    }

    _onMouseOut(e) {
        e.preventDefault();
        if (!e.target.closest('.pricegraph-col-container')) this.hideTooltips();
    }

    _onWindowResize(e) {
        this.changeBodyWidth();
    }

    hideTooltips() {
        this.elems.priceview.style.opacity  = 0;
        this.elems.pricearrow.style.opacity = 0;
    }

    slideContent(direction) {
        if (this.isSliding) return;

        this.hideTooltips();

        let table   = this.elems.table.getBoundingClientRect(),
            columns = this.elems.columns.getBoundingClientRect(),
            step    = this.opts.slideBy * this.sizes.colWidth,
            left    = columns.left - table.left,
            right   = columns.right - table.right,
            offset,
            preventMove;

        if (columns.width > table.width) {
            if (direction) {
                offset = (right - step > 0) ? - step : - right;
                preventMove = right === 0;
            } else {
                offset = (left + step < 0) ? step : - left;
                preventMove = left === 0;
            }

            this.isSliding  = true;

            this.elems.columns.style.transform = `translateX(${left + offset}px)`;
            this.changeMonthName(direction, offset, preventMove);
            this.sliding();
        }
    }

    sliding() {
        setTimeout(() => {
            this.isSliding = false;
        }, this.opts.slideTime);
    }

    alignContent() {
        let table   = this.elems.table.getBoundingClientRect(),
            columns = this.elems.columns.getBoundingClientRect(),
            offset;

        if (columns.right - table.right < 0 && columns.left - table.left < 0) {
            offset = table.width - columns.width;

            this.elems.columns.style.transform = `translateX(${offset}px)`;
        }
    }

    centerContent() {
        if (this.opts.currentDate !== null && this.opts.currentDate.length) {
            const   date  = new Date(Date.parse(this.opts.currentDate)),
                    year  = date.getFullYear(),
                    month = date.getMonth(),
                    day   = date.getDate();

            let col = this.findDayCol(year, month, day);

            if (col !== null) {
                let colPos     = col.getBoundingClientRect(),
                    columnsPos = this.elems.columns.getBoundingClientRect();

                let offset = columnsPos.left - colPos.left + this.sizes.colWidth * this.opts.slideBy;

                col.querySelector('.pricegraph-col__value').classList.add('current');

                if (offset < 0) {
                    this.elems.columns.style.transform = `translateX(${offset}px)`;
                    this.changeMonthName(1, offset, false);
                }

                // Убрать это дублирование куда подальше....
                setTimeout(() => {
                    let colPos = col.getBoundingClientRect(),
                        tablePos = this.elems.table.getBoundingClientRect();

                    let price = col.getAttribute('data-price');
                    if (price && +price !== 0) {
                        this.elems.priceview.textContent = `от ${price} ${this.opts.currency}`;
                    } else {
                        this.elems.priceview.textContent = this.opts.noDataText;
                    }
                    this.elems.pricearrow.style.left    = (colPos.left - tablePos.left + col.offsetWidth / 2) + 'px';
                    this.elems.priceview.style.left     = (colPos.left - tablePos.left) + 'px';
                    this.elems.pricearrow.style.opacity = 1;
                    this.elems.priceview.style.opacity  = 1;
                }, this.opts.slideTime);
            }
        }
    }

    calculateSizes() {
        let col       = this.el.querySelector('.pricegraph-col'),
            colWidth  = col.offsetWidth,
            colStyle  = window.getComputedStyle(col, null),
            colMargin = parseFloat(colStyle.marginLeft) + parseFloat(colStyle.marginRight);

        this.sizes.colWidth = colWidth + colMargin;

        this.changeBodyWidth();
    }

    changeBodyWidth() {
        let count = Math.floor(this.elems.bodyCont.offsetWidth / this.sizes.colWidth);

        this.elems.body.style.maxWidth = count * this.sizes.colWidth + 'px';

        setTimeout(() => {
            this.alignContent();
        }, this.opts.slideTime);
    }

    findActualCol(direction, offset) {
        let table = this.elems.table.getBoundingClientRect();

        return this.elems.columnsMonth.find((col) => {
            let colPosition = col.getBoundingClientRect();
            if (direction) {
                return colPosition.left - table.left + offset <= 0 && colPosition.right - table.left + offset >= 0;
            } else {
                return colPosition.left - table.left - offset <= 0 && colPosition.right - table.left - offset >= 0;
            }
        });
    }

    findDayCol(year, month, day) {
        let yearCol = document.querySelector('[data-year="'+ year +'"]');

        if (!yearCol) return null;

        let monthCol = yearCol.querySelector('[data-month="'+ month +'"]');

        if (!monthCol) return null;

        return monthCol.querySelector('[data-day="'+ day +'"]');
    }

    changeMonthName(direction, offset, preventMove) {
        if (!preventMove && this.opts.showSignatures) {
            let column = this.findActualCol(direction, offset),
                month  = column.getAttribute('data-month'),
                year   = column.closest('.pricegraph-col-year').getAttribute('data-year');

            this.changeSignatureText(`${this.opts.months[month]} ${year} г.`);
        }
    }

    changeSignatureText(text) {
        this.elems.signature.textContent = text;
    }

    calcGraphData() {
        let min = Infinity,
            max = 0,
            difference;

        this.opts.data.forEach((item) => {
            let val = Math.ceil(parseFloat(item.price));

            min = val < min ? val : min;
            max = val > max ? val : max;
        });

        this.graphData.min     = min;
        this.graphData.max     = max;
        this.graphData.average = Math.ceil(min + (max - min) / 2);

        difference = (max !== min) ? max - min : 1;

        this.graphData.step = parseFloat(((100 - this.opts.startHeight) / difference).toFixed(5));
    }

    parseData() {
        this.opts.data.forEach((item) => {
            const   date  = new Date(Date.parse(item.date)),
                    year  = date.getFullYear(),
                    month = date.getMonth(),
                    day   = date.getDate();

            if (this.opts.startDate && date < this.opts.startDate) return;
            if (this.opts.endDate   && date > this.opts.endDate)   return;

            if (!this.data.hasOwnProperty(year)) this.data[year] = {};

            if (!this.data[year].hasOwnProperty(month)) {
                let days = this.constructor.getDaysInMonth(year, month);

                this.data[year][month] = {};

                while(days--) {
                    this.data[year][month][days + 1] = 0;
                }
            }

            this.data[year][month][day] = item.price;
        });
    }

    static createControlsView() {
        let controls = document.createElement('div');

        controls.classList.add('pricegraph-controls');

        controls.innerHTML = `
            <div class="pricegraph-btn btn-prev">
                <span class="pricegraph-icon icon-arrow left"></span>
            </div>
            <div class="pricegraph-btn btn-next">
                <span class="pricegraph-icon icon-arrow right"></span>
            </div>
        `;

        return controls;
    }

    static createYearView(year) {
        let yearView = document.createElement('div');

        yearView.classList.add('pricegraph-col-container');
        yearView.classList.add('pricegraph-col-year');
        yearView.classList.add('pricegraph-table-columns__cell');

        yearView.setAttribute('data-year', year);

        return yearView;
    }

    static createMonthView(month) {
        let monthView = document.createElement('div');

        monthView.classList.add('pricegraph-col-container');
        monthView.classList.add('pricegraph-col-month');

        monthView.setAttribute('data-month', month);

        return monthView;
    }

    static getDaysInMonth(year, month) {
        return 33 - new Date(year, month, 33).getDate();
    }

    static datePartsFormatted(year, month, day) {
        month = month < 10 ? "0" + month : month;
        day   = day   < 10 ? "0" + day   : day;

        return `${year}-${month}-${day}`;
    }

    initPolyfills() {
        (function() {
            if (!Element.prototype.closest) {
                Element.prototype.closest = function(css) {
                    let node = this;

                    while (node) {
                        if (node.matches(css)) return node;
                        else node = node.parentElement;
                    }
                    return null;
                };
            }
        })();
    }

}

class PricegraphMonth extends PricegraphDay {
    constructor(element, defaults, options) {
        super(element, defaults, options);
    }

    parseData() {
        this.opts.data.forEach((item) => {
            let month = +item.month - 1;

            if (!this.data.hasOwnProperty(item.year)) {
                let i = 12;
                this.data[item.year] = {};

                while (i--) {
                    this.data[item.year][i] = 0;
                }
            }

            this.data[item.year][month] = item.price;
        });
    }

    createView() {
        this.createBodyView();
        this.createLinesView();

        let fragment = document.createDocumentFragment();

        for (let year in this.data) {
            if (this.data.hasOwnProperty(year)) {
                let yearFragment = this.constructor.createYearView(year);

                for (let month in this.data[year]) {
                    if (this.data[year].hasOwnProperty(month)) {
                        let monthFragment = this.constructor.createMonthView(month);

                        monthFragment.innerHTML = this.createColumnView(this.opts.months[month], this.data[year][month]);
                        yearFragment.appendChild(monthFragment);
                    }
                }

                fragment.appendChild(yearFragment);
            }
        }

        this.elems.columns.appendChild(fragment);

        this.elems.columnsMonth = Object.values(this.el.querySelectorAll('.pricegraph-col-month'));
    }

    createColumnView(title, price) {
        let cost   = +price,
            diff   = cost - this.graphData.min,
            data   = cost > 0 ? 'has-data' : 'no-data',
            height = diff >= 0 ? diff * this.graphData.step + this.opts.startHeight : this.opts.minHeight;

        return `
        <a class="pricegraph-col pricegraph-col-wide" data-price="${cost}">
            <div class="pricegraph-col__graph">
                <div class="pricegraph-col__value ${data}" style="height:${height}%"></div>
            </div>
            <div class="pricegraph-col__date">${title}</div>
        </a>`;
    }
}

// Factory for creating Pricegraph object
const Pricegraph = (element, type, options) => {
    let graph;

    const defaults = {
        baseUrl: '',
        currency: 'Р',
        slideBy: 3,
        slideTime: 300,
        startHeight: 33,
        minHeight: 20,
        minDate: null,
        maxDate: null,
        currentDate: null,
        showSignatures: true,
        noDataText: 'Нет данных',
        months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
    };

    switch (type) {
        case 'byMonth':
            graph = new PricegraphMonth(element, defaults, options);
            break;
        default:
            graph = new PricegraphDay(element, defaults, options);
            break;
    }

    return graph;
};