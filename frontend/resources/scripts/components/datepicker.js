// offset - максимальное количество доступных дней от сегодняшней даты, в днях
// start - сдвиг по старту
function initDatepicker(offset, start) {
    const $dateInput = $('#date-input');
    const $dateStart = $('#date-start');

    let minDate = new Date();
    let maxDate = new Date();
    let startDate = new Date();

    startDate.setDate(minDate.getDate() + start);
    maxDate.setDate(minDate.getDate() + offset);

    const myDatepicker = $dateInput.datepicker({
        minDate: minDate,
        maxDate: maxDate,
        startDate: startDate,
        dateFormat: 'dd M yyyy',
        showOtherYears: false,
        selectOtherYears: false,
        autoClose: true,
        toggleSelected: false,
        position: 'bottom left',
        onShow: function(dp, animationCompleted) {
            if(!animationCompleted){
                const positions = ['bottom left', 'top left', 'right center'];

                let iFits = positions.some(pos => {
                    dp.update('position', pos);
                    return isElementInViewport(dp.$datepicker[0]).all;
                });

                if (!iFits) {
                    dp.update('position', 'bottom left');
                }
            }
            if (animationCompleted) {
                dp.$el.addClass('datapicker-visible');
            }
        },
        onHide: function(dp, animationCompleted) {
            if (animationCompleted) {
                dp.$el.removeClass('datapicker-visible');
            }
        },
        onSelect: function(formattedDate, date, inst) {
            $dateStart.val( getFormattedDate(date, '-') );
        }
    }).data('datepicker');

    $dateInput.on('click', function(){
        if ( $(this).hasClass('datapicker-visible') && myDatepicker.visible ) {
            myDatepicker.hide();
        }
    });

    myDatepicker.selectDate(startDate);
}


function getFormattedDate(date, divider) {
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    month = formatDateComponent(month);
    day = formatDateComponent(day);

    return year + divider + month + divider + day;
}

function formatDateComponent(component) {
    return component.toString().length < 2
        ? '0' + component
        : component.toString();
}

// A function to check wether the element fits inside the viewport:
function isElementInViewport(el) {
    const $window = $(window);
    const rect = el.getBoundingClientRect();

    const fitsLeft = (rect.left >= 0 && rect.left <= $window.width());
    const fitsTop = (rect.top >= 0 && rect.top <= $window.height());
    const fitsRight = (rect.right >= 0 && rect.right <= $window.width());
    const fitsBottom = (rect.bottom >= 0 && rect.bottom <= $window.height());

    return {
        top: fitsTop,
        left: fitsLeft,
        right: fitsRight,
        bottom: fitsBottom,
        all: fitsLeft && fitsTop && fitsRight && fitsBottom,
    };
}

// Определение даты и времени
const time = {
    timestamp: undefined,
    date: undefined,
    formatted: undefined,
    string: undefined,
    locale: 'ru',
    timeRequest: function(offset) {
        offset === undefined ? offset = 0 : offset;

        return new Promise(function(resolve, reject) {
            $.ajax({
                url: '/php/getDate.php',
                type: 'GET',
                dataType: 'json',
                success: function(a) {
                    resolve((parseInt(a) + offset * 24 * 3600) * 1000);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    reject(thrownError);
                }
            });
        })
    },
    setTimestamp: function(a) {
        time.timestamp = a;
    },
    timestampToDate: function() {
        time.date = new Date(time.timestamp);
    },
    dateToFormat: function() {
        var timezone = new Date().getTimezoneOffset();
        var date = new Date(time.timestamp - timezone * 60 * 1000);
        time.formatted = date.toISOString().slice(0,10);
    },
    dateToString: function() {
        var day = time.date.getDate();
        var month = time.date.getMonth();
        var year = time.date.getFullYear();
        var month_ru = ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'];
        time.string = day + ' ' + month_ru[month] + ' ' + year;
    },
    getTime: function(offset) {
        return new Promise (function(resolve, reject) {
            time.timeRequest(offset)
                .then(function(timestamp) {
                    time.setTimestamp(timestamp);
                    time.timestampToDate();
                    time.dateToFormat();
                    time.dateToString();
                    resolve(time);
                });
        });
    }
};

function findBestDate() {
    const formDate = $('#date-start').val();
    let offset;

    return new Promise((resolve, reject) => {
        if (!formDate) {
            offset = (new Date()).getHours() < 16 ? 0 : 1;
            resolve(offset);
        }
        else {
            time.getTime().then(() => {
                offset = ((new Date(formDate)).getTime() - (new Date(time.formatted)).getTime()) / (1000 * 3600 * 24);
                resolve(offset);
            });
        }
    });
}
