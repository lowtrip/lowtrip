// Плейсходер на инпутах
$(document).on('blur change', '.with_help', function(){
    if ($(this).val().length > 0) {
        $(this).siblings('.help').addClass('top');
    }
    else {
        $(this).siblings('.help').removeClass('top');
    }
});

//При выборе города сбрасываем фокус с input
$(document).on("autocompleteselect", ".city_input", function(event, ui){
    setTimeout(function(){
        $(':focus').blur();
    }, 0);
} );

$(document).on('click', '.city_input.form_ui_input', function(){
    $(this).select();
});

$(document).on('focusin', '.city_input.form_ui_input', function(){
    $(this).attr('placeholder', 'Название города...');
});

$(document).on('focusout', '.city_input.form_ui_input', function(){
    $(this).attr('placeholder', '');
});

$(document).on('focusin', '.form_ui_input', function(){
    $(this).closest('.group').addClass('focused');
});

$(document).on('focusout', '.form_ui_input', function(){
    $(this).closest('.group').removeClass('focused');
});

$(document).on('click', function(event){
    if ( $(event.target).closest('form').length > 0 ) {
        return;
    }
    $('.form-el').removeClass('show_this');
});

// Анимация Кнопки Вертушка
$(document).on('click', '.btn_change.change_city', function(){
    var _this = $(this);
    _this.removeClass('rotate');
    setTimeout(function(){
        _this.addClass('rotate');
    }, 10);
});

// Закрытие всплывающего окна
// .close класс кнопки "закрыть", .help-window информационное окно
$(document).on('click', '.help-window .close', function(){
    $(this).closest('.help-window').hide();
});
