// Реклама для левой части train
const filtersAdvert = [
    '<a class="advert__image" target="_blank" href="https://c26.travelpayouts.com/click?shmarker=53486&promo_id=1611&source_type=banner&type=click"><img src="https://c26.travelpayouts.com/content?promo_id=1611&shmarker=53486&type=init"></a>',
    '<a class="advert__image" target="_blank" href="https://c24.travelpayouts.com/click?shmarker=53486&promo_id=1647&source_type=banner&type=click"><img src="http://c24.travelpayouts.com/content?promo_id=1647&shmarker=53486&type=init"></a>',
];

// Сценарий для страницы поездов
function trainScenario() {
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        })
        .then(function(){
            trip.init()
                .then(function(){
                    var trainView = new TrainView('#train-view');
                });
        });
}

function trainTypeScenario(checkedType) {
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        })
        .then(function(){
            trip.init()
                .then(function(){
                    var trainView = new TrainView('#train-view', {
                        checkedType : checkedType
                    });
                });
        });
}

function svahaTrainScenario() {
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        })
        .then(function(){
            trip.init()
                .then(function(){
                    var train_view = new TrainView('#train-view', {
                        friends: true
                    });
                });
        });
}

function noStationsTrainScenario(city, stations) {
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);
        })
        .then(function() {
            ymaps.ready(function(){
                nearbyStations(city, stations);
            });
        });
}

// Сценарий для страницы путешествий
function tripScenario() {
    findBestDate()
        .then(function(offset) {
            initDatepicker(6, offset);
        })
        .then(function(){
            trip.init().then(function(){
                trip.places.find();
                trip.car.find();
                trip.house.find();
                trip.audioGuide.find();
                trip.quests.find();
                TRAIN.findBestPrice()
                    .then(function(result){
                        TRAIN_VIEW.buildOfferSuccess('train', result);
                    })
                    .catch(function(e){
                        TRAIN_VIEW.buildOfferFail('train', e);
                    });
            });
        });
}

// Построение ближайших станций к городу бе станции
function nearbyStations(city, stations) {
    var map = new ymaps.Map("stations-map", {
        center: [city.lat, city.lng],
        zoom: 5,
        controls: [],
    });

    var main_placemark = new ymaps.Placemark([city.lat, city.lng], {
        balloonContentHeader: city.city,
        iconCaption: city.city,
    }, {
        preset: 'islands#redDotIconWithCaption'
    });
    map.geoObjects.add(main_placemark);

    stations = Object.values(stations);

    stations.forEach(function(item, i) {
        var placemark = new ymaps.Placemark([item.lat, item.lng], {
            balloonContentHeader: item.city,
            balloonContentBody: 'Расстояние до г.'+ city.city + ' ' + item.distance + ' км.',
            iconCaption: item.city,
        }, {
            preset: 'islands#blueDotIconWithCaption'
        });
        map.geoObjects.add(placemark);
    });

    map.setBounds(map.geoObjects.getBounds());
}

// Построение ближайших станций к городу бе станции
function routeStations(element,stations) {

    var map = new ymaps.Map(element, {
        center: [stations[0].lat, stations[0].lng],
        zoom: 16,
        controls: []
    });

    stations.forEach(function(item, i) {
        var placemark = new ymaps.Placemark([item.lat, item.lng], {
            balloonContentHeader: item.name,
            balloonContentBody: 'Станция ' + item.name + ', ' + item.country + ', ' + item.region,
            //iconCaption: item.name,
        }, {
            preset: 'islands#redDotIconWithCaption'
        });
        map.geoObjects.add(placemark);
    });

    map.events.add('sizechange', function(e) {
        map.setBounds(map.geoObjects.getBounds());
    });
}

// Сценарий для страницы поездов
function busScenario() {
    findBestDate()
        .then(function(offset) {
            initDatepicker(89, offset);

            // Добавляем подсказки
            $('.has_tooltip').tooltip({
                placement: 'auto'
            });
        })
        .then(function(){
            trip.init()
                .then(function(){
                    var busView = new BusView('#table-asis');
                    busView.run();
                });
        });
}