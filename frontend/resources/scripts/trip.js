"use strict"
// Модуль путешествия
var trip = {
    // Итоговая стоимость путешествия
    price: {
        car: undefined,
        train: undefined,
        house: undefined,

        show: function (item) {
            switch(item) {
                case 'car':
                    trip.price.showCar();
                    break;
                case 'train':
                    trip.price.showTrain();
                    break;
                default:
                    trip.price.showCar();
                    break;
            }
        },

        showCar: function() {
            if (trip.price.car !== undefined && trip.price.house !== undefined) {
                trip.data.price = trip.price.car + trip.price.house;
                var $price = $('#price');
                $price.find('#price-car').text(trip.price.car);
                $price.find('#price-house').text(trip.price.house);
                $price.find('#total-price').text(trip.data.price);
                $price.show();
                $('#popup_email').find('form').attr('data-type', 'car');
            }
        },

        showTrain: function() {
            if (trip.price.train !== undefined && trip.price.house !== undefined) {
                trip.data.price = trip.price.train + trip.price.house;
                var $price = $('#price');
                $price.find('#price-car').text(trip.price.train);
                $price.find('#price-house').text(trip.price.house);
                $price.find('#total-price').text(trip.data.price);
                $price.show();
                $('#popup_email').find('form').attr('data-type', 'train');
            }
        },

        clear: function () {
            $('#price').hide();
            trip.data.price = undefined;
            trip.price.car = undefined;
            trip.price.train = undefined;
            trip.price.house = undefined;
        }
    },

    // Количество найденной информации
    count: 0,

    // Загружаем в модуль исходные данные для путешествия
    init: function() {
        trip.data = getTripData();
        trip.price.clear();

        return Promise.resolve();
    },

    getData: function() {
      return trip.data;
    },

    search: function() {
        trip.init().then(function(){
            trip.places.find();
            trip.car.find();
            trip.house.find();
            trip.audioGuide.find();
            trip.quests.find();
        });
    },

    // Квесты SurprizeMe
    quests: {
        find: function() {
            $.ajax({
                url: '/api/partners/surprizeme/product',
                dataType: 'json',
                data: {
                    city_id: trip.data.second.id,
                },
                type: 'GET',
                success: function(response) {
                    if (response.data !== undefined) {
                        trip.quests.success(response);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                }
            });
        },

        success: function(a) {
            var data = a.data;

            var container = document.getElementById('quests_container');
            var slider = document.createDocumentFragment();

            for (var i = 0; i < data.length; i++)
            {
                var template = '\
                <div class="trip-offer">\
                	<a class="trip-offer-item" href="'+ data[i].url +'" onclick="yaCounter44396998.reachGoal(\'surprizeme\'); return true;" target="_blank">\
						<div class="image quest" style="background-image:url('+ data[i].preview_image +')">\
							<div class="quest_name">\
								'+ data[i].name +'\
							</div>\
						</div>\
						<div class="information">\
							<div class="trip-offer-link">\
								<div class="trip-offer-about">\
									<div class="quest_text">'+ data[i].short_caption +'</div>\
								</div>\
								<div class="offer-price"><span>'+ data[i].price +'</span> Р</div>\
							</div>\
						</div>\
						<div class="trip-cta">\
							Подробнее\
						</div>\
					</a>\
				</div>\
				';

                var div = document.createElement('div');
                div.innerHTML = template;

                slider.appendChild(div);
            }
            container.appendChild(slider);
            trip.quests.slider();
        },

        slider: function() {
            $('#quests_container').owlCarousel({
                autoplay: false,
                items: 4,
                margin: 10,
                nav: true,
                showNav: true,
                navText: ['', ''],
                responsive: {
                    0 : {
                        items: 1
                    },
                    480: {
                        items: 1
                    },
                    768 : {
                        items: 3
                    },
                    992 : {
                        items: 4
                    },
                }
            });

            $('#quests').show();
        }
    },

    // Достопримечательности Google Places
    places: {
        find: function() {
            trip.places.clearAll();

            $('#place_loader').show();
            $.ajax({
                url: '/php/findPlaces.php',
                dataType: 'json',
                data: {
                    lat: trip.data.second.lat,
                    lng: trip.data.second.lng,
                    city_id: trip.data.second.id,
                    type: trip.data.placeType
                },
                type: 'POST',
                success: function(response) {
                    trip.places.addAll(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                }
            });
        },
        addAll: function(res) {
            for (var i=0; i < res.length; i++) {
                $('#places_container').append('\
				<div class="place_item">\
					<div class="photo">\
						<img src="/images/places/'+ res[i].place_id +'.jpg">\
					</div>\
					<div class="text">\
						<span>'+ res[i].name +'</span>\
					</div>\
					<div class="vicinity">'+ res[i].vicinity +'</div>\
				</div>');
            }

            $('#places_container').owlCarousel({
                autoplay: false,
                items: 4,
                nav: true,
                showNav: true,
                navText: ['', ''],
                responsive: {
                    0 : {
                        items: 1
                    },
                    480: {
                        items: 1
                    },
                    768 : {
                        items: 3
                    },
                    992 : {
                        items: 4
                    },
                }
            });

            $('#place_loader').hide();
        },
        clearAll: function() {
            $('#places_container').empty();
            $('#places_container').trigger('destroy.owl.carousel');
        }
    },

    // Трансфер
    car: {
        find: function() {
            trip.car.loader.show();

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: '/api/partners/blablacar/search',
                    dataType: 'json',
                    data: {
                        fn: trip.data.first.city,
                        tn: trip.data.second.city,
                        db: trip.data.date,
                        seats: trip.data.adults,
                        gmt: trip.data.first.gmt
                    },
                    type: 'GET',
                    success: function(a){
                        if (a.trips.length) {
                            trip.car.success(a);
                        }
                        else {
                            trip.car.fail(a);
                        }
                        resolve(a);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                        reject();
                    }
                });
            });
        },

        loader: {
            show: function() {
                $('#car').find('.content').hide();
                $('#car').find('.trip-loader').show();
            },

            hide: function() {
                $('#car').find('.trip-loader').hide();
                $('#car').find('.content').show();
            }
        },

        success: function(a) {
            var b = a.trips[0];
            $('#car').find('.content').empty();

            var template = '\
				<div class="image transfer">\
				</div>\
				<div class="information">\
					<div class="description">\
						'+ b.departure_date +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							от г. <span class="first-city-name">'+ trip.data.first.city +'</span><br>\
							до г. <span class="second-city-name">'+ trip.data.second.city +'</span>\
						</div>\
						<div class="offer-price"><span>'+ b.price.value +'</span> Р</div>\
					</div>\
				</div>\
				<div class="trip-cta">\
					Подробнее\
				</div>\
			';
            $('#car').find('.content').append(template);
            $('#car').attr('href', a.link);

            trip.car.loader.hide();

            trip.price.car = parseInt(b.price.value);
            trip.price.show();

            trip.data.car = a;
        },

        fail: function(a) {
            $('#car').find('.content').empty();

            var template = '\
				<div class="image transfer">\
				</div>\
				<div class="information">\
					<div class="description">\
						'+ trip.data.date +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							Средняя цена поездки<br>\
							от г.'+ trip.data.first.city +' до г.'+ trip.data.second.city +'\
						</div>\
						<div class="offer-price"><span>'+ a.recommended_price +'</span> Р</div>\
					</div>\
				</div>\
				<div class="trip-cta">Подробнее</div>\
				';

            $('#car').find('.content').append(template);
            $('#car').attr('href', a.link);

            trip.car.loader.hide();
        },
    },

    // Жильё
    house: {
        limit: 2,
        count: 0,
        wait: false,
        clear: function() {
            this.count = 0;
            this.wait = false;
        },
        find: function() {
            trip.house.findHouse()
                .then(function(house){
                    trip.house.processing(house);
                })
        },

        findHouse: function() {
            trip.house.loader.show();
            if (trip.house.wait == false) {
                $('#house').find('#loader-status').text('Идет поиск жилья..');
            }

            return new Promise(function(resolve, reject) {
                $.ajax({
                    url: '/php/findHouse.php',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        city: trip.data.second.city,
                        region: trip.data.second.region,
                        date: trip.data.date,
                        adults: trip.data.adults
                    },
                    success: function(house){
                        resolve(house);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status);
                        console.log(thrownError);
                        reject();
                    }
                });
            });
        },

        processing: function(house) {
            if (house.error != null) {
                trip.house.wait = false;
                trip.house.fail();
            }
            else if (house.status === 'progress' && trip.house.count < trip.house.limit) {
                trip.house.waiting();
                setTimeout(function(){
                    trip.house.find();
                }, 3000);
            }
            else {
                trip.house.wait = false;
                trip.house.success(house);
            }
        },

        waiting: function() {
            trip.house.wait = true;
            trip.house.count++ ;
            $('#house').find('.content').empty();
            $('#house').find('#loader-status').text('Ищем лучшее предложение..');
        },

        loader: {
            show: function() {
                $('#house').find('.content').hide();
                $('#house').find('.trip-loader').show();
            },

            hide: function() {
                $('#house').find('.trip-loader').hide();
                $('#house').find('.content').show();
            }
        },

        success: function(a) {
            $('#house').find('.content').empty();

            var template = '\
				<div class="image house_bg" style="background-image: url('+ a.img +');">\
				</div>\
				<div class="information">\
					<div class="description">\
						Жильё в г. '+ a.city +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							Цена за ночь<br>\
							Описание по ссылке\
						</div>\
						<div class="offer-price">'+ parseInt(a.price) +' Р/ночь</div>\
					</div>\
				</div>\
				<div class="trip-cta">\
					Подробнее\
				</div>\
			';

            $('#house').find('.content').append(template);
            $('#house').children('.trip-offer-item').attr('href', a.url);

            trip.house.loader.hide();
            trip.house.clear();

            trip.price.house = parseInt(a.price);
            trip.price.show();
            trip.data.house = a;
        },

        fail: function() {
            $('#house').find('.content').empty();

            var template = '\
				<div class="image house_bg">\
				</div>\
				<div class="information">\
					<div class="description">\
						Жильё в г. '+ trip.data.second.city +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							Свободного жилья в городе не найдено.<br>\
							Попробуйте найти жилье неподалеку на OneTwoTrip.\
						</div>\
					</div>\
				</div>\
				<div class="trip-cta">\
					Перейти\
				</div>\
			';

            $('#house').find('.content').append(template);
            $('#house').children('.trip-offer-item').attr('href', 'https://www.onetwotrip.com/ru/hotels');

            trip.house.loader.hide();
            trip.house.clear();
        }
    },

    audioGuide: {
        find: function() {
            trip.audioGuide.loader.show();
            $.ajax({
                url: '/api/partners/izitravel/find-audio',
                type: 'GET',
                dataType: 'json',
                data: {
                    city_name: trip.data.second.city,
                },
                success: function(a) {
                    (a !== undefined && a.length !== 0) ? trip.audioGuide.success(a[0]) : trip.audioGuide.fail();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                }
            });
        },

        loader: {
            show: function() {
                $('#audiogid').find('.content').hide();
                $('#audiogid').find('.trip-loader').show();
            },

            hide: function() {
                $('#audiogid').find('.trip-loader').hide();
                $('#audiogid').find('.content').show();
            }
        },

        success: function(a) {
            $('#audiogid').find('.content').empty();

            var template = '\
				<div class="image audiogid">\
				</div>\
				<div class="information">\
					<div class="description">\
						Аудиогид по г.'+ trip.data.second.city +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							Найдено '+ a.children_count +' аудиогидов<br>\
							Подробнее\
						</div>\
						<div class="offer-price">БЕСПЛАТНО</div>\
					</div>\
				</div>\
				<div class="trip-cta">\
					Послушать\
				</div>\
			';
            var url = 'https://izi.travel/city/' + a.uuid;

            $('#audiogid').find('.content').append(template);
            $('#audiogid').children('.trip-offer-item').attr('href', url);

            trip.audioGuide.loader.hide();

            trip.data.audio = a;
            trip.data.audio.link = url;
        },

        fail: function(a) {
            $('#audiogid').find('.content').empty();

            var template = '\
				<div class="image audiogid">\
				</div>\
				<div class="information">\
					<div class="description">\
						Аудиогид по г.'+ trip.data.second.city +'\
					</div>\
					<div class="trip-offer-link">\
						<div class="trip-offer-about">\
							Аудиогидов по городу не найдено, вы можете попробовать найти их на IZI.TRAVEL\
						</div>\
					</div>\
				</div>\
				<div class="trip-cta">\
					Перейти\
				</div>\
			';

            $('#audiogid').find('.content').append(template);
            $('#audiogid').children('.trip-offer-item').attr('href', 'https://izi.travel/ru/search');
            trip.audioGuide.loader.hide();

            trip.data.audio = {};
            trip.data.audio.link = "https://izi.travel/ru/search";
        }
    }
};

//Модуль поиска билетов на поезд
var TRAIN = {

    findStation: function(city_obj) {
        var errObj = {};
        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/poezd/find-station',
                type: 'GET',
                dataType: 'json',
                data: {
                    city_id: city_obj.id
                },
                success: function(a) {
                    if (a.stations_id !== 0) {
                        resolve(a);
                    }
                    else {
                        errObj.type = 2;
                        errObj.text = 'В городе '+ city_obj.city +' нет пассажирских железнодорожных станций.<br>';
                        reject(errObj);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    errObj.type = 0;
                    errObj.text = 'Произошла ошибка. Попробуйте сменить направление.';
                    reject(errObj);
                }
            });
        });
    },

    findStationByYandex: function(code) {
        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/poezd/find-station-by-yandex',
                type: 'GET',
                dataType: 'json',
                data: {
                    code: code
                },
                success: function(a) {
                    if (!a.stations_id || a.stations_id === 0) reject();

                    resolve(a);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    reject();
                }
            });
        });
    },

    findAllStationsByYandex: function(codes) {
        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/poezd/find-all-stations-by-yandex',
                type: 'POST',
                dataType: 'json',
                data: {
                    codes: codes
                },
                success: function(response) {
                    if (!response || !response.length) reject('Коды не получены');

                    resolve(response);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    reject();
                }
            });
        });
    },

    findTrains: function(from_code, to_code, date) {
        let errObj = {};

        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/poezd/find-trains',
                type: 'GET',
                dataType: 'json',
                data: {
                    from_code: from_code,
                    to_code: to_code,
                    date: date
                },
                success: function(a) {
                    if (a.success === true && a.result && a.result.length) {
                        a.result.forEach(function(train) {
                            train.deeplink = TRAIN.buildDeeplink(train, from_code, to_code);
                        });
                        resolve(a.result);
                    } else {
                        errObj.text = 'На дату '+ date +' не осталось билетов, или нет курсирующих поездов в этот день.<br>Попробуйте начать поиск на другую дату, воспользовавшись формой поиска.';
                        errObj.type = 1;
                        reject(errObj);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    errObj.text = 'Произошла ошибка. Попробуйте повторить поиск';
                    errObj.type = 0;
                    reject(errObj);
                }
            });
        });
    },

    findTrainsYandex: function(from_code, to_code, date) {
        var errObj = {};
        return new Promise(function(resolve, reject){
            $.ajax({
                url: '/api/partners/poezd/find-trains-yandex',
                type: 'GET',
                dataType: 'json',
                data: {
                    from: from_code,
                    to: to_code,
                    date: date
                },
                success: function(a) {
                    resolve(a);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr.status);
                    console.log(thrownError);
                    errObj.text = 'Произошла ошибка при поиске поездов с пересадками. Попробуйте повторить поиск';
                    errObj.type = 0;
                    reject(errObj);
                }
            });
        });
    },

    findTickets: function() {
        return new Promise(function(resolve, reject){
            Promise.all([
                TRAIN.findStation(trip.data.first),
                TRAIN.findStation(trip.data.second),
            ]).then(function(stations){
                return TRAIN.findTrains(stations[0].stations_id, stations[1].stations_id);
            }).then(function(a){
                resolve(a);
            }).catch(function(e){
                reject(e);
            });
        });
    },

    findBestPrice: function() {
        return new Promise(function(resolve, reject) {
            TRAIN.findTickets()
                .then(function (results) {
                    var best_price, best_result = undefined;
                    for (var i = 0; i < results.length; i++) {
                        var train_best_price = TRAIN.findMinPrice(results[i].places);
                        if (best_price === undefined || train_best_price < best_price) {
                            best_price = train_best_price;
                            best_result = results[i];
                            best_result.minPrice = best_price;
                        }
                    }
                    resolve(best_result);

                    trip.data.train = best_result;
                    trip.price.train = best_result.minPrice;
                }).catch(function (e) {
                    reject(e);
                });
        });
    },

    findMinPrice: function(places) {
        var min = parseInt(places[0].cost);
        for(var i = 1; i < places.length; i++) {
            if (parseInt(places[i].cost) < min) {
                min = parseInt(places[i].cost);
            }
        }
        return min;
    },

    buildDeeplink: function(data, from_code, to_code) {
        var path = "https://www.onetwotrip.com/ru/poezda/train?";
        var classes = [];
        var arr = [];

        var date = trip.data.date.split('-').reverse().join('');

        var params = {
            fromName:   data.route[0],
            toName:     data.route[1],
            train:      data.trainNumber,
            from:       data.from.code,
            to:         data.to.code,
            engineType: data.engineType,
            metaTo:     to_code,
            metaFrom:   from_code,
            date:       date
        };

        data.places.forEach(function(place, i) {
            var str = 'classes[' + i + ']=' + place.type;
            classes.push(str);
        });

        function getParamsString(obj, prop) {
            if (typeof prop !== 'object') {
                return prop + '=' + obj[prop];
            } else {
                var buffer = [];
                for (var childProp in prop) {
                    buffer.push(getParamsString(prop, childProp));
                }
                return buffer.join('&');
            }
        }

        for(var param in params) {
            arr.push(getParamsString(params, param));
        }

        arr.push(classes.join('&'));

        return path + arr.join('&');
    }
};

//Шаблонизатор вывода модуля TRAIN
var TRAIN_VIEW = {
    months: ['января', 'февраля','марта','апреля','мая','июня','июля','августа','сентября','октября','ноября','декабря'],
    // input format: YYYY-MM-DDThh:mm:ss
    parseDateToArray: function(date) {
        return {
            day: date.slice(8,10),
            month: TRAIN_VIEW.months[date.slice(5,7) - 1],
            time: date.slice(11,16),
        };
    },

    buildOfferSuccess: function(element_id, result) {
        var parent = document.getElementById(element_id);
        var loader = parent.querySelector('.trip-loader');

        var departure = this.parseDateToArray(result.departure.time);
        var arrival = this.parseDateToArray(result.arrival.time);

        var deeplink = result.deeplink +'&adults='+ trip.data.adults +'&referrer=affiliate&referrer_mrk=4368-14624-0-2';

        var template = '\
            <div class="image train">\
            </div>\
            <div class="information">\
                <div class="description">\
                    Жд билет '+ trip.data.first.city +' - '+ trip.data.second.city +'\
                </div>\
                <div class="trip-offer-link">\
                    <div class="trip-offer-about">\
                        Отпр.: '+ departure.day + ' ' + departure.month +' '+ departure.time +'<br>\
                        Приб.: '+ arrival.day + ' ' + arrival.month +' '+ arrival.time +'\
                    </div>\
                    <div class="offer-price">'+ result.minPrice +' Р</div>\
                </div>\
            </div>\
            <div class="trip-cta">\
                Подробнее\
            </div>\
        ';

        parent.removeChild(loader);
        parent.innerHTML = template;
        parent.setAttribute('href', deeplink);
    },

    buildOfferFail: function(element_id, e) {
        var parent = document.getElementById(element_id);
        var loader = parent.querySelector('.trip-loader');

        var deeplink = 'https://www.onetwotrip.com/ru/poezda/?scp=60,affiliate,4368-14624-0-2';

        var template = '\
            <div class="image train">\
            </div>\
            <div class="information">\
                <div class="description">\
                    Жд билет '+ trip.data.first.city +' - '+ trip.data.second.city +'\
                </div>\
                <div class="trip-offer-link">\
                    <div class="trip-offer-about">\
                        '+ e.text +'\
                    </div>\
                </div>\
            </div>\
            <div class="trip-cta">\
                Подробнее\
            </div>\
        ';

        parent.removeChild(loader);
        parent.innerHTML = template;
        parent.setAttribute('href', deeplink);
    }
};