var citiesFrom = [];
var citiesTo = [];

var first = '#first';
var second = '#second';
var topCitiesFirst = $('#top-cities-first');
var topCitiesSecond = $('#top-cities-second');

// Местоположение определено?
var locationFinded = false;

// Начал вводить город
var startedTyping = false;

//Сразу навешиваем обработчик на первый город - если ввод начался, startedTyping = true
$(first).on('input keyup', function(){
    startedTyping = true;
});

// Получить популярные города из..
$.ajax({
    url: '/city/top-cities-russia',
    type: 'GET',
    data: {},
    dataType: 'json',
    success: function(data) {
        pushCitiesToList(data, citiesFrom);
        topCities(citiesFrom, topCitiesFirst);
    }
});

// Валидация поисковой формы
function validation(){
    if ($(first).attr('data-valid') === 'valid' && $(second).attr('data-valid') === 'valid') {
        return true;
    }
    if ($(first).attr('data-valid') === 'invalid') {
        validateCityInput($(first));
    } else {
        validateCityInput($(second));
    }
    return false;
}

// Валидация поля ввода города
function validateCityInput(input) {
    var help = input.closest('.group').find('.help-window');
    if (input.attr('data-valid') === 'invalid' && input.val().length > 0) {
        help.find('.text').text('Город с таким названием не найден. Начните ввод заново, и выберите город из выпадающего списка.');
        help.fadeIn(400);
    }
    if (input.attr('data-valid') === 'invalid' && input.val().length === 0) {
        help.find('.text').text('Поле не заполнено. Пожалуйста, введите название города.');
        help.fadeIn(400);
    }
}

// Сбор исходных данных для путешествия
function getTripData() {
    return {
        first: {
            label: $('#first').val(),
            id: $('#first-id').val(),
            city: $('#first-city').val(),
            city_slug: $('#first-city_slug').val(),
            region: $('#first-region').val(),
            region_slug: $('#first-region_slug').val(),
            country: $('#first-country').val(),
            country_slug: $('#first-country_slug').val(),
            lat: $('#first-lat').val(),
            lng: $('#first-lng').val(),
            gmt: $('#first-gmt').val()
        },
        second: {
            label: $('#second').val(),
            id: $('#second-id').val(),
            city: $('#second-city').val(),
            city_slug: $('#second-city_slug').val(),
            region: $('#second-region').val(),
            region_slug: $('#second-region_slug').val(),
            country: $('#second-country').val(),
            country_slug: $('#second-country_slug').val(),
            lat: $('#second-lat').val(),
            lng: $('#second-lng').val(),
            gmt: $('#second-gmt').val()
        },
    };
}

//Определение координат средствами API Yandex Maps
function yandexSearch(){
    var myCoords,
        myCityName;

    ymaps.ready(function(){
        ymaps.geolocation.get({
            provider: 'yandex',
            autoReverseGeocode: true
        }).then(function (result) {
            var item = result.geoObjects.get(0)
            myCityName = item.properties.get('name');
            myCoords = item.geometry.getCoordinates();
            console.log('Город:' + myCityName + '; Координаты: ' + myCoords);
            yandexAfterFind(myCityName, myCoords);
            //yandexAfterFind("Кукуево", [0,0]);
        }).catch(function(e){
            console.log(e);
            notFoundNotification();
        });
    });
}

function yandexAfterFind(myCityName, myCoords) {
    $.ajax({
        url: '/city/find-city-yandex',
        type: 'GET',
        data: {
            city: myCityName,
            lat: myCoords[0],
            lng: myCoords[1]
        },
        dataType: 'json',
        success: function(value) {
            if (value !== false && startedTyping === false) {
                var obj = {
                    label:value.city,
                    city: value.city,
                    city_slug: value.city_slug,
                    region: value.region,
                    region_slug: value.region_slug,
                    country: value.country,
                    country_slug: value.country_slug,
                    lat: value.lat,
                    lng: value.lng,
                    id: value.id,
                    gmt: value.GMT
                };

                $(first).blur();
                setAttr(first, obj);
                findCities(first);
                locationFinded = true;
            }
            notFoundNotification();
        }
    });
}

function notFoundNotification() {
    if ( locationFinded === false && startedTyping === false) {
        var $notification = $('#notification_city_1');
        $notification
            .find('.text')
            .text('Не удалось определить Ваше местоположение. Пожалуйста, введите название вашего города.');
        $notification.fadeIn(400);
    }
}

function setAttr(city, obj) {
    $(city).val(obj.label);
    $(city+'-id').val(obj.id);
    $(city+'-city').val(obj.city);
    $(city+'-city_slug').val(obj.city_slug);
    $(city+'-region').val(obj.region);
    $(city+'-region_slug').val(obj.region_slug);
    $(city+'-country').val(obj.country);
    $(city+'-country_slug').val(obj.country_slug);
    $(city+'-lat').val(obj.lat);
    $(city+'-lng').val(obj.lng);
    $(city+'-gmt').val(obj.gmt);

    $(city).attr('data-index', obj.index);

    if ($(city+'-id').val().length > 0 && $(city+'-city_slug').val().length > 0 ){
        $(city).attr('data-valid', 'valid');
    }
    else {
        $(city).attr('data-valid', 'invalid');
    }

    $(city).trigger('change');
}

// Функция для навешивания автокомплита на инпут city
function autocompleteList(city){
    var another = '#first';
    if (city === '#first'){
        another = '#second';
    }

    $(city).autocomplete({
        minLength: 2,
        autoFocus: true,
        appendTo: $(city).closest('.form-input-block'),
        source: function(request, response) {
            $.ajax({
                url: '/city/find-cities-by-term',
                type: 'GET',
                data: {
                    term: request.term,
                    lat: $(another+'-lat').val(),
                    lng: $(another+'-lng').val(),
                    except: $(another+'-id').val()
                },
                dataType: 'json',
                success: function(data) {
                    response($.map( data, function(value){
                        return {
                            label:value.city,
                            city: value.city,
                            city_slug: value.city_slug,
                            region: value.region,
                            region_slug: value.region_slug,
                            country: value.country,
                            country_slug: value.country_slug,
                            lat: value.lat,
                            lng: value.lng,
                            id: value.id,
                            gmt: value.GMT,
                            distance: value.distance,
                        };
                    }));
                }
            });
        },
    }).autocomplete( "instance" )._renderItem = function( ul, item ) {
        var distance_string = item.distance !== undefined ? item.distance + ' км' : '';

        return $( "<li>" )
            .append(
                '<div class="listbox">' +
                '<div class="cityname">'+ item.city +'<span class="distance">'+ distance_string +'</span></div>' +
                '<div class="region">'+ item.region +', '+ item.country +'</div>' +
                '</div>')
            .appendTo( ul );
    };

    var menu = $(city).autocomplete("widget");

    $(city).on('autocompletechange', function(event,ui) {
        if ( ui.item === null || ui.item  === undefined ){
            if ( menu.children.length > 0 ) {
                $(menu[0].children[0]).click();
            }
            else {
                $(city).attr('data-index', -1).attr('data-valid', 'invalid');
            }
        }
        else if (city === '#first') {
            setAttr(city, ui.item);
            findCities(city);
        }
        else {
            setAttr(city, ui.item);
        }
    });
}

// Функция поиска ближайших городов
function findCities(city) {
    $.ajax({
        url: '/city/find-close-cities',
        type: 'POST',
        data: {
            lat: $(city + '-lat').val(),
            lng: $(city + '-lng').val(),
            id: $(city + '-id').val(),
        },
        dataType: 'json',
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
        },
        success: function(response){
            citiesTo = [];
            pushCitiesToList(response, citiesTo);
            topCities(citiesTo, topCitiesSecond);
        }
    });
}

function pushCitiesToList(cities, list) {
    cities.forEach((value, key) => {
        list.push({
            index: key,
            label:value.city,
            city: value.city,
            city_slug: value.city_slug,
            region: value.region,
            region_slug: value.region_slug,
            country: value.country,
            country_slug: value.country_slug,
            lat: value.lat,
            lng: value.lng,
            id: value.id,
            gmt: value.GMT
        });
    });
}

// Заполнение блока топ 5 городов
function topCities(list, elem) {
    elem.empty();
    for (let i = 0; i < 3; i++) {
        var item = '<li><span class="city-btn--change" onclick="yaCounter44396998.reachGoal(\'city-btn-'+ i +'\'); return true;" data-index="'+ i +'">'+ list[i].city +'</span></li>';
        elem.append(item);
    }
}

// При нажатии на кнопку города из топ5, меняем его в поле второй город
topCitiesFirst.on('click', '.city-btn--change', function(){
    var index = $(this).attr('data-index');
    setAttr(first, citiesFrom[index]);
    findCities(first);
});

topCitiesSecond.on('click', '.city-btn--change', function(){
    var index = $(this).attr('data-index');
    setAttr(second, citiesTo[index]);
});

// Нажатие кнопки сменить город
$(document).on('click', '#change_city', function(){
    var index = parseInt( $(second).attr('data-index') );

    index = index + 1 >= citiesTo.length ? 0 : index + 1;

    setAttr(second, citiesTo[index]);
});

// Нажатие кнопки поменять города местами
$(document).on('click', '#switch_city', function(){
    var formData = getTripData();
    setAttr('#first', formData.second);
    setAttr('#second', formData.first);
    findCities('#first');
});

function addIframeModal(src = '') {
    const modal = document.createElement('div');

    modal.id = 'modalWrapper';
    modal.classList.add('modal-wrapper');

    const content = document.createElement('div');
    content.classList.add('modal-content');

    const iframe = document.createElement('iframe');

    iframe.id = 'modalIframe';
    iframe.src = src;
    iframe.classList.add('modal-iframe');
    iframe.frameBorder = '0';

    const close = document.createElement('div');
    close.classList.add('modal-close');
    close.innerHTML  = '&times;';

    content.appendChild(close);
    content.appendChild(iframe);
    modal.appendChild(content);

    document.body.appendChild(modal);

    document.addEventListener('click', (e) => {
        if (!e.target.closest('.modal-close')) return;
        const modalTarget = e.target.closest('.modal-wrapper');
        if (modalTarget) {
            modalTarget.style.display = 'none';
        }
    });
}

function openModal(id) {
    const modal = document.querySelector(id);

    if (!modal) return false;

    modal.style.display = 'block';

    return true;
}

$(document).ready(function(){
    autocompleteList(first);
    autocompleteList(second);

    if (!$(first).val().length) {
        yandexSearch();
    } else {
        findCities(first);
    }
});