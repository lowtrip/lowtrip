function BusView (element, options) {
    const DAYS = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
    const MONTHS = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

    this.$container = null;
    this.$progressbarContainer = null;
    this.$messages = null;
    this.$trainResults = null;
    this.$offers = null;
    this.$timetable = null;

    const defaults = {
        api: BUS,
        people: 1,
    };

    this.run = function () {
        if (this instanceof BusView) {
            console.log("busview run\n");
            this.setViewElement(element);
            this.setOptions(options);

            this.init();
        }
    };

    this.init = function () {
        this.renderBaseTemplate();
        this.initProgressbar();
        this.findTickets();
    };

    this.setOptions = function (options) {
        this.options = $.extend(true, {}, defaults, options);
    };

    // Задать контейнер для отображения
    this.setViewElement = function (element) {
        this.$container = $(element);
    };

    this.getStringOrNull = function (property) {
        return property || '';
    };

    // Базовый шаблон модуля
    this.renderBaseTemplate = function() {
        let template = `
            <div id="progressbar" class="progressbar-container"></div>
            <div id="bus-view" class="tickets-table"></div>
        `;

        this.$container.empty().append(template);

        this.$progressbarContainer = this.$container.find('#progressbar');

        this.$resultsTable = this.$container.find('#bus-view');
    };

    // Создаем прогрессбар и добавляем в шаблон
    this.initProgressbar = function () {
        this.progressbar = new Progressbar(this.$progressbarContainer.attr('id'));
    };

    this.initProgressbarInterval = function() {
        this.progressbarInterval = setInterval(() => {
            this.progressbar.addValue(10);
        }, 1000);
    };

    // Получить данные о билетах через апи
    // Изменение значение прогрессбара
    this.findTickets = function () {
        this.options.api.searchUrl(
            this.options.fromId,
            this.options.toId,
            this.options.date,
            this.options.people
        ).then(urls => {
            this.initProgressbarInterval();
            return Array.isArray(urls) ? urls : Promise.reject();
        }).then(urls => {
            const requests = urls.map(url => this.options.api.findTickets(url));
            return Promise.all(requests)
                .then(results => {
                    return results.reduce((res, resultArray) => {
                        return [...res, ...resultArray];
                    }, []);
                }).catch(() => {
                    this.$resultsTable.text('Билетов на выбранную дату не найдено');
                });
        }).then(results => {
            this.saveTickets(results);

            if(this.tickets.length) {
                this.renderTicketsToCache();
                this.progressbar.hide();
                clearInterval(this.progressbarInterval);
                this.$resultsTable.empty().append(this.ticketsViews);
            } else {
                this.$resultsTable.text('Билетов на выбранную дату не найдено');
            }
        }).finally(() => {
            this.progressbar.hide();
            $('#progressbar-container').hide();
        });
    };

    this.saveTickets = function(tickets) {
        this.tickets = this.sortByDate(tickets, 'START_DATETIME');
    };

    this.sortByDate = function(tickets, property) {
        return [...tickets].sort((a,b) => {
            let [x, y] = [Date.parse(a[property]), Date.parse(b[property])];
            return (x>y ? 1 : (x === y ? 0 : -1));
        });
    };

    this.renderTicketsToCache = function() {
        this.ticketsViews = '';

        this.tickets.forEach((ticket, idx) => {
            this.ticketsViews += this.renderTicketView(ticket);
        });

        return Promise.resolve();
    };



    // Рендеринг билета и return его view
    this.renderTicketView = function(ticket) {
        return `
        <div class="ticket tickets-table__item">
            <div class="ticket__departure ticket-col">
                <div class="ticket__time">
                    <span class="ticket__time--hour">${this.getStringOrNull(ticket.START_TIME)}</span>
                    <span class="ticket__time--date">${this.getStringOrNull(ticket.START_DATE)}</span>
                </div>
                <div class="ticket__point">${this.getStringOrNull(ticket.STATION_BEGIN_NAME)}</div>
                <div class="ticket__station">${this.getStringOrNull(ticket.STATION_BEGIN_ADDRESS)}</div>
                <div class="ticket__company">"${this.getStringOrNull(ticket.CARRIER_NAME)}</div>
            </div>
            <div class="ticket__duration ticket-col">
                <img class="ticket-duration__arrow" src="/img/bus/arrow.png"></img>
                <div class="ticket-duration__time">${this.getStringOrNull(ticket.TIME_IN_ROAD)}</div>
                <div class="ticket-duration__sub">время в пути</div>
            </div>
            <div class="ticket__arrival ticket-col">
                <div class="ticket__time">
                    <span class="ticket__time--hour">${this.getStringOrNull(ticket.END_TIME)}</span>
                    <span class="ticket__time--date">${this.getStringOrNull(ticket.END_DATE)}</span>
                </div>
                <div class="ticket__point">${this.getStringOrNull(ticket.STATION_END_NAME)}</div>
                <div class="ticket__station">${this.getStringOrNull(ticket.STATION_END_ADDRESS)}</div>
                <div class="ticket__company"></div>
            </div>
            <div class="ticket__days ticket-col">Ежедневно</div>
            <div class="ticket__partner ticket-col">
                <img class="ticket__partner-logo" src="/img/logo/${ticket.PARTNER_LOGO}" alt="">
            </div>
            <div class="ticket__price ticket-col">
                <div class="ticket-price__amount">${this.getStringOrNull(ticket.COST)} руб</div>
                <a class="button button--green ticket__button" href="${this.getStringOrNull(ticket.URL)}" target="_blank" rel="nofollow noopener noreferrer">Купить билет</a>
            </div>
        </div>`;
    };

}