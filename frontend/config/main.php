<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'enableCookieValidation' => false,
            'enableCsrfValidation' => false,
            'baseUrl' => '',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'appendTimestamp' => true,
            'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_ENV_DEV ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_ENV_DEV ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'distance' => [
            'class' => 'common\components\Distance'
        ],
        'statisticsTrain' => [
            'class' => 'common\components\StatisticsTrain'
        ],
        'dateFormatter' => [
            'class' => 'common\components\DateFormatter'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'trip',
                    'routePrefix' => 'trip',
                    'rules'  => [
                        '<action:(search)>'   => 'search',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from:\S+>'          => 'city'
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'car',
                    'routePrefix' => 'car',
                    'rules'  => [
                        ''                    => 'main',
                        '<action:(search)>'   => 'search',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from:\S+>'          => 'city',
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'train',
                    'routePrefix' => 'train',
                    'rules'  => [
                        '' => 'main',
                        '<action:(search)>'                  => 'search',
                        '<action:(raspisanie)>'              => 'timetable-main',
                        '<action:(raspisanie)>/<number:\S+>' => 'timetable-train',
                        '<from:\S+>/<to:\S+>/<type:\S+>'     => 'build-type',
                        '<from:\S+>/<to:\S+>'                => 'build',
                        '<from:\S+>'                         => 'city',
                    ]
                ],
                [
                    'class'  => 'yii\web\GroupUrlRule',
                    'prefix' => 'svaha',
                    'routePrefix' => 'svaha',
                    'rules'  => [
                        '' => 'index',
                        '<action:(search)>'     => 'search',
                        '<action:(rating)>'     => 'rating',
                        '<from:\S+>/<to:\S+>'   => 'build',
                    ]
                ],
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'bus',
                    'routePrefix' => 'bus',
                    'rules' => [
                        '' => 'main',
                        '<action:(search)>' => 'search',
                        '<from_slug:\S+>/stations/<station_slug:\S+>' => 'station',
                        '<from:\S+>/<to:\S+>' => 'build',
                        '<from_slug:\S+>' => 'city',
                    ]
                ],

                '<controller:(redirect)>/<action:(onetwotrip)>' => 'redirect/onetwotrip',

            ],
        ],
        
    ],
    'params' => $params,
];
