<?php

namespace frontend\controllers;

use common\models\TrainPopularCities;
use common\models\TrainPopularRoutes;
use common\models\TrainRoutes;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

use common\helpers\DateHelper;

use common\models\City;
use common\models\PageTrainTypeRoute;

class SvahaController extends Controller
{
    public $layout = 'svaha-layout';

    // Генерация страницы base/svaha
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    // Проверка формы и перенаправление на base/svaha/city1/city2
    public function actionSearch()
    {
        $request = Yii::$app->request;

        $from = $request->post('from');
        $to   = $request->post('to');
        $date = $request->post('date');
        $goal = $request->post('goal');
        $self = $request->post('self');
        $find = $request->post('find');

        if (!DateHelper::dateIsActual($date)) {
            throw new NotFoundHttpException('Дата поездки не определена или в неверном формате.');
        }

        if ($from['id'] == $to['id']) {
            throw new NotFoundHttpException('Город отправления не должен совпадать с городом прибытия.');
        }

        $url = Url::toRoute(['svaha/build', 'from' => $from['city_slug'], 'to' => $to['city_slug'], 'date' => $date, 'goal' => $goal, 'self' => $self, 'find' => $find]);
        $this->redirect($url);
    }

    // Генерация страницы base/svaha/city1/city2
    public function actionBuild($from, $to)
    {
        $request = Yii::$app->request;

        $date = $request->get('date');
        $goal = $request->get('goal');
        $self = $request->get('self');
        $find = $request->get('find');

        $first  = City::findCityWithStationsBySlug($from);
        $second = City::findCityWithStationsBySlug($to);

        if (empty($first) || empty($second)) {
            $this->redirect(Url::toRoute('train/main'));
            return;
        }

        if (empty($date) || !DateHelper::dateIsActual($date)) {
            $date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        }

        if (!empty($request->queryString)) {
            Yii::$app->view->registerLinkTag(
                [
                    'rel' => 'canonical',
                    'href' => Url::toRoute(['train/build', 'from' => $from, 'to' => $to], true)
                ]);
        }

        if ($first->stations->has_stations == 0 ||$second->stations->has_stations == 0) {
            $url = Url::toRoute(['train/build', 'from' => $from['city_slug'], 'to' => $to['city_slug'], 'date' => $date]);
            $this->redirect($url);
        }

        $page = PageTrainTypeRoute::findPage($first->id, $second->id);

        $route = TrainRoutes::getRoute($first->id, $second->id);

        $routeRating = TrainPopularRoutes::getRouteRating($route->route_id);

        return $this->render('train', [
            'first' => $first,
            'second' => $second,
            'date' => $date,
            'goal' => $goal,
            'find' => $find,
            'self' => $self,
            'page' => $page,
            'routeRating' => $routeRating,
        ]);
    }

    public function actionRating()
    {
        $popular_routes = TrainPopularRoutes::getPopularRoutes(50);
        $popular_cities = TrainPopularCities::getPopularCities(50);

        return $this->render('rating', [
            'popular_routes' => $popular_routes,
            'popular_cities' => $popular_cities
        ]);
    }

}
