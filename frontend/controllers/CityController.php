<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

use yii\web\Response;

use common\models\City;
use common\models\TrainPopularRoutes;

class CityController extends Controller
{

    public function actionGetAllCities ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return City::find()->all();
    }
    
    public function actionFindCloseCities ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $lat = $request->post('lat');
        $lng = $request->post('lng');
        $id = $request->post('id');
        
        return City::find()
            ->where(['between', 'lat', $lat - 4, $lat + 4])
            ->andWhere(['between', 'lng', $lng - 6, $lng + 6])
            ->andWhere(['country_slug' => 'rossiya'])
            ->andWhere(['not', ['city_id' => null]])
            ->andWhere(['not in', 'id',  $id])
            ->orderBy(['population' => SORT_DESC])
            ->all();
    }

    public function actionFindCitiesByTerm ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $term = $request->get('term');
        $lat = $request->get('lat');
        $lng = $request->get('lng');
        $except = $request->get('except');

        $cities = City::findCitiesByTerm($term, $except);

        if (!empty($lat) && !empty($lng)){
            $results = [];
            foreach ($cities as $city) {
                $result = $city;
                $distance = Yii::$app->distance->getDistance($lat, $lng, $city['lat'], $city['lng']);
                $result['distance'] = $distance;
                $results[] = $result;
            }
            return $results;
        }

        return $cities;
    }

    public function actionFindCityYandex ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $city = $request->get('city');
        $lat = $request->get('lat');
        $lng = $request->get('lng');

        $variants = City::findCityByNameInRadius($city, $lat, $lng, 0.2);

        return !empty($variants) ? $variants : false;
    }

    public function actionPopularCitiesTrain()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $city_id = $request->get('id');

        return TrainPopularRoutes::getPopularCitiesTrain($city_id);
    }

    public function actionTopCitiesRussia()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        return City::topCitiesRussia();
    }
}