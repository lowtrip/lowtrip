<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

use common\controllers\FrontendController;

use common\helpers\DateHelper;
use common\helpers\NumberHelper;

use common\models\City;
use common\models\Seocar;

class CarController extends FrontendController
{
    public $layout = 'car-layout';

    public function actionMain()
    {
        return $this->render('main', [
        ]);
    }

    public function actionSearch()
    {
        $request = Yii::$app->request;

        $from = $request->post('from');
        $to = $request->post('to');

        if ($from['id'] === $to['id']) {
            throw new NotFoundHttpException('Город отправления не должен совпадать с городом прибытия.');
        }

        $url = Url::toRoute(['car/build', 'from' => $from['city_slug'], 'to' => $to['city_slug']]);

        $this->redirect($url);
    }

    // Генерация страницы base/car/city
    public function actionCity($from)
    {
        $city = City::findCityBySlug($from);

        if (!$this->checkParamsNotEmpty($city)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }

        $popularFrom = $city->findPopularRoutesWithDistanceFrom(20);

        return $this->render('from', [
            'city' => $city,
            'popularFrom' => $popularFrom,
        ]);
    }

    public function actionBuild($from, $to)
    {
        $first = City::findCityBySlug($from);
        $second = City::findCityBySlug($to);

        if (empty($first) || empty($second)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }

        $date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');

        $distance = Yii::$app->distance->getDistance($first->lat, $first->lng, $second->lat, $second->lng);
        $economy = round($distance * 4);
        $time = DateHelper::secondsToTime($distance * 60);

        $routeData = [
            'distance' => $distance,
            'economy' => $economy,
            'time' => $time,
        ];

        return $this->render('city-city', [
            'first' => $first,
            'second' => $second,
            'date' => $date,
            'routeData' => $routeData,
        ]);
    }
}
