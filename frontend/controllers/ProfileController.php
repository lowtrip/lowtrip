<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

use common\models\Profile;
use common\models\Travel;
use common\models\TravelSearch;

use yii\data\ActiveDataProvider;

class ProfileController extends Controller
{
    public $layout = 'cabinet';

    public function actionIndex()
    {
        if(!($model = Profile::findOne(Yii::$app->user->id))) {
            $model = new Profile();
            $model->user_id = Yii::$app->user->id;
            return $this->redirect(['edit']);
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionEdit()
    {
        if (!($model = Profile::findOne(Yii::$app->user->id))) {
            $model = new Profile();
            $model->user_id = Yii::$app->user->id;
        }

        if ($model->load(Yii::$app->request->post()) ) {
            if ( $model->validate() ) {
                if ($model->updateProfile()) {
                    Yii::$app->session->setFlash('success', 'Профиль изменен');
                } else {
                    Yii::$app->session->setFlash('error', 'Профиль не изменен');
                    Yii::error('Ошибка записи. Профиль не изменен');
                    //return $this->refresh();
                }
            }
            else {
                print_r( $errors = $model->errors );
            }
        }

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    public function actionTravels()
    {
        $searchModel = new TravelSearch();

        $query = Travel::find()->where(['user_id' => Yii::$app->user->id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $this->render('travels', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionDelete($id)
    {
        Travel::findOne($id)->delete();

        return $this->redirect(['travels']);
    }

}
