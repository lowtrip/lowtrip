<?php

namespace frontend\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

use common\controllers\FrontendController;

use common\helpers\DateHelper;
use common\helpers\NumberHelper;

use common\models\TrainNumbers;
use common\models\TrainPlaceTypes;
use common\models\TrainPopularCities;
use common\models\TrainPopularRoutes;
use common\models\TrainRoutes;
use common\models\TrainRoutesStatistics;
use common\models\TrainRoutesStatisticsMonths;

use common\models\City;
use common\models\PageTrainTypeCity;
use common\models\PageTrainTypeRoute;

use common\classes\TrainHandler;

class TrainController extends FrontendController
{
    public $layout = 'train-layout';

    // Проверка формы и перенаправление на base/train/city1/city2
    public function actionSearch()
    {
        $request = Yii::$app->request;

        $from   = $request->post('from');
        $to     = $request->post('to');
        $date   = $request->post('date');
        $people = $request->post('people');
        $type   = $request->post('type');

        if (!DateHelper::dateIsActual($date)) {
            throw new NotFoundHttpException('Дата поездки не определена или в неверном формате.');
        }

        if (!NumberHelper::inRange($people, 1, 4)) {
            throw new NotFoundHttpException('Не определено количество человек.');
        }

        if ($from['id'] === $to['id']) {
            throw new NotFoundHttpException('Город отправления не должен совпадать с городом прибытия.');
        }

        if(!empty($type)) {
            $url = Url::toRoute(['train/build-type', 'from' => $from['city_slug'], 'to' => $to['city_slug'], 'date' => $date, 'people' => $people, 'type' => $type]);
        } else {
            $url = Url::toRoute(['train/build', 'from' => $from['city_slug'], 'to' => $to['city_slug'], 'date' => $date, 'people' => $people]);
        }

        $this->redirect($url);
    }

    // Генерация страницы base/train/city1/city2
    public function actionBuild($from, $to)
    {
        $request = Yii::$app->request;

        $date   = $request->get('date');
        $people = $request->get('people');

        $first  = City::findCityWithStationsBySlug($from);
        $second = City::findCityWithStationsBySlug($to);

        if (!$this->checkParamsNotEmpty($first, $second)) {
            $this->redirect(Url::toRoute('train/main'));
            return;
        }

        if (empty($date) || !DateHelper::dateIsActual($date)) {
            $date = (new \DateTime())->format('Y-m-d');
        }

        if (empty($people) || !NumberHelper::inRange($people, 1, 4)) {
            $people = 1;
        }

        if (!empty($request->queryString)) {
            Yii::$app->view->registerLinkTag(
                [
                    'rel' => 'canonical',
                    'href' => Url::toRoute(['train/build', 'from' => $from, 'to' => $to], true)
                ]);
        }

        // Если в городах нет жд станций, то показываем страницу выбора другого города
        $noStations = $this->checkRouteCityStations($first, $second);

        if (!empty($noStations)) {
            return $this->render($noStations['view'], [
                'first' => $first,
                'second' => $second,
                'date' => $date,
                'people' => $people,
                'near_stations' => $noStations['near_stations'],
            ]);
        }


        $page = PageTrainTypeRoute::findPage($first->id, $second->id);

        $route = TrainRoutes::getRoute($first->id, $second->id);

        $statistics = TrainRoutesStatistics::findStatisticsByRouteMinPrices($route->route_id, 90);

        $types = TrainPlaceTypes::find()->all();

        $min_price = TrainRoutesStatisticsMonths::getMinPrice($route->route_id);
        $max_price = TrainRoutesStatisticsMonths::getMaxPrice($route->route_id);

        $lowPrice  = !empty($min_price) ? $min_price[0]['min_price'] : null;
        $highPrice = !empty($max_price) ? $max_price[0]['min_price'] : null;

        $minPrices = TrainRoutesStatisticsMonths::getMinPricesLastMonth($route->route_id);

        // Минимальные цены по месяцам, для графика
        $monthsMinPrices = TrainRoutesStatisticsMonths::getMinPricesByMonths($route->route_id);

        return $this->render('index', [
            'first' => $first,
            'second' => $second,
            'date' => $date,
            'people' => $people,
            'page' => $page,
            'statistics' => $statistics,
            'avaliable_types' => $types,
            'lowPrice' => $lowPrice,
            'highPrice' => $highPrice,
            'minPrices' => $minPrices,
            'monthsMinPrices' => $monthsMinPrices,
            'direct' => $route->direct
        ]);
    }

    // Генерация страницы base/train/city1/city2/type
    public function actionBuildType($from, $to, $type)
    {
        $request = Yii::$app->request;
        
        $date   = $request->get('date');
        $people = $request->get('people');

        $first  = City::findCityWithStationsBySlug($from);
        $second = City::findCityWithStationsBySlug($to);
        $place_type = TrainPlaceTypes::findPlaceType($type);

        if (!$this->checkParamsNotEmpty($first, $second, $place_type)) {
            $this->redirect(Url::toRoute('train/main'));
            return;
        }

        if (empty($date) || !DateHelper::dateIsActual($date)) {
            $date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        }

        if (empty($people) || !NumberHelper::inRange($people, 1, 4)) {
            $people = 1;
        }

        if (!empty($request->queryString)) {
            Yii::$app->view->registerLinkTag(
                [
                    'rel' => 'canonical',
                    'href' => Url::toRoute(['train/build-type', 'from' => $from, 'to' => $to, 'type' => $type], true)
                ]);
        }

        // Если в городах нет жд станций, то показываем страницу выбора другого города
        // Если в городах нет жд станций, то показываем страницу выбора другого города
        $noStations = $this->checkRouteCityStations($first, $second);

        if (!empty($noStations)) {
            return $this->render($noStations['view'], [
                'first' => $first,
                'second' => $second,
                'date' => $date,
                'people' => $people,
                'near_stations' => $noStations['near_stations'],
            ]);
        }

        $page = PageTrainTypeRoute::findPage($first->id, $second->id);

        $route = TrainRoutes::getRoute($first->id, $second->id);

        // Минимальная стоимость билета для микроразметки
        $lowPriceRows = TrainRoutesStatisticsMonths::getMinTypePrice($route->route_id, $place_type->id);
        $lowPrice = !empty($lowPriceRows) ? $lowPriceRows[0]['min_price'] : null;

        $statistics = TrainRoutesStatistics::findStatisticsMinPricesWithType($route->route_id, $place_type->id);

        $types = TrainPlaceTypes::find()->all();

        // Мин.стоимости по месяцам для графика
        //$month_stats = TrainRoutesStatisticsMonths::getStats($route->route_id, $place_type->id);

        return $this->render('index-type', [
            'first' => $first,
            'second' => $second,
            'date' => $date,
            'people' => $people,
            'page' => $page,
            'type' => $place_type,
            'avaliable_types' => $types,
            'lowPrice' => $lowPrice,
            'statistics' => $statistics,
            'direct' => $route->direct
        ]);
    }


    // Генерация страницы base/train/city
    public function actionCity($from)
    {
        $first = City::findCityWithStationsBySlug($from);

        if (!$this->checkParamsNotEmpty($first)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }

        $page = PageTrainTypeCity::findPage($first->id);

        $popular_from = $first->findPopularRoutesFromWithMinPrice();
        $popular_to   = $first->findPopularRoutesToWithMinPrice();

        $topCitiesCountry = $first->topCitiesCountry('rossiya', 5);

        return $this->render('from', [
            'first' => $first,
            'page' => $page,
            'topCities' => $topCitiesCountry,
            'popular_from' => $popular_from,
            'popular_to' => $popular_to
        ]);
    }

    // Генерация страницы base/train
    public function actionMain()
    {
        $popular_routes = TrainPopularRoutes::getPopularRoutesWithPrices(10);
        $popular_cities = TrainPopularCities::getPopularCities(10);

        return $this->render('main', [
            'popular_routes' => $popular_routes,
            'popular_cities' => $popular_cities,
        ]);
    }

    // Генерация страницы base/train/raspisanie
    public function actionTimetableMain()
    {
        return $this->render('timetable-main', [
        ]);
    }

    // Генерация страницы base/train/raspisanie/number
    public function actionTimetableTrain($number)
    {
        $request = Yii::$app->request;
        $date = $request->get('date');

        $trainData = TrainNumbers::findTrainData($number);

        if (!$this->checkParamsNotEmpty($trainData)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }

        $trainHandler = new TrainHandler($trainData);

        $timetable = $trainHandler->findTimetable($date);
        $duration  = $trainHandler->getTrainDuration();
        $distance  = $trainHandler->getTrainDistance();

        return $this->render('timetable-train', [
            'train' => $trainData,
            'timetable' => $timetable,
            'duration' => $duration,
            'distance' => $distance
        ]);
    }


    // Проверить, есть ли в городе отправления, и в городе прибытия вокзалы (станции)
    // В противном случае перенаправить на страницу подбора ближайшей станции
    protected function checkRouteCityStations($first, $second)
    {
        if ($first->stations->has_stations === 0) {
            // Города со станциями, ближайшие к первому городу
            $res = $this->nearbyStationsWithDirectRoutes($first, $second, false, true);

            return [
                'view' => 'near-stations-first',
                'near_stations' => $res
            ];
        }

        if ($second->stations->has_stations === 0) {
            // Города со станциями, ближайшие ко второму городу
            $res = $this->nearbyStationsWithDirectRoutes($first, $second, true, false);

            return [
                'view' => 'near-stations-second',
                'near_stations' => $res
            ];
        }

        return null;
    }

    // Просто поменьше дублирования, код неоптимален
    // Нужно перенести в класс а то пздц
    protected function nearbyStationsWithDirectRoutes($from, $to, $fromHasStations = true, $toHasStations = true)
    {
        if (!$fromHasStations) {
            // Города со станциями, ближайшие к первому городу
            return $from->nearbyCityWithStations($to->id);
        }

        if (!$toHasStations) {
            return $to->nearbyCityWithStations($from->id);
        }
    }
}
