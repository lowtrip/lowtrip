<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;


class RedirectController extends Controller
{
    public $layout = 'redirect-layout';

    public function actionOnetwotrip()
    {
        $request = Yii::$app->request;
        $url = $request->get('url');

        if ($url) {
            $url = urldecode($url);
        }

        return $this->render('onetwotrip', [
            'url' => $url
        ]);
    }

}
