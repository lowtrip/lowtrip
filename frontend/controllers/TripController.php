<?php

namespace frontend\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

use common\helpers\DateHelper;
use common\helpers\NumberHelper;

use common\models\City;
use common\models\Seo;

class TripController extends Controller
{
    public $layout = 'main';

    public function actionSearch()
    {
        $session = Yii::$app->session;
        if (!$session->isActive) {
            $session->open();
        }

        $request = Yii::$app->request;

        $from = $request->post('from');
        $to = $request->post('to');
        $date = $request->post('date');
        $people = $request->post('people');

        if (!DateHelper::dateIsActual($date)) {
            throw new NotFoundHttpException('Дата поездки не определена или в неверном формате.');
        }

        if (!NumberHelper::inRange($people, 1, 4)) {
            throw new NotFoundHttpException('Не определено количество человек.');
        }

        if ($from['id'] === $to['id']) {
            throw new NotFoundHttpException('Город отправления не должен совпадать с городом прибытия.');
        }

        $session->set('date', $date);
        $session->set('people', $people);

        $this->redirect(['trip/'.$from['city_slug'].'/'.$to['city_slug'].'']);
    }

    public function actionBuild($from, $to)
    {
        //Параметры $data и $people берем из сессии
        $session = Yii::$app->session;
        $date = $session->get('date');
        $people = $session->get('people');
        $session->destroy();

        $first = City::find()->where(['city_slug' => $from])->one();
        $second = City::find()->where(['city_slug' => $to])->with('info')->with('reviews')->one();

        if (empty($first) || empty($second)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }
        if (empty($date) || !DateHelper::dateIsActual($date)) {
            $date = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
        }
        if (empty($people) || !NumberHelper::inRange($people, 1, 4)) {
            $people = 1;
        }

        $cities =  $first->topCities();
        $next_cities = $second->topCities();

        $seotext = Seo::find()->where(['first_id' => $first->id])->andWhere(['second_id' => $second->id])->one();

        return $this->render('index', [
            'first' => $first,
            'second' => $second,
            'date' => $date,
            'people' => $people,
            'cities' => $cities,
            'next' => $next_cities,
            'seotext' => $seotext
        ]);
    }

    public function actionCity($from)
    {
        $first = City::find()->where(['city_slug' => $from])->one();

        if (empty($first)) {
            throw new \yii\web\NotFoundHttpException('Страницы не существует.');
        }
        
        $cities = $first->closeCities();
        $topCities = $first->topCities();

        return $this->render('from', [
            'first' => $first,
            'cities' => $cities,
            'topCities' => $topCities,
        ]);
    }
}