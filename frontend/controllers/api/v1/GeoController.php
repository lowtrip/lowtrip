<?php

namespace frontend\controllers\api\v1;

use Yii;
use yii\web\Controller;
use yii\web\Response;

use common\controllers\ApiController;

use common\models\City;

class GeoController extends ApiController
{

    public function init()
    {
        parent::init();
    }

    public function actionFindCitiesByTerm ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $term = $request->get('term');

        return City::findCitiesByTerm($term);
    }

    public function actionFindCloseCities ()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $id = $request->get('id');

        $city = City::findCityById($id);

        if (epmty($city)) return [];
        
        return $city->closeCities(5);
    }
}