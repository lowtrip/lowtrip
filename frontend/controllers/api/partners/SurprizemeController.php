<?php

namespace frontend\controllers\api\partners;

use Yii;
use yii\web\Controller;

use yii\db\Query;

use common\classes\api\SurprizemeApi;

class SurprizemeController extends Controller
{
    public function actionProduct()
    {
        //Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;
        $city_id = $request->get('city_id');

        $city_slug = $this->findCitySlug($city_id);

        $products = array();

        if (!empty($city_slug)) {
            $api = new SurprizemeApi();
            return $api->getProducts($city_slug); 
        }
        return json_encode($products);
    }

    private function findCitySlug($city_id)
    {
        $row = (new Query())
            ->select('city_slug')
            ->from('surprizeme')
            ->where(['city_id' => $city_id])
            ->one();

        return $row['city_slug'];
    }
}