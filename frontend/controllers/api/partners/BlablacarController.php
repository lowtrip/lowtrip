<?php

namespace frontend\controllers\api\partners;

use Yii;
use yii\web\Controller;

use common\classes\api\BlablacarApi;

class BlablacarController extends Controller
{
    public function actionSearch()
    {
        $request = Yii::$app->request;

        $fn = $request->get('fn');
        $tn = $request->get('tn');
        $db = $request->get('db');
        $seats = $request->get('seats');
        $gmt = $request->get('gmt');
        $limit = $request->get('limit');

        date_default_timezone_set("UTC");
        $time = time();
        $time += (int)$gmt * 3600;
        $hb = $db == date("Y-m-d", $time) ?  date("H", $time) : '0';

        if (is_null($limit)) {
            $limit = 1;
        }

        $api = new BlablacarApi(
            array(
                'fn' => $fn,
                'tn' => $tn,
                'db' => $db,
                'de' => $db,
                'hb' => $hb,
                'seats' => $seats,
                'limit' => $limit,
            )
        );

        $data = $api->sendRequest();

        return $data;
    }
}