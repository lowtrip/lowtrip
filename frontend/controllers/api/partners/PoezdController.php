<?php

namespace frontend\controllers\api\partners;

use common\models\TrainStations;
use Yii;
use yii\web\Controller;
use yii\web\Response;

use common\helpers\DateHelper;
use common\controllers\ApiController;

use common\classes\api\OnetwotripTrainApi;
use common\classes\api\YandexRaspApi;

use common\models\CityStations;
use common\models\TrainNumbers;

class PoezdController extends ApiController
{

    public function init()
    {
        parent::init();
    }

    public function actionFindStation()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        $request = Yii::$app->request;

        $city_id = $request->get('city_id');

        return CityStations::findOnetwotripCode($city_id);
    }

    public function actionFindStationByYandex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $yandexCode = $request->get('code');

        return CityStations::findByYandexCode($yandexCode);
    }

    public function actionFindAllStationsByYandex()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $yandexCodes = $request->post('codes');

        return CityStations::findAllByYandexCode($yandexCodes);
    }

    public function actionFindStationRequest()
    {
        $request = Yii::$app->request;

        $text = $request->get('text');

        $api = new OnetwotripTrainApi();

        return $api->suggestStations($text);
    }

    public function actionTimetable()
    {
        $request = Yii::$app->request;

        $from = $request->get('from');
        $to   = $request->get('to');
        $date = $request->get('date');

        $api = new OnetwotripTrainApi();
        
        return $api->metaTimetable($from, $to, $date);
    }

    public function actionFindTrains()
    {
        try {
            $request = Yii::$app->request;

            $from_code = $request->get('from_code');
            $to_code   = $request->get('to_code');
            $date      = $request->get('date');

            $dateFormat = DateHelper::reverseDateFormats($date);

            $api = new OnetwotripTrainApi();
            $result = $api->metaTimetable($from_code, $to_code, $dateFormat);

            $this->sendResponse($result);

            // Отправляем результаты на анализ для сбора данных
            $from_id = CityStations::findCityIdByOnetwotrip($from_code);
            $to_id   = CityStations::findCityIdByOnetwotrip($to_code);

            Yii::$app->statisticsTrain->analyze($result, $from_id, $to_id, $date);
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    // Найти прямые поезда долгим запросом к бд
    public function actionFindDirectTrains()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $first_id = $request->get('from');
        $second_id = $request->get('to');

        return TrainNumbers::findTrainsOnRoute($first_id, $second_id);
    }

    /**
     * Найти поезда с пересадками через YandexRaspApi
     */
    public function actionFindTrainsYandex()
    {
        $request = Yii::$app->request;

        $firstCode  = $request->get('from');
        $secondCode = $request->get('to');
        $date       = $request->get('date');

        $api = new YandexRaspApi();
        $api->setTransfers(true);

        return $api->searchTrains($firstCode, $secondCode, $date);
    }

}