<?php
/**
 * Created by PhpStorm.
 * User: roman
 * Date: 14.09.2018
 * Time: 23:46
 */

namespace frontend\controllers\api\partners;

use common\models\City;
use common\controllers\ApiController;
use Yii;
use yii\web\Response;
use common\classes\api\partners\BusforApi;
use common\classes\api\OnetwotripBusApi;
use common\classes\api\UnitikiBusApi;

use common\models\BusBusforCities;
use common\helpers\response\BusHelper;


class BusTicketsController extends ApiController
{
    public function init(){
        parent::init();
    }

    public function actionSearchUrl(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $params = array(
            'fromId' => $request->get('fromId'),
            'toId' => $request->get('toId'),
            'date' => $request->get('date'),
            'adults' => $request->get('adults'),
        );

        $query = http_build_query($params);

        return [
            //"/api/partners/bus-tickets/onetwotrip?$query",
            "/api/partners/bus-tickets/unitiki?$query",
            "/api/partners/bus-tickets/busfor?$query",
        ];
    }

    public function actionOnetwotrip(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $from_id = $request->get('fromId');
        $to_id = $request->get('toId');
        $date = (new \DateTime($request->get('date')))->format('Y-m-d');

        $from_partner = BusBusforCities::find()->where(['lowtrip_city_id' => $from_id])->one();
        $to_partner = BusBusforCities::find()->where(['lowtrip_city_id' => $to_id])->one();

        $api = new OnetwotripBusApi();

        $response = $api->runSearch($from_partner->onetwotrip, $to_partner->onetwotrip, $date);

        $results = json_decode($response);

        if (empty($results) || empty($results->data)) return [];

        $actionResponse = [];

        foreach($results->data as $data) {
            $actionResponse[] = BusHelper::onetwotrip($data, $date);
        }

        return $actionResponse;
    }

    /**
     * Найти билеты на автобус Busfor
     * @return array
     */
    public function actionBusfor(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $from_id = $request->get('fromId');
        $to_id = $request->get('toId');
        $date = (new \DateTime($request->get('date')))->format('d.m.Y');

        $from_busfor = BusBusforCities::find()->where(['lowtrip_city_id' => $from_id])->one();
        $to_busfor = BusBusforCities::find()->where(['lowtrip_city_id' => $to_id])->one();

        $api = new BusforApi();

        $response = $api->searchTrips($from_busfor->busfor_city_id, $to_busfor->busfor_city_id, $date);

        $actionResponse = [];

        foreach($response->TRIP as $data){
            $actionResponse[] = BusHelper::busfor($data, $date);
        }

        return $actionResponse;
    }

    /**
     * Найти билеты на автобус Unitiki
     * @return array
     */
    public function actionUnitiki(){
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $from_id = $request->get('fromId');
        $to_id = $request->get('toId');
        $date = (new \DateTime($request->get('date')))->format('Y-m-d');

        $from_partner = BusBusforCities::find()->where(['lowtrip_city_id' => $from_id])->one();
        $to_partner = BusBusforCities::find()->where(['lowtrip_city_id' => $to_id])->one();

        $api = new UnitikiBusApi();

        $response = $api->ride_list(array(
            'city_id_start' => $from_partner->unitiki,
            'city_id_end' => $to_partner->unitiki,
            'date' => $date,
            'show_similar' => 2,
        ));

        $actionResponse = [];

        foreach($response->data->ride_list as $data){
            $actionResponse[] = BusHelper::unitiki($data, $date, $response->hash);
        }

        return $actionResponse;

        return $response;
    }
}