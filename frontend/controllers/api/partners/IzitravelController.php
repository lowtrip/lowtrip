<?php

namespace frontend\controllers\api\partners;

use Yii;
use yii\web\Controller;
use yii\web\Response;

use common\controllers\ApiController;

use common\classes\api\IziTravelApi;

class IzitravelController extends ApiController
{
    public function init()
    {
        parent::init();
    }

    public function actionFindAudio($city_name)
    {
        $api = new IziTravelApi();
	    return $api->findCityUUID($city_name);
    }
}