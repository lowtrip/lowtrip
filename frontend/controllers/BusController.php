<?php

namespace frontend\controllers;

use Yii;
use common\classes\api\YandexRaspApi;
use common\models\BusBusforCities;
use common\models\BusStations;
use common\models\PageBusCity;
use common\models\PageBusCityStation;
use common\models\PageBusCityToCity;
use console\controllers\BusforController;
use PHPMailer\PHPMailer\Exception;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\Controller;

use common\helpers\DateHelper;
use common\helpers\NumberHelper;
use common\controllers\FrontendController;

use common\classes\api\partners\BusforApi;
use common\models\City;
class BusController extends FrontendController
{
    public $layout = 'bus-layout';

    public function actionSearch()
    {
        $request = Yii::$app->request;

        $from   = $request->post('from');
        $to     = $request->post('to');
        $date   = $request->post('date');

        if (!DateHelper::dateIsActual($date)) {
            throw new NotFoundHttpException('Дата поездки не определена или в неверном формате.');
        }

        if ($from['id'] === $to['id']) {
            throw new NotFoundHttpException('Город отправления не должен совпадать с городом прибытия.');
        }

        $url = Url::toRoute(['bus/build', 'from' => $from['city_slug'], 'to' => $to['city_slug'], 'date' => $date]);

        $this->redirect($url);
    }

    public function actionBuild($from, $to)
    {
        $request = Yii::$app->request;

        $date   = $request->get('date');
        $people = $request->get('people');

        $from = City::findCityWithStationsBySlug($from);
        $to = City::findCityWithStationsBySlug($to);
        $page = PageBusCityToCity::findOne(['from_id' => $from->id]);

        if (empty($date) || !DateHelper::dateIsActual($date)) {
            $date = (new \DateTime())->format('Y-m-d');
        }

        if (empty($people) || !NumberHelper::inRange($people, 1, 4)) {
            $people = 1;
        }

        return $this->render('city-city', [
            'first' => $from,
            'second' => $to,
            'date' => $date,
            'people' => $people,
            'page' => $page
        ]);
    }

    public function actionCity($from_slug)
    {
        $city = City::find()->where(['city_slug' => $from_slug])->one();

        $page = PageBusCity::findPage($city->id);

        $stations = BusStations::find()->where(['city_id' => $city->id])
            ->andWhere(['type' => 'bus_station'])
            ->all();

        return $this->render('city', [
            'page' => $page,
            'city' => $city,
            'stations' => $stations
        ]);
    }

    public function actionStation($from_slug, $station_slug)
    {
        $from = City::findOne(['city_slug' => $from_slug]);
        $station = BusStations::findOne(['name_slug' => $station_slug]);
        $page = PageBusCityStation::findone(['station_id' => $station->id]);

        try {
            $yandex_api = new YandexRaspApi();
            $response = $yandex_api->busSchedule($station->yandex);
            $schedules = $response->schedule;
        } catch (\Exception $e) {
            $schedules = null;
        }

        return $this->render('station', [
            'city' => $from,
            'station' => $station,
            'stationName' => $station->name,
            'page' => $page,
            'schedules' => $schedules,
        ]);
    }

    public function actionMain()
    {
        $popularRoutes = null;
        $popularStations = null;

        $cities = City::find()->where("city.population > 100000")
            ->joinWith('busStations')
            ->andWhere('bus_stations.type = \'bus_station\'')
            ->limit(91)
            ->all();

        return $this->render('index', [
            'popularStations' => $popularStations,
            'popularRoutes' => $popularRoutes,
            'cities' => $cities
        ]);
    }

}