Lowtrip website
===============================

На фреймворке Yii 2 (Advanced Project Template) [Yii 2](http://www.yiiframework.com/) 


Установка
-------------------
```
git clone git@bitbucket.org:lowtrip/lowtrip.git
php init
```
1) Выбрать Development режим
2) После установки установить подключение к БД в файле:
```
common/config/main-local.php
```

Структура проекта (стандартное описание)
-------------------

```
common
    classes/             Классы для самых различных задач (парсеры, запросы к API партнеров)
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```

Установка node_modules
-------------------
В корне проекта выполнить
```
npm install
```
