var gulp           = require('gulp'),
	gutil          = require('gulp-util' ),
	sass           = require('gulp-sass'),
	babel 		   = require('gulp-babel'),
	browserSync    = require('browser-sync'),
	concat         = require('gulp-concat'),
	uglify         = require('gulp-uglify'),
	cleanCSS       = require('gulp-clean-css'),
	rename         = require('gulp-rename'),
	del            = require('del'),
	imagemin       = require('gulp-imagemin'),
	cache          = require('gulp-cache'),
	autoprefixer   = require('gulp-autoprefixer'),
	ftp            = require('vinyl-ftp'),
	notify         = require("gulp-notify"),
	php     	   = require('gulp-connect-php');

// Скрипты проекта

// Сборка скриптов для trip и для car
gulp.task('trip-bundle', ['trip-libs', 'common-scripts'], function() {
	return gulp.src([
		'frontend/resources/scripts/trip-libs.min.js',
		'frontend/resources/scripts/common-scripts.min.js',
		])
	.pipe(concat('trip-bundle.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/web/js'));
});

// Сборка скриптов для trip и для car
gulp.task('trip-libs', ['common-libs'], function() {
	return gulp.src([
		'frontend/resources/scripts/common-libs.min.js',
		'frontend/resources/libs/owl.carousel/dist/owl.carousel.min.js',
		])
	.pipe(concat('trip-libs.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/resources/scripts'));
});

// Сборка скриптов для train
gulp.task('train-bundle', ['common-libs', 'common-scripts'], function() {
	return gulp.src([
		'frontend/resources/scripts/common-libs.min.js',
		'frontend/resources/scripts/common-scripts.min.js',
		'frontend/resources/scripts/components/progressbar.js',
		'frontend/resources/scripts/components/filters.js',
		'frontend/resources/scripts/components/trainview.js',
		'frontend/resources/scripts/components/pricegraph.js'
		])
	.pipe(concat('train-bundle.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/web/js'));
});

// Сборка скриптов для svaha
gulp.task('svaha-bundle', ['common-libs', 'common-scripts'], function() {
    return gulp.src([
        'frontend/resources/libs/three/three.min.js',
        'frontend/resources/scripts/common-libs.min.js',
        'frontend/resources/scripts/common-scripts.min.js',
        'frontend/resources/scripts/svaha/modules/canvas-header.js',
        'frontend/resources/scripts/svaha/modules/trainview.js',
        'frontend/resources/scripts/svaha/scripts.js'
    ])
	.pipe(concat('svaha-bundle.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/web/js'));
});

// Сборка скриптов для car
gulp.task('car-bundle', function() {
    return gulp.src([
        'frontend/resources/libs/ui/jquery-ui.min.js',
        'frontend/resources/libs/air-datepicker-master/dist/js/datepicker.min.js',
        'frontend/resources/scripts/components/progressbar.js',
        'frontend/resources/scripts/components/search-form.js',
        'frontend/resources/scripts/components/dropdown-helper.js',
        'frontend/resources/scripts/components/datepicker.js',
        'frontend/resources/scripts/car/car.js'
    ])
	.pipe(concat('car-bundle.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/web/js'));
});

//Сборка скриптов для bus
gulp.task('bus-bundle', ['common-libs', 'common-scripts'], function() {
    return gulp.src([
        'frontend/resources/libs/ui/jquery-ui.min.js',
        'frontend/resources/libs/air-datepicker-master/dist/js/datepicker.min.js',
        'frontend/resources/scripts/components/progressbar.js',
        'frontend/resources/scripts/components/search-form.js',
        'frontend/resources/scripts/components/dropdown-helper.js',
        'frontend/resources/scripts/components/datepicker.js',
        'frontend/resources/scripts/api/bus.js',
        'frontend/resources/scripts/trip.js',
        'frontend/resources/scripts/bus/bus.js',
        'frontend/resources/scripts/bus/busview.js',
    ])
        .pipe(concat('bus-bundle.min.js'))
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(uglify())
        .pipe(gulp.dest('frontend/web/js'));
});

// Сборка библиотечных скриптов, общих для всех модулей
gulp.task('common-libs', function() {
	return gulp.src([
		'frontend/resources/libs/air-datepicker-master/dist/js/datepicker.js',
		'frontend/resources/libs/ui/jquery-ui.min.js',
		'frontend/resources/libs/magnific-popup/dist/jquery.magnific-popup.min.js',
		])
	.pipe(concat('common-libs.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/resources/scripts'));
});

// Сборка моих скриптов, общих для всех модулей
gulp.task('common-scripts', function() {
	return gulp.src([
		'frontend/resources/scripts/scripts.js',
		'frontend/resources/scripts/trip.js',
		'frontend/resources/scripts/scenarios.js',
		])
	.pipe(concat('common-scripts.min.js'))
	.pipe(babel({
		presets: ['env']
	}))
	.pipe(uglify())
	.pipe(gulp.dest('frontend/resources/scripts'));
});

gulp.task('browser-sync', function() {
  	browserSync({
  		proxy:'127.0.0.1:8010',
  		port: 8080,
  		open: true,
  		notify: false
  	});
});

gulp.task('redirect', function() {
	return gulp.src([
		'frontend/resources/styles/redirect/*.sass',
        'frontend/resources/libs/loader/loader.css',
	])
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('frontend/web/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('train-styles', function() {
	return gulp.src([
		'frontend/resources/styles/train/train.sass',
	])
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('frontend/web/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('trip-styles', function() {
	return gulp.src('frontend/resources/styles/trip/trip.sass')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('frontend/web/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('car-styles', function() {
    return gulp.src('frontend/resources/styles/car/car.sass')
        .pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix : ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS()) // Опционально, закомментировать при отладке
        .pipe(gulp.dest('frontend/web/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('svaha-styles', function() {
    return gulp.src('frontend/resources/styles/svaha/svaha.sass')
	.pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
	.pipe(rename({suffix: '.min', prefix : ''}))
	.pipe(autoprefixer(['last 15 versions']))
	.pipe(cleanCSS()) // Опционально, закомментировать при отладке
	.pipe(gulp.dest('frontend/web/css'))
	.pipe(browserSync.reload({stream: true}));
});

gulp.task('bus-styles', function() {
    return gulp.src('frontend/resources/styles/bus/bus.sass')
        .pipe(sass({outputStyle: 'expand'}).on("error", notify.onError()))
        .pipe(rename({suffix: '.min', prefix : ''}))
        .pipe(autoprefixer(['last 15 versions']))
        .pipe(cleanCSS()) // Опционально, закомментировать при отладке
        .pipe(gulp.dest('frontend/web/css'))
        .pipe(browserSync.reload({stream: true}));
});

gulp.task('watch-styles', ['styles'], function() {
	gulp.watch('frontend/resources/styles/redirect/*.sass', ['redirect']);

    gulp.watch('frontend/resources/styles/svaha/**/*.sass', ['svaha-styles']);

	gulp.watch('frontend/resources/styles/train/**/*.sass', ['train-styles']);

	gulp.watch('frontend/resources/styles/trip/**/*.sass', ['trip-styles']);

    gulp.watch('frontend/resources/styles/car/**/*.sass', ['car-styles']);

    gulp.watch('frontend/resources/styles/bus/**/*.sass', ['bus-styles']);
});

gulp.task('styles', [
    'redirect',
    'train-styles',
    'trip-styles',
    'svaha-styles',
    'car-styles',
    'bus-styles',
]);

gulp.task('default', ['watch-styles']);