exports.ids = [8];
exports.modules = {

/***/ "06Mq":
/***/ (function(module, exports) {



/***/ }),

/***/ "CNL9":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js
var esm_extends = __webpack_require__("kOwS");

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "react-leaflet"
var external_react_leaflet_ = __webpack_require__("AuoD");

// EXTERNAL MODULE: external "geojson-world-map"
var external_geojson_world_map_ = __webpack_require__("mKi/");
var external_geojson_world_map_default = /*#__PURE__*/__webpack_require__.n(external_geojson_world_map_);

// EXTERNAL MODULE: external "next/dynamic"
var dynamic_ = __webpack_require__("/T1H");
var dynamic_default = /*#__PURE__*/__webpack_require__.n(dynamic_);

// CONCATENATED MODULE: ./src/components/application/results/CustomMap/Marker/index.tsx
var __jsx = external_react_["createElement"];


const Marker = dynamic_default()(() => __webpack_require__.e(/* import() */ 9).then(__webpack_require__.bind(null, "k0sj")), {
  ssr: false,
  loading: () => __jsx("div", null),
  loadableGenerated: {
    webpack: () => [/*require.resolve*/("k0sj")],
    modules: ['./Marker']
  }
});
/* harmony default export */ var CustomMap_Marker = (Marker);
// EXTERNAL MODULE: ./src/components/application/results/CustomMap/CustomMap.scss
var CustomMap = __webpack_require__("06Mq");

// CONCATENATED MODULE: ./src/components/application/results/CustomMap/CustomMap.tsx

var CustomMap_jsx = external_react_default.a.createElement;






const CustomMap_CustomMap = props => CustomMap_jsx(external_react_leaflet_["Map"], {
  center: props.center,
  zoom: props.zoom,
  className: 'custom-map'
}, CustomMap_jsx(external_react_leaflet_["GeoJSON"], {
  data: external_geojson_world_map_default.a,
  style: () => ({
    color: '#ceeefd',
    weight: 0.5,
    fillColor: "#fff",
    fillOpacity: 1,
    backgroundColor: '#ceeefd'
  })
}), CustomMap_jsx("div", {
  style: {
    display: "none"
  }
}, props.markers.map(marker => CustomMap_jsx(CustomMap_Marker, Object(esm_extends["a" /* default */])({
  key: marker.iata
}, marker)))));

/* harmony default export */ var results_CustomMap_CustomMap = __webpack_exports__["default"] = (CustomMap_CustomMap);

/***/ })

};;