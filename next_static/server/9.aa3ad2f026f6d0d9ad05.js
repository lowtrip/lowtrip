exports.ids = [9];
exports.modules = {

/***/ "R2pS":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/destination-marker-78ee0534c9e022841806e1a8aa6b9186.png";

/***/ }),

/***/ "k0sj":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopupMarker", function() { return PopupMarker; });
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("AuoD");
/* harmony import */ var react_leaflet__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_leaflet__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("hgx0");
/* harmony import */ var leaflet__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(leaflet__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;



const iconPerson = new leaflet__WEBPACK_IMPORTED_MODULE_2___default.a.Icon({
  iconUrl: __webpack_require__("R2pS"),
  iconRetinaUrl: __webpack_require__("R2pS"),
  iconSize: new leaflet__WEBPACK_IMPORTED_MODULE_2___default.a.Point(20, 20),
  className: 'leaflet-div-icon'
});
const PopupMarker = ({
  position,
  onClick,
  iata,
  name,
  price
}) => __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_0__["Marker"], {
  position: position,
  onClick: () => onClick({
    place: {
      code: iata,
      name
    },
    city: {
      code: iata,
      name
    },
    country: {
      code: iata,
      name
    },
    type: 'city'
  }),
  icon: iconPerson,
  onMouseOver: e => {
    e.target.openPopup();
  },
  onMouseOut: e => {
    e.target.closePopup();
  }
}, __jsx(react_leaflet__WEBPACK_IMPORTED_MODULE_0__["Popup"], null, __jsx("div", {
  className: 'popup-city'
}, name), __jsx("div", {
  className: 'popup-price'
}, "\u043E\u0442 ", price, " \u20BD.")));
/* harmony default export */ __webpack_exports__["default"] = (react__WEBPACK_IMPORTED_MODULE_1___default.a.memo(PopupMarker));

/***/ })

};;