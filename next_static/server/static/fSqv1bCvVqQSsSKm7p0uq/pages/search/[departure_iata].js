module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../ssr-module-cache.js');
/******/
/******/ 	// object to store loaded chunks
/******/ 	// "0" means "already loaded"
/******/ 	var installedChunks = {
/******/ 		7: 0
/******/ 	};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/ 	// This file contains only the entry chunk.
/******/ 	// The chunk loading function for additional chunks
/******/ 	__webpack_require__.e = function requireEnsure(chunkId) {
/******/ 		var promises = [];
/******/
/******/
/******/ 		// require() chunk loading for javascript
/******/
/******/ 		// "0" is the signal for "already loaded"
/******/ 		if(installedChunks[chunkId] !== 0) {
/******/ 			var chunk = require("../../../../" + ({}[chunkId]||chunkId) + "." + {"8":"309638cd1cb339011666","9":"aa3ad2f026f6d0d9ad05"}[chunkId] + ".js");
/******/ 			var moreModules = chunk.modules, chunkIds = chunk.ids;
/******/ 			for(var moduleId in moreModules) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 			for(var i = 0; i < chunkIds.length; i++)
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 		}
/******/ 		return Promise.all(promises);
/******/ 	};
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// uncaught error handler for webpack runtime
/******/ 	__webpack_require__.oe = function(err) {
/******/ 		process.nextTick(function() {
/******/ 			throw err; // catch this error by using import().catch()
/******/ 		});
/******/ 	};
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 5);
/******/ })
/************************************************************************/
/******/ ({

/***/ "/0+H":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __importDefault(__webpack_require__("cDcd"));

const amp_context_1 = __webpack_require__("lwAK");

function isInAmpMode({
  ampFirst = false,
  hybrid = false,
  hasQuery = false
} = {}) {
  return ampFirst || hybrid && hasQuery;
}

exports.isInAmpMode = isInAmpMode;

function useAmp() {
  // Don't assign the context value to a variable to save bytes
  return isInAmpMode(react_1.default.useContext(amp_context_1.AmpStateContext));
}

exports.useAmp = useAmp;

/***/ }),

/***/ "/T1H":
/***/ (function(module, exports) {

module.exports = require("next/dynamic");

/***/ }),

/***/ "0Zyy":
/***/ (function(module, exports) {



/***/ }),

/***/ "0lfv":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export serializePassengers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return deserializePassengers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return serializeURLDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return deserializeURLDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return buildUrlByParams; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return buildUrlByStringParams; });
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6BQ9");
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("YLtl");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


const serializePassengers = ({
  adults = 1,
  children = 0,
  infants = 0
}) => `${adults}${children}${infants}`;
const deserializePassengers = (passengers = '') => {
  const defaultsPassengeers = {
    adults: 1,
    children: 0,
    infants: 0
  };

  if (!passengers) {
    return defaultsPassengeers;
  }

  return {
    adults: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[0]) || defaultsPassengeers.adults,
    children: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[1]) || defaultsPassengeers.children,
    infants: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[2]) || defaultsPassengeers.infants
  };
};
const serializeURLDate = date => `${Object(lodash__WEBPACK_IMPORTED_MODULE_1__["padStart"])(date.getDate() + '', 2, '0')}${Object(lodash__WEBPACK_IMPORTED_MODULE_1__["padStart"])(date.getMonth() + 1 + '', 2, '0')}${date.getFullYear().toString(10).substr(2, 2)}`;
const deserializeURLDate = date => {
  if (!date) {
    return undefined;
  }

  return new Date(_babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(`20${date.substr(4, 2)}`), _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(date.substr(2, 2)) - 1, _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(date.substr(0, 2)));
};
function buildUrlByParams(params) {
  let nextRoute = '';

  if (params.destination) {
    nextRoute = `/results/${params.origin.place.code}/${params.destination.place.code}`;
  } else {
    nextRoute = `/search/${params.origin.place.code}`;
  }

  nextRoute += `?dept_date=${serializeURLDate(params.departureDate)}`;

  if (params.returnDate) {
    nextRoute += `&ret_date=${serializeURLDate(params.returnDate)}`;
  }

  const passengers = serializePassengers({
    adults: params.adults,
    children: params.children,
    infants: params.infants
  });
  nextRoute += `&passengers=${passengers}`;
  return nextRoute;
}
function buildUrlByStringParams(params) {
  let nextRoute = '';

  if (params.destination) {
    nextRoute = `/results/${params.origin}/${params.destination}`;
  } else {
    nextRoute = `/search/${params.origin}`;
  }

  nextRoute += `?dept_date=${params.departureDate}`;

  if (params.returnDate) {
    nextRoute += `&ret_date=${params.returnDate}`;
  }

  nextRoute += `&passengers=${params.passengers || '100'}`;
  return nextRoute;
}

/***/ }),

/***/ "16zm":
/***/ (function(module, exports) {



/***/ }),

/***/ "1GcG":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pricesMapActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PRICES_MAP_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pricesMap';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_PRICES_MAP"] = 0] = "GET_PRICES_MAP";
  ActionTypes[ActionTypes["GET_PRICES_MAP_SUCCESS"] = 1] = "GET_PRICES_MAP_SUCCESS";
  ActionTypes[ActionTypes["GET_PRICES_MAP_FAIL"] = 2] = "GET_PRICES_MAP_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pricesMapActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const PRICES_MAP_ACTION_TYPES = pricesMapActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "2/0G":
/***/ (function(module, exports) {



/***/ }),

/***/ "2Eek":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ltjX");

/***/ }),

/***/ "2wwy":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Loka");

/***/ }),

/***/ "3FEJ":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypesGenerator; });
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2wwy");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__);

class ActionTypesGenerator {
  constructor(enumActionTypes, moduleName) {
    this.moduleName = void 0;
    this.enumActionTypes = void 0;
    this.actionTypes = {};
    this.moduleActionTypes = {};
    this.moduleName = moduleName;
    this.enumActionTypes = enumActionTypes;
    this.generateActionTypes();
  }

  generateActionTypes() {
    _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default()(this.enumActionTypes).forEach(value => {
      if (typeof value !== 'number') {
        this.actionTypes[value] = `${this.moduleName}/${value}`;
        this.moduleActionTypes[this.actionTypes[value]] = this.actionTypes[value];
      }
    });
  }

  getActionTypes() {
    return this.actionTypes;
  }

  getRequestActionTypes(actionType) {
    return [this.moduleActionTypes[actionType], this.moduleActionTypes[`${actionType}_SUCCESS`], this.moduleActionTypes[`${actionType}_FAIL`]];
  }

}

/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "4mXO":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("k1wZ");

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("RnhE");


/***/ }),

/***/ "5RaE":
/***/ (function(module, exports) {



/***/ }),

/***/ "5chS":
/***/ (function(module, exports) {



/***/ }),

/***/ "6BQ9":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("wa65");

/***/ }),

/***/ "78gN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return prepareDate; });
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("B1jN");
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_padStart__WEBPACK_IMPORTED_MODULE_0__);

const prepareDate = date => `${date.getFullYear()}-${lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default()(date.getMonth() + 1 + '', 2, '0')}-${lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default()(date.getDate() + '', 2, '0')}`;

/***/ }),

/***/ "8Kt/":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Set = __webpack_require__("ttDY");

var _Object$defineProperty = __webpack_require__("hfKm");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __importDefault(__webpack_require__("cDcd"));

const side_effect_1 = __importDefault(__webpack_require__("Xuae"));

const amp_context_1 = __webpack_require__("lwAK");

const head_manager_context_1 = __webpack_require__("FYa8");

const amp_1 = __webpack_require__("/0+H");

function defaultHead(inAmpMode = false) {
  const head = [react_1.default.createElement("meta", {
    key: "charSet",
    charSet: "utf-8"
  })];

  if (!inAmpMode) {
    head.push(react_1.default.createElement("meta", {
      key: "viewport",
      name: "viewport",
      content: "width=device-width,minimum-scale=1,initial-scale=1"
    }));
  }

  return head;
}

exports.defaultHead = defaultHead;

function onlyReactElement(list, child) {
  // React children can be "string" or "number" in this case we ignore them for backwards compat
  if (typeof child === 'string' || typeof child === 'number') {
    return list;
  } // Adds support for React.Fragment


  if (child.type === react_1.default.Fragment) {
    return list.concat(react_1.default.Children.toArray(child.props.children).reduce((fragmentList, fragmentChild) => {
      if (typeof fragmentChild === 'string' || typeof fragmentChild === 'number') {
        return fragmentList;
      }

      return fragmentList.concat(fragmentChild);
    }, []));
  }

  return list.concat(child);
}

const METATYPES = ['name', 'httpEquiv', 'charSet', 'itemProp'];
/*
 returns a function for filtering head child elements
 which shouldn't be duplicated, like <title/>
 Also adds support for deduplicated `key` properties
*/

function unique() {
  const keys = new _Set();
  const tags = new _Set();
  const metaTypes = new _Set();
  const metaCategories = {};
  return h => {
    if (h.key && typeof h.key !== 'number' && h.key.indexOf('.$') === 0) {
      if (keys.has(h.key)) return false;
      keys.add(h.key);
      return true;
    } // If custom meta tag has been added the key will be prepended with `.$`, we can
    // check for this and dedupe in favor of the custom one, so the default
    // is not rendered as well


    if (keys.has(`.$${h.key}`)) return false;

    switch (h.type) {
      case 'title':
      case 'base':
        if (tags.has(h.type)) return false;
        tags.add(h.type);
        break;

      case 'meta':
        for (let i = 0, len = METATYPES.length; i < len; i++) {
          const metatype = METATYPES[i];
          if (!h.props.hasOwnProperty(metatype)) continue;

          if (metatype === 'charSet') {
            if (metaTypes.has(metatype)) return false;
            metaTypes.add(metatype);
          } else {
            const category = h.props[metatype];
            const categories = metaCategories[metatype] || new _Set();
            if (categories.has(category)) return false;
            categories.add(category);
            metaCategories[metatype] = categories;
          }
        }

        break;
    }

    return true;
  };
}
/**
 *
 * @param headElement List of multiple <Head> instances
 */


function reduceComponents(headElements, props) {
  return headElements.reduce((list, headElement) => {
    const headElementChildren = react_1.default.Children.toArray(headElement.props.children);
    return list.concat(headElementChildren);
  }, []).reduce(onlyReactElement, []).reverse().concat(defaultHead(props.inAmpMode)).filter(unique()).reverse().map((c, i) => {
    const key = c.key || i;
    return react_1.default.cloneElement(c, {
      key
    });
  });
}

const Effect = side_effect_1.default();
/**
 * This component injects elements to `<head>` of your page.
 * To avoid duplicated `tags` in `<head>` you can use the `key` property, which will make sure every tag is only rendered once.
 */

function Head({
  children
}) {
  return react_1.default.createElement(amp_context_1.AmpStateContext.Consumer, null, ampState => react_1.default.createElement(head_manager_context_1.HeadManagerContext.Consumer, null, updateHead => react_1.default.createElement(Effect, {
    reduceComponentsToState: reduceComponents,
    handleStateChange: updateHead,
    inAmpMode: amp_1.isInAmpMode(ampState)
  }, children)));
}

Head.rewind = Effect.rewind;
exports.default = Head;

/***/ }),

/***/ "AZIi":
/***/ (function(module, exports) {



/***/ }),

/***/ "AuoD":
/***/ (function(module, exports) {

module.exports = require("react-leaflet");

/***/ }),

/***/ "B1jN":
/***/ (function(module, exports) {

module.exports = require("lodash/padStart");

/***/ }),

/***/ "Cg2A":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("y6vh");

/***/ }),

/***/ "FYa8":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__("cDcd"));

exports.HeadManagerContext = React.createContext(null);

/***/ }),

/***/ "FjY3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pointInfoTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_POINT_INFO_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pointInfo';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_POINT_INFO"] = 0] = "GET_POINT_INFO";
  ActionTypes[ActionTypes["GET_POINT_INFO_SUCCESS"] = 1] = "GET_POINT_INFO_SUCCESS";
  ActionTypes[ActionTypes["GET_POINT_INFO_FAIL"] = 2] = "GET_POINT_INFO_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pointInfoTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const GET_POINT_INFO_ACTION_TYPES = pointInfoTypesGenerator.getActionTypes();

/***/ }),

/***/ "IJ9z":
/***/ (function(module, exports) {



/***/ }),

/***/ "Jo+v":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Z6Kq");

/***/ }),

/***/ "Loka":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/values");

/***/ }),

/***/ "NTJK":
/***/ (function(module, exports) {



/***/ }),

/***/ "NUC6":
/***/ (function(module, exports) {

module.exports = require("lodash/debounce");

/***/ }),

/***/ "NnTR":
/***/ (function(module, exports) {



/***/ }),

/***/ "No/t":
/***/ (function(module, exports) {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ "NzGr":
/***/ (function(module, exports) {



/***/ }),

/***/ "QRnw":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/date/now.js
var now = __webpack_require__("Cg2A");
var now_default = /*#__PURE__*/__webpack_require__.n(now);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js + 1 modules
var objectWithoutProperties = __webpack_require__("qNsG");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: ./src/components/application/search/form/form.styles.scss
var form_styles = __webpack_require__("AZIi");

// EXTERNAL MODULE: ./src/components/application/search/form/form.styles.mobile.scss
var form_styles_mobile = __webpack_require__("5RaE");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__("uhWA");

// EXTERNAL MODULE: ./src/components/application/search/inputs/place/styles.scss
var styles = __webpack_require__("TwRD");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: ./src/store/modules/autocomplete/IAutocompleteActionTypes.ts
var IAutocompleteActionTypes = __webpack_require__("qdML");

// CONCATENATED MODULE: ./src/store/modules/autocomplete/AutocompleteActions.ts

const getSuggetsions = data => ({
  type: IAutocompleteActionTypes["a" /* AUTOCOMPLETE_ACTION_TYPES */].AUTOCOMPLETE,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/autocomplete',
      data
    }
  }
});
// CONCATENATED MODULE: ./src/components/application/search/inputs/place/InputPlace.tsx
var __jsx = external_react_["createElement"];







class InputPlace_InputPlace extends external_react_["PureComponent"] {
  constructor(...args) {
    super(...args);
    this.state = {
      isShowTypeahead: false
    };
    this.input = void 0;

    this.bindInput = ref => this.input = ref;

    this.onInputFocus = () => this.setState({
      isShowTypeahead: true
    });

    this.onInputFocusOut = () => {
      const {
        results,
        onSuggestionClick: setPoint
      } = this.props;

      if (results && results.length) {
        setPoint(results[0]);
      }

      setTimeout(() => {
        this.setState({
          isShowTypeahead: false
        });
      }, 250);
    };

    this.findAutocomplete = Object(external_lodash_["debounce"])(value => {
      const {
        getSuggestions
      } = this.props;
      getSuggestions({
        term: value,
        locale: 'ru',
        types: ['city', 'country', 'airport']
      });
    }, 300);

    this.updateAutocomplete = e => {
      const value = e.target.value;

      if (value.length >= 3) {
        this.findAutocomplete(value);
      }

      this.props.onChange(value);
    };

    this.onTypeaheadPress = point => {
      const {
        onSuggestionClick: setPoint
      } = this.props;
      setPoint(point);
    };

    this.onClearClick = () => {
      this.props.onClearClick && this.props.onClearClick();
    };
  }

  render() {
    const {
      children,
      label,
      id,
      results,
      className,
      currentValue,
      invalid,
      showClear,
      historyItems,
      constSuggestions,
      fakeSuggestions
    } = this.props;
    return __jsx("div", {
      className: `place-input ${className}`
    }, __jsx("input", {
      ref: this.bindInput,
      type: "",
      name: "",
      className: `${invalid && 'invalid-input'}`,
      placeholder: label,
      id: id,
      autoComplete: 'off',
      value: currentValue,
      onFocus: this.onInputFocus,
      onBlur: this.onInputFocusOut,
      onChange: this.updateAutocomplete
    }), showClear && __jsx("div", {
      className: 'clear-button',
      onClick: this.onClearClick
    }, "\xD7"), children, this.state.isShowTypeahead && __jsx("ul", {
      className: "typeahead"
    }, constSuggestions && constSuggestions.length && constSuggestions.map(suggestion => __jsx("li", {
      key: suggestion.place.place.name,
      onClick: () => this.onTypeaheadPress(suggestion.place)
    }, __jsx(react_fontawesome_["FontAwesomeIcon"], {
      className: "circle-icon",
      icon: suggestion.icon
    }), __jsx("span", {
      className: 'main'
    }, " ", suggestion.place.place.name, " "), suggestion.place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, suggestion.place.country.name), suggestion.place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, suggestion.place.place.code))), results.map(place => __jsx("li", {
      key: place.place.name,
      onClick: () => this.onTypeaheadPress(place)
    }, place.type === 'airport' && __jsx("img", {
      alt: place.place.name,
      src: '/images/circle-grey.svg',
      className: "circle-icon"
    }), __jsx("span", {
      className: 'main'
    }, " ", place.place.name, " "), place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, place.country.name), place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, place.place.code))), fakeSuggestions && fakeSuggestions.length && fakeSuggestions.map(suggestion => __jsx("li", {
      key: suggestion.title,
      onClick: suggestion.onClick
    }, __jsx(react_fontawesome_["FontAwesomeIcon"], {
      className: "circle-icon",
      icon: suggestion.icon
    }), __jsx("span", {
      className: 'main'
    }, " ", suggestion.title, " "))), __jsx("li", {
      className: 'divider'
    }, "\u0438\u0441\u0442\u043E\u0440\u0438\u044F \u0432\u0430\u0448\u0435\u0433\u043E \u043F\u043E\u0438\u0441\u043A\u0430"), historyItems.map(place => __jsx("li", {
      key: place.place.name,
      onClick: () => this.onTypeaheadPress(place)
    }, place.type === 'airport' && __jsx("img", {
      alt: place.place.name,
      src: '/images/circle-grey.svg',
      className: "circle-icon"
    }), __jsx("span", {
      className: 'main'
    }, " ", place.place.name, " "), place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, place.country.name), place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, place.place.code)))));
  }

}

/* harmony default export */ var place_InputPlace = (Object(external_react_redux_["connect"])(state => ({
  results: state.autocomplete.results
}), dispatch => ({
  getSuggestions: data => dispatch(getSuggetsions(data))
}))(InputPlace_InputPlace));
// EXTERNAL MODULE: ./src/components/application/search/inputs/passengers/styles.scss
var passengers_styles = __webpack_require__("mq8p");

// CONCATENATED MODULE: ./src/store/utils/pluralize.ts
const pluralizePassengers = count => {
  const meaningNumber = count % 10;
  const endian = meaningNumber === 1 ? '' : 1 < meaningNumber && meaningNumber < 5 ? 'а' : meaningNumber >= 5 || meaningNumber === 0 ? 'ов' : 'ов';
  return 10 < count && count < 21 ? 'пассажиров' : `пассажир${endian}`;
};
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__("No/t");

// CONCATENATED MODULE: ./src/components/application/search/inputs/passengers/passengerType.tsx
var passengerType_jsx = external_react_["createElement"];




const PassengerType = ({
  title,
  tip,
  value,
  onChange,
  disabled
}) => {
  const decreasePassengersCount = e => {
    e.preventDefault();
    e.stopPropagation();
    onChange(Math.max(value - 1, 0));
  };

  const increasePassengersCount = e => {
    e.preventDefault();
    e.stopPropagation();

    if (!disabled) {
      onChange(value + 1);
    }
  };

  const preventBlur = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  return passengerType_jsx("div", {
    className: 'passenger-type'
  }, passengerType_jsx("div", {
    className: 'description'
  }, passengerType_jsx("div", {
    className: 'title'
  }, title), passengerType_jsx("div", {
    className: 'tip'
  }, tip)), passengerType_jsx("div", {
    className: 'count'
  }, passengerType_jsx("div", {
    className: `decrease-button ${value === 0 && 'disabled'}`,
    onClick: decreasePassengersCount,
    onMouseDown: preventBlur
  }, passengerType_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faMinus"]
  })), passengerType_jsx("div", {
    className: 'value'
  }, value), passengerType_jsx("div", {
    className: `increase-button ${disabled && 'disabled'}`,
    onClick: increasePassengersCount,
    onMouseDown: preventBlur
  }, passengerType_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faPlus"]
  }))));
};

/* harmony default export */ var passengerType = (external_react_["memo"](PassengerType));
// CONCATENATED MODULE: ./src/components/shared/utils/useOutsideListener.ts


function useOutsideListener(ref, onClickOutside) {
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      onClickOutside();
    }
  }

  Object(external_react_["useEffect"])(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });
}

/* harmony default export */ var utils_useOutsideListener = (useOutsideListener);
// CONCATENATED MODULE: ./src/components/shared/utils/index.ts

// CONCATENATED MODULE: ./src/components/application/search/inputs/passengers/index.tsx







var passengers_jsx = external_react_["createElement"];

function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }







const InputPassengers = ({
  onMetaChange,
  placeholder,
  id,
  className = '',
  value
}) => {
  const [isShowTypeahead, toggleTypeahead] = external_react_["useState"](false);
  const inputRef = external_react_["useRef"](null);
  const wrapperRef = external_react_["useRef"](null);

  const onInputFocus = () => toggleTypeahead(true);

  const onInputFocusOut = () => toggleTypeahead(false);

  const setAdults = adults => {
    onMetaChange(_objectSpread({}, value, {
      adults
    }));
  };

  const setChildren = children => {
    onMetaChange(_objectSpread({}, value, {
      children
    }));
  };

  const setInfants = infants => {
    onMetaChange(_objectSpread({}, value, {
      infants
    }));
  };

  utils_useOutsideListener(wrapperRef, onInputFocusOut);
  const passengersSum = value.adults + value.children + value.infants;
  return passengers_jsx("div", {
    className: `passengers-input ${className}`,
    ref: wrapperRef
  }, passengers_jsx("input", {
    ref: inputRef,
    type: "",
    name: "",
    placeholder: placeholder,
    id: id,
    value: `${passengersSum}  ${pluralizePassengers(passengersSum)}`,
    autoComplete: 'off',
    onFocus: onInputFocus,
    readOnly: true
  }), isShowTypeahead && passengers_jsx("div", {
    className: 'passengers-typeahead'
  }, passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Врослые',
    tip: 'От 12 лет',
    value: value.adults,
    onChange: setAdults
  }), passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Дети',
    tip: 'От 2 до 12 лет',
    value: value.children,
    onChange: setChildren
  }), passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Младенцы',
    tip: 'До 2 лет',
    value: value.infants,
    onChange: setInfants
  }), passengers_jsx("div", {
    className: "close-container"
  }, passengers_jsx("img", {
    alt: 'close',
    src: '/images/X.svg',
    onClick: () => toggleTypeahead(false),
    className: "close-btn"
  }))));
};

/* harmony default export */ var passengers = (external_react_["memo"](InputPassengers));
// EXTERNAL MODULE: ./src/store/modules/searchHistory/ISearchHistoryActionTypes.ts
var ISearchHistoryActionTypes = __webpack_require__("oden");

// CONCATENATED MODULE: ./src/store/modules/searchHistory/SearchHistoryActions.ts

const pushOrigin = data => ({
  type: ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_ORIGIN,
  payload: data
});
const pushDestination = data => ({
  type: ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_DESTINATION,
  payload: data
});
// EXTERNAL MODULE: external "lodash/debounce"
var debounce_ = __webpack_require__("NUC6");
var debounce_default = /*#__PURE__*/__webpack_require__.n(debounce_);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js
var esm_extends = __webpack_require__("kOwS");

// EXTERNAL MODULE: external "react-day-picker/DayPickerInput"
var DayPickerInput_ = __webpack_require__("dUzv");
var DayPickerInput_default = /*#__PURE__*/__webpack_require__.n(DayPickerInput_);

// EXTERNAL MODULE: external "react-day-picker/moment"
var moment_ = __webpack_require__("my5g");
var moment_default = /*#__PURE__*/__webpack_require__.n(moment_);

// EXTERNAL MODULE: external "moment/locale/ru"
var ru_ = __webpack_require__("gvj5");

// EXTERNAL MODULE: ./src/components/shared/DatePicker/input/styles.scss
var input_styles = __webpack_require__("NTJK");

// EXTERNAL MODULE: ./src/components/shared/DatePicker/input/styles.mobile.scss
var styles_mobile = __webpack_require__("jQ5Y");

// CONCATENATED MODULE: ./src/components/shared/DatePicker/input/index.tsx


var input_jsx = external_react_["createElement"];




const DateCustomInput = (_ref) => {
  let {
    buttonText,
    className,
    isFocus,
    onButtonClick,
    placeholderFocusText,
    placeholder,
    value
  } = _ref,
      inputProps = Object(objectWithoutProperties["a" /* default */])(_ref, ["buttonText", "className", "isFocus", "onButtonClick", "placeholderFocusText", "placeholder", "value"]);

  const onButtonMouseDown = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  return input_jsx("div", {
    className: `custom-date-input ${isFocus && 'focus'}`
  }, input_jsx("input", Object(esm_extends["a" /* default */])({
    className: `fake-input ${className ? className : ''}`,
    value: value,
    readOnly: true,
    placeholder: placeholder,
    style: {
      display: isFocus ? 'hidden' : 'block'
    }
  }, inputProps)), isFocus && input_jsx("div", {
    className: 'input-overlay'
  }, input_jsx("div", {
    className: 'input-overlay__title'
  }, placeholderFocusText), input_jsx("div", {
    className: 'date-action-button',
    onClick: onButtonClick,
    onMouseDown: onButtonMouseDown
  }, buttonText)));
};

/* harmony default export */ var input = (DateCustomInput);
// EXTERNAL MODULE: ./src/components/shared/DatePicker/header/style.scss
var style = __webpack_require__("0Zyy");

// CONCATENATED MODULE: ./src/components/shared/DatePicker/header/index.tsx

var header_jsx = external_react_["createElement"];
 // import { format, Locale } from "date-fns";



const CustomCalendarHeader = (_ref) => {
  let {
    date,
    localeUtils,
    locale
  } = _ref,
      props = Object(objectWithoutProperties["a" /* default */])(_ref, ["date", "localeUtils", "locale"]);

  // const captionTitle = format(month, "LLLL Y", { locale: dayPickerProps.locale as any });
  const months = localeUtils.getMonths(locale);
  const currentYear = date.getFullYear();
  const currentMouth = months[date.getMonth()];
  return header_jsx("div", {
    className: "custom-caption"
  }, header_jsx("div", {
    className: "custom-caption__title"
  }, `${currentMouth} ${currentYear}`));
};

/* harmony default export */ var header = (external_react_["memo"](CustomCalendarHeader));
// EXTERNAL MODULE: ./src/components/shared/DatePicker/style.scss
var DatePicker_style = __webpack_require__("t9pA");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./src/components/shared/DatePicker/index.tsx








var DatePicker_jsx = external_react_["createElement"];

function DatePicker_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function DatePicker_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { DatePicker_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { DatePicker_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


 // @ts-ignore









const CustomDatePicker = ({
  className,
  inputClassName,
  placeholderText,
  placeholderFocusText,
  onChange,
  buttonText,
  dayPickerProps,
  onButtonClick,
  selected
}) => {
  const [isFocus, toggleFocus] = external_react_["useState"](false);
  const wrapperRef = external_react_["useRef"](null);
  const calendarRef = external_react_["useRef"](null);

  const onDayPickerHide = () => {
    toggleFocus(false);
  };

  const onDayPickerShow = () => {
    toggleFocus(true);
  };

  const onOutsideClick = () => {
    onDayPickerHide();
    calendarRef.current.hideDayPicker();
  };

  utils_useOutsideListener(wrapperRef, onOutsideClick);

  const _minDate = dayPickerProps.initialMonth || new Date();

  const _onButtonClick = () => {
    onButtonClick();
    onOutsideClick();
  };

  const value = selected ? external_moment_default()(selected).format('LL') : '';
  const inputProps = {
    buttonText: buttonText,
    className: inputClassName,
    isFocus: isFocus,
    onButtonClick: _onButtonClick,
    placeholderFocusText: placeholderFocusText,
    placeholder: placeholderText,
    value
  };
  return DatePicker_jsx("div", {
    className: `custom-date-picker ${className}`,
    ref: wrapperRef
  }, DatePicker_jsx(DayPickerInput_default.a, {
    ref: calendarRef,
    dayPickerProps: DatePicker_objectSpread({
      initialMonth: _minDate,
      localeUtils: moment_default.a,
      locale: 'ru',
      captionElement: DatePicker_jsx(header, {
        dayPickerProps: dayPickerProps
      })
    }, dayPickerProps),
    onDayChange: onChange,
    onDayPickerHide: onDayPickerHide,
    onDayPickerShow: onDayPickerShow,
    format: 'LL',
    formatDate: moment_["formatDate"],
    parseDate: moment_["parseDate"],
    inputProps: inputProps,
    component: props => DatePicker_jsx(input, Object(esm_extends["a" /* default */])({}, props, {
      value: value
    }))
  }));
};

/* harmony default export */ var DatePicker = (external_react_["memo"](CustomDatePicker));
// CONCATENATED MODULE: ./src/components/application/search/form/form.tsx









var form_jsx = external_react_["createElement"];

function form_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function form_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { form_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { form_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }












var DatePickerInputs;

(function (DatePickerInputs) {
  DatePickerInputs["From"] = "from";
  DatePickerInputs["To"] = "to";
})(DatePickerInputs || (DatePickerInputs = {}));

class form_SearchForm extends external_react_["Component"] {
  constructor(props) {
    super(props);
    this.changeLayout = debounce_default()(isMobileLayout => this.setState({
      isMobileLayout
    }), 500);

    this.onDocumentResize = () => this.changeLayout(window.innerWidth <= 425);

    this.onSearchPress = e => {
      e.preventDefault();
      const {
        state
      } = this;
      const validation = {
        validationDepartureDateSelected: !!state.departureDate,
        validationOriginSelected: !!state.origin,
        validationSameCity: !!state.destination && !!state.origin && state.origin.city.code === state.destination.city.code
      };
      this.setState(form_objectSpread({}, validation), () => {
        const _this$state = this.state,
              {
          validationDepartureDateSelected,
          validationOriginSelected,
          validationSameCity
        } = _this$state,
              params = Object(objectWithoutProperties["a" /* default */])(_this$state, ["validationDepartureDateSelected", "validationOriginSelected", "validationSameCity"]);

        const isValid = validationDepartureDateSelected && !validationSameCity && validationOriginSelected;

        if (isValid) {
          this.props.pushOriginHistory(state.origin);

          if (state.destination && state.destination.place.code) {
            this.props.pushDestinationHistory(state.destination);
          }

          const searchParams = {
            adults: params.adults,
            children: params.children,
            departureDate: params.departureDate,
            destination: params.destination,
            infants: params.infants,
            origin: params.origin,
            returnDate: params.returnDate
          };
          setTimeout(() => {
            this.props.onSearchPress(searchParams);
          }, 700);
        }
      });
    };

    this.resetValidationState = () => {
      this.setState({
        validationDepartureDateSelected: true,
        validationOriginSelected: true
      });
    };

    this.onOriginValueChange = origin => this.setState({
      origin,
      originValue: origin.place.name
    });

    this.onDestinationValueChange = destination => this.setState({
      destination,
      destinationValue: destination.place.name
    });

    this.setDepartureDate = date => {
      this.setState({
        departureDate: date
      });
      this.resetValidationState();
    };

    this.setReturnDate = date => {
      this.setState({
        returnDate: date
      });
      this.resetValidationState();
    };

    this.setPassengersCount = ({
      adults,
      children,
      infants
    }) => {
      this.setState({
        adults,
        children,
        infants
      });
    };

    this.onReversePlacesClick = () => {
      const {
        origin,
        destination,
        originValue,
        destinationValue
      } = this.state;
      this.setState({
        origin: destination,
        destination: origin,
        originValue: destinationValue,
        destinationValue: originValue
      });
      this.resetValidationState();
    };

    this.onRemoveDestination = () => {
      this.setState({
        destination: undefined,
        destinationValue: '',
        validationSameCity: false
      });
    };

    this.onRemoveReturnDate = () => {
      this.setState({
        returnDate: undefined
      });
    };

    this.setTodayDepature = () => {
      this.setState({
        departureDate: new Date()
      });
    };

    this.onOriginInputValueChange = originValue => this.setState({
      originValue
    });

    this.onDestinationInputValueChange = destinationValue => this.setState({
      destinationValue
    });

    const _state = {
      validationDepartureDateSelected: true,
      validationOriginSelected: true,
      validationSameCity: false,
      adults: 1,
      children: 0,
      infants: 0,
      originValue: '',
      destinationValue: '',
      departureDate: new Date()
    };
    this.state = form_objectSpread({}, _state);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onDocumentResize);
    const {
      props
    } = this;
    this.setState({
      isMobileLayout: window.innerWidth <= 425
    }, () => {
      this.propsToState(props);
    });
  }

  componentDidUpdate(prevProps) {
    if (!Object(external_lodash_["isEqual"])(prevProps, this.props)) {
      this.propsToState(this.props);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onDocumentResize);
  }

  propsToState(props) {
    let nextState = {};

    if (props.defaultValues && keys_default()(props.defaultValues).length) {
      nextState = form_objectSpread({}, nextState, {}, props.defaultValues);

      if (props.defaultValues.origin && props.defaultValues.origin.place) {
        nextState.originValue = props.defaultValues.origin.place.name;
      }

      if (props.defaultValues.destination && props.defaultValues.destination.place) {
        nextState.destinationValue = props.defaultValues.destination.place.name;
      }
    }

    this.setState(nextState);
  }

  render() {
    const {
      state
    } = this;
    const {
      className,
      onHideClick,
      history,
      myLocation
    } = this.props;
    const {
      validationOriginSelected,
      validationDepartureDateSelected,
      adults,
      children,
      origin,
      destination,
      infants,
      validationSameCity,
      originValue,
      destinationValue
    } = this.state;
    const currentDay = new Date();
    let departureDate;
    let returnDate;

    if (state.departureDate) {
      departureDate = typeof state.departureDate === 'string' ? new Date(state.departureDate) : state.departureDate;

      if (departureDate.getTime() < now_default()()) {
        departureDate = new Date();
      }
    }

    if (state.returnDate) {
      returnDate = typeof state.returnDate === 'string' ? new Date(state.returnDate) : state.returnDate;

      if (departureDate && returnDate && returnDate.getTime() < departureDate.getTime()) {
        returnDate = new Date(departureDate.getTime() + 1000 * 60 * 60 * 24);
      }
    }

    const modifiers = {
      start: departureDate,
      end: returnDate
    };
    return form_jsx("form", {
      className: `search-form ${className ? className : ''}`
    }, form_jsx(place_InputPlace, {
      id: "from",
      className: "from",
      historyItems: history.origins_history,
      invalid: !validationOriginSelected,
      label: 'Из России',
      onSuggestionClick: this.onOriginValueChange,
      currentValue: originValue,
      pointValue: origin,
      onChange: this.onOriginInputValueChange,
      constSuggestions: [{
        icon: free_solid_svg_icons_["faLocationArrow"],
        place: myLocation
      }]
    }, form_jsx("div", {
      className: "icon",
      onClick: this.onReversePlacesClick
    }, form_jsx("svg", {
      width: "46",
      height: "46",
      viewBox: "0 0 46 46",
      xmlns: "http://www.w3.org/2000/svg",
      className: "icon"
    }, form_jsx("path", {
      d: "M29.3536 18.3536C29.5488 18.1583 29.5488 17.8417 29.3536 17.6464L26.1716 14.4645C25.9763 14.2692 25.6597 14.2692 25.4645 14.4645C25.2692 14.6597 25.2692 14.9763 25.4645 15.1716L28.2929 18L25.4645 20.8284C25.2692 21.0237 25.2692 21.3403 25.4645 21.5355C25.6597 21.7308 25.9763 21.7308 26.1716 21.5355L29.3536 18.3536ZM17 18.5L29 18.5L29 17.5L17 17.5L17 18.5Z"
    }), form_jsx("path", {
      d: "M16.6464 27.6464C16.4512 27.8417 16.4512 28.1583 16.6464 28.3536L19.8284 31.5355C20.0237 31.7308 20.3403 31.7308 20.5355 31.5355C20.7308 31.3403 20.7308 31.0237 20.5355 30.8284L17.7071 28L20.5355 25.1716C20.7308 24.9763 20.7308 24.6597 20.5355 24.4645C20.3403 24.2692 20.0237 24.2692 19.8284 24.4645L16.6464 27.6464ZM29 27.5H17V28.5H29V27.5Z"
    })))), form_jsx(place_InputPlace, {
      id: "to",
      className: "to",
      historyItems: history.destinations_history,
      invalid: validationSameCity,
      label: 'Куда угодно',
      onClearClick: this.onRemoveDestination,
      onSuggestionClick: this.onDestinationValueChange,
      currentValue: destinationValue,
      pointValue: destination,
      onChange: this.onDestinationInputValueChange,
      fakeSuggestions: [{
        icon: free_solid_svg_icons_["faGlobeEurope"],
        title: 'Куда угодно',
        onClick: this.onRemoveDestination
      }]
    }), !(this.state.activeDateInput === 'to' && this.state.isMobileLayout) && form_jsx(DatePicker, {
      buttonText: 'Прямо сейчас',
      className: `input_range input_range__departure`,
      inputClassName: `${!validationDepartureDateSelected && 'invalid-input'}`,
      onChange: this.setDepartureDate,
      placeholderFocusText: 'Когда хотите уехать?',
      placeholderText: 'Когда туда',
      selected: departureDate,
      onButtonClick: this.setTodayDepature,
      dayPickerProps: {
        selectedDays: [departureDate, {
          from: departureDate,
          to: returnDate
        }],
        disabledDays: {
          after: returnDate,
          before: currentDay
        },
        toMonth: returnDate,
        modifiers
      }
    }), form_jsx(DatePicker, {
      buttonText: 'Нет, вы что!',
      className: 'input_range input_range__return',
      onChange: this.setReturnDate,
      placeholderFocusText: 'Хотите обратно?',
      placeholderText: 'Обратно',
      selected: returnDate,
      onButtonClick: this.onRemoveReturnDate,
      dayPickerProps: {
        selectedDays: [departureDate, {
          from: departureDate,
          to: returnDate
        }],
        disabledDays: {
          before: departureDate
        },
        modifiers,
        month: departureDate,
        fromMonth: departureDate
      }
    }), form_jsx(passengers, {
      className: "passengers__select",
      id: "ticket-type",
      placeholder: "1 \u0432\u0437\u0440\u043E\u0441\u043B\u044B\u0439",
      value: {
        adults,
        children: children,
        infants
      },
      onMetaChange: this.setPassengersCount
    }), form_jsx("input", {
      type: "submit",
      name: "",
      value: "\u041D\u0430\u0447\u0430\u0442\u044C \u043F\u043E\u0438\u0441\u043A",
      className: "search-btn",
      onClick: this.onSearchPress
    }), onHideClick && form_jsx("img", {
      alt: 'close',
      src: '/images/X.svg',
      onClick: onHideClick,
      className: "search-form close-btn"
    }));
  }

}

/* harmony default export */ var form_form = __webpack_exports__["a"] = (Object(external_react_redux_["connect"])(state => ({
  history: state.searchHistory,
  myLocation: state.geolocation.myLocation
}), dispatch => ({
  pushOriginHistory: data => dispatch(pushOrigin(data)),
  pushDestinationHistory: data => dispatch(pushDestination(data))
}))(form_SearchForm));

/***/ }),

/***/ "QTVn":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "RnhE":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");
var router_default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: ./node_modules/react-day-picker/lib/style.css
var style = __webpack_require__("UNSI");

// EXTERNAL MODULE: ./src/styles/App.scss
var App = __webpack_require__("16zm");

// EXTERNAL MODULE: ./src/styles/App.mobile.scss
var App_mobile = __webpack_require__("2/0G");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.tsx + 1 modules
var header_filters = __webpack_require__("UIQ4");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: ./src/components/application/results/ResultOfferItem/style.scss
var ResultOfferItem_style = __webpack_require__("NnTR");

// CONCATENATED MODULE: ./src/components/application/results/ResultOfferItem/ResultOfferItem.tsx
var __jsx = external_react_["createElement"];



class ResultOfferItem_ResultOfferItem extends external_react_["PureComponent"] {
  constructor(props) {
    super(props);
    this.detailedList = void 0;

    this.bindDetailsList = ref => this.detailedList = ref;

    this.onMorePress = () => {
      this.setState({
        isNeedShowMoreButton: false,
        isNeedShowRenderAll: true
      });
    };

    this.toggleDetails = () => {
      this.detailedList.classList.toggle('hidden');
    };

    this.onPointClick = point => {
      this.props.onClick({
        place: point.city,
        city: point.city,
        country: this.props.country,
        type: 'city'
      });
    };

    this.state = {
      isNeedShowMoreButton: this.props.destinations.length > 6,
      isNeedShowRenderAll: !(this.props.destinations.length > 6)
    };
  }

  render() {
    const {
      destinations,
      country,
      minimalPrice
    } = this.props;
    const {
      state
    } = this;
    const sortedDestinations = destinations.sort((a, b) => a.price - b.price);
    const renderedDestinations = state.isNeedShowRenderAll ? sortedDestinations : sortedDestinations.slice(0, 6);
    return __jsx("div", {
      className: "offer"
    }, __jsx("div", {
      className: "offer-header",
      onClick: this.toggleDetails
    }, country.code === 'KX' ? __jsx("div", {
      className: 'country-flag',
      style: {
        backgroundSize: 'contain',
        backgroundImage: `url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8AgMAAABHkjHhAAAACVBMVEX///8Ab7DaJR1zqguDAAAAGElEQVQoz2MIRQWDjj8KRsFAglWoYLDxAWR2lWvT0mYdAAAAAElFTkSuQmCC)`
      }
    }) : __jsx("div", {
      className: 'country-flag',
      style: {
        backgroundImage: `url('https://www.countryflags.io/${country.code}/flat/64.png')`
      }
    }), __jsx("p", null, __jsx("span", {
      className: "offer-country"
    }, country.name), __jsx("br", null), __jsx("span", {
      className: "offer-directions-count"
    }, __jsx("span", null, destinations.length), " \u043D\u0430\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0439")), __jsx("span", {
      className: "offer-min-price"
    }, "\u043E\u0442 ", minimalPrice, " \u20BD")), __jsx("div", {
      ref: this.bindDetailsList,
      className: "offers hidden"
    }, renderedDestinations.map(destination => __jsx("p", {
      key: destination.city.code,
      className: "offer-detailed__item",
      onClick: () => this.onPointClick(destination)
    }, __jsx("span", {
      className: "offer-city"
    }, destination.city.name), __jsx("span", {
      className: "offer-price"
    }, destination.price, " \u20BD"))), state.isNeedShowMoreButton && !state.isNeedShowRenderAll && destinations.length > 6 && __jsx("p", {
      className: "show-all-offers",
      onClick: this.onMorePress
    }, "\u0415\u0449\u0435 +", __jsx("span", {
      className: "count_offers"
    }, destinations.length - 6), "\xA0\u043D\u0430\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u0439")));
  }

}

/* harmony default export */ var results_ResultOfferItem_ResultOfferItem = (ResultOfferItem_ResultOfferItem);
// EXTERNAL MODULE: external "next/dynamic"
var dynamic_ = __webpack_require__("/T1H");
var dynamic_default = /*#__PURE__*/__webpack_require__.n(dynamic_);

// CONCATENATED MODULE: ./src/components/application/results/CustomMap/index.tsx
var CustomMap_jsx = external_react_["createElement"];


const CustomMap = dynamic_default()(() => __webpack_require__.e(/* import() */ 8).then(__webpack_require__.bind(null, "CNL9")), {
  ssr: false,
  loading: () => CustomMap_jsx("div", {
    className: "custom-map"
  }),
  loadableGenerated: {
    webpack: () => [/*require.resolve*/("CNL9")],
    modules: ['./CustomMap']
  }
});
/* harmony default export */ var results_CustomMap = (CustomMap);
// EXTERNAL MODULE: ./src/utils/index.ts
var utils = __webpack_require__("0lfv");

// EXTERNAL MODULE: ./src/store/utils/prepareDate.ts
var prepareDate = __webpack_require__("78gN");

// EXTERNAL MODULE: ./src/store/modules/pricesMap/IPricesMapActionTypes.ts
var IPricesMapActionTypes = __webpack_require__("1GcG");

// CONCATENATED MODULE: ./src/store/modules/pricesMap/PricesMapActions.ts

const getPricesMap = data => ({
  type: IPricesMapActionTypes["a" /* PRICES_MAP_ACTION_TYPES */].GET_PRICES_MAP,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/pricesMap',
      data
    }
  }
});
// EXTERNAL MODULE: ./node_modules/next/dist/next-server/lib/head.js
var head = __webpack_require__("8Kt/");
var head_default = /*#__PURE__*/__webpack_require__.n(head);

// CONCATENATED MODULE: ./src/containers/ResultsContainer/index.tsx







var ResultsContainer_jsx = external_react_default.a.createElement;

function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }












class ResultsContainer_ResultsContainer extends external_react_default.a.Component {
  constructor(props) {
    super(props);
    this.defaultSearchValues = void 0;

    this.startSearch = params => {
      this.props.getPricesMap(_objectSpread({}, params, {
        locale: 'ru'
      }));
    };

    this.onMarkerClick = point => {
      const {
        router: {
          query
        }
      } = this.props;
      const route = Object(utils["b" /* buildUrlByStringParams */])({
        departureDate: query.dept_date,
        returnDate: query.ret_date ? query.ret_date : undefined,
        destination: point.place.code,
        origin: query.departure_iata,
        passengers: query.passengers
      });
      router_default.a.push(route);
    };

    this.onSearchPress = params => {
      if (params.origin && params.origin.place.code && params.departureDate) {
        if (params.destination) {
          const nextRoute = Object(utils["a" /* buildUrlByParams */])(params);
          return router_default.a.push(nextRoute);
        }

        const nextURL = Object(utils["a" /* buildUrlByParams */])(params);
        router_default.a.replace(nextURL, nextURL, {
          shallow: true
        });
        this.startSearch({
          origin: params.origin.place.code,
          direct: true,
          departDate: Object(prepareDate["a" /* prepareDate */])(params.departureDate),
          returnDate: Object(prepareDate["a" /* prepareDate */])(params.returnDate)
        });
      } else {
        console.warn('Departure IATA not found!', params);
      }
    };

    const {
      router: {
        query: _query
      },
      pointInfo
    } = props;
    const origin = pointInfo && pointInfo.points ? pointInfo.points[_query.departure_iata] : undefined;
    const departureDate = Object(utils["d" /* deserializeURLDate */])(_query.dept_date);
    const returnDate = Object(utils["d" /* deserializeURLDate */])(_query.ret_date);
    const passengers = Object(utils["c" /* deserializePassengers */])(_query.passengers);
    this.defaultSearchValues = _objectSpread({
      origin,
      departureDate,
      returnDate
    }, passengers);
  }

  componentDidMount() {
    const {
      router: {
        query
      }
    } = this.props;
    this.startSearch({
      origin: query.departure_iata,
      direct: true,
      departDate: query.dept_date,
      returnDate: query.ret_date ? query.ret_date : undefined
    });
  }

  render() {
    const {
      originMapPoint,
      destinationCountriesMapPoints,
      isLoading,
      router: {
        query
      },
      pointInfo
    } = this.props;
    const mapPoints = destinationCountriesMapPoints.flatMap(country => country.destinations.slice(0, country.optimalMapAirportsAmount).map(city => ({
      position: city.coordinates,
      name: city.city.name,
      price: city.price,
      iata: city.city.code,
      onClick: this.onMarkerClick
    })));
    const cityFrom = pointInfo.points[query.departure_iata].place.name;
    return ResultsContainer_jsx(external_react_default.a.Fragment, null, ResultsContainer_jsx(head_default.a, null, ResultsContainer_jsx("title", null, "\u0410\u044D\u0440\u043E\u043F\u043E\u0440\u0442\u044B \u0433\u043E\u0440\u043E\u0434\u0430 ", cityFrom, " \u2014 \u041A\u0443\u043F\u0438\u0442\u044C \u0434\u0435\u0448\u0435\u0432\u044B\u0435 \u0431\u0438\u043B\u0435\u0442\u044B \u043D\u0430 \u0441\u0430\u043C\u043E\u043B\u0435\u0442 \u0438\u0437 \u0433\u043E\u0440\u043E\u0434\u0430 ", cityFrom), ResultsContainer_jsx("meta", {
      name: "description",
      content: `Купить дешевые билеты на самолет из ${cityFrom} можно на официальном сайте Lowtrip. Мы являемся сервисом по поиску дешевого транспорта по России и Миру.`
    }), ResultsContainer_jsx("meta", {
      name: "keywords",
      content: `${cityFrom}, аэропорт, аэродром, аеро, самолет, как улететь`
    }), ResultsContainer_jsx("link", {
      rel: "canonical",
      href: `https://lowtrip.ru/avia/search/${query.departure_iata}`
    })), ResultsContainer_jsx(header_filters["a" /* default */], {
      defaultValues: this.defaultSearchValues,
      showLoading: isLoading,
      onSearchPress: this.onSearchPress
    }), ResultsContainer_jsx("div", {
      className: "content-wrapper"
    }, ResultsContainer_jsx("div", {
      className: "all-offers"
    }, destinationCountriesMapPoints.map(country => ResultsContainer_jsx(results_ResultOfferItem_ResultOfferItem, {
      key: country.country.code,
      country: country.country,
      destinations: country.destinations,
      onClick: this.onMarkerClick,
      minimalPrice: country.minimalPrice
    }))), originMapPoint && ResultsContainer_jsx(results_CustomMap, {
      center: originMapPoint.coordinates,
      zoom: 3,
      markers: mapPoints
    })));
  }

}

/* harmony default export */ var containers_ResultsContainer = (Object(external_react_redux_["connect"])(state => ({
  originMapPoint: state.pricesMap.origin,
  destinationCountriesMapPoints: state.pricesMap.destinationCountries,
  isLoading: state.pricesMap.isLoading,
  pointInfo: state.pointInfo
}), dispatch => ({
  getPricesMap: data => dispatch(getPricesMap(data))
}))(Object(router_["withRouter"])(ResultsContainer_ResultsContainer)));
// EXTERNAL MODULE: ./src/store/modules/pointInfo/PointInfoActions.ts
var PointInfoActions = __webpack_require__("z3C8");

// CONCATENATED MODULE: ./src/pages/search/[departure_iata].tsx
var _departure_iata_jsx = external_react_default.a.createElement;








class _departure_iata_Results extends external_react_default.a.Component {
  static async getInitialProps({
    query,
    res,
    store: {
      dispatch
    }
  }) {
    // TODO: Paste passengers check > 0
    if (query && (!query.departure_iata || !query.dept_date)) {
      if (res) {
        res.writeHead(302, {
          Location: '/'
        });
        res.end();
      } else {
        router_default.a.push('/');
      }
    }

    try {
      await dispatch(Object(PointInfoActions["a" /* getPointInfo */])([query.departure_iata, query.arrival_iata]));
    } catch (err) {
      console.error(err);
    }

    return {};
  }

  render() {
    return _departure_iata_jsx(external_react_default.a.Fragment, null, _departure_iata_jsx(containers_ResultsContainer, null));
  }

}

/* harmony default export */ var _departure_iata_ = __webpack_exports__["default"] = (_departure_iata_Results);

/***/ }),

/***/ "S8h5":
/***/ (function(module, exports) {



/***/ }),

/***/ "TUA0":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "TwRD":
/***/ (function(module, exports) {



/***/ }),

/***/ "UIQ4":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: ./src/components/application/header/header.styles.scss
var header_styles = __webpack_require__("IJ9z");

// EXTERNAL MODULE: ./src/components/application/header/header.styles.mobile.scss
var header_styles_mobile = __webpack_require__("5chS");

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.styles.scss
var header_filters_styles = __webpack_require__("NzGr");

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.styles.mobile.scss
var header_filters_styles_mobile = __webpack_require__("S8h5");

// EXTERNAL MODULE: ./src/components/application/search/form/form.tsx + 11 modules
var form_form = __webpack_require__("QRnw");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./src/components/application/header-miniform/header-miniform.tsx
var __jsx = external_react_["createElement"];



const Miniform = ({
  search,
  onHideToggle
}) => {
  const originPlace = search.origin && search.origin.place.name ? search.origin.place.name : '';
  const destinationPlace = search.destination && search.destination.place.name ? `— ${search.destination.place.name}` : '— Куда угодно';
  const passengersCount = search.adults + search.children + search.infants;
  const departureDateLabel = search.departureDate ? external_moment_default()(search.departureDate).format('Do MMMM') : '';
  const returnDateLabel = search.returnDate ? `, ${external_moment_default()(search.returnDate).format('Do MMMM')}` : '';
  const passengersCountLabel = passengersCount ? `, ${passengersCount} пас, эконом` : '';
  return __jsx("div", {
    className: "mini-form"
  }, __jsx("p", null, originPlace, " ", destinationPlace), __jsx("p", null, `${departureDateLabel}${returnDateLabel}${passengersCountLabel}`), __jsx("img", {
    src: '/images/pencil.svg',
    className: "mini-form",
    onClick: onHideToggle,
    alt: 'edit'
  }));
};

/* harmony default export */ var header_miniform = (Miniform);
// CONCATENATED MODULE: ./src/components/application/header-filters/header-filters.tsx
var header_filters_jsx = external_react_["createElement"];










class header_filters_HeaderFilters extends external_react_["Component"] {
  constructor(...args) {
    super(...args);
    this.state = {
      isFilterVisible: false
    };
    this.burgerRef = void 0;
    this.logoRef = void 0;
    this.menuRef = void 0;

    this.onMenuPress = () => {
      this.burgerRef.classList.toggle("burger-X");
      this.logoRef.classList.toggle("black");
      this.menuRef.classList.toggle("active-menu");
    };

    this.bindBurger = ref => this.burgerRef = ref;

    this.bindLogo = ref => this.logoRef = ref;

    this.bindMenu = ref => this.menuRef = ref;

    this.onHideToggle = () => {
      this.setState({
        isFilterVisible: !this.state.isFilterVisible
      });
    };
  }

  componentDidMount() {
    this.setState({
      isFilterVisible: window.innerWidth <= 425 ? true : false
    });
  }

  render() {
    const {
      isFilterVisible
    } = this.state;
    const {
      showLoading,
      onSearchPress,
      defaultValues
    } = this.props;
    return header_filters_jsx("header", null, header_filters_jsx("div", {
      className: `header-content black ${isFilterVisible ? 'items-hidden' : ''}`
    }, header_filters_jsx("span", {
      className: "logo",
      ref: this.bindLogo
    }, "\u041B\u043E\u0443\u0442\u0440\u0438\u043F"), header_filters_jsx("ul", {
      className: "menu",
      ref: this.bindMenu
    }, header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041F\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044F")), header_filters_jsx("li", {
      className: "active-menu-black"
    }, header_filters_jsx("a", {
      href: "/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0410\u0432\u0438\u0430\u0431\u0438\u043B\u0435\u0442\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/train",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0416\u0414 \u0411\u0438\u043B\u0435\u0442\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/bus",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0410\u0432\u0442\u043E\u0431\u0443\u0441\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041F\u043E\u043F\u0443\u0442\u043A\u0430")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/car",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041D\u0430 \u0441\u0432\u043E\u0435\u0439 \u043C\u0430\u0448\u0438\u043D\u0435"))), header_filters_jsx("div", {
      className: "menu-btn",
      onClick: this.onMenuPress
    }, header_filters_jsx("span", {
      ref: this.bindBurger,
      className: "burger grey"
    })), header_filters_jsx("div", {
      className: "clear"
    }, "\u0423\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435 \u043B\u0438\u0448\u043D\u0435\u0435", header_filters_jsx("div", {
      className: `clear-btn ${isFilterVisible ? 'clear-btn-ok' : ''}`,
      onClick: this.onHideToggle
    }, header_filters_jsx("div", {
      className: isFilterVisible ? 'clear-ok' : ''
    }, header_filters_jsx("img", {
      alt: "",
      src: isFilterVisible ? '/images/clear-ok.svg' : '/images/clear-x.svg'
    })))), header_filters_jsx(header_miniform, {
      search: defaultValues,
      onHideToggle: this.onHideToggle
    }), header_filters_jsx(form_form["a" /* default */], {
      className: 'border-btm',
      defaultValues: defaultValues,
      onHideClick: this.onHideToggle,
      onSearchPress: onSearchPress
    }), showLoading && header_filters_jsx("div", {
      className: 'rainbow-loader'
    }), this.props.children));
  }

}

/* harmony default export */ var header_filters = __webpack_exports__["a"] = (header_filters_HeaderFilters);

/***/ }),

/***/ "UNSI":
/***/ (function(module, exports) {



/***/ }),

/***/ "UXZV":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("dGr4");

/***/ }),

/***/ "XoMD":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("QTVn");

/***/ }),

/***/ "Xuae":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Set = __webpack_require__("ttDY");

var _Object$defineProperty = __webpack_require__("hfKm");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __webpack_require__("cDcd");

const isServer = true;

exports.default = () => {
  const mountedInstances = new _Set();
  let state;

  function emitChange(component) {
    state = component.props.reduceComponentsToState([...mountedInstances], component.props);

    if (component.props.handleStateChange) {
      component.props.handleStateChange(state);
    }
  }

  return class extends react_1.Component {
    // Used when server rendering
    static rewind() {
      const recordedState = state;
      state = undefined;
      mountedInstances.clear();
      return recordedState;
    }

    constructor(props) {
      super(props);

      if (isServer) {
        mountedInstances.add(this);
        emitChange(this);
      }
    }

    componentDidMount() {
      mountedInstances.add(this);
      emitChange(this);
    }

    componentDidUpdate() {
      emitChange(this);
    }

    componentWillUnmount() {
      mountedInstances.delete(this);
      emitChange(this);
    }

    render() {
      return null;
    }

  };
};

/***/ }),

/***/ "YLtl":
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "Z6Kq":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dGr4":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "dUzv":
/***/ (function(module, exports) {

module.exports = require("react-day-picker/DayPickerInput");

/***/ }),

/***/ "gvj5":
/***/ (function(module, exports) {

module.exports = require("moment/locale/ru");

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "hfKm":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("TUA0");

/***/ }),

/***/ "hgx0":
/***/ (function(module, exports) {

module.exports = require("leaflet");

/***/ }),

/***/ "jQ5Y":
/***/ (function(module, exports) {



/***/ }),

/***/ "k1wZ":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "kOwS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _extends; });
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("UXZV");
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function _extends() {
  _extends = _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default.a || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ }),

/***/ "ltjX":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "lwAK":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__("cDcd"));

exports.AmpStateContext = React.createContext({});

/***/ }),

/***/ "mKi/":
/***/ (function(module, exports) {

module.exports = require("geojson-world-map");

/***/ }),

/***/ "mq8p":
/***/ (function(module, exports) {



/***/ }),

/***/ "my5g":
/***/ (function(module, exports) {

module.exports = require("react-day-picker/moment");

/***/ }),

/***/ "oden":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export searchHistoryActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SEARCH_HISTORY_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'search_history';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["PUSH_ORIGIN"] = 0] = "PUSH_ORIGIN";
  ActionTypes[ActionTypes["PUSH_DESTINATION"] = 1] = "PUSH_DESTINATION";
})(ActionTypes || (ActionTypes = {}));

const searchHistoryActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const SEARCH_HISTORY_ACTION_TYPES = searchHistoryActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "pLtp":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("qJj/");

/***/ }),

/***/ "qJj/":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "qNsG":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(keys);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutPropertiesLoose.js

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};

  var sourceKeys = keys_default()(source);

  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _objectWithoutProperties; });


function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = _objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (get_own_property_symbols_default.a) {
    var sourceSymbolKeys = get_own_property_symbols_default()(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

/***/ }),

/***/ "qdML":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export autocompleteActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AUTOCOMPLETE_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'autocomplete';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["AUTOCOMPLETE"] = 0] = "AUTOCOMPLETE";
  ActionTypes[ActionTypes["AUTOCOMPLETE_SUCCESS"] = 1] = "AUTOCOMPLETE_SUCCESS";
  ActionTypes[ActionTypes["AUTOCOMPLETE_FAIL"] = 2] = "AUTOCOMPLETE_FAIL";
})(ActionTypes || (ActionTypes = {}));

const autocompleteActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const AUTOCOMPLETE_ACTION_TYPES = autocompleteActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "t9pA":
/***/ (function(module, exports) {



/***/ }),

/***/ "ttDY":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("zQQD");

/***/ }),

/***/ "uhWA":
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ "vYYK":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("hfKm");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "wa65":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/parse-int");

/***/ }),

/***/ "wy2R":
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "y6vh":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/date/now");

/***/ }),

/***/ "z3C8":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPointInfo; });
/* harmony import */ var _IPointInfoActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FjY3");

const getPointInfo = data => ({
  type: _IPointInfoActionTypes__WEBPACK_IMPORTED_MODULE_0__[/* GET_POINT_INFO_ACTION_TYPES */ "a"].GET_POINT_INFO,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/pointInfo',
      data: {
        codes: data
      }
    }
  }
});

/***/ }),

/***/ "zQQD":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/set");

/***/ })

/******/ });