module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+oT+":
/***/ (function(module, exports, __webpack_require__) {

var _Promise = __webpack_require__("eVuF");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    _Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new _Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

module.exports = _asyncToGenerator;

/***/ }),

/***/ "/J4M":
/***/ (function(module, exports) {

module.exports = require("redux-axios-middleware");

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("hUgY");


/***/ }),

/***/ "1GcG":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pricesMapActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PRICES_MAP_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pricesMap';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_PRICES_MAP"] = 0] = "GET_PRICES_MAP";
  ActionTypes[ActionTypes["GET_PRICES_MAP_SUCCESS"] = 1] = "GET_PRICES_MAP_SUCCESS";
  ActionTypes[ActionTypes["GET_PRICES_MAP_FAIL"] = 2] = "GET_PRICES_MAP_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pricesMapActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const PRICES_MAP_ACTION_TYPES = pricesMapActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "2Eek":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ltjX");

/***/ }),

/***/ "2V+x":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pricesCalendarTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PRICES_CALENDAR_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pricesCalendar';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["SET_OFFSET"] = 0] = "SET_OFFSET";
  ActionTypes[ActionTypes["SET_LIMIT"] = 1] = "SET_LIMIT";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR"] = 2] = "GET_PRICES_CALENDAR";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR_SUCCESS"] = 3] = "GET_PRICES_CALENDAR_SUCCESS";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR_FAIL"] = 4] = "GET_PRICES_CALENDAR_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pricesCalendarTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const PRICES_CALENDAR_ACTION_TYPES = pricesCalendarTypesGenerator.getActionTypes();

/***/ }),

/***/ "2wwy":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Loka");

/***/ }),

/***/ "3FEJ":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypesGenerator; });
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2wwy");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__);

class ActionTypesGenerator {
  constructor(enumActionTypes, moduleName) {
    this.moduleName = void 0;
    this.enumActionTypes = void 0;
    this.actionTypes = {};
    this.moduleActionTypes = {};
    this.moduleName = moduleName;
    this.enumActionTypes = enumActionTypes;
    this.generateActionTypes();
  }

  generateActionTypes() {
    _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default()(this.enumActionTypes).forEach(value => {
      if (typeof value !== 'number') {
        this.actionTypes[value] = `${this.moduleName}/${value}`;
        this.moduleActionTypes[this.actionTypes[value]] = this.actionTypes[value];
      }
    });
  }

  getActionTypes() {
    return this.actionTypes;
  }

  getRequestActionTypes(actionType) {
    return [this.moduleActionTypes[actionType], this.moduleActionTypes[`${actionType}_SUCCESS`], this.moduleActionTypes[`${actionType}_FAIL`]];
  }

}

/***/ }),

/***/ "4mXO":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("k1wZ");

/***/ }),

/***/ "5chS":
/***/ (function(module, exports) {



/***/ }),

/***/ "8Bbg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("B5Ud")


/***/ }),

/***/ "9Jkg":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("fozc");

/***/ }),

/***/ "B5Ud":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireDefault = __webpack_require__("KI45");

exports.__esModule = true;
exports.Container = Container;
exports.createUrl = createUrl;
exports.default = void 0;

var _extends2 = _interopRequireDefault(__webpack_require__("htGi"));

var _asyncToGenerator2 = _interopRequireDefault(__webpack_require__("+oT+"));

var _react = _interopRequireDefault(__webpack_require__("cDcd"));

var _utils = __webpack_require__("g/15");

exports.AppInitialProps = _utils.AppInitialProps;
/**
* `App` component is used for initialize of pages. It allows for overwriting and full control of the `page` initialization.
* This allows for keeping state between navigation, custom error handling, injecting additional data.
*/

function appGetInitialProps(_x) {
  return _appGetInitialProps.apply(this, arguments);
}

function _appGetInitialProps() {
  _appGetInitialProps = (0, _asyncToGenerator2.default)(function* (_ref) {
    var {
      Component,
      ctx
    } = _ref;
    var pageProps = yield (0, _utils.loadGetInitialProps)(Component, ctx);
    return {
      pageProps
    };
  });
  return _appGetInitialProps.apply(this, arguments);
}

class App extends _react.default.Component {
  // Kept here for backwards compatibility.
  // When someone ended App they could call `super.componentDidCatch`.
  // @deprecated This method is no longer needed. Errors are caught at the top level
  componentDidCatch(error, _errorInfo) {
    throw error;
  }

  render() {
    var {
      router,
      Component,
      pageProps
    } = this.props;
    var url = createUrl(router);
    return _react.default.createElement(Component, (0, _extends2.default)({}, pageProps, {
      url: url
    }));
  }

}

exports.default = App;
App.origGetInitialProps = appGetInitialProps;
App.getInitialProps = appGetInitialProps;
var warnContainer;
var warnUrl;

if (false) {} // @deprecated noop for now until removal


function Container(p) {
  if (false) {}
  return p.children;
}

function createUrl(router) {
  // This is to make sure we don't references the router object at call time
  var {
    pathname,
    asPath,
    query
  } = router;
  return {
    get query() {
      if (false) {}
      return query;
    },

    get pathname() {
      if (false) {}
      return pathname;
    },

    get asPath() {
      if (false) {}
      return asPath;
    },

    back: () => {
      if (false) {}
      router.back();
    },
    push: (url, as) => {
      if (false) {}
      return router.push(url, as);
    },
    pushTo: (href, as) => {
      if (false) {}
      var pushRoute = as ? href : '';
      var pushUrl = as || href;
      return router.push(pushRoute, pushUrl);
    },
    replace: (url, as) => {
      if (false) {}
      return router.replace(url, as);
    },
    replaceTo: (href, as) => {
      if (false) {}
      var replaceRoute = as ? href : '';
      var replaceUrl = as || href;
      return router.replace(replaceRoute, replaceUrl);
    }
  };
}

/***/ }),

/***/ "FjY3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pointInfoTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_POINT_INFO_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pointInfo';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_POINT_INFO"] = 0] = "GET_POINT_INFO";
  ActionTypes[ActionTypes["GET_POINT_INFO_SUCCESS"] = 1] = "GET_POINT_INFO_SUCCESS";
  ActionTypes[ActionTypes["GET_POINT_INFO_FAIL"] = 2] = "GET_POINT_INFO_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pointInfoTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const GET_POINT_INFO_ACTION_TYPES = pointInfoTypesGenerator.getActionTypes();

/***/ }),

/***/ "IJ9z":
/***/ (function(module, exports) {



/***/ }),

/***/ "JMOJ":
/***/ (function(module, exports) {

module.exports = require("next-redux-wrapper");

/***/ }),

/***/ "JPPj":
/***/ (function(module, exports) {

module.exports = require("redux-persist/integration/react");

/***/ }),

/***/ "Jo+v":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Z6Kq");

/***/ }),

/***/ "KI45":
/***/ (function(module, exports) {

function _interopRequireDefault(obj) {
  return obj && obj.__esModule ? obj : {
    "default": obj
  };
}

module.exports = _interopRequireDefault;

/***/ }),

/***/ "LeJ0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// CONCATENATED MODULE: ./src/utils/envParams.ts

function getEnvBoolean(env) {
  return !Object(external_lodash_["isUndefined"])(env) && +env === 1;
}
function getEnvNumber(env) {
  return !Object(external_lodash_["isUndefined"])(env) ? +env : 0;
}
function getEnvString(env, defaultVal = '') {
  return !Object(external_lodash_["isUndefined"])(env) ? env : defaultVal;
}
// CONCATENATED MODULE: ./src/config/envConfig.ts

const apiPort = getEnvNumber("8751") || '';
const apiHost = getEnvString("http://ci.lowtrip.ru", 'localhost') + (apiPort ? `:${apiPort}` : '');
const apiRoot = getEnvString("/api/v1");
const apiUrl = apiHost + apiRoot;
const envConfig = {
  apiHost,
  apiPort,
  apiUrl,
  apiSocketUrl: apiHost,
  env: {
    DEV: "production" === 'development'
  }
};
/* harmony default export */ var config_envConfig = (envConfig);
// CONCATENATED MODULE: ./src/config/index.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });








function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const Config = _objectSpread({}, config_envConfig);

/***/ }),

/***/ "Loka":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/values");

/***/ }),

/***/ "QTVn":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "T8f9":
/***/ (function(module, exports) {

module.exports = require("redux-persist/lib/storage");

/***/ }),

/***/ "TUA0":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "Ti3b":
/***/ (function(module, exports) {

module.exports = require("lodash/uniq");

/***/ }),

/***/ "UXZV":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("dGr4");

/***/ }),

/***/ "VNb8":
/***/ (function(module, exports) {

module.exports = require("redux-persist");

/***/ }),

/***/ "VRjO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export filtersActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FILTERS_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'filters';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["CHANGE_TRANSFERS_FILTER"] = 0] = "CHANGE_TRANSFERS_FILTER";
  ActionTypes[ActionTypes["CHANGE_COAST_FILTER"] = 1] = "CHANGE_COAST_FILTER";
  ActionTypes[ActionTypes["CHANGE_TIMING_FILTERS"] = 2] = "CHANGE_TIMING_FILTERS";
  ActionTypes[ActionTypes["CHANGE_IATA_FILTER"] = 3] = "CHANGE_IATA_FILTER";
  ActionTypes[ActionTypes["CHANGE_SORTING_FILTER"] = 4] = "CHANGE_SORTING_FILTER";
  ActionTypes[ActionTypes["CHANGE_BAGGAGE_INCLUDED_FILTER"] = 5] = "CHANGE_BAGGAGE_INCLUDED_FILTER";
  ActionTypes[ActionTypes["SET_RESULTS_LIMIT"] = 6] = "SET_RESULTS_LIMIT";
})(ActionTypes || (ActionTypes = {}));

const filtersActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const FILTERS_ACTION_TYPES = filtersActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "XoMD":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("QTVn");

/***/ }),

/***/ "YLtl":
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "Z6Kq":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "aC71":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/promise");

/***/ }),

/***/ "auws":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _header_styles_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("IJ9z");
/* harmony import */ var _header_styles_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_header_styles_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("5chS");
/* harmony import */ var _header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0__["createElement"];



class Header extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(...args) {
    super(...args);
    this.burgerRef = void 0;
    this.logoRef = void 0;
    this.menuRef = void 0;

    this.onMenuPress = () => {
      this.burgerRef.classList.toggle("burger-X");
      this.logoRef.classList.toggle("black");
      this.menuRef.classList.toggle("active-menu");
    };

    this.bindBurger = ref => this.burgerRef = ref;

    this.bindLogo = ref => this.logoRef = ref;

    this.bindMenu = ref => this.menuRef = ref;
  }

  render() {
    return __jsx("header", null, __jsx("div", {
      className: "header-content"
    }, __jsx("span", {
      className: "logo",
      ref: this.bindLogo
    }, "\u041B\u043E\u0443\u0442\u0440\u0438\u043F"), __jsx("ul", {
      className: "menu",
      ref: this.bindMenu
    }, __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/"
    }, "\u041F\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044F")), __jsx("li", {
      className: "active-menu-white"
    }, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/"
    }, "\u0410\u0432\u0438\u0430\u0431\u0438\u043B\u0435\u0442\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/train"
    }, "\u0416\u0414 \u0411\u0438\u043B\u0435\u0442\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/bus"
    }, "\u0410\u0432\u0442\u043E\u0431\u0443\u0441\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/car"
    }, "\u041F\u043E\u043F\u0443\u0442\u043A\u0430")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/car"
    }, "\u041D\u0430 \u0441\u0432\u043E\u0435\u0439 \u043C\u0430\u0448\u0438\u043D\u0435"))), __jsx("div", {
      className: "menu-btn",
      onClick: this.onMenuPress
    }, __jsx("span", {
      ref: this.bindBurger,
      className: "burger"
    }))));
  }

}

/***/ }),

/***/ "bzos":
/***/ (function(module, exports) {

module.exports = require("url");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dGr4":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "eVuF":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("aC71");

/***/ }),

/***/ "fozc":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/json/stringify");

/***/ }),

/***/ "g/15":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$keys = __webpack_require__("pLtp");

var _Object$defineProperty = __webpack_require__("hfKm");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const url_1 = __webpack_require__("bzos");
/**
 * Utils
 */


function execOnce(fn) {
  let used = false;
  let result = null;
  return (...args) => {
    if (!used) {
      used = true;
      result = fn.apply(this, args);
    }

    return result;
  };
}

exports.execOnce = execOnce;

function getLocationOrigin() {
  const {
    protocol,
    hostname,
    port
  } = window.location;
  return `${protocol}//${hostname}${port ? ':' + port : ''}`;
}

exports.getLocationOrigin = getLocationOrigin;

function getURL() {
  const {
    href
  } = window.location;
  const origin = getLocationOrigin();
  return href.substring(origin.length);
}

exports.getURL = getURL;

function getDisplayName(Component) {
  return typeof Component === 'string' ? Component : Component.displayName || Component.name || 'Unknown';
}

exports.getDisplayName = getDisplayName;

function isResSent(res) {
  return res.finished || res.headersSent;
}

exports.isResSent = isResSent;

async function loadGetInitialProps(App, ctx) {
  if (false) {} // when called from _app `ctx` is nested in `ctx`


  const res = ctx.res || ctx.ctx && ctx.ctx.res;

  if (!App.getInitialProps) {
    if (ctx.ctx && ctx.Component) {
      // @ts-ignore pageProps default
      return {
        pageProps: await loadGetInitialProps(ctx.Component, ctx.ctx)
      };
    }

    return {};
  }

  const props = await App.getInitialProps(ctx);

  if (res && isResSent(res)) {
    return props;
  }

  if (!props) {
    const message = `"${getDisplayName(App)}.getInitialProps()" should resolve to an object. But found "${props}" instead.`;
    throw new Error(message);
  }

  if (false) {}

  return props;
}

exports.loadGetInitialProps = loadGetInitialProps;
exports.urlObjectKeys = ['auth', 'hash', 'host', 'hostname', 'href', 'path', 'pathname', 'port', 'protocol', 'query', 'search', 'slashes'];

function formatWithValidation(url, options) {
  if (false) {}

  return url_1.format(url, options);
}

exports.formatWithValidation = formatWithValidation;
exports.SUPPORTS_PERFORMANCE = typeof performance !== 'undefined';
exports.SUPPORTS_PERFORMANCE_USER_TIMING = exports.SUPPORTS_PERFORMANCE && typeof performance.mark === 'function' && typeof performance.measure === 'function';

/***/ }),

/***/ "gUJn":
/***/ (function(module, exports) {



/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "hSC+":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_application_header_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("auws");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("gUJn");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0__["createElement"];



const ERRORS = {
  404: 'Простите, такой страницы не существует',
  500: 'Что-то пошло не так'
};

const ErrorContainer = ({
  statusCode
}) => {
  react__WEBPACK_IMPORTED_MODULE_0__["useEffect"](() => {
    document.body.classList.toggle('image-bg');
  }, []);
  const errorTitle = statusCode || 'Упс';
  const errorDescription = ERRORS[statusCode] || 'Что-то пошло не так :(';
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx(_components_application_header_header__WEBPACK_IMPORTED_MODULE_1__[/* Header */ "a"], null), __jsx("div", {
    className: 'forms-wrapper error-page'
  }, __jsx("div", {
    className: "search-forms"
  }, __jsx("h1", null, errorTitle), __jsx("h2", null, errorDescription))));
};

/* harmony default export */ __webpack_exports__["a"] = (ErrorContainer);

/***/ }),

/***/ "hUgY":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: external "next-redux-wrapper"
var external_next_redux_wrapper_ = __webpack_require__("JMOJ");
var external_next_redux_wrapper_default = /*#__PURE__*/__webpack_require__.n(external_next_redux_wrapper_);

// EXTERNAL MODULE: ./node_modules/next/app.js
var app = __webpack_require__("8Bbg");
var app_default = /*#__PURE__*/__webpack_require__.n(app);

// EXTERNAL MODULE: external "redux"
var external_redux_ = __webpack_require__("rKB8");

// EXTERNAL MODULE: external "redux-persist"
var external_redux_persist_ = __webpack_require__("VNb8");

// EXTERNAL MODULE: external "redux-persist/lib/storage"
var storage_ = __webpack_require__("T8f9");
var storage_default = /*#__PURE__*/__webpack_require__.n(storage_);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/json/stringify.js
var stringify = __webpack_require__("9Jkg");
var stringify_default = /*#__PURE__*/__webpack_require__.n(stringify);

// CONCATENATED MODULE: ./src/store/utils/persistTransform.ts



const replacer = (key, value) => value instanceof Date ? value.toISOString() : value;

const reviver = (key, value) => typeof value === 'string' && value.match(/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/) ? new Date(value) : value;

const encode = toDeshydrate => stringify_default()(toDeshydrate, replacer);
const decode = toRehydrate => JSON.parse(toRehydrate, reviver);
const dateTransform = Object(external_redux_persist_["createTransform"])(encode, decode);
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: external "redux-axios-middleware"
var external_redux_axios_middleware_ = __webpack_require__("/J4M");
var external_redux_axios_middleware_default = /*#__PURE__*/__webpack_require__.n(external_redux_axios_middleware_);

// EXTERNAL MODULE: ./src/config/index.ts + 2 modules
var config = __webpack_require__("LeJ0");

// CONCATENATED MODULE: ./src/store/middlewares/axios.ts



const clients = external_axios_default.a.create({
  baseURL: config["a" /* Config */].apiUrl,
  responseType: 'json',
  timeout: 30000
});
const options = {
  returnRejectedPromiseOnError: true
};
/* harmony default export */ var axios = (external_redux_axios_middleware_default()(clients, options));
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: ./src/store/modules/autocomplete/IAutocompleteActionTypes.ts
var IAutocompleteActionTypes = __webpack_require__("qdML");

// CONCATENATED MODULE: ./src/store/modules/autocomplete/AutocompleteReducer.ts








function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const AutocompleteReducer_initialState = {
  results: []
};
function autocompleteReducer(state = AutocompleteReducer_initialState, action) {
  switch (action.type) {
    case IAutocompleteActionTypes["a" /* AUTOCOMPLETE_ACTION_TYPES */].AUTOCOMPLETE:
      {
        return _objectSpread({}, state);
      }

    case IAutocompleteActionTypes["a" /* AUTOCOMPLETE_ACTION_TYPES */].AUTOCOMPLETE_SUCCESS:
      {
        return _objectSpread({}, state, {
          results: action.payload.data.slice(0, 5)
        });
      }

    case IAutocompleteActionTypes["a" /* AUTOCOMPLETE_ACTION_TYPES */].AUTOCOMPLETE_FAIL:
      {
        return _objectSpread({}, state);
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/number/max-safe-integer.js
var max_safe_integer = __webpack_require__("i9IY");
var max_safe_integer_default = /*#__PURE__*/__webpack_require__.n(max_safe_integer);

// EXTERNAL MODULE: ./src/store/modules/pricesCalendar/IPricesCalendarActionTypes.ts
var IPricesCalendarActionTypes = __webpack_require__("2V+x");

// CONCATENATED MODULE: ./src/store/modules/pricesCalendar/PricesCalendarReducer.ts









function PricesCalendarReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function PricesCalendarReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { PricesCalendarReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { PricesCalendarReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const initialPricesCalendarState = {
  bestPrices: [],
  minimalPrice: max_safe_integer_default.a,
  maximalPrice: 0,
  offset: 0,
  limit: 30,
  total: 0
};
function pricesCalendarReducer(state = initialPricesCalendarState, action) {
  switch (action.type) {
    case IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].GET_PRICES_CALENDAR:
      {
        return PricesCalendarReducer_objectSpread({}, state);
      }

    case IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].GET_PRICES_CALENDAR_SUCCESS:
      {
        const {
          bestPrices
        } = action.payload.data;
        return PricesCalendarReducer_objectSpread({}, state, {
          bestPrices,
          total: bestPrices.length,
          minimalPrice: Math.min(state.minimalPrice, ...bestPrices.map(price => price.price)),
          maximalPrice: Math.max(state.maximalPrice, ...bestPrices.map(price => price.price))
        });
      }

    case IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].GET_PRICES_CALENDAR_FAIL:
      {
        return PricesCalendarReducer_objectSpread({}, state);
      }

    case IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].SET_LIMIT:
      {
        return PricesCalendarReducer_objectSpread({}, state, {
          limit: action.payload.limit
        });
      }

    case IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].SET_OFFSET:
      {
        return PricesCalendarReducer_objectSpread({}, state, {
          offset: action.payload.offset
        });
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./src/store/modules/searchTickets/ISearchTicketsActionsTypes.ts
var ISearchTicketsActionsTypes = __webpack_require__("mPxa");

// EXTERNAL MODULE: external "lodash/uniq"
var uniq_ = __webpack_require__("Ti3b");
var uniq_default = /*#__PURE__*/__webpack_require__.n(uniq_);

// CONCATENATED MODULE: ./src/store/modules/searchTickets/SearchTicketsReducer.ts









function SearchTicketsReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function SearchTicketsReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { SearchTicketsReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { SearchTicketsReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }



const initialPlacesState = {
  isLoading: false,
  totalTickets: 0,
  airports: {},
  allFlightNumbers: [],
  allCarriers: [],
  minimalPrice: max_safe_integer_default.a,
  minimalDuration: max_safe_integer_default.a,
  maximalPrice: 0,
  tickets: [],
  searchId: ''
};
function searchTicketsReducer(state = initialPlacesState, action) {
  switch (action.type) {
    case ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS:
      {
        return SearchTicketsReducer_objectSpread({}, state, {
          isLoading: true,
          airports: {},
          allFlightNumbers: [],
          allCarriers: [],
          minimalPrice: max_safe_integer_default.a,
          minimalDuration: max_safe_integer_default.a,
          maximalPrice: 0,
          tickets: [],
          searchId: ''
        });
      }

    case ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS_SUCCESS:
      {
        return SearchTicketsReducer_objectSpread({}, state, {
          searchId: action.payload.data.searchId
        });
      }

    case ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS_FAIL:
      {
        return SearchTicketsReducer_objectSpread({}, state, {
          isLoading: false
        });
      }

    case ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS_INCOMING_CHUNK:
      {
        return SearchTicketsReducer_objectSpread({}, state, {
          isLoading: false,
          totalTickets: state.tickets.length + action.payload.tickets.length,
          airports: SearchTicketsReducer_objectSpread({}, state.airports, {}, action.payload.airports),
          allCarriers: uniq_default()([...state.allCarriers, ...action.payload.allCarriers]),
          allFlightNumbers: uniq_default()([...state.allFlightNumbers, ...action.payload.allFlightNumbers]),
          minimalPrice: action.payload.minimalPrice > 0 ? Math.min(state.minimalPrice, action.payload.minimalPrice) : state.minimalPrice,
          minimalDuration: action.payload.minimalDuration > 0 ? Math.min(state.minimalDuration, action.payload.minimalDuration) : state.minimalDuration,
          maximalPrice: Math.max(state.maximalPrice, action.payload.maximalPrice),
          tickets: [...state.tickets, ...action.payload.tickets]
        });
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./src/store/modules/filters/IFiltersActionsTypes.ts
var IFiltersActionsTypes = __webpack_require__("VRjO");

// CONCATENATED MODULE: ./src/store/modules/filters/FiltersReducer.ts








function FiltersReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function FiltersReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { FiltersReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { FiltersReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const FiltersReducer_initialState = {
  resultsLimit: 10,
  baggageIncluded: 'all',
  transfers: {
    0: true,
    1: true,
    2: true
  },
  coast: {
    from: 0,
    to: 50000
  },
  timing: {
    '0': {
      arrivalTimeRange: {
        from: 0,
        to: 24
      },
      departureTimeRange: {
        from: 0,
        to: 24
      }
    },
    '1': {
      arrivalTimeRange: {
        from: 0,
        to: 24
      },
      departureTimeRange: {
        from: 0,
        to: 24
      }
    }
  },
  iata: '',
  sort: {
    key: 'coast',
    dir: 'asc'
  }
};
function filtersReducer(state = FiltersReducer_initialState, action) {
  switch (action.type) {
    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_TRANSFERS_FILTER:
      {
        return FiltersReducer_objectSpread({}, state, {
          transfers: FiltersReducer_objectSpread({}, state.transfers, {}, action.payload),
          resultsLimit: 10
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_COAST_FILTER:
      {
        return FiltersReducer_objectSpread({}, state, {
          coast: FiltersReducer_objectSpread({}, state.coast, {}, action.payload),
          resultsLimit: 10
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_TIMING_FILTERS:
      {
        return FiltersReducer_objectSpread({}, state, {
          timing: FiltersReducer_objectSpread({}, action.payload.filter),
          resultsLimit: 10
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_IATA_FILTER:
      {
        return FiltersReducer_objectSpread({}, state, {
          iata: action.payload,
          resultsLimit: 10
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_SORTING_FILTER:
      {
        return FiltersReducer_objectSpread({}, state, {
          sort: action.payload
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].SET_RESULTS_LIMIT:
      {
        return FiltersReducer_objectSpread({}, state, {
          resultsLimit: action.payload
        });
      }

    case IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_BAGGAGE_INCLUDED_FILTER:
      {
        return FiltersReducer_objectSpread({}, state, {
          baggageIncluded: action.payload,
          resultsLimit: 10
        });
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./src/store/modules/searchHistory/ISearchHistoryActionTypes.ts
var ISearchHistoryActionTypes = __webpack_require__("oden");

// EXTERNAL MODULE: external "lodash/uniqBy"
var uniqBy_ = __webpack_require__("qitR");
var uniqBy_default = /*#__PURE__*/__webpack_require__.n(uniqBy_);

// CONCATENATED MODULE: ./src/store/modules/searchHistory/SearchHistoryReducer.ts








function SearchHistoryReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function SearchHistoryReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { SearchHistoryReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { SearchHistoryReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }



const initialSearchState = {
  origins_history: [],
  destinations_history: []
};
function searchHistoryReducer(state = initialSearchState, action) {
  switch (action.type) {
    case ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_DESTINATION:
      {
        return SearchHistoryReducer_objectSpread({}, state, {
          destinations_history: uniqBy_default()(state.destinations_history.concat(action.payload), 'place.code')
        });
      }

    case ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_ORIGIN:
      {
        return SearchHistoryReducer_objectSpread({}, state, {
          origins_history: uniqBy_default()(state.origins_history.concat(action.payload), 'place.code')
        });
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./src/store/modules/pricesMap/IPricesMapActionTypes.ts
var IPricesMapActionTypes = __webpack_require__("1GcG");

// CONCATENATED MODULE: ./src/store/modules/pricesMap/PricesMapReducer.ts








function PricesMapReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function PricesMapReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { PricesMapReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { PricesMapReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const initialPricesMapState = {
  origin: {
    city: {
      code: '',
      name: ''
    },
    coordinates: {
      lat: 0,
      lng: 0
    },
    country: ''
  },
  destinationCountries: [],
  isLoading: false
};
function pricesMapReducer(state = initialPricesMapState, action) {
  switch (action.type) {
    case IPricesMapActionTypes["a" /* PRICES_MAP_ACTION_TYPES */].GET_PRICES_MAP:
      {
        return PricesMapReducer_objectSpread({}, state, {
          isLoading: true
        });
      }

    case IPricesMapActionTypes["a" /* PRICES_MAP_ACTION_TYPES */].GET_PRICES_MAP_SUCCESS:
      {
        return PricesMapReducer_objectSpread({}, state, {
          origin: action.payload.data.origin,
          destinationCountries: action.payload.data.destinationCountries,
          isLoading: false
        });
      }

    case IPricesMapActionTypes["a" /* PRICES_MAP_ACTION_TYPES */].GET_PRICES_MAP_FAIL:
      {
        return PricesMapReducer_objectSpread({}, state, {
          isLoading: false
        });
      }

    default:
      return state;
  }
}
// EXTERNAL MODULE: ./src/store/modules/pointInfo/IPointInfoActionTypes.ts
var IPointInfoActionTypes = __webpack_require__("FjY3");

// CONCATENATED MODULE: ./src/store/modules/pointInfo/PointInfoReducer.ts








function PointInfoReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function PointInfoReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { PointInfoReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { PointInfoReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const initialPointInfoState = {
  points: {},
  isLoading: false
};
function pointInfoReducer(state = initialPointInfoState, action) {
  switch (action.type) {
    case IPointInfoActionTypes["a" /* GET_POINT_INFO_ACTION_TYPES */].GET_POINT_INFO:
      {
        return PointInfoReducer_objectSpread({}, state, {
          isLoading: true
        });
      }

    case IPointInfoActionTypes["a" /* GET_POINT_INFO_ACTION_TYPES */].GET_POINT_INFO_SUCCESS:
      {
        return PointInfoReducer_objectSpread({}, state, {
          points: PointInfoReducer_objectSpread({}, action.payload.data),
          isLoading: false
        });
      }

    case IPointInfoActionTypes["a" /* GET_POINT_INFO_ACTION_TYPES */].GET_POINT_INFO_FAIL:
      {
        return PointInfoReducer_objectSpread({}, state, {
          isLoading: false
        });
      }

    default:
      {
        return state;
      }
  }
}
// EXTERNAL MODULE: ./src/store/ActionsTypesGenerator.ts
var ActionsTypesGenerator = __webpack_require__("3FEJ");

// CONCATENATED MODULE: ./src/store/modules/geolocation/IGeolocationActionTypes.ts

const moduleName = 'geolocation';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_GEOLOCATION"] = 0] = "GET_GEOLOCATION";
  ActionTypes[ActionTypes["GET_GEOLOCATION_SUCCESS"] = 1] = "GET_GEOLOCATION_SUCCESS";
  ActionTypes[ActionTypes["GET_GEOLOCATION_FAIL"] = 2] = "GET_GEOLOCATION_FAIL";
})(ActionTypes || (ActionTypes = {}));

const geolocationActionTypesGenerator = new ActionsTypesGenerator["a" /* ActionTypesGenerator */](ActionTypes, moduleName);
const GEOLOCATION_ACTION_TYPES = geolocationActionTypesGenerator.getActionTypes();
// CONCATENATED MODULE: ./src/store/modules/geolocation/GeolocationReducer.ts








function GeolocationReducer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function GeolocationReducer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { GeolocationReducer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { GeolocationReducer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const GeolocationReducer_initialState = {
  myLocation: {
    city: {
      code: 'KZNXXX',
      name: 'Моё местоположение'
    },
    place: {
      code: 'KZN',
      name: 'Казань'
    },
    country: {
      code: 'RU',
      name: 'Россия'
    },
    type: 'city'
  }
};
function geolocationReducer(state = GeolocationReducer_initialState, action) {
  switch (action.type) {
    case GEOLOCATION_ACTION_TYPES.GET_GEOLOCATION:
      {
        return GeolocationReducer_objectSpread({}, state);
      }

    case GEOLOCATION_ACTION_TYPES.GET_GEOLOCATION_SUCCESS:
      {
        const place = action.payload.data;
        place.place.name = 'Моё местоположение';
        return GeolocationReducer_objectSpread({}, state, {
          myLocation: place
        });
      }

    case GEOLOCATION_ACTION_TYPES.GET_GEOLOCATION_FAIL:
      {
        return GeolocationReducer_objectSpread({}, state);
      }

    default:
      return state;
  }
}
// CONCATENATED MODULE: ./src/store/RootReducer.ts









const RootReducer = Object(external_redux_["combineReducers"])({
  autocomplete: autocompleteReducer,
  filters: filtersReducer,
  searchTickets: searchTicketsReducer,
  pricesCalendar: pricesCalendarReducer,
  searchHistory: searchHistoryReducer,
  pricesMap: pricesMapReducer,
  pointInfo: pointInfoReducer,
  geolocation: geolocationReducer
});
// CONCATENATED MODULE: ./src/store/index.ts







const isDebugging = config["a" /* Config */].env.DEV;
let store_store;
function configureStore(initialState, {
  isServer
}) {
  const middlewares = [axios];

  if (isServer && true) {
    return Object(external_redux_["createStore"])(RootReducer, initialState, Object(external_redux_["applyMiddleware"])(...middlewares));
  }

  const composeEnhancers = isDebugging && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || external_redux_["compose"];

  if (!store_store) {
    if (!isDebugging) {
      if (window.__REACT_DEVTOOLS_GLOBAL_HOOK__) {
        window.__REACT_DEVTOOLS_GLOBAL_HOOK__.inject = () => {};
      }
    }

    const clientMiddleWares = [...middlewares];
    const persistConfig = {
      key: 'root',
      storage: storage_default.a,
      whitelist: ['sharedSearch', 'searchHistory'],
      transforms: [dateTransform]
    };
    const persistedReducer = Object(external_redux_persist_["persistReducer"])(persistConfig, RootReducer);
    store_store = Object(external_redux_["createStore"])(persistedReducer, initialState, composeEnhancers(Object(external_redux_["applyMiddleware"])(...clientMiddleWares)));
  }

  return store_store;
}
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: external "redux-persist/integration/react"
var react_ = __webpack_require__("JPPj");

// CONCATENATED MODULE: ./src/store/modules/geolocation/GeolocationActions.ts

const getGeolocation = () => ({
  type: GEOLOCATION_ACTION_TYPES.GET_GEOLOCATION,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/geolocation'
    }
  }
});
// EXTERNAL MODULE: ./src/containers/ErrorContainer/index.tsx
var ErrorContainer = __webpack_require__("hSC+");

// CONCATENATED MODULE: ./src/pages/_app.tsx
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _app_App; });
var _dec, _class, _temp;

var __jsx = external_react_["createElement"];









let _app_App = (_dec = external_next_redux_wrapper_default()(configureStore), _dec(_class = (_temp = class App extends app_default.a {
  static async getInitialProps({
    Component,
    ctx
  }) {
    const {
      store: {
        getState,
        dispatch
      }
    } = ctx;
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    let error;

    try {
      await dispatch(getGeolocation());
    } catch (err) {
      error = err;
      console.error('Error getInitialProps in _app', err);
    }

    return {
      pageProps,
      initialReduxState: getState(),
      error
    };
  }

  constructor(props, context) {
    super(props, context);
    this.persistor = void 0;
    this.persistor = Object(external_redux_persist_["persistStore"])(props.store);
  }

  componentDidMount() {
    const {
      error
    } = this.props;

    if (error) {
      console.error('Application error', error);
    }
  }

  render() {
    const {
      Component,
      pageProps,
      store,
      error
    } = this.props;

    if (error) {
      return __jsx(ErrorContainer["a" /* default */], null);
    }

    return __jsx(external_react_redux_["Provider"], {
      store: store
    }, __jsx(react_["PersistGate"], {
      loading: __jsx(Component, pageProps),
      persistor: this.persistor
    }, __jsx(Component, pageProps)));
  }

}, _temp)) || _class);


/***/ }),

/***/ "hfKm":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("TUA0");

/***/ }),

/***/ "htGi":
/***/ (function(module, exports, __webpack_require__) {

var _Object$assign = __webpack_require__("UXZV");

function _extends() {
  module.exports = _extends = _Object$assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "i9IY":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("irLM");

/***/ }),

/***/ "irLM":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/number/max-safe-integer");

/***/ }),

/***/ "k1wZ":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "ltjX":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "mPxa":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export searchTicketsActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SEARCH_TICKETS_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'searchTicketsPlaces';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["SEARCH_TICKETS"] = 0] = "SEARCH_TICKETS";
  ActionTypes[ActionTypes["SEARCH_TICKETS_SUCCESS"] = 1] = "SEARCH_TICKETS_SUCCESS";
  ActionTypes[ActionTypes["SEARCH_TICKETS_FAIL"] = 2] = "SEARCH_TICKETS_FAIL";
  ActionTypes[ActionTypes["SEARCH_TICKETS_INCOMING_CHUNK"] = 3] = "SEARCH_TICKETS_INCOMING_CHUNK";
  ActionTypes[ActionTypes["SET_RESULTS_LIMIT"] = 4] = "SET_RESULTS_LIMIT";
})(ActionTypes || (ActionTypes = {}));

const searchTicketsActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const SEARCH_TICKETS_ACTION_TYPES = searchTicketsActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "oden":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export searchHistoryActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SEARCH_HISTORY_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'search_history';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["PUSH_ORIGIN"] = 0] = "PUSH_ORIGIN";
  ActionTypes[ActionTypes["PUSH_DESTINATION"] = 1] = "PUSH_DESTINATION";
})(ActionTypes || (ActionTypes = {}));

const searchHistoryActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const SEARCH_HISTORY_ACTION_TYPES = searchHistoryActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "pLtp":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("qJj/");

/***/ }),

/***/ "qJj/":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "qdML":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export autocompleteActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AUTOCOMPLETE_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'autocomplete';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["AUTOCOMPLETE"] = 0] = "AUTOCOMPLETE";
  ActionTypes[ActionTypes["AUTOCOMPLETE_SUCCESS"] = 1] = "AUTOCOMPLETE_SUCCESS";
  ActionTypes[ActionTypes["AUTOCOMPLETE_FAIL"] = 2] = "AUTOCOMPLETE_FAIL";
})(ActionTypes || (ActionTypes = {}));

const autocompleteActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const AUTOCOMPLETE_ACTION_TYPES = autocompleteActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "qitR":
/***/ (function(module, exports) {

module.exports = require("lodash/uniqBy");

/***/ }),

/***/ "rKB8":
/***/ (function(module, exports) {

module.exports = require("redux");

/***/ }),

/***/ "vYYK":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("hfKm");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });