module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "16zm":
/***/ (function(module, exports) {



/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("LixI");


/***/ }),

/***/ "2/0G":
/***/ (function(module, exports) {



/***/ }),

/***/ "5chS":
/***/ (function(module, exports) {



/***/ }),

/***/ "IJ9z":
/***/ (function(module, exports) {



/***/ }),

/***/ "LixI":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("xnum");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_day_picker_lib_style_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("UNSI");
/* harmony import */ var react_day_picker_lib_style_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_day_picker_lib_style_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_App_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("16zm");
/* harmony import */ var _styles_App_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_App_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _styles_App_mobile_scss__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("2/0G");
/* harmony import */ var _styles_App_mobile_scss__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_styles_App_mobile_scss__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _containers_ErrorContainer__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("hSC+");
var __jsx = react__WEBPACK_IMPORTED_MODULE_0__["createElement"];







class Search extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  render() {
    const {
      statusCode
    } = this.props;
    return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, null, __jsx("title", null, "\u041A\u0443\u043F\u0438\u0442\u044C \u0434\u0435\u0448\u0435\u0432\u044B\u0435 \u0431\u0438\u043B\u0435\u0442\u044B \u043D\u0430 \u0441\u0430\u043C\u043E\u043B\u0435\u0442 \u043D\u0430 \u043E\u0444\u0438\u0446\u0438\u0430\u043B\u044C\u043D\u043E\u043C \u0441\u0430\u0439\u0442\u0435 Lowtrip"), __jsx("meta", {
      name: "description",
      content: "\u041D\u0430\u0439\u0442\u0438 \u0438 \u043A\u0443\u043F\u0438\u0442\u044C \u043D\u0435\u0434\u043E\u0440\u043E\u0433\u0438\u0435 \u0431\u0438\u043B\u0435\u0442\u044B \u043D\u0430 \u0430\u0432\u0438\u0430 \u043C\u043E\u0436\u043D\u043E \u043D\u0430 \u043E\u0444\u0438\u0446\u0438\u0430\u043B\u044C\u043D\u043E\u043C \u0441\u0430\u0439\u0442\u0435 Lowtrip"
    }), __jsx("link", {
      rel: "canonical",
      href: `https://lowtrip.ru/`
    })), __jsx(_containers_ErrorContainer__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"], {
      statusCode: statusCode
    }));
  }

}

Search.getInitialProps = ({
  res,
  err
}) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404;
  return {
    statusCode
  };
};

/* harmony default export */ __webpack_exports__["default"] = (Search);

/***/ }),

/***/ "UNSI":
/***/ (function(module, exports) {



/***/ }),

/***/ "auws":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Header; });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _header_styles_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("IJ9z");
/* harmony import */ var _header_styles_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_header_styles_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("5chS");
/* harmony import */ var _header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_header_styles_mobile_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0__["createElement"];



class Header extends react__WEBPACK_IMPORTED_MODULE_0__["Component"] {
  constructor(...args) {
    super(...args);
    this.burgerRef = void 0;
    this.logoRef = void 0;
    this.menuRef = void 0;

    this.onMenuPress = () => {
      this.burgerRef.classList.toggle("burger-X");
      this.logoRef.classList.toggle("black");
      this.menuRef.classList.toggle("active-menu");
    };

    this.bindBurger = ref => this.burgerRef = ref;

    this.bindLogo = ref => this.logoRef = ref;

    this.bindMenu = ref => this.menuRef = ref;
  }

  render() {
    return __jsx("header", null, __jsx("div", {
      className: "header-content"
    }, __jsx("span", {
      className: "logo",
      ref: this.bindLogo
    }, "\u041B\u043E\u0443\u0442\u0440\u0438\u043F"), __jsx("ul", {
      className: "menu",
      ref: this.bindMenu
    }, __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/"
    }, "\u041F\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044F")), __jsx("li", {
      className: "active-menu-white"
    }, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/"
    }, "\u0410\u0432\u0438\u0430\u0431\u0438\u043B\u0435\u0442\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/train"
    }, "\u0416\u0414 \u0411\u0438\u043B\u0435\u0442\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/bus"
    }, "\u0410\u0432\u0442\u043E\u0431\u0443\u0441\u044B")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/car"
    }, "\u041F\u043E\u043F\u0443\u0442\u043A\u0430")), __jsx("li", null, __jsx("a", {
      rel: "noopener noreferrer",
      target: "_blank",
      href: "/car"
    }, "\u041D\u0430 \u0441\u0432\u043E\u0435\u0439 \u043C\u0430\u0448\u0438\u043D\u0435"))), __jsx("div", {
      className: "menu-btn",
      onClick: this.onMenuPress
    }, __jsx("span", {
      ref: this.bindBurger,
      className: "burger"
    }))));
  }

}

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "gUJn":
/***/ (function(module, exports) {



/***/ }),

/***/ "hSC+":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("cDcd");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_application_header_header__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("auws");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("gUJn");
/* harmony import */ var _styles_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_styles_scss__WEBPACK_IMPORTED_MODULE_2__);
var __jsx = react__WEBPACK_IMPORTED_MODULE_0__["createElement"];



const ERRORS = {
  404: 'Простите, такой страницы не существует',
  500: 'Что-то пошло не так'
};

const ErrorContainer = ({
  statusCode
}) => {
  react__WEBPACK_IMPORTED_MODULE_0__["useEffect"](() => {
    document.body.classList.toggle('image-bg');
  }, []);
  const errorTitle = statusCode || 'Упс';
  const errorDescription = ERRORS[statusCode] || 'Что-то пошло не так :(';
  return __jsx(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], null, __jsx(_components_application_header_header__WEBPACK_IMPORTED_MODULE_1__[/* Header */ "a"], null), __jsx("div", {
    className: 'forms-wrapper error-page'
  }, __jsx("div", {
    className: "search-forms"
  }, __jsx("h1", null, errorTitle), __jsx("h2", null, errorDescription))));
};

/* harmony default export */ __webpack_exports__["a"] = (ErrorContainer);

/***/ }),

/***/ "xnum":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ })

/******/ });