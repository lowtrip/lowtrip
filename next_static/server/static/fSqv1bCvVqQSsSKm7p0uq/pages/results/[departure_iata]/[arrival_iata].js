module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "+Hn+":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);

// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__("4Q3z");
var router_default = /*#__PURE__*/__webpack_require__.n(router_);

// EXTERNAL MODULE: ./node_modules/react-day-picker/lib/style.css
var style = __webpack_require__("UNSI");

// EXTERNAL MODULE: ./src/styles/App.scss
var App = __webpack_require__("16zm");

// EXTERNAL MODULE: ./src/styles/App.mobile.scss
var App_mobile = __webpack_require__("2/0G");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/number/max-safe-integer.js
var max_safe_integer = __webpack_require__("i9IY");
var max_safe_integer_default = /*#__PURE__*/__webpack_require__.n(max_safe_integer);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/parse-int.js
var parse_int = __webpack_require__("6BQ9");
var parse_int_default = /*#__PURE__*/__webpack_require__.n(parse_int);

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: external "socket.io-client"
var external_socket_io_client_ = __webpack_require__("pI2v");
var external_socket_io_client_default = /*#__PURE__*/__webpack_require__.n(external_socket_io_client_);

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.tsx + 1 modules
var header_filters = __webpack_require__("UIQ4");

// EXTERNAL MODULE: external "react-tooltip"
var external_react_tooltip_ = __webpack_require__("E8iq");
var external_react_tooltip_default = /*#__PURE__*/__webpack_require__.n(external_react_tooltip_);

// EXTERNAL MODULE: ./src/components/application/header-costs/header-costs.styles.scss
var header_costs_styles = __webpack_require__("Q58H");

// CONCATENATED MODULE: ./src/store/utils/isSameDay.ts
const isSameDay = (date1, date2) => date1.getDate() === date2.getDate() && date1.getMonth() === date2.getMonth() && date1.getFullYear() === date2.getFullYear();
// EXTERNAL MODULE: external "reselect"
var external_reselect_ = __webpack_require__("MWqi");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./src/store/modules/pricesCalendar/PricesCalendarSelector.ts


const MAX_BAR_HEIGHT = 100;
const MIN_BAR_HEIGHT = 20;
const actualBarHeight = MAX_BAR_HEIGHT - MIN_BAR_HEIGHT;

const getAllPrices = state => state.pricesCalendar.bestPrices;

const getPricesLimit = state => state.pricesCalendar.limit;

const getPricesOffset = state => state.pricesCalendar.offset;

const selectPrices = Object(external_reselect_["createSelector"])([getAllPrices, getPricesLimit, getPricesOffset], (allPrices, limit, offset) => {
  const prices = allPrices.slice(offset, offset + limit);
  const pricesValues = prices.map(price => price.price);
  const [maxPrice, minPrice] = [Math.max(...pricesValues), Math.min(...pricesValues)];
  return prices.map(price => ({
    price: price.price,
    departDate: price.departDate,
    parsedDate: new Date(price.departDate),
    humanReadableDate: external_moment_default()(price.departDate).format('D MMMM, dd'),
    barHeight: actualBarHeight * ((price.price - minPrice) / (maxPrice - minPrice) || 0) + MIN_BAR_HEIGHT
  }));
});
// EXTERNAL MODULE: ./src/store/modules/pricesCalendar/IPricesCalendarActionTypes.ts
var IPricesCalendarActionTypes = __webpack_require__("2V+x");

// CONCATENATED MODULE: ./src/store/modules/pricesCalendar/PricesCalendarActions.ts

const getPricesCalendar = data => ({
  type: IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].GET_PRICES_CALENDAR,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/pricesCalendar',
      data
    }
  }
});
const PricesCalendarActions_setOffset = data => ({
  type: IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].SET_OFFSET,
  payload: {
    offset: data
  }
});
const PricesCalendarActions_setLimit = data => ({
  type: IPricesCalendarActionTypes["a" /* PRICES_CALENDAR_ACTION_TYPES */].SET_LIMIT,
  payload: {
    limit: data
  }
});
// CONCATENATED MODULE: ./src/components/application/header-costs/header-costs.tsx
var __jsx = external_react_["createElement"];







const HeaderCosts = ({
  prices,
  offset,
  setOffset,
  setLimit,
  limit,
  onDateClick,
  currentDepartDate,
  total
}) => {
  const graphProps = {
    maxItems: 90,
    stepSize: 30
  };
  const tableProps = {
    maxItems: 28,
    stepSize: 7
  };
  const [isGraphVisible, toggleGraphVisibility] = external_react_["useState"](true);

  const toggleGraph = () => {
    toggleGraphVisibility(!isGraphVisible);
    setLimit(isGraphVisible ? 7 : 30);
    setOffset(0);
  };

  const increaseGraphOffset = () => {
    setOffset(Math.min(offset + graphProps.stepSize, graphProps.maxItems));
  };

  const decreaseGraphOffset = () => {
    setOffset(Math.max(offset - graphProps.stepSize, 0));
  };

  const increaseTableOffset = () => {
    setOffset(Math.min(offset + tableProps.stepSize, tableProps.maxItems));
  };

  const decreaseTableOffset = () => {
    setOffset(Math.max(offset - tableProps.stepSize, 0));
  };

  const onBarClick = date => {
    onDateClick(date);
  };

  return __jsx("div", {
    className: "graphs"
  }, __jsx("div", {
    className: "graph-change"
  }, __jsx("div", {
    className: `graph-btn ${!isGraphVisible ? 'graph-btn-second' : ''}`,
    onClick: toggleGraph
  }, __jsx("div", {
    className: `${!isGraphVisible ? 'graph-btn-second-position' : ''}`
  })), __jsx("p", null, __jsx("span", {
    className: `${isGraphVisible ? 'graph-active' : ''}`
  }, "\u0413\u0440\u0430\u0444\u0438\u043A \u0446\u0435\u043D"), __jsx("span", {
    className: `${!isGraphVisible ? 'graph-active' : ''}`
  }, "\u0422\u0430\u0431\u043B\u0438\u0446\u0430 \u0446\u0435\u043D"))), __jsx("div", {
    className: `${!isGraphVisible ? 'hidden' : ''} graph1`
  }, __jsx("div", {
    className: 'graph-wrapper'
  }, !!prices.length && offset > 0 && __jsx("div", {
    className: "arrowArea"
  }, __jsx("img", {
    alt: 'Previous chunk',
    className: 'prev-arrow',
    src: '/images/next-graph.svg',
    onClick: decreaseGraphOffset
  })), prices.map(price => __jsx(external_react_["Fragment"], {
    key: price.departDate
  }, __jsx(external_react_tooltip_default.a, {
    effect: 'solid',
    id: `tt-${price.departDate}`
  }, __jsx("span", null, price.humanReadableDate, " ", !!price.price && 'от ' + price.price)), __jsx("div", {
    onClick: () => onBarClick(price.parsedDate),
    className: `bar ${!!price.price && 'available'} ${isSameDay(price.parsedDate, currentDepartDate) ? 'active' : ''}`,
    style: {
      height: `${price.barHeight}px`
    },
    "data-tip": `${price.humanReadableDate} от ${price.price}`,
    "data-for": `tt-${price.departDate}`
  }))), !!prices.length && offset + graphProps.stepSize < total && __jsx("div", {
    className: "arrowArea right"
  }, __jsx("img", {
    alt: 'Next chunk',
    className: 'next-arrow',
    src: '/images/next-graph.svg',
    onClick: increaseGraphOffset
  })))), __jsx("div", {
    className: `${isGraphVisible ? 'hidden' : ''} graph2`
  }, __jsx("div", {
    className: 'graph-wrapper'
  }, !!prices.length && offset > 0 && __jsx("div", {
    className: "arrowArea"
  }, __jsx("img", {
    alt: 'Previous chunk',
    className: 'prev-arrow',
    src: '/images/next-graph.svg',
    onClick: decreaseTableOffset
  })), prices.map((price, index) => __jsx(external_react_["Fragment"], {
    key: price.departDate
  }, __jsx("div", {
    onClick: () => onBarClick(price.parsedDate),
    className: `proposal-item ${isSameDay(price.parsedDate, currentDepartDate) ? 'active' : ''}`
  }, __jsx("div", {
    className: 'price'
  }, price.price), __jsx("div", {
    className: 'date'
  }, price.humanReadableDate)), index < prices.length - 1 && __jsx("div", {
    className: 'divider'
  }))), !!prices.length && -8 < total && __jsx("div", {
    className: "arrowArea right"
  }, __jsx("img", {
    alt: 'Next chunk',
    className: 'next-arrow',
    src: '/images/next-graph.svg',
    onClick: increaseTableOffset
  })))));
};
/* harmony default export */ var header_costs = (Object(external_react_redux_["connect"])(state => ({
  limit: state.pricesCalendar.limit,
  offset: state.pricesCalendar.offset,
  prices: selectPrices(state),
  minmalPrice: state.pricesCalendar.minimalPrice,
  maximalPrice: state.pricesCalendar.maximalPrice,
  total: state.pricesCalendar.total
}), dispatch => ({
  setLimit: data => dispatch(PricesCalendarActions_setLimit(data)),
  setOffset: data => dispatch(PricesCalendarActions_setOffset(data))
}))(HeaderCosts));
// EXTERNAL MODULE: ./src/config/index.ts + 2 modules
var config = __webpack_require__("LeJ0");

// EXTERNAL MODULE: ./src/store/modules/searchTickets/ISearchTicketsActionsTypes.ts
var ISearchTicketsActionsTypes = __webpack_require__("mPxa");

// CONCATENATED MODULE: ./src/store/modules/searchTickets/SearchTicketsActions.ts

const searchTickets = data => {
  return {
    type: ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS,
    payload: {
      returnRejectedPromiseOnError: true,
      request: {
        method: 'POST',
        url: '/searchTickets',
        data
      }
    }
  };
};
const searchTicketsIncomingChunk = data => {
  return {
    type: ISearchTicketsActionsTypes["a" /* SEARCH_TICKETS_ACTION_TYPES */].SEARCH_TICKETS_INCOMING_CHUNK,
    payload: data
  };
};
// EXTERNAL MODULE: ./src/store/modules/filters/IFiltersActionsTypes.ts
var IFiltersActionsTypes = __webpack_require__("VRjO");

// CONCATENATED MODULE: ./src/store/modules/filters/FiltersActions.ts

const FiltersActions_changeTransfersFilter = filter => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_TRANSFERS_FILTER,
  payload: filter
});
const changeCoastFilter = filter => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_COAST_FILTER,
  payload: filter
});
const changeTimingFilters = (key, filter) => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_TIMING_FILTERS,
  payload: {
    key,
    filter
  }
});
const changeIATAFilter = iata => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_IATA_FILTER,
  payload: iata
});
const changeSortFilter = filter => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_SORTING_FILTER,
  payload: filter
});
const setResultsLimit = limit => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].SET_RESULTS_LIMIT,
  payload: limit
});
const setBaggageIncludedFilter = baggageIncluded => ({
  type: IFiltersActionsTypes["a" /* FILTERS_ACTION_TYPES */].CHANGE_BAGGAGE_INCLUDED_FILTER,
  payload: baggageIncluded
});
// EXTERNAL MODULE: ./src/components/application/results/FiltersDetails/styles.scss
var styles = __webpack_require__("BL1G");

// EXTERNAL MODULE: ./src/components/application/results/FiltersDetails/styles.mobile.scss
var styles_mobile = __webpack_require__("/mnI");

// EXTERNAL MODULE: ./src/components/application/results/FilterDropdownSection/styles.scss
var FilterDropdownSection_styles = __webpack_require__("HzH2");

// CONCATENATED MODULE: ./src/components/application/results/FilterDropdownSection/FilterDropdownSection.tsx
var FilterDropdownSection_jsx = external_react_["createElement"];


const FilterDropdownSection = props => {
  const [isExpanded, toggleExpand] = external_react_["useState"](true);

  function toggle() {
    toggleExpand(!isExpanded);
  }

  return FilterDropdownSection_jsx("div", {
    className: `filter-dropdown__section ${props.className}`
  }, FilterDropdownSection_jsx("span", {
    onClick: toggle
  }, props.title, FilterDropdownSection_jsx("img", {
    alt: "",
    src: '/images/ArrowUp.svg',
    style: {
      transform: isExpanded ? 'scaleY(1)' : 'scaleY(-1)'
    }
  })), FilterDropdownSection_jsx("div", {
    className: 'filter-dropdown__section_content',
    style: isExpanded ? {} : {
      display: 'none'
    }
  }, props.children));
};
// EXTERNAL MODULE: external "react-input-range"
var external_react_input_range_ = __webpack_require__("5U/F");
var external_react_input_range_default = /*#__PURE__*/__webpack_require__.n(external_react_input_range_);

// EXTERNAL MODULE: ./node_modules/react-input-range/lib/css/index.css
var css = __webpack_require__("P5Gt");

// EXTERNAL MODULE: ./src/components/shared/RangeSlider/styles.scss
var RangeSlider_styles = __webpack_require__("kqQL");

// CONCATENATED MODULE: ./src/components/shared/RangeSlider/index.tsx
var RangeSlider_jsx = external_react_["createElement"];





const RangeSlider = ({
  minValue,
  maxValue,
  formatLabel,
  currentMaxValue,
  currentMinValue,
  onRangeChange
}) => {
  const onChange = value => {
    onRangeChange(value.min, value.max);
  };

  return RangeSlider_jsx("div", {
    className: `range-clider__custom `
  }, RangeSlider_jsx(external_react_input_range_default.a, {
    allowSameValues: true,
    maxValue: maxValue,
    minValue: minValue,
    formatLabel: formatLabel,
    value: {
      min: currentMinValue,
      max: currentMaxValue
    },
    onChange: onChange
  }));
};

/* harmony default export */ var shared_RangeSlider = (external_react_["memo"](RangeSlider));
// EXTERNAL MODULE: ./src/components/shared/Checkbox/styles.scss
var Checkbox_styles = __webpack_require__("rzUo");

// CONCATENATED MODULE: ./src/components/shared/Checkbox/index.tsx
var Checkbox_jsx = external_react_["createElement"];



const Checkbox = ({
  children,
  id,
  fullColored,
  withIcon,
  checked,
  onToggle,
  className
}) => Checkbox_jsx("div", {
  id: id,
  className: `checkbox-component ${fullColored && checked ? 'checked' : ''} ${className ? className : ''}`,
  onClick: onToggle
}, Checkbox_jsx("input", {
  type: "checkbox",
  name: "checkbox",
  hidden: true
}), withIcon && Checkbox_jsx("div", {
  className: `icon-wrapper ${checked ? 'checked' : ''}`
}, Checkbox_jsx("img", {
  className: 'icon',
  alt: "",
  src: '/images/ok-white.svg'
})), children);

/* harmony default export */ var shared_Checkbox = (external_react_["memo"](Checkbox));
// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/values.js
var values = __webpack_require__("2wwy");
var values_default = /*#__PURE__*/__webpack_require__.n(values);

// CONCATENATED MODULE: ./src/store/modules/filters/FiltersSelectors.ts



const isAllTrue = (obj = {}) => {
  return keys_default()(obj).every(k => obj[k]);
};
const isOneTrue = (obj = {}, key) => {
  return obj[key] && values_default()(obj).filter(v => !v).length === values_default()(obj).length - 1;
};
const makeAllValuesFalse = (obj = {}) => {
  return keys_default()(obj).reduce((acc, i) => {
    acc[i] = obj[i] ? !obj[i] : obj[i];
    return acc;
  }, {});
};
const makeAllValuesTrue = (obj = {}) => {
  return keys_default()(obj).reduce((acc, i) => {
    acc[i] = obj[i] ? obj[i] : !obj[i];
    return acc;
  }, {});
};

const allTicketsSelector = state => state.searchTickets.tickets;

const transfersFilterSelector = state => state.filters.transfers;

const coastFilterSelector = state => state.filters.coast;

const timingFilterSelector = state => state.filters.timing;

const iataFilterSelector = state => state.filters.iata;

const sortFilterSelector = state => state.filters.sort;

const getResultLimit = state => state.filters.resultsLimit;

const baggageFilterSelector = state => state.filters.baggageIncluded;

const getFiltredByTransfer = (tickets, transfersFilter) => {
  return tickets.filter(ticket => {
    return transfersFilter[ticket.totalTransfers];
  });
};

const getFiltredByCoast = (tickets, coastFilter) => {
  return tickets.filter(ticket => {
    return coastFilter.from <= ticket.price && ticket.price <= coastFilter.to;
  });
};

const getFiltredByTiming = (tickets, timingFilter) => {
  return tickets.filter(ticket => {
    return ticket.segments.every((item, index) => {
      const arrival = +item.flightChunks[item.flightChunks.length - 1].departureTime.split(':')[0];
      const departure = +item.flightChunks[0].departureTime.split(':')[0];
      const currentFilter = timingFilter[index];
      const isDepartureTimeValid = currentFilter.departureTimeRange.from <= departure && currentFilter.departureTimeRange.to >= departure;
      const isArrivalTimeValid = currentFilter.arrivalTimeRange.from <= arrival && currentFilter.arrivalTimeRange.to >= arrival;
      return isDepartureTimeValid && isArrivalTimeValid;
    });
  });
};

const getFiltredByIATA = (tickets, iataFilter) => {
  return tickets.filter(ticket => {
    return ticket.segments.flatMap(segment => segment.flightChunks.map(chunk => chunk.carrier)).includes(iataFilter);
  });
};

const getFilteredByBaggage = (tickets, isBaggageIncluded) => {
  return tickets.filter(ticket => isBaggageIncluded === 'without' ? !ticket.baggage : ticket.baggage);
};

const getSorted = (tickets, sort) => {
  if (tickets && !tickets.length) {
    return tickets;
  }

  if (sort.key === 'departure') {
    return tickets.sort((a, b) => {
      const firstDepartureTime = a.segments[0].flightChunks[0].departureTime;
      const secondDepartureTime = b.segments[0].flightChunks[0].departureTime;
      return sort.dir === 'asc' ? firstDepartureTime.localeCompare(secondDepartureTime) : secondDepartureTime.localeCompare(firstDepartureTime);
    });
  }

  if (sort.key === 'coast') {
    return tickets.sort((a, b) => {
      return sort.dir === 'asc' ? a.price - b.price : b.price - a.price;
    });
  }

  if (sort.key === 'total-time') {
    return tickets.sort((a, b) => {
      return sort.dir === 'asc' ? a.totalDuration - b.totalDuration : b.totalDuration - a.totalDuration;
    });
  }

  return tickets;
};

const getFilteredTicketsAmount = Object(external_reselect_["createSelector"])([allTicketsSelector, baggageFilterSelector, transfersFilterSelector, coastFilterSelector, timingFilterSelector, iataFilterSelector, sortFilterSelector], (allTickets, baggageFilter, transfersFilter, coastFilter, timingFilter, iataFilter, sortFilter) => {
  if (allTickets && !allTickets.length) {
    return allTickets.length;
  }

  let tickets = allTickets;

  if (!isAllTrue(transfersFilter)) {
    tickets = getFiltredByTransfer(tickets, transfersFilter);
  } // Coast filter


  if (coastFilter.from > 0 || coastFilter.to < 50000) {
    tickets = getFiltredByCoast(tickets, coastFilter);
  } // Time filter


  if (values_default()(timingFilter).some(filter => filter.arrivalTimeRange.from > 0 || filter.arrivalTimeRange.to < 24 || filter.departureTimeRange.from > 0 || filter.departureTimeRange.to < 24)) {
    tickets = getFiltredByTiming(tickets, timingFilter);
  }

  if (iataFilter) {
    tickets = getFiltredByIATA(tickets, iataFilter);
  }

  if (sortFilter && sortFilter.key && sortFilter.dir) {
    tickets = getSorted(tickets, sortFilter);
  }

  if (baggageFilter !== 'all') {
    tickets = getFilteredByBaggage(tickets, baggageFilter);
  }

  return tickets.length;
});
const getFiltredTickets = Object(external_reselect_["createSelector"])([getResultLimit, allTicketsSelector, baggageFilterSelector, transfersFilterSelector, coastFilterSelector, timingFilterSelector, iataFilterSelector, sortFilterSelector], (resultsLimit, allTickets, baggageFilter, transfersFilter, coastFilter, timingFilter, iataFilter, sortFilter) => {
  if (allTickets && !allTickets.length) {
    return allTickets;
  }

  let tickets = allTickets;

  if (!isAllTrue(transfersFilter)) {
    tickets = getFiltredByTransfer(tickets, transfersFilter);
  } // Coast filter


  if (coastFilter.from > 0 || coastFilter.to < 50000) {
    tickets = getFiltredByCoast(tickets, coastFilter);
  } // Time filter


  if (values_default()(timingFilter).some(filter => filter.arrivalTimeRange.from > 0 || filter.arrivalTimeRange.to < 24 || filter.departureTimeRange.from > 0 || filter.departureTimeRange.to < 24)) {
    tickets = getFiltredByTiming(tickets, timingFilter);
  }

  if (iataFilter) {
    tickets = getFiltredByIATA(tickets, iataFilter);
  }

  if (sortFilter && sortFilter.key && sortFilter.dir) {
    tickets = getSorted(tickets, sortFilter);
  }

  if (baggageFilter !== 'all') {
    tickets = getFilteredByBaggage(tickets, baggageFilter);
  }

  return tickets.slice(0, resultsLimit);
});
// CONCATENATED MODULE: ./src/components/application/results/FiltersDetails/FiltersDetails.tsx







var FiltersDetails_jsx = external_react_["createElement"];

function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }











const DEFAULT_TIMES_RANGE = {
  from: 0,
  to: 24
};
const DEFAULT_PRICE_RANGE = {
  from: 1000,
  to: 50000
};

class FiltersDetails_FilterDetails extends external_react_["Component"] {
  constructor(props) {
    super(props);
    this.transferCheckboxes = void 0;
    this.changeCoastRange = Object(external_lodash_["debounce"])(() => this.props.changeCoastFilter({
      from: this.state.ticketRange.from,
      to: this.state.ticketRange.to
    }), 600);
    this.changeTimeRange = Object(external_lodash_["debounce"])(index => this.props.changeTimingFilters(index, this.state.timeSegments), 600);

    this.onDepartureTimeRangeChange = (from, to, key) => {
      const prevState = this.state.timeSegments[key];

      if (!prevState) {
        return;
      }

      this.setState({
        timeSegments: _objectSpread({}, this.state.timeSegments, {
          [key]: _objectSpread({}, this.state.timeSegments[key], {
            departureTimeRange: {
              from,
              to
            }
          })
        })
      }, () => this.changeTimeRange(`${key}`));
    };

    this.onArrivalTimeRangeChange = (from, to, key) => {
      const prevState = this.state.timeSegments[key];

      if (!prevState) {
        return;
      }

      this.setState({
        timeSegments: _objectSpread({}, this.state.timeSegments, {
          [key]: _objectSpread({}, this.state.timeSegments[key], {
            arrivalTimeRange: {
              from,
              to
            }
          })
        })
      }, () => this.changeTimeRange(`${key}`));
    };

    this.onTicketPriceChangeRange = (from, to) => this.setState({
      ticketRange: {
        from,
        to
      }
    }, this.changeCoastRange);

    this.formatTimeLabel = time => {
      return `${time}:00`;
    };

    this.formatePriceLabel = price => {
      // const priceString = `${price}`;
      // return price > 1000 ? `${Math.floor(price/1000)} 000 ₽` : `${price} ₽`;
      return `${price} ₽`;
    };

    this.onTransferCheckboxToggle = id => {
      const filter = {
        [id]: !this.props.transfers[id]
      };
      this.props.changeTransfersFilter(filter);
    };

    this.onTransferCheckBoxAll = () => {
      const {
        changeTransfersFilter,
        transfers
      } = this.props;
      changeTransfersFilter(isAllTrue(transfers) ? makeAllValuesFalse(transfers) : makeAllValuesTrue(transfers));
    };

    this.onStraightWayToggle = () => {
      const {
        transfers,
        changeTransfersFilter
      } = this.props;

      let currentFilters = _objectSpread({}, transfers);

      if (isOneTrue(transfers, 0)) {
        currentFilters = makeAllValuesTrue(currentFilters);
      } else {
        currentFilters = makeAllValuesFalse(currentFilters);
        currentFilters[0] = true;
      }

      changeTransfersFilter(currentFilters);
    };

    this.onIATAToggle = () => {
      const {
        iata,
        changeIATAFilter
      } = this.props;

      if (iata) {
        changeIATAFilter('');
      } else {
        changeIATAFilter('DP');
      }
    };

    this.onBaggageIncludedToggle = () => {
      const {
        isBaggageIncluded,
        setBaggageIncludedFilter
      } = this.props;
      setBaggageIncludedFilter(isBaggageIncluded === 'all' ? 'without' : 'all');
    };

    this.onMorningToggle = () => {
      const isMorinigFilterEnabled = this.state.timeSegments['0'].departureTimeRange.from === 7 && this.state.timeSegments['0'].departureTimeRange.to === 12;
      this.onDepartureTimeRangeChange(isMorinigFilterEnabled ? 0 : 7, isMorinigFilterEnabled ? 24 : 12, 0);
    };

    this.onFastestToggle = () => {
      const {
        sort,
        changeSortingFilter
      } = this.props;
      const isFastetsSortEnabled = sort.key === 'total-time' && sort.dir === 'asc';
      changeSortingFilter({
        key: isFastetsSortEnabled ? 'coast' : 'total-time',
        dir: 'asc'
      });
    };

    this.onSubmit = e => {
      e.preventDefault();
      this.props.onCloseClick();
    };

    const timeSegments = {
      '0': {
        departureTimeRange: DEFAULT_TIMES_RANGE,
        arrivalTimeRange: DEFAULT_TIMES_RANGE
      },
      '1': {
        departureTimeRange: DEFAULT_TIMES_RANGE,
        arrivalTimeRange: DEFAULT_TIMES_RANGE
      }
    };
    this.state = {
      timeSegments,
      ticketRange: DEFAULT_PRICE_RANGE
    };
    this.transferCheckboxes = [{
      id: 0,
      name: 'no-transfer',
      label: 'Без пересадок'
    }, {
      id: 1,
      name: 'one-transfer',
      label: '1 пересадка'
    }, {
      id: 2,
      name: 'two-transfer',
      label: '2 пересадки'
    }];
  }

  render() {
    const {
      transfers,
      iata,
      tickets,
      isMobileLayout,
      isVisible,
      isBaggageIncluded,
      sort,
      onCloseClick,
      airports
    } = this.props;
    const isMorinigFilterEnabled = this.state.timeSegments['0'].departureTimeRange.from === 7 && this.state.timeSegments['0'].departureTimeRange.to === 12;
    return FiltersDetails_jsx("form", {
      className: `filters ${isMobileLayout && isVisible ? 'visible' : ''}`,
      onSubmit: this.onSubmit
    }, isMobileLayout && FiltersDetails_jsx("div", {
      className: "filter-header"
    }, FiltersDetails_jsx("span", {
      className: "reset"
    }), FiltersDetails_jsx("span", null, "\u0424\u0438\u043B\u044C\u0442\u0440\u044B"), FiltersDetails_jsx("img", {
      src: '/images/X.svg',
      className: "filters",
      alt: 'close',
      onClick: onCloseClick
    })), FiltersDetails_jsx(FilterDropdownSection, {
      className: "popular",
      title: 'Популярные фильтры'
    }, FiltersDetails_jsx(external_react_["Fragment"], null, FiltersDetails_jsx(shared_Checkbox, {
      fullColored: true,
      checked: sort.dir === 'asc' && sort.key === 'total-time',
      onToggle: this.onFastestToggle
    }, "\u0421\u0430\u043C\u044B\u0439 \u0431\u044B\u0441\u0442\u0440\u044B\u0439"), FiltersDetails_jsx(shared_Checkbox, {
      fullColored: true,
      checked: isOneTrue(transfers, '0'),
      onToggle: this.onStraightWayToggle
    }, "\u041F\u0440\u044F\u043C\u043E\u0439"), FiltersDetails_jsx(shared_Checkbox, {
      fullColored: true,
      checked: isMorinigFilterEnabled,
      onToggle: this.onMorningToggle
    }, "\u0423\u0442\u0440\u0435\u043D\u043D\u0438\u0439"), FiltersDetails_jsx(shared_Checkbox, {
      fullColored: true,
      checked: isBaggageIncluded === 'without',
      onToggle: this.onBaggageIncludedToggle
    }, "\u0411\u0435\u0437 \u0431\u0430\u0433\u0430\u0436\u0430"), FiltersDetails_jsx(shared_Checkbox, {
      fullColored: true,
      checked: !!iata,
      onToggle: this.onIATAToggle
    }, "\u041F\u043E\u0431\u0435\u0434\u0430"))), FiltersDetails_jsx(FilterDropdownSection, {
      className: "change",
      title: 'Пересадки'
    }, FiltersDetails_jsx(external_react_["Fragment"], null, FiltersDetails_jsx(shared_Checkbox, {
      id: "all-transfer",
      withIcon: true,
      onToggle: this.onTransferCheckBoxAll,
      checked: isAllTrue(transfers)
    }, "\u0412\u0441\u0435"), this.transferCheckboxes.map(checkbox => {
      const onToggle = () => this.onTransferCheckboxToggle(checkbox.id);

      return FiltersDetails_jsx(shared_Checkbox, {
        key: checkbox.id,
        checked: transfers[checkbox.id],
        id: checkbox.name,
        withIcon: true,
        onToggle: onToggle
      }, checkbox.label);
    }))), FiltersDetails_jsx(FilterDropdownSection, {
      className: "duration",
      title: 'Время'
    }, !!tickets.length && tickets[0].segments.map((segment, index) => {
      const departureAirport = airports[segment.flightChunks[0].departureAirport];
      const arrivalAirport = airports[segment.flightChunks[segment.flightChunks.length - 1].destinationAirport];
      const departureName = index === 0 ? departureAirport.cases.ro : arrivalAirport.cases.ro;
      const arrivalName = index === 0 ? arrivalAirport.cases.vi : departureAirport.cases.vi;

      const onDepartureRangeChange = (from, to) => this.onDepartureTimeRangeChange(from, to, index);

      const onArrivalRangeChange = (from, to) => this.onArrivalTimeRangeChange(from, to, index);

      return FiltersDetails_jsx("div", {
        key: index,
        className: 'duration__filter_item'
      }, FiltersDetails_jsx("span", null, "\u0412\u044B\u043B\u0435\u0442 \u0438\u0437 ", departureName), FiltersDetails_jsx(shared_RangeSlider, {
        minValue: DEFAULT_TIMES_RANGE.from,
        maxValue: DEFAULT_TIMES_RANGE.to,
        currentMinValue: this.state.timeSegments[index].departureTimeRange.from,
        currentMaxValue: this.state.timeSegments[index].departureTimeRange.to,
        maxDisabled: true,
        formatLabel: this.formatTimeLabel,
        onRangeChange: onDepartureRangeChange
      }), FiltersDetails_jsx("span", null, "\u041F\u0440\u0438\u0431\u044B\u0442\u0438\u0435 ", arrivalName), FiltersDetails_jsx(shared_RangeSlider, {
        minValue: DEFAULT_TIMES_RANGE.from,
        maxValue: DEFAULT_TIMES_RANGE.to,
        currentMinValue: this.state.timeSegments[index].arrivalTimeRange.from,
        currentMaxValue: this.state.timeSegments[index].arrivalTimeRange.to,
        maxDisabled: true,
        formatLabel: this.formatTimeLabel,
        onRangeChange: onArrivalRangeChange
      }));
    })), FiltersDetails_jsx(FilterDropdownSection, {
      className: "price",
      title: 'Цена на билет'
    }, FiltersDetails_jsx(shared_RangeSlider, {
      minValue: DEFAULT_PRICE_RANGE.from,
      maxValue: DEFAULT_PRICE_RANGE.to,
      currentMinValue: this.state.ticketRange.from,
      currentMaxValue: this.state.ticketRange.to,
      maxDisabled: true,
      formatLabel: this.formatePriceLabel,
      onRangeChange: this.onTicketPriceChangeRange
    })), isMobileLayout && FiltersDetails_jsx("input", {
      type: "submit",
      className: "search-btn filter-search",
      name: "",
      value: "\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C",
      onClick: onCloseClick
    }));
  }

}

/* harmony default export */ var FiltersDetails = (Object(external_react_redux_["connect"])(state => ({
  transfers: state.filters.transfers,
  coast: state.filters.coast,
  iata: state.filters.iata,
  timig: state.filters.timing,
  tickets: state.searchTickets.tickets,
  airports: state.searchTickets.airports,
  pointInfo: state.pointInfo.points,
  isBaggageIncluded: state.filters.baggageIncluded,
  sort: state.filters.sort
}), dispatch => ({
  changeTransfersFilter: filter => dispatch(FiltersActions_changeTransfersFilter(filter)),
  changeCoastFilter: filter => dispatch(changeCoastFilter(filter)),
  changeTimingFilters: (key, filter) => dispatch(changeTimingFilters(key, filter)),
  changeIATAFilter: iata => dispatch(changeIATAFilter(iata)),
  changeSortingFilter: filter => dispatch(changeSortFilter(filter)),
  setBaggageIncludedFilter: baggageIncluded => dispatch(setBaggageIncludedFilter(baggageIncluded))
}))(FiltersDetails_FilterDetails));
// EXTERNAL MODULE: ./src/components/application/results/ResultSorting/styles.scss
var ResultSorting_styles = __webpack_require__("iqkk");

// EXTERNAL MODULE: ./src/components/application/results/ResultSorting/styles.mobile.scss
var ResultSorting_styles_mobile = __webpack_require__("FOf+");

// CONCATENATED MODULE: ./src/components/application/results/ResultSorting/index.tsx
var ResultSorting_jsx = external_react_["createElement"];







class ResultSorting_ResultSorting extends external_react_["Component"] {
  constructor(...args) {
    super(...args);

    this.changeSorting = sortKey => {
      const {
        changeSortingFilter,
        sortBy
      } = this.props;
      const nextSortFilter = {
        key: sortKey,
        dir: 'asc'
      };

      if (sortBy && sortBy.key) {
        if (sortBy.key === sortKey) {
          switch (sortBy.dir) {
            case 'asc':
              nextSortFilter.dir = 'desc';
              break;

            case 'desc':
              nextSortFilter.key = undefined;
              nextSortFilter.dir = undefined;
              break;

            default:
              nextSortFilter.key = sortKey;
              nextSortFilter.dir = 'asc';
              break;
          }
        }
      }

      changeSortingFilter(nextSortFilter);
    };

    this.toggleDepartureSort = () => this.changeSorting('departure');

    this.toggleTotalTimeSort = () => this.changeSorting('total-time');

    this.toggleCoastSort = () => this.changeSorting('coast');
  }

  render() {
    const {
      sortBy,
      isMobileLayout,
      onFiltersShowClick
    } = this.props;
    return ResultSorting_jsx("div", {
      className: "results-filter"
    }, isMobileLayout && ResultSorting_jsx(shared_Checkbox, {
      fullColored: true,
      checked: false,
      onToggle: onFiltersShowClick
    }, "\u0424\u0438\u043B\u044C\u0442\u0440\u044B", ResultSorting_jsx("svg", {
      width: "16",
      height: "16",
      viewBox: "0 0 16 16",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, ResultSorting_jsx("path", {
      d: "M7.79846 7.53351H0.449719C0.199875 7.53351 0 7.73339 0 7.98323C0 8.23307 0.199875 8.43295 0.449719 8.43295H7.79846C8.00833 9.47563 8.93108 10.2651 10.0371 10.2651C11.143 10.2651 12.0658 9.47563 12.2757 8.43295H15.547C15.7968 8.43295 15.9967 8.23307 15.9967 7.98323C15.9967 7.73339 15.7968 7.53351 15.547 7.53351H12.2757C12.0658 6.49083 11.143 5.70132 10.0371 5.70132C8.93442 5.70132 8.00833 6.4875 7.79846 7.53351ZM11.4195 7.98323C11.4195 8.74609 10.7999 9.3657 10.0371 9.3657C9.2742 9.3657 8.65459 8.74609 8.65459 7.98323C8.65459 7.22037 9.2742 6.60076 10.0371 6.60076C10.7999 6.60076 11.4195 7.22037 11.4195 7.98323Z",
      fill: "#C4C4C4"
    }), ResultSorting_jsx("path", {
      d: "M4.02748 13.0402H0.449719C0.199875 13.0402 0 13.2401 0 13.4899C0 13.7398 0.199875 13.9397 0.449719 13.9397H4.02748C4.23735 14.9823 5.16011 15.7719 6.26608 15.7719C7.37206 15.7719 8.29482 14.9823 8.50469 13.9397H15.547C15.7968 13.9397 15.9967 13.7398 15.9967 13.4899C15.9967 13.2401 15.7968 13.0402 15.547 13.0402H8.50135C8.29148 11.9975 7.36873 11.208 6.26275 11.208C5.15678 11.208 4.23735 11.9975 4.02748 13.0402ZM7.64855 13.4899C7.64855 14.2528 7.02894 14.8724 6.26608 14.8724C5.50323 14.8724 4.88362 14.2528 4.88362 13.4899C4.88362 12.7271 5.50323 12.1075 6.26608 12.1075C7.02894 12.1075 7.64855 12.7304 7.64855 13.4899Z",
      fill: "#C4C4C4"
    }), ResultSorting_jsx("path", {
      d: "M4.02748 2.06049L0.449719 2.06049C0.199875 2.06049 0 2.26036 0 2.51021C0 2.76005 0.199875 2.95993 0.449719 2.95993H4.02748C4.23735 4.00261 5.16011 4.79211 6.26608 4.79211C7.37206 4.79211 8.29482 4.00261 8.50469 2.95993L15.5503 2.95993C15.8001 2.95993 16 2.76005 16 2.51021C16 2.26036 15.8001 2.06049 15.5503 2.06049L8.50135 2.06049C8.29148 1.01781 7.36873 0.2283 6.26275 0.2283C5.15678 0.2283 4.23735 1.01448 4.02748 2.06049ZM7.64855 2.51021C7.64855 3.27306 7.02894 3.89268 6.26608 3.89268C5.50323 3.89268 4.88362 3.27306 4.88362 2.51021C4.88362 1.74735 5.50323 1.12774 6.26608 1.12774C7.02894 1.12774 7.64855 1.74735 7.64855 2.51021Z",
      fill: "#C4C4C4"
    }))), ResultSorting_jsx(shared_Checkbox, {
      fullColored: true,
      className: sortBy.key === 'departure' ? sortBy.dir : '',
      checked: sortBy.key === 'departure',
      onToggle: this.toggleDepartureSort
    }, "\u0412\u0440\u0435\u043C\u044F \u043E\u0442\u043F\u0440\u0430\u0432\u043B\u0435\u043D\u0438\u044F", ResultSorting_jsx("svg", {
      width: "16",
      height: "12",
      viewBox: "0 0 16 12",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, ResultSorting_jsx("path", {
      d: "M0 0.666708H5.33333V2.44449H0L0 0.666708ZM0 11.3334L0 9.5556L16 9.5556V11.3334L0 11.3334ZM0 5.11115L10.6667 5.11115V6.88893L0 6.88893L0 5.11115Z",
      fill: "#E5E5E5"
    }))), ResultSorting_jsx(shared_Checkbox, {
      fullColored: true,
      className: sortBy.key === 'total-time' ? sortBy.dir : '',
      checked: sortBy.key === 'total-time',
      onToggle: this.toggleTotalTimeSort
    }, "\u0412\u0440\u0435\u043C\u0435\u043D\u0438 \u0432 \u043F\u0443\u0442\u0438", ResultSorting_jsx("svg", {
      width: "16",
      height: "12",
      viewBox: "0 0 16 12",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, ResultSorting_jsx("path", {
      d: "M0 0.666708H5.33333V2.44449H0L0 0.666708ZM0 11.3334L0 9.5556L16 9.5556V11.3334L0 11.3334ZM0 5.11115L10.6667 5.11115V6.88893L0 6.88893L0 5.11115Z",
      fill: "#E5E5E5"
    }))), ResultSorting_jsx(shared_Checkbox, {
      fullColored: true,
      className: sortBy.key === 'coast' ? sortBy.dir : '',
      checked: sortBy.key === 'coast',
      onToggle: this.toggleCoastSort
    }, "\u0426\u0435\u043D\u0430", ResultSorting_jsx("svg", {
      width: "16",
      height: "12",
      viewBox: "0 0 16 12",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, ResultSorting_jsx("path", {
      d: "M0 0.666708H5.33333V2.44449H0L0 0.666708ZM0 11.3334L0 9.5556L16 9.5556V11.3334L0 11.3334ZM0 5.11115L10.6667 5.11115V6.88893L0 6.88893L0 5.11115Z",
      fill: "#E5E5E5"
    }))));
  }

}

/* harmony default export */ var results_ResultSorting = (Object(external_react_redux_["connect"])(state => ({
  sortBy: state.filters.sort
}), dispatch => ({
  changeSortingFilter: filter => dispatch(changeSortFilter(filter))
}))(ResultSorting_ResultSorting));
// EXTERNAL MODULE: external "moment/locale/ru"
var ru_ = __webpack_require__("gvj5");

// EXTERNAL MODULE: ./src/components/application/results/ResultDetailedItem/style.scss
var ResultDetailedItem_style = __webpack_require__("3Bhi");

// EXTERNAL MODULE: ./src/components/application/results/ResultDetailedItem/style.mobile.scss
var style_mobile = __webpack_require__("BdYQ");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js + 1 modules
var objectWithoutProperties = __webpack_require__("qNsG");

// CONCATENATED MODULE: ./src/store/utils/getFormattedDuration.ts
const getFormattedDuration = duration => `${Math.floor(duration / 60)}ч ${duration % 60}м`;
// EXTERNAL MODULE: external "axios"
var external_axios_ = __webpack_require__("zr5I");
var external_axios_default = /*#__PURE__*/__webpack_require__.n(external_axios_);

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__("uhWA");

// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__("No/t");

// CONCATENATED MODULE: ./src/components/application/results/ResultsItemSegment/ResultsItemSegment.tsx

var ResultsItemSegment_jsx = external_react_["createElement"];








external_moment_default.a.locale('ru');

const ResultsItemSegment = (_ref) => {
  let {
    segment,
    searchId,
    ticket,
    index,
    price,
    isMobileLayout,
    airports
  } = _ref,
      props = Object(objectWithoutProperties["a" /* default */])(_ref, ["segment", "searchId", "ticket", "index", "price", "isMobileLayout", "airports"]);

  const [isLoading, setLoading] = external_react_["useState"](false);
  const [termsParams, setTermsParams] = external_react_["useState"]({});

  const getRedirectUrl = async termsUrl => {
    const response = await external_axios_default.a.post(`${config["a" /* Config */].apiUrl}/getBuyUrl`, {
      searchId,
      ticketId: termsUrl
    });
    setTermsParams({
      gateId: response.data.gateId,
      clickId: response.data.clickId
    });
    return response.data.url;
  };

  const onBuyClick = async () => {
    setLoading(true);
    const redirectData = await getRedirectUrl(ticket.generateUrlId.toString());
    setTimeout(() => {
      setLoading(false);
      setTermsParams({});

      if (redirectData) {
        window.open(redirectData);
      } else {
        console.error('Error on get redirect URL', redirectData);
      }
    }, 500);
  };

  const onItemClick = () => {
    if (isMobileLayout) {
      onBuyClick();
    }
  };

  const getTransfer = transferAirport => {
    const transfer = segment.transfers.find(transfer => transfer.to === transferAirport);
    const totalDuration = segment.totalDuration;
    const durationFraction = transfer.duration.seconds / 60 / totalDuration * 100;
    return ResultsItemSegment_jsx("span", {
      key: `${durationFraction}_${index}`,
      className: `line`,
      style: {
        width: `${durationFraction}%`
      }
    });
  };

  return ResultsItemSegment_jsx(external_react_["Fragment"], null, termsParams.gateId && ResultsItemSegment_jsx("img", {
    src: `//yasen.aviasales.ru/adaptors/pixel_click.png?click_id=${termsParams.clickId}&gate_id=${termsParams.gateId}`,
    width: "0",
    height: "0",
    id: "pixel"
  }), ResultsItemSegment_jsx("div", {
    className: "results-item",
    onClick: onItemClick
  }, ResultsItemSegment_jsx("div", {
    className: "logos"
  }, segment.carrierLogoUrls.map((url, index) => ResultsItemSegment_jsx("div", {
    key: `${url}_${index}`,
    className: 'carrier-icon',
    style: {
      backgroundImage: `url('${url}')`
    }
  }))), ResultsItemSegment_jsx("div", {
    className: "time-desc"
  }, ResultsItemSegment_jsx("div", {
    className: "departure"
  }, ResultsItemSegment_jsx("span", {
    className: "day"
  }, external_moment_default()(segment.flightChunks[0].departureDate).format('D MMM')), ResultsItemSegment_jsx("span", {
    className: "time"
  }, ResultsItemSegment_jsx("span", null, segment.flightChunks[0].departureTime), isMobileLayout && ResultsItemSegment_jsx(external_react_["Fragment"], null, ResultsItemSegment_jsx("span", {
    className: 'transfer-in-time'
  }, segment.transfers && segment.transfers.length > 0 ? getFormattedDuration(segment.transfers.reduce((s, c) => s + c.duration.seconds, 0) / 60) : '–'), ResultsItemSegment_jsx("span", null, segment.flightChunks[segment.flightChunks.length - 1].destinationTime))), ResultsItemSegment_jsx("span", {
    className: "iata-code"
  }, ResultsItemSegment_jsx("span", null, segment.flightChunks[0].departureAirport), isMobileLayout && ResultsItemSegment_jsx("span", null, segment.flightChunks[segment.flightChunks.length - 1].destinationAirport, segment.daysDiff > 0 && ResultsItemSegment_jsx("div", {
    className: "days-diff"
  }, "+", segment.daysDiff))), ResultsItemSegment_jsx("span", {
    className: "air-name"
  }, airports[segment.flightChunks[0].departureAirport].name)), ResultsItemSegment_jsx("div", {
    className: "time-line"
  }, ResultsItemSegment_jsx("span", {
    className: "trip-duration"
  }, isMobileLayout ? ResultsItemSegment_jsx(external_react_["Fragment"], null, segment.transfers && segment.transfers.length > 0 ? ResultsItemSegment_jsx("span", null, "\u041F\u0435\u0440\u0435\u0441\u0430\u0434\u043A", segment.flightChunks.length > 2 ? 'и' : 'а', segment.transfers.map((transfer, index) => ResultsItemSegment_jsx(external_react_["Fragment"], {
    key: `${transfer.at}_${transfer.to}__${index}`
  }, ResultsItemSegment_jsx("span", {
    className: "transfer-code"
  }, "[", transfer.at === transfer.to ? transfer.at : `${transfer.at}→${transfer.to}`, "]"), ":", ResultsItemSegment_jsx("span", {
    className: "transfer-dur"
  }, getFormattedDuration(transfer.duration.seconds / 60))))) : ResultsItemSegment_jsx("span", null, getFormattedDuration(segment.totalDuration), ",  \u041F\u0440\u044F\u043C\u043E\u0439")) : ResultsItemSegment_jsx(external_react_["Fragment"], null, "\u0412 \u043F\u0443\u0442\u0438:", ResultsItemSegment_jsx("span", {
    className: "duration-trip-time"
  }, getFormattedDuration(segment.totalDuration)))), ResultsItemSegment_jsx("div", {
    className: "line-holder"
  }, segment.flightChunks.map((chunk, index) => ResultsItemSegment_jsx(external_react_["Fragment"], {
    key: `${chunk.durationFraction}_${index}`
  }, index > 0 && getTransfer(chunk.departureAirport), ResultsItemSegment_jsx("span", {
    className: `line`,
    style: {
      width: `${chunk.durationFraction * 100}%`
    }
  })))), !isMobileLayout && (segment.flightChunks.length === 1 ? ResultsItemSegment_jsx("span", {
    className: "transfer"
  }, "\u041F\u0440\u044F\u043C\u043E\u0439") : ResultsItemSegment_jsx("span", {
    className: "transfer"
  }, "\u041F\u0435\u0440\u0435\u0441\u0430\u0434\u043A", segment.flightChunks.length > 2 ? 'и' : 'а', segment.transfers.map((transfer, index) => ResultsItemSegment_jsx(external_react_["Fragment"], {
    key: `${transfer.at}_${transfer.to}__${index}`
  }, ResultsItemSegment_jsx("br", null), ResultsItemSegment_jsx("span", {
    className: "transfer-code"
  }, "[", transfer.at === transfer.to ? transfer.at : `${transfer.at}→${transfer.to}`, "]"), ":", ResultsItemSegment_jsx("span", {
    className: "transfer-dur"
  }, getFormattedDuration(transfer.duration.seconds / 60))))))), !isMobileLayout && ResultsItemSegment_jsx("div", {
    className: "arrival"
  }, segment.daysDiff > 0 && ResultsItemSegment_jsx("div", {
    className: "days-diff"
  }, "+", segment.daysDiff), ResultsItemSegment_jsx("span", {
    className: "day"
  }, external_moment_default()(segment.flightChunks[segment.flightChunks.length - 1].destinationDate).format('D MMM')), ResultsItemSegment_jsx("span", {
    className: "time"
  }, segment.flightChunks[segment.flightChunks.length - 1].destinationTime), ResultsItemSegment_jsx("span", {
    className: "iata-code"
  }, segment.flightChunks[segment.flightChunks.length - 1].destinationAirport), ResultsItemSegment_jsx("span", {
    className: "air-name"
  }, airports[segment.flightChunks[segment.flightChunks.length - 1].destinationAirport].name))), !isMobileLayout && ResultsItemSegment_jsx(external_react_["Fragment"], null, price && index === 0 ? ResultsItemSegment_jsx("div", {
    className: "buy-info"
  }, ResultsItemSegment_jsx("div", {
    className: "buy",
    onClick: onBuyClick
  }, ResultsItemSegment_jsx("span", {
    className: "buy-price"
  }, "\u041A\u0443\u043F\u0438\u0442\u044C ", ResultsItemSegment_jsx("br", null), " \u0437\u0430 ", price, " \u20BD"), isLoading && ResultsItemSegment_jsx("div", {
    className: 'loading-mask'
  }, ResultsItemSegment_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faSpinner"],
    spin: true,
    size: "3x"
  }))), ResultsItemSegment_jsx("div", null, ticket.baggage ? ResultsItemSegment_jsx("div", {
    className: "baggage with"
  }, "\u0411\u0430\u0433\u0430\u0436 \u0432\u043A\u043B\u044E\u0447\u0435\u043D") : ResultsItemSegment_jsx("div", {
    className: "baggage without"
  }, "\u0411\u0435\u0437 \u0431\u0430\u0433\u0430\u0436\u0430"))) : ResultsItemSegment_jsx("div", {
    className: "fake-button"
  })), isMobileLayout && ResultsItemSegment_jsx(external_react_["Fragment"], null, price && index === 0 && ResultsItemSegment_jsx("span", {
    className: "buy"
  }, !isLoading && ResultsItemSegment_jsx(external_react_["Fragment"], null, price, " \u20BD", ResultsItemSegment_jsx("div", null, ticket.baggage ? ResultsItemSegment_jsx("div", {
    className: "baggage with"
  }, "\u0411\u0430\u0433\u0430\u0436 \u0432\u043A\u043B\u044E\u0447\u0435\u043D") : ResultsItemSegment_jsx("div", {
    className: "baggage without"
  }, "\u0411\u0435\u0437 \u0431\u0430\u0433\u0430\u0436\u0430"))), isLoading && ResultsItemSegment_jsx("div", {
    className: 'loading-mask'
  }, ResultsItemSegment_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faSpinner"],
    spin: true,
    size: "3x"
  }))))), index !== ticket.segments.length - 1 && ResultsItemSegment_jsx("div", {
    className: 'horizontal-divider'
  }));
};

/* harmony default export */ var ResultsItemSegment_ResultsItemSegment = (Object(external_react_redux_["connect"])(state => ({
  searchId: state.searchTickets.searchId,
  airports: state.searchTickets.airports
}), null)(ResultsItemSegment));
// CONCATENATED MODULE: ./src/components/application/results/ResultDetailedItem/ResultDetailedItem.tsx
var ResultDetailedItem_jsx = external_react_["createElement"];





class ResultDetailedItem_ResultDetailedItem extends external_react_["Component"] {
  render() {
    const {
      ticket,
      isMobileLayout
    } = this.props;
    return ResultDetailedItem_jsx("div", {
      className: "results-item-wrapper"
    }, ticket.segments.length && ticket.segments.map((segment, index) => ResultDetailedItem_jsx(ResultsItemSegment_ResultsItemSegment, {
      segment: segment,
      key: index,
      ticket: ticket,
      index: index,
      price: ticket.price,
      isMobileLayout: isMobileLayout
    })));
  }

}
// EXTERNAL MODULE: ./src/store/utils/prepareDate.ts
var prepareDate = __webpack_require__("78gN");

// EXTERNAL MODULE: ./src/utils/index.ts
var utils = __webpack_require__("0lfv");

// EXTERNAL MODULE: ./node_modules/next/dist/next-server/lib/head.js
var head = __webpack_require__("8Kt/");
var head_default = /*#__PURE__*/__webpack_require__.n(head);

// EXTERNAL MODULE: external "lodash/sample"
var sample_ = __webpack_require__("SeML");
var sample_default = /*#__PURE__*/__webpack_require__.n(sample_);

// CONCATENATED MODULE: ./src/containers/ResultsDetailedContainer/index.tsx









var ResultsDetailedContainer_jsx = external_react_default.a.createElement;

function ResultsDetailedContainer_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function ResultsDetailedContainer_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ResultsDetailedContainer_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ResultsDetailedContainer_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }





















class ResultsDetailedContainer_ResultsDetailedContainer extends external_react_default.a.Component {
  constructor(props) {
    super(props);
    this.io = void 0;
    this.defaultSearchValues = void 0;

    this.onDocumentResize = () => this.changeLayout(window.innerWidth <= 425);

    this.changeLayout = Object(external_lodash_["debounce"])(isMobileLayout => this.setState({
      isMobileLayout
    }), 500);

    this.getRouteParams = defaultDepartureDate => {
      const departureAirport = router_default.a.query.departure_iata;
      const arrivalAirport = router_default.a.query.arrival_iata;
      const departureDate = defaultDepartureDate || Object(utils["d" /* deserializeURLDate */])(router_default.a.query.dept_date);
      const returnDate = Object(utils["d" /* deserializeURLDate */])(router_default.a.query.ret_date);
      const passengersAmount = router_default.a.query.passengers ? {
        adults: parse_int_default()(router_default.a.query.passengers[0]),
        children: parse_int_default()(router_default.a.query.passengers[1]),
        infants: parse_int_default()(router_default.a.query.passengers[2])
      } : {
        adults: 1,
        children: 0,
        infants: 0
      };
      const searchQuery = {
        locale: 'ru',
        tripClass: 'Y',
        passengers: passengersAmount,
        segments: [{
          origin: departureAirport,
          destination: arrivalAirport,
          date: Object(prepareDate["a" /* prepareDate */])(departureDate)
        }]
      };

      if (returnDate) {
        searchQuery.segments.push({
          origin: arrivalAirport,
          destination: departureAirport,
          date: Object(prepareDate["a" /* prepareDate */])(returnDate)
        });
      }

      return searchQuery;
    };

    this.onSearchPress = params => {
      this.props.setResultsLimit(10);

      if (params.origin) {
        if (!params.destination) {
          return router_default.a.push(Object(utils["a" /* buildUrlByParams */])(params));
        }
      } else {
        return router_default.a.push('/');
      }

      const searchQuery = {
        locale: 'ru',
        tripClass: 'Y',
        passengers: {
          adults: params.adults,
          children: params.children,
          infants: params.infants
        },
        segments: [{
          origin: params.origin.place.code,
          destination: params.destination.place.code,
          date: Object(prepareDate["a" /* prepareDate */])(params.departureDate)
        }]
      };

      if (params.returnDate) {
        searchQuery.segments.push({
          origin: params.destination.place.code,
          destination: params.origin.place.code,
          date: Object(prepareDate["a" /* prepareDate */])(params.returnDate)
        });
      }

      const nextURL = Object(utils["a" /* buildUrlByParams */])(params);
      router_default.a.replace(nextURL, nextURL, {
        shallow: true
      });
      this.searchTickets(searchQuery);
    };

    this.searchTickets = searchQuery => {
      this.props.searchTickets(searchQuery).then(res => {
        this.io.on(`search/${res.payload.data.searchId}`, msg => {
          this.props.searchTicketsIncomingChunk(JSON.parse(msg));
        });
      });
    };

    this.increaseResultsLimit = () => {
      this.props.setResultsLimit(this.props.currentResultsLimit + 10);
    };

    this.setDepartureDate = departureDate => {
      const {
        router: {
          query
        }
      } = this.props;
      const searchQuery = this.getRouteParams();
      searchQuery.segments[0].date = Object(prepareDate["a" /* prepareDate */])(departureDate);
      const urlParams = {
        departureDate: Object(utils["e" /* serializeURLDate */])(departureDate),
        origin: query.departure_iata,
        destination: query.arrival_iata,
        passengers: query.passengers
      };

      if (query.ret_date) {
        const timeDiff = Object(utils["d" /* deserializeURLDate */])(query.ret_date).getTime() - Object(utils["d" /* deserializeURLDate */])(query.dept_date).getTime();
        const retDate = new Date(departureDate.getTime() + timeDiff);
        searchQuery.segments[1].date = Object(prepareDate["a" /* prepareDate */])(retDate);
        urlParams.returnDate = Object(utils["e" /* serializeURLDate */])(retDate);
      }

      const nextURL = Object(utils["b" /* buildUrlByStringParams */])(urlParams);
      router_default.a.replace(nextURL, nextURL, {
        shallow: true
      });
      this.searchTickets(searchQuery);
      const routeParams = this.getRouteParams(departureDate);
      this.searchTickets(routeParams);
    };

    this.onShowFiltersModalClick = () => this.setState({
      isFiltersModalOpen: true
    });

    this.onCloseFiltersModalClick = () => this.setState({
      isFiltersModalOpen: false
    });

    this.io = external_socket_io_client_default()(config["a" /* Config */].apiSocketUrl);
    this.state = {
      isFiltersModalOpen: false
    };
    const {
      router: {
        query: _query
      },
      pointInfo
    } = props;
    const origin = pointInfo[_query.departure_iata];
    const destination = pointInfo[_query.arrival_iata];

    const _departureDate = Object(utils["d" /* deserializeURLDate */])(_query.dept_date);

    const _returnDate = Object(utils["d" /* deserializeURLDate */])(_query.ret_date);

    const passengers = Object(utils["c" /* deserializePassengers */])(_query.passengers);
    this.defaultSearchValues = ResultsDetailedContainer_objectSpread({
      origin,
      destination,
      departureDate: _departureDate,
      returnDate: _returnDate
    }, passengers);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onDocumentResize);
    const routeParams = this.getRouteParams();
    this.searchTickets(routeParams);
    this.setState({
      isMobileLayout: window.innerWidth <= 425
    });
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onDocumentResize);
  }

  render() {
    const {
      isMobileLayout,
      isFiltersModalOpen
    } = this.state;
    const {
      tickets,
      pricesCalendar,
      isLoading,
      searchResult,
      pointInfo,
      router: {
        query
      },
      currentResultsLimit,
      totalResults
    } = this.props;
    const cityFrom = pointInfo[query.departure_iata].city.name;
    const cityTo = pointInfo[query.arrival_iata].city.name;
    const departDate = Object(utils["d" /* deserializeURLDate */])(query.dept_date);
    return ResultsDetailedContainer_jsx(external_react_default.a.Fragment, null, ResultsDetailedContainer_jsx(head_default.a, null, ResultsDetailedContainer_jsx("title", null, "\u0414\u0435\u0448\u0435\u0432\u044B\u0435 \u0430\u0432\u0438\u0430\u0431\u0438\u043B\u0435\u0442\u044B ", cityFrom, " \u2014 ", cityTo, " \u043E\u0442 ", pricesCalendar.minimalPrice, " \u0440\u0443\u0431\u043B\u0435\u0439"), ResultsDetailedContainer_jsx("meta", {
      name: "description",
      content: `Мы ищем ${sample_default()(['дешевые', 'недорогие'])} билеты по направлению ${cityFrom} – ${cityTo} на всех сайтах и ` + `показываем самую ${sample_default()(['низкую', 'доступную'])} цену. ` + `Самый дешевый билет стоит от ${searchResult.minimalPrice} рублей. ${sample_default()(['Количество', 'Наличие'])} пересадок надо уточнять на сайте по запросу. ` + `Расписание рейсов постояно меняется, потому проверяйте онлайн прибытие и ${sample_default()(['отбытие', 'время отправления'])} самолетов.`
    }), ResultsDetailedContainer_jsx("meta", {
      name: "keywords",
      content: `${cityFrom}, ${cityTo}, авиа, дешевые, без пересадок, как добраться, на самолете, цена билета, стоимость, самолет, расписание рейсов, время вылета, прибытие самолета`
    }), ResultsDetailedContainer_jsx("link", {
      rel: "canonical",
      href: `https://lowtrip.ru/results/${query.departure_iata}/${query.arrival_iata}`
    })), ResultsDetailedContainer_jsx(header_filters["a" /* default */], {
      showLoading: isLoading,
      onSearchPress: this.onSearchPress,
      defaultValues: this.defaultSearchValues
    }, ResultsDetailedContainer_jsx(header_costs, {
      currentDepartDate: departDate,
      onDateClick: this.setDepartureDate
    })), ResultsDetailedContainer_jsx("div", {
      className: "content-wrapper"
    }, ResultsDetailedContainer_jsx(FiltersDetails, {
      isVisible: isFiltersModalOpen,
      isMobileLayout: isMobileLayout,
      onCloseClick: this.onCloseFiltersModalClick
    }), ResultsDetailedContainer_jsx("div", {
      className: "result"
    }, ResultsDetailedContainer_jsx("h1", {
      className: "results-header"
    }, "\u0421\u0430\u043C\u044B\u0435 \u0434\u0435\u0448\u0435\u0432\u044B\u0435 \u0431\u0438\u043B\u0435\u0442\u044B ", ResultsDetailedContainer_jsx("span", null, cityFrom), !!cityTo && ResultsDetailedContainer_jsx(external_react_default.a.Fragment, null, " \u2192 ", ResultsDetailedContainer_jsx("span", null, cityTo)), searchResult.minimalPrice < max_safe_integer_default.a - 1 && ResultsDetailedContainer_jsx("span", null, " \u043E\u0442 ", searchResult.minimalPrice, "\u20BD")), ResultsDetailedContainer_jsx(results_ResultSorting, {
      isMobileLayout: isMobileLayout,
      onFiltersShowClick: this.onShowFiltersModalClick
    }), ResultsDetailedContainer_jsx("div", {
      className: "results"
    }, ResultsDetailedContainer_jsx(external_react_default.a.Fragment, null, tickets.map((ticket, i) => ResultsDetailedContainer_jsx(ResultDetailedItem_ResultDetailedItem, {
      isMobileLayout: isMobileLayout,
      key: i,
      ticket: ticket
    })), !isLoading && !!tickets.length && currentResultsLimit < totalResults && ResultsDetailedContainer_jsx("div", {
      className: 'show-more-items',
      onClick: this.increaseResultsLimit
    }, "\u041F\u043E\u043A\u0430\u0437\u0430\u0442\u044C \u0435\u0449\u0451 ", Math.min(10, totalResults - currentResultsLimit), " \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442\u043E\u0432")), !tickets.length && !isLoading && ResultsDetailedContainer_jsx("div", {
      className: 'not-found'
    }, "\u041A\u0430\u0436\u0435\u0442\u0441\u044F, \u043D\u0430 \u044D\u0442\u0443 \u0434\u0430\u0442\u0443 \u043D\u0435\u0442 \u0431\u0438\u043B\u0435\u0442\u043E\u0432 \u0438\u043B\u0438 \u043E\u043D\u0438 \u0437\u0430\u043A\u043E\u043D\u0447\u0438\u043B\u0438\u0441\u044C. ", ResultsDetailedContainer_jsx("br", null), "\u041C\u043E\u0436\u0435\u0442 \u0431\u044B\u0442\u044C \u043F\u043E\u043F\u0440\u043E\u0431\u0443\u0435\u043C \u043F\u043E\u0438\u0441\u043A\u0430\u0442\u044C \u043D\u0430 \u0434\u0440\u0443\u0433\u0443\u044E \u0434\u0430\u0442\u0443?")), !!tickets.length && ResultsDetailedContainer_jsx("div", {
      className: 'seo-text'
    }, ResultsDetailedContainer_jsx("h2", {
      className: 'title'
    }, "\u0418\u043D\u0442\u0435\u0440\u0435\u0441\u043D\u044B\u0435 \u0434\u0430\u043D\u043D\u044B\u0435 \u043F\u043E \u043C\u0430\u0440\u0448\u0440\u0443\u0442\u0443 ", cityFrom, " \u2192 ", cityTo, " ",  false && false, " "), ResultsDetailedContainer_jsx("div", {
      className: 'info'
    }, "\u0415\u0441\u043B\u0438 \u043B\u0435\u0442\u0435\u0442\u044C \u0431\u0435\u0437 \u043F\u0435\u0440\u0435\u0441\u0430\u0434\u043E\u043A, \u0442\u043E \u0432\u0440\u0435\u043C\u044F \u0432 \u043F\u0443\u0442\u0438 \u0441\u043E\u0441\u0442\u0430\u0432\u043B\u044F\u0435\u0442: ", searchResult.minimalDuration, ".", ResultsDetailedContainer_jsx("br", null), "\u0421\u0430\u043C\u0430\u044F \u043D\u0438\u0437\u043A\u0430\u044F \u0437\u0430\u0444\u0438\u043A\u0441\u0438\u0440\u043E\u0432\u0430\u043D\u043D\u0430\u044F \u0446\u0435\u043D\u0430: ", searchResult.minimalPrice, " \u0440\u0443\u0431\u043B\u0435\u0439 , \u0430 \u0441\u0430\u043C\u0430 \u0432\u044B\u0441\u043E\u043A\u0430\u044F \u0446\u0435\u043D\u0430: ", searchResult.maximalPrice, " \u0440\u0443\u0431\u043B\u0435\u0439. ", ResultsDetailedContainer_jsx("br", null), "\u041F\u043E \u0434\u0430\u043D\u043D\u043E\u043C\u0443 \u0440\u0435\u0439\u0441\u0443 \u043B\u0435\u0442\u0430\u044E\u0442 \u0430\u0432\u0438\u0430\u043A\u043E\u043C\u043F\u0430\u043D\u0438\u0438:", searchResult.allCarriers.map((airline, index) => ResultsDetailedContainer_jsx("h3", {
      key: airline
    }, airline, index !== searchResult.allCarriers.length - 1 && ', ')), ".", ResultsDetailedContainer_jsx("br", null), "\u041D\u0430\u0437\u0432\u0430\u043D\u0438\u0435 \u0440\u0435\u0439\u0441\u043E\u0432: ", searchResult.allFlightNumbers.join(', '), ResultsDetailedContainer_jsx("br", null), "\u0412\u0440\u0435\u043C\u044F \u043F\u0440\u0438\u0431\u044B\u0442\u0438\u0435 \u0438 \u0432\u044B\u043B\u0435\u0442\u0430 \u0440\u0435\u0439\u0441\u043E\u0432 \u043B\u0443\u0447\u0448\u0435 \u0443\u0442\u043E\u0447\u043D\u044F\u0442\u044C \u043E\u043D\u043B\u0430\u0439\u043D. ", ResultsDetailedContainer_jsx("br", null))))));
  }

}

/* harmony default export */ var containers_ResultsDetailedContainer = (Object(external_react_redux_["connect"])(state => ({
  isLoading: state.searchTickets.isLoading,
  totalResults: getFilteredTicketsAmount(state),
  tickets: getFiltredTickets(state),
  searchResult: state.searchTickets,
  pricesCalendar: state.pricesCalendar,
  pointInfo: state.pointInfo.points,
  currentResultsLimit: state.filters.resultsLimit
}), dispatch => ({
  setResultsLimit: data => dispatch(setResultsLimit(data)),
  searchTickets: data => dispatch(searchTickets(data)),
  searchTicketsIncomingChunk: data => dispatch(searchTicketsIncomingChunk(data))
}))(Object(router_["withRouter"])(ResultsDetailedContainer_ResultsDetailedContainer)));
// EXTERNAL MODULE: ./src/store/modules/pointInfo/PointInfoActions.ts
var PointInfoActions = __webpack_require__("z3C8");

// CONCATENATED MODULE: ./src/pages/results/[departure_iata]/[arrival_iata].tsx
var _arrival_iata_jsx = external_react_default.a.createElement;











class _arrival_iata_ResultsDetailed extends external_react_default.a.Component {
  static async getInitialProps({
    query,
    res,
    store: {
      dispatch
    }
  }) {
    if (query && (!query.departure_iata || !query.arrival_iata || !query.dept_date)) {
      const redirect = url => {
        if (res) {
          res.writeHead(302, {
            Location: url
          });
          res.end();
        } else {
          router_default.a.push(url);
        }
      }; // TODO: Paste passengers check > 0
      //!query.passengers || parseInt(query.passengers) <= 0)


      if (query.dept_date && query.departure_iata && !query.arrival_iata) {
        const redirectRoute = Object(utils["b" /* buildUrlByStringParams */])({
          departureDate: query.dept_date,
          returnDate: query.ret_date,
          origin: query.departure_iata,
          passengers: query.passengers
        });
        return redirect(redirectRoute);
      }

      if (!query.departure_iata || !query.dept_date) {
        return redirect('/');
      }
    }

    try {
      const pointInfo = (await dispatch(Object(PointInfoActions["a" /* getPointInfo */])([query.departure_iata, query.arrival_iata]))).payload.data;
      await dispatch(getPricesCalendar({
        origin: pointInfo[query.departure_iata].city.code,
        destination: pointInfo[query.arrival_iata].city.code,
        departDate: Object(prepareDate["a" /* prepareDate */])(Object(utils["d" /* deserializeURLDate */])(query.dept_date)),
        returnDate: query.ret_date
      }));
    } catch (err) {
      console.error(err);
    }

    return {};
  }

  render() {
    return _arrival_iata_jsx(external_react_default.a.Fragment, null, _arrival_iata_jsx(containers_ResultsDetailedContainer, null));
  }

}

/* harmony default export */ var _arrival_iata_ = __webpack_exports__["default"] = (_arrival_iata_ResultsDetailed);

/***/ }),

/***/ "/0+H":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __importDefault(__webpack_require__("cDcd"));

const amp_context_1 = __webpack_require__("lwAK");

function isInAmpMode({
  ampFirst = false,
  hybrid = false,
  hasQuery = false
} = {}) {
  return ampFirst || hybrid && hasQuery;
}

exports.isInAmpMode = isInAmpMode;

function useAmp() {
  // Don't assign the context value to a variable to save bytes
  return isInAmpMode(react_1.default.useContext(amp_context_1.AmpStateContext));
}

exports.useAmp = useAmp;

/***/ }),

/***/ "/mnI":
/***/ (function(module, exports) {



/***/ }),

/***/ "0Zyy":
/***/ (function(module, exports) {



/***/ }),

/***/ "0lfv":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export serializePassengers */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return deserializePassengers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "e", function() { return serializeURLDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return deserializeURLDate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return buildUrlByParams; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return buildUrlByStringParams; });
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("6BQ9");
/* harmony import */ var _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("YLtl");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_1__);


const serializePassengers = ({
  adults = 1,
  children = 0,
  infants = 0
}) => `${adults}${children}${infants}`;
const deserializePassengers = (passengers = '') => {
  const defaultsPassengeers = {
    adults: 1,
    children: 0,
    infants: 0
  };

  if (!passengers) {
    return defaultsPassengeers;
  }

  return {
    adults: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[0]) || defaultsPassengeers.adults,
    children: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[1]) || defaultsPassengeers.children,
    infants: _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(passengers[2]) || defaultsPassengeers.infants
  };
};
const serializeURLDate = date => `${Object(lodash__WEBPACK_IMPORTED_MODULE_1__["padStart"])(date.getDate() + '', 2, '0')}${Object(lodash__WEBPACK_IMPORTED_MODULE_1__["padStart"])(date.getMonth() + 1 + '', 2, '0')}${date.getFullYear().toString(10).substr(2, 2)}`;
const deserializeURLDate = date => {
  if (!date) {
    return undefined;
  }

  return new Date(_babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(`20${date.substr(4, 2)}`), _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(date.substr(2, 2)) - 1, _babel_runtime_corejs2_core_js_parse_int__WEBPACK_IMPORTED_MODULE_0___default()(date.substr(0, 2)));
};
function buildUrlByParams(params) {
  let nextRoute = '';

  if (params.destination) {
    nextRoute = `/results/${params.origin.place.code}/${params.destination.place.code}`;
  } else {
    nextRoute = `/search/${params.origin.place.code}`;
  }

  nextRoute += `?dept_date=${serializeURLDate(params.departureDate)}`;

  if (params.returnDate) {
    nextRoute += `&ret_date=${serializeURLDate(params.returnDate)}`;
  }

  const passengers = serializePassengers({
    adults: params.adults,
    children: params.children,
    infants: params.infants
  });
  nextRoute += `&passengers=${passengers}`;
  return nextRoute;
}
function buildUrlByStringParams(params) {
  let nextRoute = '';

  if (params.destination) {
    nextRoute = `/results/${params.origin}/${params.destination}`;
  } else {
    nextRoute = `/search/${params.origin}`;
  }

  nextRoute += `?dept_date=${params.departureDate}`;

  if (params.returnDate) {
    nextRoute += `&ret_date=${params.returnDate}`;
  }

  nextRoute += `&passengers=${params.passengers || '100'}`;
  return nextRoute;
}

/***/ }),

/***/ "16zm":
/***/ (function(module, exports) {



/***/ }),

/***/ "2/0G":
/***/ (function(module, exports) {



/***/ }),

/***/ "2Eek":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("ltjX");

/***/ }),

/***/ "2V+x":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pricesCalendarTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PRICES_CALENDAR_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pricesCalendar';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["SET_OFFSET"] = 0] = "SET_OFFSET";
  ActionTypes[ActionTypes["SET_LIMIT"] = 1] = "SET_LIMIT";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR"] = 2] = "GET_PRICES_CALENDAR";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR_SUCCESS"] = 3] = "GET_PRICES_CALENDAR_SUCCESS";
  ActionTypes[ActionTypes["GET_PRICES_CALENDAR_FAIL"] = 4] = "GET_PRICES_CALENDAR_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pricesCalendarTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const PRICES_CALENDAR_ACTION_TYPES = pricesCalendarTypesGenerator.getActionTypes();

/***/ }),

/***/ "2wwy":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Loka");

/***/ }),

/***/ "3Bhi":
/***/ (function(module, exports) {



/***/ }),

/***/ "3FEJ":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActionTypesGenerator; });
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("2wwy");
/* harmony import */ var _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0__);

class ActionTypesGenerator {
  constructor(enumActionTypes, moduleName) {
    this.moduleName = void 0;
    this.enumActionTypes = void 0;
    this.actionTypes = {};
    this.moduleActionTypes = {};
    this.moduleName = moduleName;
    this.enumActionTypes = enumActionTypes;
    this.generateActionTypes();
  }

  generateActionTypes() {
    _babel_runtime_corejs2_core_js_object_values__WEBPACK_IMPORTED_MODULE_0___default()(this.enumActionTypes).forEach(value => {
      if (typeof value !== 'number') {
        this.actionTypes[value] = `${this.moduleName}/${value}`;
        this.moduleActionTypes[this.actionTypes[value]] = this.actionTypes[value];
      }
    });
  }

  getActionTypes() {
    return this.actionTypes;
  }

  getRequestActionTypes(actionType) {
    return [this.moduleActionTypes[actionType], this.moduleActionTypes[`${actionType}_SUCCESS`], this.moduleActionTypes[`${actionType}_FAIL`]];
  }

}

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("+Hn+");


/***/ }),

/***/ "4Q3z":
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "4mXO":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("k1wZ");

/***/ }),

/***/ "5RaE":
/***/ (function(module, exports) {



/***/ }),

/***/ "5U/F":
/***/ (function(module, exports) {

module.exports = require("react-input-range");

/***/ }),

/***/ "5chS":
/***/ (function(module, exports) {



/***/ }),

/***/ "6BQ9":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("wa65");

/***/ }),

/***/ "78gN":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return prepareDate; });
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("B1jN");
/* harmony import */ var lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash_padStart__WEBPACK_IMPORTED_MODULE_0__);

const prepareDate = date => `${date.getFullYear()}-${lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default()(date.getMonth() + 1 + '', 2, '0')}-${lodash_padStart__WEBPACK_IMPORTED_MODULE_0___default()(date.getDate() + '', 2, '0')}`;

/***/ }),

/***/ "8Kt/":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Set = __webpack_require__("ttDY");

var _Object$defineProperty = __webpack_require__("hfKm");

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __importDefault(__webpack_require__("cDcd"));

const side_effect_1 = __importDefault(__webpack_require__("Xuae"));

const amp_context_1 = __webpack_require__("lwAK");

const head_manager_context_1 = __webpack_require__("FYa8");

const amp_1 = __webpack_require__("/0+H");

function defaultHead(inAmpMode = false) {
  const head = [react_1.default.createElement("meta", {
    key: "charSet",
    charSet: "utf-8"
  })];

  if (!inAmpMode) {
    head.push(react_1.default.createElement("meta", {
      key: "viewport",
      name: "viewport",
      content: "width=device-width,minimum-scale=1,initial-scale=1"
    }));
  }

  return head;
}

exports.defaultHead = defaultHead;

function onlyReactElement(list, child) {
  // React children can be "string" or "number" in this case we ignore them for backwards compat
  if (typeof child === 'string' || typeof child === 'number') {
    return list;
  } // Adds support for React.Fragment


  if (child.type === react_1.default.Fragment) {
    return list.concat(react_1.default.Children.toArray(child.props.children).reduce((fragmentList, fragmentChild) => {
      if (typeof fragmentChild === 'string' || typeof fragmentChild === 'number') {
        return fragmentList;
      }

      return fragmentList.concat(fragmentChild);
    }, []));
  }

  return list.concat(child);
}

const METATYPES = ['name', 'httpEquiv', 'charSet', 'itemProp'];
/*
 returns a function for filtering head child elements
 which shouldn't be duplicated, like <title/>
 Also adds support for deduplicated `key` properties
*/

function unique() {
  const keys = new _Set();
  const tags = new _Set();
  const metaTypes = new _Set();
  const metaCategories = {};
  return h => {
    if (h.key && typeof h.key !== 'number' && h.key.indexOf('.$') === 0) {
      if (keys.has(h.key)) return false;
      keys.add(h.key);
      return true;
    } // If custom meta tag has been added the key will be prepended with `.$`, we can
    // check for this and dedupe in favor of the custom one, so the default
    // is not rendered as well


    if (keys.has(`.$${h.key}`)) return false;

    switch (h.type) {
      case 'title':
      case 'base':
        if (tags.has(h.type)) return false;
        tags.add(h.type);
        break;

      case 'meta':
        for (let i = 0, len = METATYPES.length; i < len; i++) {
          const metatype = METATYPES[i];
          if (!h.props.hasOwnProperty(metatype)) continue;

          if (metatype === 'charSet') {
            if (metaTypes.has(metatype)) return false;
            metaTypes.add(metatype);
          } else {
            const category = h.props[metatype];
            const categories = metaCategories[metatype] || new _Set();
            if (categories.has(category)) return false;
            categories.add(category);
            metaCategories[metatype] = categories;
          }
        }

        break;
    }

    return true;
  };
}
/**
 *
 * @param headElement List of multiple <Head> instances
 */


function reduceComponents(headElements, props) {
  return headElements.reduce((list, headElement) => {
    const headElementChildren = react_1.default.Children.toArray(headElement.props.children);
    return list.concat(headElementChildren);
  }, []).reduce(onlyReactElement, []).reverse().concat(defaultHead(props.inAmpMode)).filter(unique()).reverse().map((c, i) => {
    const key = c.key || i;
    return react_1.default.cloneElement(c, {
      key
    });
  });
}

const Effect = side_effect_1.default();
/**
 * This component injects elements to `<head>` of your page.
 * To avoid duplicated `tags` in `<head>` you can use the `key` property, which will make sure every tag is only rendered once.
 */

function Head({
  children
}) {
  return react_1.default.createElement(amp_context_1.AmpStateContext.Consumer, null, ampState => react_1.default.createElement(head_manager_context_1.HeadManagerContext.Consumer, null, updateHead => react_1.default.createElement(Effect, {
    reduceComponentsToState: reduceComponents,
    handleStateChange: updateHead,
    inAmpMode: amp_1.isInAmpMode(ampState)
  }, children)));
}

Head.rewind = Effect.rewind;
exports.default = Head;

/***/ }),

/***/ "AZIi":
/***/ (function(module, exports) {



/***/ }),

/***/ "B1jN":
/***/ (function(module, exports) {

module.exports = require("lodash/padStart");

/***/ }),

/***/ "BL1G":
/***/ (function(module, exports) {



/***/ }),

/***/ "BdYQ":
/***/ (function(module, exports) {



/***/ }),

/***/ "Cg2A":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("y6vh");

/***/ }),

/***/ "E8iq":
/***/ (function(module, exports) {

module.exports = require("react-tooltip");

/***/ }),

/***/ "FOf+":
/***/ (function(module, exports) {



/***/ }),

/***/ "FYa8":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__("cDcd"));

exports.HeadManagerContext = React.createContext(null);

/***/ }),

/***/ "FjY3":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export pointInfoTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GET_POINT_INFO_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'pointInfo';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["GET_POINT_INFO"] = 0] = "GET_POINT_INFO";
  ActionTypes[ActionTypes["GET_POINT_INFO_SUCCESS"] = 1] = "GET_POINT_INFO_SUCCESS";
  ActionTypes[ActionTypes["GET_POINT_INFO_FAIL"] = 2] = "GET_POINT_INFO_FAIL";
})(ActionTypes || (ActionTypes = {}));

const pointInfoTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const GET_POINT_INFO_ACTION_TYPES = pointInfoTypesGenerator.getActionTypes();

/***/ }),

/***/ "HzH2":
/***/ (function(module, exports) {



/***/ }),

/***/ "IJ9z":
/***/ (function(module, exports) {



/***/ }),

/***/ "Jo+v":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("Z6Kq");

/***/ }),

/***/ "LeJ0":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// CONCATENATED MODULE: ./src/utils/envParams.ts

function getEnvBoolean(env) {
  return !Object(external_lodash_["isUndefined"])(env) && +env === 1;
}
function getEnvNumber(env) {
  return !Object(external_lodash_["isUndefined"])(env) ? +env : 0;
}
function getEnvString(env, defaultVal = '') {
  return !Object(external_lodash_["isUndefined"])(env) ? env : defaultVal;
}
// CONCATENATED MODULE: ./src/config/envConfig.ts

const apiPort = getEnvNumber("8751") || '';
const apiHost = getEnvString("http://ci.lowtrip.ru", 'localhost') + (apiPort ? `:${apiPort}` : '');
const apiRoot = getEnvString("/api/v1");
const apiUrl = apiHost + apiRoot;
const envConfig = {
  apiHost,
  apiPort,
  apiUrl,
  apiSocketUrl: apiHost,
  env: {
    DEV: "production" === 'development'
  }
};
/* harmony default export */ var config_envConfig = (envConfig);
// CONCATENATED MODULE: ./src/config/index.ts
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Config; });








function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


const Config = _objectSpread({}, config_envConfig);

/***/ }),

/***/ "Loka":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/values");

/***/ }),

/***/ "MWqi":
/***/ (function(module, exports) {

module.exports = require("reselect");

/***/ }),

/***/ "NTJK":
/***/ (function(module, exports) {



/***/ }),

/***/ "NUC6":
/***/ (function(module, exports) {

module.exports = require("lodash/debounce");

/***/ }),

/***/ "No/t":
/***/ (function(module, exports) {

module.exports = require("@fortawesome/free-solid-svg-icons");

/***/ }),

/***/ "NzGr":
/***/ (function(module, exports) {



/***/ }),

/***/ "P5Gt":
/***/ (function(module, exports) {



/***/ }),

/***/ "Q58H":
/***/ (function(module, exports) {



/***/ }),

/***/ "QRnw":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-property.js
var define_property = __webpack_require__("hfKm");
var define_property_default = /*#__PURE__*/__webpack_require__.n(define_property);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js
var define_properties = __webpack_require__("2Eek");
var define_properties_default = /*#__PURE__*/__webpack_require__.n(define_properties);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptors.js
var get_own_property_descriptors = __webpack_require__("XoMD");
var get_own_property_descriptors_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptors);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-descriptor.js
var get_own_property_descriptor = __webpack_require__("Jo+v");
var get_own_property_descriptor_default = /*#__PURE__*/__webpack_require__.n(get_own_property_descriptor);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/date/now.js
var now = __webpack_require__("Cg2A");
var now_default = /*#__PURE__*/__webpack_require__.n(now);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var object_keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(object_keys);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js + 1 modules
var objectWithoutProperties = __webpack_require__("qNsG");

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/defineProperty.js
var defineProperty = __webpack_require__("vYYK");

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: ./src/components/application/search/form/form.styles.scss
var form_styles = __webpack_require__("AZIi");

// EXTERNAL MODULE: ./src/components/application/search/form/form.styles.mobile.scss
var form_styles_mobile = __webpack_require__("5RaE");

// EXTERNAL MODULE: external "lodash"
var external_lodash_ = __webpack_require__("YLtl");

// EXTERNAL MODULE: external "@fortawesome/react-fontawesome"
var react_fontawesome_ = __webpack_require__("uhWA");

// EXTERNAL MODULE: ./src/components/application/search/inputs/place/styles.scss
var styles = __webpack_require__("TwRD");

// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__("h74D");

// EXTERNAL MODULE: ./src/store/modules/autocomplete/IAutocompleteActionTypes.ts
var IAutocompleteActionTypes = __webpack_require__("qdML");

// CONCATENATED MODULE: ./src/store/modules/autocomplete/AutocompleteActions.ts

const getSuggetsions = data => ({
  type: IAutocompleteActionTypes["a" /* AUTOCOMPLETE_ACTION_TYPES */].AUTOCOMPLETE,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/autocomplete',
      data
    }
  }
});
// CONCATENATED MODULE: ./src/components/application/search/inputs/place/InputPlace.tsx
var __jsx = external_react_["createElement"];







class InputPlace_InputPlace extends external_react_["PureComponent"] {
  constructor(...args) {
    super(...args);
    this.state = {
      isShowTypeahead: false
    };
    this.input = void 0;

    this.bindInput = ref => this.input = ref;

    this.onInputFocus = () => this.setState({
      isShowTypeahead: true
    });

    this.onInputFocusOut = () => {
      const {
        results,
        onSuggestionClick: setPoint
      } = this.props;

      if (results && results.length) {
        setPoint(results[0]);
      }

      setTimeout(() => {
        this.setState({
          isShowTypeahead: false
        });
      }, 250);
    };

    this.findAutocomplete = Object(external_lodash_["debounce"])(value => {
      const {
        getSuggestions
      } = this.props;
      getSuggestions({
        term: value,
        locale: 'ru',
        types: ['city', 'country', 'airport']
      });
    }, 300);

    this.updateAutocomplete = e => {
      const value = e.target.value;

      if (value.length >= 3) {
        this.findAutocomplete(value);
      }

      this.props.onChange(value);
    };

    this.onTypeaheadPress = point => {
      const {
        onSuggestionClick: setPoint
      } = this.props;
      setPoint(point);
    };

    this.onClearClick = () => {
      this.props.onClearClick && this.props.onClearClick();
    };
  }

  render() {
    const {
      children,
      label,
      id,
      results,
      className,
      currentValue,
      invalid,
      showClear,
      historyItems,
      constSuggestions,
      fakeSuggestions
    } = this.props;
    return __jsx("div", {
      className: `place-input ${className}`
    }, __jsx("input", {
      ref: this.bindInput,
      type: "",
      name: "",
      className: `${invalid && 'invalid-input'}`,
      placeholder: label,
      id: id,
      autoComplete: 'off',
      value: currentValue,
      onFocus: this.onInputFocus,
      onBlur: this.onInputFocusOut,
      onChange: this.updateAutocomplete
    }), showClear && __jsx("div", {
      className: 'clear-button',
      onClick: this.onClearClick
    }, "\xD7"), children, this.state.isShowTypeahead && __jsx("ul", {
      className: "typeahead"
    }, constSuggestions && constSuggestions.length && constSuggestions.map(suggestion => __jsx("li", {
      key: suggestion.place.place.name,
      onClick: () => this.onTypeaheadPress(suggestion.place)
    }, __jsx(react_fontawesome_["FontAwesomeIcon"], {
      className: "circle-icon",
      icon: suggestion.icon
    }), __jsx("span", {
      className: 'main'
    }, " ", suggestion.place.place.name, " "), suggestion.place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, suggestion.place.country.name), suggestion.place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, suggestion.place.place.code))), results.map(place => __jsx("li", {
      key: place.place.name,
      onClick: () => this.onTypeaheadPress(place)
    }, place.type === 'airport' && __jsx("img", {
      alt: place.place.name,
      src: '/images/circle-grey.svg',
      className: "circle-icon"
    }), __jsx("span", {
      className: 'main'
    }, " ", place.place.name, " "), place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, place.country.name), place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, place.place.code))), fakeSuggestions && fakeSuggestions.length && fakeSuggestions.map(suggestion => __jsx("li", {
      key: suggestion.title,
      onClick: suggestion.onClick
    }, __jsx(react_fontawesome_["FontAwesomeIcon"], {
      className: "circle-icon",
      icon: suggestion.icon
    }), __jsx("span", {
      className: 'main'
    }, " ", suggestion.title, " "))), __jsx("li", {
      className: 'divider'
    }, "\u0438\u0441\u0442\u043E\u0440\u0438\u044F \u0432\u0430\u0448\u0435\u0433\u043E \u043F\u043E\u0438\u0441\u043A\u0430"), historyItems.map(place => __jsx("li", {
      key: place.place.name,
      onClick: () => this.onTypeaheadPress(place)
    }, place.type === 'airport' && __jsx("img", {
      alt: place.place.name,
      src: '/images/circle-grey.svg',
      className: "circle-icon"
    }), __jsx("span", {
      className: 'main'
    }, " ", place.place.name, " "), place.type === 'city' && __jsx("span", {
      className: 'sub'
    }, place.country.name), place.type === 'airport' && __jsx("span", {
      className: 'sub'
    }, place.place.code)))));
  }

}

/* harmony default export */ var place_InputPlace = (Object(external_react_redux_["connect"])(state => ({
  results: state.autocomplete.results
}), dispatch => ({
  getSuggestions: data => dispatch(getSuggetsions(data))
}))(InputPlace_InputPlace));
// EXTERNAL MODULE: ./src/components/application/search/inputs/passengers/styles.scss
var passengers_styles = __webpack_require__("mq8p");

// CONCATENATED MODULE: ./src/store/utils/pluralize.ts
const pluralizePassengers = count => {
  const meaningNumber = count % 10;
  const endian = meaningNumber === 1 ? '' : 1 < meaningNumber && meaningNumber < 5 ? 'а' : meaningNumber >= 5 || meaningNumber === 0 ? 'ов' : 'ов';
  return 10 < count && count < 21 ? 'пассажиров' : `пассажир${endian}`;
};
// EXTERNAL MODULE: external "@fortawesome/free-solid-svg-icons"
var free_solid_svg_icons_ = __webpack_require__("No/t");

// CONCATENATED MODULE: ./src/components/application/search/inputs/passengers/passengerType.tsx
var passengerType_jsx = external_react_["createElement"];




const PassengerType = ({
  title,
  tip,
  value,
  onChange,
  disabled
}) => {
  const decreasePassengersCount = e => {
    e.preventDefault();
    e.stopPropagation();
    onChange(Math.max(value - 1, 0));
  };

  const increasePassengersCount = e => {
    e.preventDefault();
    e.stopPropagation();

    if (!disabled) {
      onChange(value + 1);
    }
  };

  const preventBlur = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  return passengerType_jsx("div", {
    className: 'passenger-type'
  }, passengerType_jsx("div", {
    className: 'description'
  }, passengerType_jsx("div", {
    className: 'title'
  }, title), passengerType_jsx("div", {
    className: 'tip'
  }, tip)), passengerType_jsx("div", {
    className: 'count'
  }, passengerType_jsx("div", {
    className: `decrease-button ${value === 0 && 'disabled'}`,
    onClick: decreasePassengersCount,
    onMouseDown: preventBlur
  }, passengerType_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faMinus"]
  })), passengerType_jsx("div", {
    className: 'value'
  }, value), passengerType_jsx("div", {
    className: `increase-button ${disabled && 'disabled'}`,
    onClick: increasePassengersCount,
    onMouseDown: preventBlur
  }, passengerType_jsx(react_fontawesome_["FontAwesomeIcon"], {
    icon: free_solid_svg_icons_["faPlus"]
  }))));
};

/* harmony default export */ var passengerType = (external_react_["memo"](PassengerType));
// CONCATENATED MODULE: ./src/components/shared/utils/useOutsideListener.ts


function useOutsideListener(ref, onClickOutside) {
  function handleClickOutside(event) {
    if (ref.current && !ref.current.contains(event.target)) {
      onClickOutside();
    }
  }

  Object(external_react_["useEffect"])(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  });
}

/* harmony default export */ var utils_useOutsideListener = (useOutsideListener);
// CONCATENATED MODULE: ./src/components/shared/utils/index.ts

// CONCATENATED MODULE: ./src/components/application/search/inputs/passengers/index.tsx







var passengers_jsx = external_react_["createElement"];

function ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }







const InputPassengers = ({
  onMetaChange,
  placeholder,
  id,
  className = '',
  value
}) => {
  const [isShowTypeahead, toggleTypeahead] = external_react_["useState"](false);
  const inputRef = external_react_["useRef"](null);
  const wrapperRef = external_react_["useRef"](null);

  const onInputFocus = () => toggleTypeahead(true);

  const onInputFocusOut = () => toggleTypeahead(false);

  const setAdults = adults => {
    onMetaChange(_objectSpread({}, value, {
      adults
    }));
  };

  const setChildren = children => {
    onMetaChange(_objectSpread({}, value, {
      children
    }));
  };

  const setInfants = infants => {
    onMetaChange(_objectSpread({}, value, {
      infants
    }));
  };

  utils_useOutsideListener(wrapperRef, onInputFocusOut);
  const passengersSum = value.adults + value.children + value.infants;
  return passengers_jsx("div", {
    className: `passengers-input ${className}`,
    ref: wrapperRef
  }, passengers_jsx("input", {
    ref: inputRef,
    type: "",
    name: "",
    placeholder: placeholder,
    id: id,
    value: `${passengersSum}  ${pluralizePassengers(passengersSum)}`,
    autoComplete: 'off',
    onFocus: onInputFocus,
    readOnly: true
  }), isShowTypeahead && passengers_jsx("div", {
    className: 'passengers-typeahead'
  }, passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Врослые',
    tip: 'От 12 лет',
    value: value.adults,
    onChange: setAdults
  }), passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Дети',
    tip: 'От 2 до 12 лет',
    value: value.children,
    onChange: setChildren
  }), passengers_jsx(passengerType, {
    disabled: passengersSum > 8,
    title: 'Младенцы',
    tip: 'До 2 лет',
    value: value.infants,
    onChange: setInfants
  }), passengers_jsx("div", {
    className: "close-container"
  }, passengers_jsx("img", {
    alt: 'close',
    src: '/images/X.svg',
    onClick: () => toggleTypeahead(false),
    className: "close-btn"
  }))));
};

/* harmony default export */ var passengers = (external_react_["memo"](InputPassengers));
// EXTERNAL MODULE: ./src/store/modules/searchHistory/ISearchHistoryActionTypes.ts
var ISearchHistoryActionTypes = __webpack_require__("oden");

// CONCATENATED MODULE: ./src/store/modules/searchHistory/SearchHistoryActions.ts

const pushOrigin = data => ({
  type: ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_ORIGIN,
  payload: data
});
const pushDestination = data => ({
  type: ISearchHistoryActionTypes["a" /* SEARCH_HISTORY_ACTION_TYPES */].PUSH_DESTINATION,
  payload: data
});
// EXTERNAL MODULE: external "lodash/debounce"
var debounce_ = __webpack_require__("NUC6");
var debounce_default = /*#__PURE__*/__webpack_require__.n(debounce_);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/extends.js
var esm_extends = __webpack_require__("kOwS");

// EXTERNAL MODULE: external "react-day-picker/DayPickerInput"
var DayPickerInput_ = __webpack_require__("dUzv");
var DayPickerInput_default = /*#__PURE__*/__webpack_require__.n(DayPickerInput_);

// EXTERNAL MODULE: external "react-day-picker/moment"
var moment_ = __webpack_require__("my5g");
var moment_default = /*#__PURE__*/__webpack_require__.n(moment_);

// EXTERNAL MODULE: external "moment/locale/ru"
var ru_ = __webpack_require__("gvj5");

// EXTERNAL MODULE: ./src/components/shared/DatePicker/input/styles.scss
var input_styles = __webpack_require__("NTJK");

// EXTERNAL MODULE: ./src/components/shared/DatePicker/input/styles.mobile.scss
var styles_mobile = __webpack_require__("jQ5Y");

// CONCATENATED MODULE: ./src/components/shared/DatePicker/input/index.tsx


var input_jsx = external_react_["createElement"];




const DateCustomInput = (_ref) => {
  let {
    buttonText,
    className,
    isFocus,
    onButtonClick,
    placeholderFocusText,
    placeholder,
    value
  } = _ref,
      inputProps = Object(objectWithoutProperties["a" /* default */])(_ref, ["buttonText", "className", "isFocus", "onButtonClick", "placeholderFocusText", "placeholder", "value"]);

  const onButtonMouseDown = e => {
    e.preventDefault();
    e.stopPropagation();
  };

  return input_jsx("div", {
    className: `custom-date-input ${isFocus && 'focus'}`
  }, input_jsx("input", Object(esm_extends["a" /* default */])({
    className: `fake-input ${className ? className : ''}`,
    value: value,
    readOnly: true,
    placeholder: placeholder,
    style: {
      display: isFocus ? 'hidden' : 'block'
    }
  }, inputProps)), isFocus && input_jsx("div", {
    className: 'input-overlay'
  }, input_jsx("div", {
    className: 'input-overlay__title'
  }, placeholderFocusText), input_jsx("div", {
    className: 'date-action-button',
    onClick: onButtonClick,
    onMouseDown: onButtonMouseDown
  }, buttonText)));
};

/* harmony default export */ var input = (DateCustomInput);
// EXTERNAL MODULE: ./src/components/shared/DatePicker/header/style.scss
var style = __webpack_require__("0Zyy");

// CONCATENATED MODULE: ./src/components/shared/DatePicker/header/index.tsx

var header_jsx = external_react_["createElement"];
 // import { format, Locale } from "date-fns";



const CustomCalendarHeader = (_ref) => {
  let {
    date,
    localeUtils,
    locale
  } = _ref,
      props = Object(objectWithoutProperties["a" /* default */])(_ref, ["date", "localeUtils", "locale"]);

  // const captionTitle = format(month, "LLLL Y", { locale: dayPickerProps.locale as any });
  const months = localeUtils.getMonths(locale);
  const currentYear = date.getFullYear();
  const currentMouth = months[date.getMonth()];
  return header_jsx("div", {
    className: "custom-caption"
  }, header_jsx("div", {
    className: "custom-caption__title"
  }, `${currentMouth} ${currentYear}`));
};

/* harmony default export */ var header = (external_react_["memo"](CustomCalendarHeader));
// EXTERNAL MODULE: ./src/components/shared/DatePicker/style.scss
var DatePicker_style = __webpack_require__("t9pA");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./src/components/shared/DatePicker/index.tsx








var DatePicker_jsx = external_react_["createElement"];

function DatePicker_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function DatePicker_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { DatePicker_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { DatePicker_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }


 // @ts-ignore









const CustomDatePicker = ({
  className,
  inputClassName,
  placeholderText,
  placeholderFocusText,
  onChange,
  buttonText,
  dayPickerProps,
  onButtonClick,
  selected
}) => {
  const [isFocus, toggleFocus] = external_react_["useState"](false);
  const wrapperRef = external_react_["useRef"](null);
  const calendarRef = external_react_["useRef"](null);

  const onDayPickerHide = () => {
    toggleFocus(false);
  };

  const onDayPickerShow = () => {
    toggleFocus(true);
  };

  const onOutsideClick = () => {
    onDayPickerHide();
    calendarRef.current.hideDayPicker();
  };

  utils_useOutsideListener(wrapperRef, onOutsideClick);

  const _minDate = dayPickerProps.initialMonth || new Date();

  const _onButtonClick = () => {
    onButtonClick();
    onOutsideClick();
  };

  const value = selected ? external_moment_default()(selected).format('LL') : '';
  const inputProps = {
    buttonText: buttonText,
    className: inputClassName,
    isFocus: isFocus,
    onButtonClick: _onButtonClick,
    placeholderFocusText: placeholderFocusText,
    placeholder: placeholderText,
    value
  };
  return DatePicker_jsx("div", {
    className: `custom-date-picker ${className}`,
    ref: wrapperRef
  }, DatePicker_jsx(DayPickerInput_default.a, {
    ref: calendarRef,
    dayPickerProps: DatePicker_objectSpread({
      initialMonth: _minDate,
      localeUtils: moment_default.a,
      locale: 'ru',
      captionElement: DatePicker_jsx(header, {
        dayPickerProps: dayPickerProps
      })
    }, dayPickerProps),
    onDayChange: onChange,
    onDayPickerHide: onDayPickerHide,
    onDayPickerShow: onDayPickerShow,
    format: 'LL',
    formatDate: moment_["formatDate"],
    parseDate: moment_["parseDate"],
    inputProps: inputProps,
    component: props => DatePicker_jsx(input, Object(esm_extends["a" /* default */])({}, props, {
      value: value
    }))
  }));
};

/* harmony default export */ var DatePicker = (external_react_["memo"](CustomDatePicker));
// CONCATENATED MODULE: ./src/components/application/search/form/form.tsx









var form_jsx = external_react_["createElement"];

function form_ownKeys(object, enumerableOnly) { var keys = keys_default()(object); if (get_own_property_symbols_default.a) { var symbols = get_own_property_symbols_default()(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return get_own_property_descriptor_default()(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function form_objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { form_ownKeys(source, true).forEach(function (key) { Object(defineProperty["a" /* default */])(target, key, source[key]); }); } else if (get_own_property_descriptors_default.a) { define_properties_default()(target, get_own_property_descriptors_default()(source)); } else { form_ownKeys(source).forEach(function (key) { define_property_default()(target, key, get_own_property_descriptor_default()(source, key)); }); } } return target; }












var DatePickerInputs;

(function (DatePickerInputs) {
  DatePickerInputs["From"] = "from";
  DatePickerInputs["To"] = "to";
})(DatePickerInputs || (DatePickerInputs = {}));

class form_SearchForm extends external_react_["Component"] {
  constructor(props) {
    super(props);
    this.changeLayout = debounce_default()(isMobileLayout => this.setState({
      isMobileLayout
    }), 500);

    this.onDocumentResize = () => this.changeLayout(window.innerWidth <= 425);

    this.onSearchPress = e => {
      e.preventDefault();
      const {
        state
      } = this;
      const validation = {
        validationDepartureDateSelected: !!state.departureDate,
        validationOriginSelected: !!state.origin,
        validationSameCity: !!state.destination && !!state.origin && state.origin.city.code === state.destination.city.code
      };
      this.setState(form_objectSpread({}, validation), () => {
        const _this$state = this.state,
              {
          validationDepartureDateSelected,
          validationOriginSelected,
          validationSameCity
        } = _this$state,
              params = Object(objectWithoutProperties["a" /* default */])(_this$state, ["validationDepartureDateSelected", "validationOriginSelected", "validationSameCity"]);

        const isValid = validationDepartureDateSelected && !validationSameCity && validationOriginSelected;

        if (isValid) {
          this.props.pushOriginHistory(state.origin);

          if (state.destination && state.destination.place.code) {
            this.props.pushDestinationHistory(state.destination);
          }

          const searchParams = {
            adults: params.adults,
            children: params.children,
            departureDate: params.departureDate,
            destination: params.destination,
            infants: params.infants,
            origin: params.origin,
            returnDate: params.returnDate
          };
          setTimeout(() => {
            this.props.onSearchPress(searchParams);
          }, 700);
        }
      });
    };

    this.resetValidationState = () => {
      this.setState({
        validationDepartureDateSelected: true,
        validationOriginSelected: true
      });
    };

    this.onOriginValueChange = origin => this.setState({
      origin,
      originValue: origin.place.name
    });

    this.onDestinationValueChange = destination => this.setState({
      destination,
      destinationValue: destination.place.name
    });

    this.setDepartureDate = date => {
      this.setState({
        departureDate: date
      });
      this.resetValidationState();
    };

    this.setReturnDate = date => {
      this.setState({
        returnDate: date
      });
      this.resetValidationState();
    };

    this.setPassengersCount = ({
      adults,
      children,
      infants
    }) => {
      this.setState({
        adults,
        children,
        infants
      });
    };

    this.onReversePlacesClick = () => {
      const {
        origin,
        destination,
        originValue,
        destinationValue
      } = this.state;
      this.setState({
        origin: destination,
        destination: origin,
        originValue: destinationValue,
        destinationValue: originValue
      });
      this.resetValidationState();
    };

    this.onRemoveDestination = () => {
      this.setState({
        destination: undefined,
        destinationValue: '',
        validationSameCity: false
      });
    };

    this.onRemoveReturnDate = () => {
      this.setState({
        returnDate: undefined
      });
    };

    this.setTodayDepature = () => {
      this.setState({
        departureDate: new Date()
      });
    };

    this.onOriginInputValueChange = originValue => this.setState({
      originValue
    });

    this.onDestinationInputValueChange = destinationValue => this.setState({
      destinationValue
    });

    const _state = {
      validationDepartureDateSelected: true,
      validationOriginSelected: true,
      validationSameCity: false,
      adults: 1,
      children: 0,
      infants: 0,
      originValue: '',
      destinationValue: '',
      departureDate: new Date()
    };
    this.state = form_objectSpread({}, _state);
  }

  componentDidMount() {
    window.addEventListener('resize', this.onDocumentResize);
    const {
      props
    } = this;
    this.setState({
      isMobileLayout: window.innerWidth <= 425
    }, () => {
      this.propsToState(props);
    });
  }

  componentDidUpdate(prevProps) {
    if (!Object(external_lodash_["isEqual"])(prevProps, this.props)) {
      this.propsToState(this.props);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.onDocumentResize);
  }

  propsToState(props) {
    let nextState = {};

    if (props.defaultValues && keys_default()(props.defaultValues).length) {
      nextState = form_objectSpread({}, nextState, {}, props.defaultValues);

      if (props.defaultValues.origin && props.defaultValues.origin.place) {
        nextState.originValue = props.defaultValues.origin.place.name;
      }

      if (props.defaultValues.destination && props.defaultValues.destination.place) {
        nextState.destinationValue = props.defaultValues.destination.place.name;
      }
    }

    this.setState(nextState);
  }

  render() {
    const {
      state
    } = this;
    const {
      className,
      onHideClick,
      history,
      myLocation
    } = this.props;
    const {
      validationOriginSelected,
      validationDepartureDateSelected,
      adults,
      children,
      origin,
      destination,
      infants,
      validationSameCity,
      originValue,
      destinationValue
    } = this.state;
    const currentDay = new Date();
    let departureDate;
    let returnDate;

    if (state.departureDate) {
      departureDate = typeof state.departureDate === 'string' ? new Date(state.departureDate) : state.departureDate;

      if (departureDate.getTime() < now_default()()) {
        departureDate = new Date();
      }
    }

    if (state.returnDate) {
      returnDate = typeof state.returnDate === 'string' ? new Date(state.returnDate) : state.returnDate;

      if (departureDate && returnDate && returnDate.getTime() < departureDate.getTime()) {
        returnDate = new Date(departureDate.getTime() + 1000 * 60 * 60 * 24);
      }
    }

    const modifiers = {
      start: departureDate,
      end: returnDate
    };
    return form_jsx("form", {
      className: `search-form ${className ? className : ''}`
    }, form_jsx(place_InputPlace, {
      id: "from",
      className: "from",
      historyItems: history.origins_history,
      invalid: !validationOriginSelected,
      label: 'Из России',
      onSuggestionClick: this.onOriginValueChange,
      currentValue: originValue,
      pointValue: origin,
      onChange: this.onOriginInputValueChange,
      constSuggestions: [{
        icon: free_solid_svg_icons_["faLocationArrow"],
        place: myLocation
      }]
    }, form_jsx("div", {
      className: "icon",
      onClick: this.onReversePlacesClick
    }, form_jsx("svg", {
      width: "46",
      height: "46",
      viewBox: "0 0 46 46",
      xmlns: "http://www.w3.org/2000/svg",
      className: "icon"
    }, form_jsx("path", {
      d: "M29.3536 18.3536C29.5488 18.1583 29.5488 17.8417 29.3536 17.6464L26.1716 14.4645C25.9763 14.2692 25.6597 14.2692 25.4645 14.4645C25.2692 14.6597 25.2692 14.9763 25.4645 15.1716L28.2929 18L25.4645 20.8284C25.2692 21.0237 25.2692 21.3403 25.4645 21.5355C25.6597 21.7308 25.9763 21.7308 26.1716 21.5355L29.3536 18.3536ZM17 18.5L29 18.5L29 17.5L17 17.5L17 18.5Z"
    }), form_jsx("path", {
      d: "M16.6464 27.6464C16.4512 27.8417 16.4512 28.1583 16.6464 28.3536L19.8284 31.5355C20.0237 31.7308 20.3403 31.7308 20.5355 31.5355C20.7308 31.3403 20.7308 31.0237 20.5355 30.8284L17.7071 28L20.5355 25.1716C20.7308 24.9763 20.7308 24.6597 20.5355 24.4645C20.3403 24.2692 20.0237 24.2692 19.8284 24.4645L16.6464 27.6464ZM29 27.5H17V28.5H29V27.5Z"
    })))), form_jsx(place_InputPlace, {
      id: "to",
      className: "to",
      historyItems: history.destinations_history,
      invalid: validationSameCity,
      label: 'Куда угодно',
      onClearClick: this.onRemoveDestination,
      onSuggestionClick: this.onDestinationValueChange,
      currentValue: destinationValue,
      pointValue: destination,
      onChange: this.onDestinationInputValueChange,
      fakeSuggestions: [{
        icon: free_solid_svg_icons_["faGlobeEurope"],
        title: 'Куда угодно',
        onClick: this.onRemoveDestination
      }]
    }), !(this.state.activeDateInput === 'to' && this.state.isMobileLayout) && form_jsx(DatePicker, {
      buttonText: 'Прямо сейчас',
      className: `input_range input_range__departure`,
      inputClassName: `${!validationDepartureDateSelected && 'invalid-input'}`,
      onChange: this.setDepartureDate,
      placeholderFocusText: 'Когда хотите уехать?',
      placeholderText: 'Когда туда',
      selected: departureDate,
      onButtonClick: this.setTodayDepature,
      dayPickerProps: {
        selectedDays: [departureDate, {
          from: departureDate,
          to: returnDate
        }],
        disabledDays: {
          after: returnDate,
          before: currentDay
        },
        toMonth: returnDate,
        modifiers
      }
    }), form_jsx(DatePicker, {
      buttonText: 'Нет, вы что!',
      className: 'input_range input_range__return',
      onChange: this.setReturnDate,
      placeholderFocusText: 'Хотите обратно?',
      placeholderText: 'Обратно',
      selected: returnDate,
      onButtonClick: this.onRemoveReturnDate,
      dayPickerProps: {
        selectedDays: [departureDate, {
          from: departureDate,
          to: returnDate
        }],
        disabledDays: {
          before: departureDate
        },
        modifiers,
        month: departureDate,
        fromMonth: departureDate
      }
    }), form_jsx(passengers, {
      className: "passengers__select",
      id: "ticket-type",
      placeholder: "1 \u0432\u0437\u0440\u043E\u0441\u043B\u044B\u0439",
      value: {
        adults,
        children: children,
        infants
      },
      onMetaChange: this.setPassengersCount
    }), form_jsx("input", {
      type: "submit",
      name: "",
      value: "\u041D\u0430\u0447\u0430\u0442\u044C \u043F\u043E\u0438\u0441\u043A",
      className: "search-btn",
      onClick: this.onSearchPress
    }), onHideClick && form_jsx("img", {
      alt: 'close',
      src: '/images/X.svg',
      onClick: onHideClick,
      className: "search-form close-btn"
    }));
  }

}

/* harmony default export */ var form_form = __webpack_exports__["a"] = (Object(external_react_redux_["connect"])(state => ({
  history: state.searchHistory,
  myLocation: state.geolocation.myLocation
}), dispatch => ({
  pushOriginHistory: data => dispatch(pushOrigin(data)),
  pushDestinationHistory: data => dispatch(pushDestination(data))
}))(form_SearchForm));

/***/ }),

/***/ "QTVn":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptors");

/***/ }),

/***/ "S8h5":
/***/ (function(module, exports) {



/***/ }),

/***/ "SeML":
/***/ (function(module, exports) {

module.exports = require("lodash/sample");

/***/ }),

/***/ "TUA0":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-property");

/***/ }),

/***/ "TwRD":
/***/ (function(module, exports) {



/***/ }),

/***/ "UIQ4":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__("cDcd");

// EXTERNAL MODULE: ./src/components/application/header/header.styles.scss
var header_styles = __webpack_require__("IJ9z");

// EXTERNAL MODULE: ./src/components/application/header/header.styles.mobile.scss
var header_styles_mobile = __webpack_require__("5chS");

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.styles.scss
var header_filters_styles = __webpack_require__("NzGr");

// EXTERNAL MODULE: ./src/components/application/header-filters/header-filters.styles.mobile.scss
var header_filters_styles_mobile = __webpack_require__("S8h5");

// EXTERNAL MODULE: ./src/components/application/search/form/form.tsx + 11 modules
var form_form = __webpack_require__("QRnw");

// EXTERNAL MODULE: external "moment"
var external_moment_ = __webpack_require__("wy2R");
var external_moment_default = /*#__PURE__*/__webpack_require__.n(external_moment_);

// CONCATENATED MODULE: ./src/components/application/header-miniform/header-miniform.tsx
var __jsx = external_react_["createElement"];



const Miniform = ({
  search,
  onHideToggle
}) => {
  const originPlace = search.origin && search.origin.place.name ? search.origin.place.name : '';
  const destinationPlace = search.destination && search.destination.place.name ? `— ${search.destination.place.name}` : '— Куда угодно';
  const passengersCount = search.adults + search.children + search.infants;
  const departureDateLabel = search.departureDate ? external_moment_default()(search.departureDate).format('Do MMMM') : '';
  const returnDateLabel = search.returnDate ? `, ${external_moment_default()(search.returnDate).format('Do MMMM')}` : '';
  const passengersCountLabel = passengersCount ? `, ${passengersCount} пас, эконом` : '';
  return __jsx("div", {
    className: "mini-form"
  }, __jsx("p", null, originPlace, " ", destinationPlace), __jsx("p", null, `${departureDateLabel}${returnDateLabel}${passengersCountLabel}`), __jsx("img", {
    src: '/images/pencil.svg',
    className: "mini-form",
    onClick: onHideToggle,
    alt: 'edit'
  }));
};

/* harmony default export */ var header_miniform = (Miniform);
// CONCATENATED MODULE: ./src/components/application/header-filters/header-filters.tsx
var header_filters_jsx = external_react_["createElement"];










class header_filters_HeaderFilters extends external_react_["Component"] {
  constructor(...args) {
    super(...args);
    this.state = {
      isFilterVisible: false
    };
    this.burgerRef = void 0;
    this.logoRef = void 0;
    this.menuRef = void 0;

    this.onMenuPress = () => {
      this.burgerRef.classList.toggle("burger-X");
      this.logoRef.classList.toggle("black");
      this.menuRef.classList.toggle("active-menu");
    };

    this.bindBurger = ref => this.burgerRef = ref;

    this.bindLogo = ref => this.logoRef = ref;

    this.bindMenu = ref => this.menuRef = ref;

    this.onHideToggle = () => {
      this.setState({
        isFilterVisible: !this.state.isFilterVisible
      });
    };
  }

  componentDidMount() {
    this.setState({
      isFilterVisible: window.innerWidth <= 425 ? true : false
    });
  }

  render() {
    const {
      isFilterVisible
    } = this.state;
    const {
      showLoading,
      onSearchPress,
      defaultValues
    } = this.props;
    return header_filters_jsx("header", null, header_filters_jsx("div", {
      className: `header-content black ${isFilterVisible ? 'items-hidden' : ''}`
    }, header_filters_jsx("span", {
      className: "logo",
      ref: this.bindLogo
    }, "\u041B\u043E\u0443\u0442\u0440\u0438\u043F"), header_filters_jsx("ul", {
      className: "menu",
      ref: this.bindMenu
    }, header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041F\u0443\u0442\u0435\u0448\u0435\u0441\u0442\u0432\u0438\u044F")), header_filters_jsx("li", {
      className: "active-menu-black"
    }, header_filters_jsx("a", {
      href: "/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0410\u0432\u0438\u0430\u0431\u0438\u043B\u0435\u0442\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/train",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0416\u0414 \u0411\u0438\u043B\u0435\u0442\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/bus",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u0410\u0432\u0442\u043E\u0431\u0443\u0441\u044B")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041F\u043E\u043F\u0443\u0442\u043A\u0430")), header_filters_jsx("li", null, header_filters_jsx("a", {
      href: "https://lowtrip.ru/car",
      rel: "noopener noreferrer",
      target: "_blank"
    }, "\u041D\u0430 \u0441\u0432\u043E\u0435\u0439 \u043C\u0430\u0448\u0438\u043D\u0435"))), header_filters_jsx("div", {
      className: "menu-btn",
      onClick: this.onMenuPress
    }, header_filters_jsx("span", {
      ref: this.bindBurger,
      className: "burger grey"
    })), header_filters_jsx("div", {
      className: "clear"
    }, "\u0423\u0431\u0440\u0430\u0442\u044C \u0432\u0441\u0435 \u043B\u0438\u0448\u043D\u0435\u0435", header_filters_jsx("div", {
      className: `clear-btn ${isFilterVisible ? 'clear-btn-ok' : ''}`,
      onClick: this.onHideToggle
    }, header_filters_jsx("div", {
      className: isFilterVisible ? 'clear-ok' : ''
    }, header_filters_jsx("img", {
      alt: "",
      src: isFilterVisible ? '/images/clear-ok.svg' : '/images/clear-x.svg'
    })))), header_filters_jsx(header_miniform, {
      search: defaultValues,
      onHideToggle: this.onHideToggle
    }), header_filters_jsx(form_form["a" /* default */], {
      className: 'border-btm',
      defaultValues: defaultValues,
      onHideClick: this.onHideToggle,
      onSearchPress: onSearchPress
    }), showLoading && header_filters_jsx("div", {
      className: 'rainbow-loader'
    }), this.props.children));
  }

}

/* harmony default export */ var header_filters = __webpack_exports__["a"] = (header_filters_HeaderFilters);

/***/ }),

/***/ "UNSI":
/***/ (function(module, exports) {



/***/ }),

/***/ "UXZV":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("dGr4");

/***/ }),

/***/ "VRjO":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export filtersActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FILTERS_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'filters';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["CHANGE_TRANSFERS_FILTER"] = 0] = "CHANGE_TRANSFERS_FILTER";
  ActionTypes[ActionTypes["CHANGE_COAST_FILTER"] = 1] = "CHANGE_COAST_FILTER";
  ActionTypes[ActionTypes["CHANGE_TIMING_FILTERS"] = 2] = "CHANGE_TIMING_FILTERS";
  ActionTypes[ActionTypes["CHANGE_IATA_FILTER"] = 3] = "CHANGE_IATA_FILTER";
  ActionTypes[ActionTypes["CHANGE_SORTING_FILTER"] = 4] = "CHANGE_SORTING_FILTER";
  ActionTypes[ActionTypes["CHANGE_BAGGAGE_INCLUDED_FILTER"] = 5] = "CHANGE_BAGGAGE_INCLUDED_FILTER";
  ActionTypes[ActionTypes["SET_RESULTS_LIMIT"] = 6] = "SET_RESULTS_LIMIT";
})(ActionTypes || (ActionTypes = {}));

const filtersActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const FILTERS_ACTION_TYPES = filtersActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "XoMD":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("QTVn");

/***/ }),

/***/ "Xuae":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Set = __webpack_require__("ttDY");

var _Object$defineProperty = __webpack_require__("hfKm");

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const react_1 = __webpack_require__("cDcd");

const isServer = true;

exports.default = () => {
  const mountedInstances = new _Set();
  let state;

  function emitChange(component) {
    state = component.props.reduceComponentsToState([...mountedInstances], component.props);

    if (component.props.handleStateChange) {
      component.props.handleStateChange(state);
    }
  }

  return class extends react_1.Component {
    // Used when server rendering
    static rewind() {
      const recordedState = state;
      state = undefined;
      mountedInstances.clear();
      return recordedState;
    }

    constructor(props) {
      super(props);

      if (isServer) {
        mountedInstances.add(this);
        emitChange(this);
      }
    }

    componentDidMount() {
      mountedInstances.add(this);
      emitChange(this);
    }

    componentDidUpdate() {
      emitChange(this);
    }

    componentWillUnmount() {
      mountedInstances.delete(this);
      emitChange(this);
    }

    render() {
      return null;
    }

  };
};

/***/ }),

/***/ "YLtl":
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "Z6Kq":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-descriptor");

/***/ }),

/***/ "cDcd":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "dGr4":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/assign");

/***/ }),

/***/ "dUzv":
/***/ (function(module, exports) {

module.exports = require("react-day-picker/DayPickerInput");

/***/ }),

/***/ "gvj5":
/***/ (function(module, exports) {

module.exports = require("moment/locale/ru");

/***/ }),

/***/ "h74D":
/***/ (function(module, exports) {

module.exports = require("react-redux");

/***/ }),

/***/ "hfKm":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("TUA0");

/***/ }),

/***/ "i9IY":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("irLM");

/***/ }),

/***/ "iqkk":
/***/ (function(module, exports) {



/***/ }),

/***/ "irLM":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/number/max-safe-integer");

/***/ }),

/***/ "jQ5Y":
/***/ (function(module, exports) {



/***/ }),

/***/ "k1wZ":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/get-own-property-symbols");

/***/ }),

/***/ "kOwS":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _extends; });
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("UXZV");
/* harmony import */ var _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_assign__WEBPACK_IMPORTED_MODULE_0__);

function _extends() {
  _extends = _core_js_object_assign__WEBPACK_IMPORTED_MODULE_0___default.a || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

/***/ }),

/***/ "kqQL":
/***/ (function(module, exports) {



/***/ }),

/***/ "ltjX":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "lwAK":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _Object$defineProperty = __webpack_require__("hfKm");

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
  result["default"] = mod;
  return result;
};

_Object$defineProperty(exports, "__esModule", {
  value: true
});

const React = __importStar(__webpack_require__("cDcd"));

exports.AmpStateContext = React.createContext({});

/***/ }),

/***/ "mPxa":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export searchTicketsActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SEARCH_TICKETS_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'searchTicketsPlaces';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["SEARCH_TICKETS"] = 0] = "SEARCH_TICKETS";
  ActionTypes[ActionTypes["SEARCH_TICKETS_SUCCESS"] = 1] = "SEARCH_TICKETS_SUCCESS";
  ActionTypes[ActionTypes["SEARCH_TICKETS_FAIL"] = 2] = "SEARCH_TICKETS_FAIL";
  ActionTypes[ActionTypes["SEARCH_TICKETS_INCOMING_CHUNK"] = 3] = "SEARCH_TICKETS_INCOMING_CHUNK";
  ActionTypes[ActionTypes["SET_RESULTS_LIMIT"] = 4] = "SET_RESULTS_LIMIT";
})(ActionTypes || (ActionTypes = {}));

const searchTicketsActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const SEARCH_TICKETS_ACTION_TYPES = searchTicketsActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "mq8p":
/***/ (function(module, exports) {



/***/ }),

/***/ "my5g":
/***/ (function(module, exports) {

module.exports = require("react-day-picker/moment");

/***/ }),

/***/ "oden":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export searchHistoryActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SEARCH_HISTORY_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'search_history';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["PUSH_ORIGIN"] = 0] = "PUSH_ORIGIN";
  ActionTypes[ActionTypes["PUSH_DESTINATION"] = 1] = "PUSH_DESTINATION";
})(ActionTypes || (ActionTypes = {}));

const searchHistoryActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const SEARCH_HISTORY_ACTION_TYPES = searchHistoryActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "pI2v":
/***/ (function(module, exports) {

module.exports = require("socket.io-client");

/***/ }),

/***/ "pLtp":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("qJj/");

/***/ }),

/***/ "qJj/":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/keys");

/***/ }),

/***/ "qNsG":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/get-own-property-symbols.js
var get_own_property_symbols = __webpack_require__("4mXO");
var get_own_property_symbols_default = /*#__PURE__*/__webpack_require__.n(get_own_property_symbols);

// EXTERNAL MODULE: ./node_modules/@babel/runtime-corejs2/core-js/object/keys.js
var keys = __webpack_require__("pLtp");
var keys_default = /*#__PURE__*/__webpack_require__.n(keys);

// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutPropertiesLoose.js

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};

  var sourceKeys = keys_default()(source);

  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}
// CONCATENATED MODULE: ./node_modules/@babel/runtime-corejs2/helpers/esm/objectWithoutProperties.js
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _objectWithoutProperties; });


function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = _objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (get_own_property_symbols_default.a) {
    var sourceSymbolKeys = get_own_property_symbols_default()(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

/***/ }),

/***/ "qdML":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export autocompleteActionTypesGenerator */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AUTOCOMPLETE_ACTION_TYPES; });
/* harmony import */ var _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("3FEJ");

const moduleName = 'autocomplete';
var ActionTypes;

(function (ActionTypes) {
  ActionTypes[ActionTypes["AUTOCOMPLETE"] = 0] = "AUTOCOMPLETE";
  ActionTypes[ActionTypes["AUTOCOMPLETE_SUCCESS"] = 1] = "AUTOCOMPLETE_SUCCESS";
  ActionTypes[ActionTypes["AUTOCOMPLETE_FAIL"] = 2] = "AUTOCOMPLETE_FAIL";
})(ActionTypes || (ActionTypes = {}));

const autocompleteActionTypesGenerator = new _ActionsTypesGenerator__WEBPACK_IMPORTED_MODULE_0__[/* ActionTypesGenerator */ "a"](ActionTypes, moduleName);
const AUTOCOMPLETE_ACTION_TYPES = autocompleteActionTypesGenerator.getActionTypes();

/***/ }),

/***/ "rzUo":
/***/ (function(module, exports) {



/***/ }),

/***/ "t9pA":
/***/ (function(module, exports) {



/***/ }),

/***/ "ttDY":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("zQQD");

/***/ }),

/***/ "uhWA":
/***/ (function(module, exports) {

module.exports = require("@fortawesome/react-fontawesome");

/***/ }),

/***/ "vYYK":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _defineProperty; });
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("hfKm");
/* harmony import */ var _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0__);

function _defineProperty(obj, key, value) {
  if (key in obj) {
    _core_js_object_define_property__WEBPACK_IMPORTED_MODULE_0___default()(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

/***/ }),

/***/ "wa65":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/parse-int");

/***/ }),

/***/ "wy2R":
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "y6vh":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/date/now");

/***/ }),

/***/ "z3C8":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getPointInfo; });
/* harmony import */ var _IPointInfoActionTypes__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("FjY3");

const getPointInfo = data => ({
  type: _IPointInfoActionTypes__WEBPACK_IMPORTED_MODULE_0__[/* GET_POINT_INFO_ACTION_TYPES */ "a"].GET_POINT_INFO,
  payload: {
    returnRejectedPromiseOnError: true,
    request: {
      method: 'POST',
      url: '/pointInfo',
      data: {
        codes: data
      }
    }
  }
});

/***/ }),

/***/ "zQQD":
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/set");

/***/ }),

/***/ "zr5I":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ })

/******/ });